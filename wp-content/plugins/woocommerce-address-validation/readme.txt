=== WooCommerce Address Validation ===
Author: skyverge
Tags: woocommerce
Requires at least: 4.4
Tested up to: 4.9.8

Adds Address Validation and Lookup to WooCommerce via PostCodeAnywhere, SmartyStreets and more!

See http://docs.woothemes.com/document/address-validation/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-address-validation' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
