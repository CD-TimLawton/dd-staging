<?php

$stopprocessing = 0;

$halt = array();
$halt[]= "Elkington’s Sparkling 'Gin' Rhubarb 70cl - 1 Bottle";
$halt[]= "Elkington’s Sparkling 'Gin' Rhubarb 70cl - 3 bottles for the price of 2";
$halt[]= "Elkington’s Sparkling 'Gin' Rhubarb 70cl \"3 for 2 Offer\" - 1 Bottle";
$halt[]= "Elkington’s Sparkling 'Gin' Rhubarb 70cl \"3 for 2 Offer\" - 3 bottles for the price of 2";

$single = array();
// $single[]= "Ariel Chardonnay White Wine 0.5%";
$single[]= "Barrels and Drums Non Alcoholic Sparkling Chardonnay";
$single[]= "Borrago #47 Paloma Blend Non-Alcoholic Spirit 500ml";
$single[]= "Chapel Hill Alcohol Free Sparkling Wine";
$single[]= "Festillant Alcohol Free Sparkling Rosé Wine";
$single[]= "Festillant Alcohol Free Sparkling Wine";
$single[]= "Henkell Alcohol Free Sparkling Rosé Wine";
//$single[]= "Pierre Zéro Alcohol Free Rosé Wine";
//$single[]= "Pierre Zéro Grand Reserve Alcohol Free Red Wine";
$single[]= "Sohnlein Alcohol Free Sparkling Wine";
$single[]= "Henkell Alcohol Free Sparkling Wine";


foreach ($halt as $exception)
{
	if($search == strtolower($exception))
	{ 
		$stopprocessing = 1; 
	}
}

foreach ($single as $exception)
{
	if($search == strtolower($exception))
	{ 
	

			$winebox = $winebox + (0.167*$qtykey); 
			$totalitems = $totalitems + $qtykey; 		
				
	}
}

		if($stopprocessing == 0)
		{
		
			$beercase24 = strpos($search, "24 x 330ml");
		 	$beercase12 = strpos($search, "case of 12 bottles");
			if(!$beercase12){$beercase12 = strpos($search, "12 bottles");}
			if(!$beercase12){$beercase12 = strpos($search, "12 cans");}
			if(!$beercase12){$beercase12 = strpos($search, "mystery beer box");}	
			$beercase6 = strpos($search, "case of 6 bottles");
			if(!$beercase6){$beercase6 = strpos($search, "6 bottles");}
			if(!$beercase6){$beercase6 = strpos($search, "6 cans");}
			if(!$beercase6){$beercase6 = strpos($search, "case of 6 cans");}
			if($mixedqty<0)
			{
				$beercase = strpos($search, "single bottle");
				if(!$beercase && !$beercase12)
		  		{    
	  				$beercase = strpos($search, "500ml");
		  		}
			}
		  	if(!$beercase)
		  	{    
	  			$beercase = strpos($search, "single can");
		  	}
			if(!$beercase12 && !$beercase6){$beercase12 = strpos($search, "12 x 330ml");}
			if(!$beercase12 && !$beercase6){$beercase12 = strpos($search, "12 x330ml");}			
			if(!$beercase12 && !$beercase6){$beercase12 = strpos($search, "12x 330ml");}
			if(!$beercase12 && !$beercase6){$beercase12 = strpos($search, "12x330ml");}
                	if(!$beercase12 && !$beercase6){$beercase12 = strpos($search, "mixed case");}


			if(!$beercase24 && !$beercase12 && !$beercase6 && !$beercase){$beercase = strpos($search, "500ml");}
			if(!$beercase24 && !$beercase12 && !$beercase6 && !$beercase){$beercase = strpos($search, "330ml");}
			if(!$beercase24 && !$beercase12 && !$beercase6 && !$beercase){$beercase = strpos($search, "250ml");}



			$bottle6 = strpos($search, "6 x 750ml");
			if(!$bottle6){$bottle6 = strpos($search, "6x 750ml");}
			if(!$bottle6){$bottle6 = strpos($search, "6x750ml");}	
				
			$bottle3 = strpos($search, "3 x 750ml");
			if(!$bottle3){$bottle3 = strpos($search, "3x 750ml");}
			if(!$bottle3){$bottle3 = strpos($search, "3x750ml");}
		
			$bottle2 = strpos($search, "2 x 70cl");
		
			$bottle = strpos($search, "1 bottle");
			if(!$bottle){$bottle = strpos($search, "1 x 750ml");}
			if(!$bottle){$bottle = strpos($search, "1 x 75cl");}
			if(!$bottle){$bottle = strpos($search, "1 x 70cl");}
			if(!$bottle && !$bottle3 && !$bottle6){$bottle = strpos($search, "750ml");}
			if(!$bottle && !$bottle3 && !$bottle6){$bottle = strpos($search, "75cl");}
			if(!$bottle && !$bottle3 && !$bottle6){$bottle = strpos($search, "70cl");}
														
			if(!$bottle && !$bottle3 && !$bottle6 && !$beercase24 && !$beercase12 && !$beercase6){$bottle = strpos($search, "50cl");}
			
			if(!$bottle && !$bottle2){$bottle = strpos($search, "70cl");}
			if(!$bottle && !$bottle2 && !$beercase){$bottle = strpos($search, "(single bottle)");}
						
			/* expand this for future use */
			$special = strpos($search,"ginfest mixed case");
 			$special = strpos($search,"value variety pack");
			/* end of expansion */
			
			$extra_special = strpos($search,"ariel chardonnay white wine");
			$extra_special = strpos($search,"pierre zéro alcohol free rosé wine");
			$extra_special = strpos($search,"pierre zéro grand reserve alcohol free red wine");
			$extra_special = strpos($search,"pierre zéro grand reserve alcohol free white wine");
			
			if($special===false){
			
			if($beercase24 !== false){$beerbox=$beerbox+(2 * $qtykey); $totalitems = $totalitems + (24*$qtykey); global $beercase24; unset($beercase24);}
			if($beercase12 !== false){$beerbox=$beerbox+$qtykey; $totalitems = $totalitems + (12*$qtykey); global $beercase12; unset($beercase12);}
			if($beercase6 !== false){$beerbox=$beerbox+(.5*$qtykey); $totalitems = $totalitems + (6*$qtykey); global $beercase6; unset($beercase6);}
			if($beercase !== false){$beerbox = $beerbox + (0.084*$qtykey); $totalitems = $totalitems + $qtykey; global $beercase; unset($beercase);}
		
			if($bottle6 !==false){$winebox = $winebox + (1*$qtykey); $totalitems = $totalitems + $qtykey; global $bottle6; unset($bottle6);}
			if($bottle3 !==false){$winebox = $winebox + (0.5*$qtykey); $totalitems = $totalitems + $qtykey; global $bottle3; unset($bottle3);}
			if($bottle2 !==false){$winebox = $winebox + (0.334*$qtykey); $totalitems = $totalitems + $qtykey; global $bottle2; unset($bottle2);}
			
			if($bottle !==false){$winebox = $winebox + (0.167*$qtykey); $totalitems = $totalitems + $qtykey; global $bottle; unset($bottle);}
			
	            	}
	            	
			if($special !== false){$winebox = $winebox + (1*$qtykey); $totalitems = $totalitems + $qtykey; global $special; unset($special);}
			if($extra_special !==false){$winebox = $winebox + (0.167*$qtykey); $totalitems = $totalitems + $qtykey; global $bottle; unset($bottle);}
		}