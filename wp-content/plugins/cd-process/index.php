<?php
/*
  Plugin Name:  Label Assistant
  Description:  Dry Drinker Label Assistant by Channel Digital
  Version: 	    0.1 RC
  Author: 	    Channel Digital
  Author URI:   https://www.channeldigital.co.uk
  License: 	    GPLv2
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


/*
04.10.2018 

v0.04
Hardcoded a special catagory.   Expand this is custom/user available.


v0.03
Some messy bits added in at lines 229-231 & 362-364 to handle unexpected items.
I've a feeling that the $qtykey isn't available here, but don't want to mess around at this point in case of breaking something.
Review if it comes up again.
*/


add_action('admin_menu', 'label_assistant_menu');

function label_assistant_menu(){
  add_menu_page('Label Assistant Page', 'Label Assistant', 'manage_options', 'label-assistant-slug', 'label_assistant_admin_page');
}

function label_assistant_admin_page() {

  if (!current_user_can('manage_options'))  {
    wp_die( __('You do not have sufficient pilchards to access this page.')    );
  } ?>


  <div class="wrap">
    <h2><div style="margin-bottom: -6px;">Channel Digital Label Assistant</div><div style="padding-left:5px; font-size:12px;">v0.1RC  Released 2018.10.16</div></h2>
  
  <?php
  if (isset($_POST['label_assistant']) && check_admin_referer('label_assistant_clicked')) 
  {
    label_assistant_action();
  }
  ?>
  
  <form enctype="multipart/form-data" action="options-general.php?page=label-assistant-slug" method="post">
  <?php wp_nonce_field('label_assistant_clicked'); ?>
  <input type="file" name="up" /><br />
  <?php  submit_button('Process File'); ?><br />
  

  
  <div style="
	background: #fff;
	border-left: 4px solid #fff;
        box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
        margin: 5px 15px 2px;
        padding: 1px 12px;
        max-width: 360px;
        border-left-color: #46b450;">
  
  <input type="hidden" value="true" name="label_assistant" />
  <h3>Advanced Settings</h3>
    <table>
    
    <tr><td style="border-bottom:1px solid #ddd;">Output Delimiter</td>        <td><input type="text" value="~" name="delimiter" size="1"></td><tr>
    
      <tr>
        <td style="border-bottom:1px solid #ddd;">Order Number is in Column</td>
        <td><input type="text" value="0" name="order" size="1"></td>
      </tr>
      <tr>
        <td style="border-bottom:1px solid #ddd;">Order Item is in Column</td>
        <td><input type="text" value="14" name="item" size="1"></td>
      </tr>
      <tr>
        <td style="border-bottom:1px solid #ddd;">Item Quantity is in Column</td>
        <td><input type="text" value="15" name="qty" size="1"></td>
      </tr>
        <tr>
        <td style="border-bottom:1px solid #ddd;">Item Weight is in Column</td>
        <td><input type="text" value="17" name="weight" size="1"></td>
      </tr>
      
      <tr>
        <td style="border-bottom:1px solid #ddd;">Item number is in Column</td>
        <td><input type="text" value="18" name="inumber" size="1"></td>
      </tr>
      
      
      <tr>
	      <td style="border-bottom:1px solid #ddd;">Keep original item numbers</td>
	      <td><input type="checkbox" id="itemno" name="itemno" ></td>
      </tr>
      
      <tr>
        <td style="border-bottom:1px solid #ddd; padding-right:5px;"><b>Write all to file</b><br />This will output all 'box' and item information<br /> after the main columns.</td>
        <td style="position: relative; bottom: -18px;"><input type="checkbox" id="atf" name="atf" ></td>
      </tr>
      
      
      
      <tr>
        <td style="border-bottom:1px solid #ddd;">Show useful infomation</td>
        <td><input type="checkbox" id="info" name="info" ></td>
      </tr>
      <tr>
        <td style="border-bottom:1px solid #ddd;">Expert Debug Results</td>
        <td><input type="checkbox" id="debug" name="debug" ></td>
      </tr>
    </table><br />
   </div>
  </form>
  </div>

<?php }

function label_assistant_action()
{

$pluginpath = plugin_dir_path( __DIR__ );
$adminpath = get_admin_url();
$par = $_FILES['up'];
  
  echo '<div id="message" class="updated fade"><p>'
    .'Download processed file from <a href="'.$adminpath."/tmp/CD-".basename($_FILES["up"]["name"]).'">Here</a>' . '</p></div>';

$delimiter = $_POST["delimiter"];  

$debug =  $_POST["debug"];  
$info =  $_POST["info"];  
$order = $_POST["order"];
$item = $_POST["item"];
$qty = $_POST["qty"];
$inumber = $_POST["inumber"];

$weight = $_POST["weight"];
$atf = $_POST["atf"];  
$itemxxx = $_POST["itemno"];  
  
$uploads_dir = "/temp";

// echo ($par['tmp_name']);
  $name = basename($_FILES["up"]["name"]);
 
  if($debug =="on"){echo $name;}

  $loadarray = array();
  $allarray = array();
  
  $row = 0;
  $previous = -1; 
  
if (($handle = fopen($par['tmp_name'], "r")) !== FALSE) {
  while (($data = fgetcsv($handle, 3000, "~")) !== FALSE) {
   
    $num = count($data);
    if($debug =="on"){echo "<p> $num fields in line $row: <br /></p>\n";}
    
 
    $row++;
    if($debug =="on"){    
    echo "<pre>";
    print_r($data);
    echo "</pre>";    }
    
    for($c=0; $c < $num; $c++) 
    {
      
    /* if($previous!=$data[$order]){ */
      $allarray[$row][$c] = $data[$c];
 /*     }
      else{

        $allarray[$row][$item] .= "|".$data[$item];
      } */
      
      if($info == "on")
      {
        if($c==$order && $previous!=$data[$c]){echo "<br />Order - " .$data[$c] . "<br />\n"; $previous = $data[$c];}
        if($c==$item)  {echo "Item - " .$data[$c] . "<br />\n";}
        if($c==$qty)   {echo "Qty - " .$data[$c] . "<br />\n";} 
        if($c==$weight){echo "Weight - " .$data[$c] . "<br />\n";} 
      }
        
      if($debug =="on"){echo  $c." - ".$data[$c] . "<br />\n";}
            
      if($c==$order) {$loadarray[$row][0]=$data[$c];}
      if($c==$item)  {$res = preg_replace("/[^a-zA-Z0-9\s]/", "", $data[$c]); $loadarray[$row][1]=$res;}
      if($c==$qty)   {$loadarray[$row][2]=$data[$c];}
      if($c==$weight){$loadarray[$row][3]=$data[$c];}
            
    }
    

   
  }

  fclose($handle);
  
      if($debug =="on"){
  echo "<pre>";
  print_r($loadarray);
  echo "</pre>";  
  }
  

  
  $previousordernumber = -1;
  $mixedcase = -1;
  $my_file = "tmp/CD-".$name;
  $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
  foreach ($loadarray as $key=>$value)
  {
    $ordernumber = $value[0];
    $itemkey = $value[1];
    $qtykey  = $value[2]; 
    $weightkey  = $value[3];     

    
    if($ordernumber!=$previousordernumber)
    {
    
      if($previousordernumber!="-1")
      {
        $writedata = "";
        
        if($beerbox==1.084){$beerbox=1;}
        if($beerbox==1.008){$beerbox=1;}
	if($beerbox==1.5 && $winebox>0){$beerbox=2;}
	if($beerbox==2.5 && $winebox>0){$beerbox=3;}
	if($beerbox==3.5 && $winebox>0){$beerbox=4;}
        if($winebox==3.006){$winebox=3;}
        if($winebox==2.004){$winebox=2;}
        if($winebox==2.002){$winebox=2;}
        if($winebox==1.002){$winebox=1;}
        if($winebox==0.5){$winebox=1;}
        
        if($beerbox == 0.5 && $winebox == 0.334){$beerbox = 1; $winebox = 1;} 
        
        $boxes = ceil($beerbox+$winebox);
        
        if($boxes==0 && $qtykey<=12){$boxes = 1;}
        if($boxes==0 && $qtykey>12){$boxes = 2;}
        if($boxes==0 && $qtykey>24){$boxes = 3;}
                
      foreach($allarray[$key-1] as $keyx=>$valuex)
      {
      
      /* all this needed */
	if($keyx!=$weight){
	      
	      if($keyx!=$inumber){
	      	      
        	$writedata .= $valuex.$delimiter;
        	}
        	else
        	{
        	
        	
        	   if($itemxxx=="on")
        	   {
	        	$writedata .= $valuex.$delimiter;        	   
	           }
	           else
	           {
		   	$writedata .= $boxes.$delimiter;
	           }
	           
           	}
        
        }

	if($keyx==$weight)
        {
        $writedata .= $weightbox.$delimiter;
        }
      /* all this needed */
              
      }
        
        
      
        
        if($previousordernumber!="Order Number")
        {  
                  if($atf=="on"){ $writedata .= $previousordernumber.$delimiter.$beerbox.$delimiter.$winebox.$delimiter.$totalitems.$delimiter.$boxes.$delimiter.$weightbox."\n";}else{$writedata .= "\n";}
        }
        else
        {
        
	$writedata = "Order Number".$delimiter."Order Notes".$delimiter."Email (Billing)".$delimiter."Phone (Billing)".$delimiter."Full Name (Shipping)".$delimiter."Company (Shipping)".$delimiter."Address 1 (Shipping)".$delimiter."Address 2 (Shipping)".$delimiter."City (Shipping)".$delimiter."State Name (Shipping)".$delimiter."Zip (Shipping)".$delimiter."Country Name(Shipping)".$delimiter."Item #".$delimiter."SKU".$delimiter."Name".$delimiter."Quantity".$delimiter."Item Cost".$delimiter."Total weight".$delimiter."Total items".$delimiter."PreDelNot".$delimiter."PreDelType".$delimiter."IOD Email".$delimiter."Address 1&2 (Billing)".$delimiter."Address 1&2 (Shipping)".$delimiter."Delivery Instructions".$delimiter."";
         if($atf=="on"){ $writedata .= "Order Number".$delimiter."Beer Boxes".$delimiter."Wine Boxes".$delimiter."Total Items".$delimiter."Total Boxes\n";}else{$writedata .= "\n";}
        }
      
	  $_i=0;
	  
	  if($previousordernumber == "Order Number"){$_i=1;}
	  
	  do {
		  fwrite($handle,$writedata);
		  $_i++;
		 } while ($_i < $boxes);
		
		
      }
      $beerbox=0; $winebox=0; $totalitems=0; $boxes = 0; $weightbox = 0;
    }
    
    $previousordernumber = $ordernumber;
     
    if($debug=="on")  
    {   
      $magicdata = $ordernumber.",".$itemkey.",".$qtykey."\r\n";
      if($key==0){echo "MagicData<br />";}  
      echo $magicdata."<br />"; 
    }  
    
    if($mixedcase == 1)
    {
      $mixedqty = ($mixedqty - $qtykey);
    }
   
    if($mixedqty<0){$mixedcase = 0;}

    
    if($mixedcase!=1)
    {
      $search = strtolower($itemkey);
		  $mixed = strpos($search, "pick your own mixed case");
        if($mixed !== false)
        {
			    $mixed_24 = strpos($search, "24 bottles");
			    $mixed_12 = strpos($search, "12 bottles");
			    $mixedcase = 1;
			    if($mixed_24 !== false){$mixedqty = 24;  $beerbox = $beerbox + 2; $totalitems = $totalitems + 23;}
			    if($mixed_12 !== false){$mixedqty = 12;  $beerbox++; $totalitems = $totalitems + 11;}         
		    }
      
      else
        
      {
        $search = strtolower($itemkey);

        include $pluginpath.'cd-process/selections.php';
        
     
        
      }
      
      $weightbox = $weightkey;
      
    }
    
  
  }
  
  
  
  
  /*
//  $writedata = "";  
  $boxes = ceil($beerbox+$winebox);
  $writedata .= $previousordernumber."~".$beerbox."~".$winebox."~".$totalitems."~".$boxes."~".$weightbox."\n";
  fwrite($handle,$writedata); */

	
  
        $writedata = "";
        
        if($beerbox==1.084){$beerbox=1;}
        if($beerbox==1.008){$beerbox=1;}
	if($beerbox==1.5 && $winebox>0){$beerbox=2;}
	if($beerbox==2.5 && $winebox>0){$beerbox=3;}
	if($beerbox==3.5 && $winebox>0){$beerbox=4;}
        if($winebox==3.006){$winebox=3;}
        if($winebox==2.004){$winebox=2;}
        if($winebox==2.002){$winebox=2;}
        if($winebox==1.002){$winebox=1;}
        if($winebox==0.5){$winebox=1;}
        
        if($beerbox == 0.5 && $winebox == 0.334){$beerbox = 1; $winebox = 1;} 
        
        $boxes = ceil($beerbox+$winebox);
        
        if($boxes==0 && $qtykey<=12){$boxes = 1;}
        if($boxes==0 && $qtykey>12){$boxes = 2;}
        if($boxes==0 && $qtykey>24){$boxes = 3;}
        

        
      foreach($allarray[$key] as $keyx=>$valuex)
      {
      
      /* all this needed */
	if($keyx!=$weight){
	      
	      if($keyx!=$inumber){
	      	      
        	$writedata .= $valuex.$delimiter;
        	}
        	else
        	{
        	   if($itemno=="on")
        	   {
	        	$writedata .= $valuex.$delimiter;        	   
	           }
	           else
	           {
		        	$writedata .= $boxes.$delimiter;
		        	
	           }
           	}
        
        }

	if($keyx==$weight)
        {
        $writedata .= $weightbox.$delimiter;
        }
      /* all this needed */
        

        
      }
        

        
        if($previousordernumber!="Order Number")
        {  
                 if($atf=="on"){  $writedata .= $previousordernumber.$delimiter.$beerbox.$delimiter.$winebox.$delimiter.$totalitems.$delimiter.$boxes.$delimiter.$weightbox."\n";}else{$writedata .= "\n";}
        }
  $_i=0;
	  do {
		  fwrite($handle,$writedata);
		  $_i++;
		 } while ($_i < $boxes);
		 
		 
		 
  fclose($handle);
  
}
  
  
  
 
}  
?>