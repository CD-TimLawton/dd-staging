<?php
/*
  Plugin Name:  Abandoned Breakdown
  Description:  Abandoned Cart Breakdown by Channel Digital
  Version: 	    0.1 RC
  Author: 	    Channel Digital
  Author URI:   https://www.channeldigital.co.uk
  License: 	    GPLv2
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

add_action('admin_menu', 'abandoned_breakdown_menu');

function abandoned_breakdown_menu(){
    add_menu_page('Abandoned Breakdown Page', 'Abandoned Breakdown', 'manage_options', 'abandoned-breakdown-slug', 'abandoned_breakdown_admin_page');
}

function abandoned_breakdown_admin_page() {


    $band = array();
    $band[0]=0.00;$band[1]=29.99;
    $band[2]=30.00;$band[3]=49.99;
    $band[4]=50.00;$band[5]=99.99;
    $band[6]=100.00;$band[7]=9999;
    $magic = strtotime("-2 week");







    $bands=0;
    foreach($band as $_tmp){$bands++;}
    $count = array();
    $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT * FROM `wp_ac_abandoned_cart_history_lite` WHERE abandoned_cart_time>".$magic;
    $result = mysqli_query($conn, $sql);
    while($row = $result->fetch_assoc()) {
        $data = json_decode($row['abandoned_cart_info'], TRUE);
        if ($data) {

            $cart = $data["cart"];
            $time = json_decode($row['abandoned_cart_time'], TRUE);
            $sub = 0;
            foreach ($cart as $array) {
                foreach ($array as $key => $value) {
                    if ($key == "line_total") {
                        $sub = $sub + $value;
                    }
                }
            }

            $num = number_format((float)$sub, 2, '.', '');
            if($num>0) {
                $a=0;
                $b=1;
                $i=0;
                do {
                    if($num>$band[$a] && $num<$band[$b]) {
                        $count[$i]++;
                    }
                    $a=$a+2;
                    $b=$b+2;
                    $i++;
                } while ($b < $bands);
            }
        }
    }

    $_magic = date('d/m/Y', $magic);
?>
<html>
<?php
$body = "['Range','Number'],";
    $c = 0;
    foreach($count as $value){

        $numa = number_format((float)$band[$c], 2, '.', '');
        $numb = number_format((float)$band[$c+1], 2, '.', '');



        $body .= "['";
        $body .= "£".$numa;
        $body .= " - ";
        if($band[$c+1]!=9999) {
        $body .= "£" . $numb;
        }
        else{
        $body .= " ";
        }
        $body .= "',";
        $body .=  $value."],";
    $c=$c+2;
    }
    $body .="['','']"; ?>

  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

          var data = google.visualization.arrayToDataTable([
                  <?php echo $body; ?>
              ]);

          var options = {
              title: 'Abandoned Cart Breakdown since <?php echo $_magic; ?>',
              is3D: true,
             // pieSliceText: 'value'
              pieSliceText: 'value-and-percentage'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
  </head>
  <body><br />
    <div id="piechart" style="width: 900px; height: 500px;"></div>
  <br />
  <a href="/wp-admin/admin.php?page=abandoned-breakdown-slug&action=true" ></a>

<?php
    if (isset($_GET['action'])) {
        abandoned_cart_action();
    }

}


function abandoned_cart_action() {


}
