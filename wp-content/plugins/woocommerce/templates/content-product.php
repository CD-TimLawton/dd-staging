<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<style>
    .featured-button {border-radius:5px;}
    .black-button {background-color:#000 !important;}
</style>
<li <?php wc_product_class(); ?>>
	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' ); 
	?>


  <?php  if ( md_is_mobile() ) { ?>
	
	<?php $id = $product->get_id(); ?>
	<?php $_type = $product->get_type(); ?>
	<?php $value['variation_id']=99999; ?>



	
	<?php if($_type=="simple"){?>
<?php $button_text = "Add To Cart | "; ?>
        <?php $price = $product->get_regular_price(); ?>
        <?php  $onsale = $product->is_on_sale();  ?>
        <?php if($onsale==1){$price = $product->get_sale_price();} ?>
      <?php $wccur = get_option('woocommerce_currency'); ?>
      <?php $sym = get_woocommerce_currency_symbol($wccur); ?>
      <?php $button_text .= $sym.$price; ?>

<div><input value="1" type="text" name="qty<?php echo $id.$value['variation_id']; ?>" id="qty<?php echo $id.$value['variation_id']; ?>" size="3"></div><br />
<a id="buy<?php echo $id.$value['variation_id']; ?>" href="#"><button id="button<?php echo $id.$value['variation_id']; ?>" style="letter-spacing: -0.5px; font-size: 16px; height:35px; width:70%; padding:0 5px 0 5px;" class="single_add_to_cart_button button alt"><?php echo $button_text; ?></button></a>
	 <script>    
       jQuery('#buy<?php echo $id.$value['variation_id']; ?>').click(function(e) {
          e.preventDefault();
          addToCarts(<?php echo $id; ?>,<?php echo $value['variation_id']; ?>,<?php echo $price; ?>);
          return false;
       });    

       function addToCarts(p_id,v_id,s_id) {


var q_id = jQuery("#qty"+p_id+v_id).val();
jQuery("#button"+p_id+v_id).html("Adding <i class='fas fa-spinner fa-spin'></i>");
                 jQuery.get('/?post_type=product&add-to-cart=' + p_id + '&quantity=' + q_id, function() {
			jQuery("#button"+p_id+v_id).html("Added");
                     s_id = (s_id * q_id);
                     var x = jQuery( ".count" ).first().text();
                     var res = x.split(" ",1);

                     res = parseInt(res) + parseInt(q_id);

                     jQuery(".count").text(res);
                     var cart = jQuery(".woocommerce-Price-amount").first().text();
                     var cart = cart.replace("£", "");
                     var middle = parseFloat(cart);
                     var final = "£"+(middle+s_id).toFixed(2);
                     jQuery(".woocommerce-Price-amount").first().html(final);

          });
       }
    </script>





	<?php } ?>
	
	<?php if($_type=="variable"){?>	
	<?php $available_variations = $product->get_available_variations(); ?>
	
	<?php $count_variations = count($available_variations); ?>

	<?php foreach($available_variations as $value){ ?>
	
	<?php if($count_variations!=1) { ?>
<?php $_tmp = $value['attributes']['attribute_pa_select-size']; ?>
<?php $_tmp = str_replace("-"," ",$_tmp); ?>
<?php $_tmp = ucwords($_tmp); ?>
<?php $_tmp = str_replace("Case Of ","",$_tmp); ?>
<?php $_tmp = str_replace(" Bottles","",$_tmp); ?>
<?php $_tmp = str_replace(" Cans","",$_tmp); ?>




<?php $button_text = "Add ".$_tmp." To Cart | " ?>
<?php } else { $button_text = "Add To Cart | "; } ?>
	
	<!-- <a href="https://staging5.drydrinker.com/?add-to-cart=<?php echo $id; ?>&variation_id=	<?php echo $value['variation_id']; ?>">
	<button class="single_add_to_cart_button button alt"><?php echo $button_text; ?></button>
	</a> -->

    <?php $variable_product1= new WC_Product_Variation( $value['variation_id']); ?>



            <?php $sales_price = $variable_product1->get_regular_price(); ?>
            <?php  $onsale = $variable_product1->is_on_sale();  ?>
            <?php if($onsale==1){$sales_price = $variable_product1->sale_price;} ?>
            <?php $wccur = get_option('woocommerce_currency'); ?>
            <?php $sym = get_woocommerce_currency_symbol($wccur); ?>
            <?php $button_text .= $sym.$sales_price; ?>
<a id="buy<?php echo $id.$value['variation_id']; ?>" href="#"><button id="button<?php echo $id.$value['variation_id']; ?>" style="letter-spacing: -0.5px; font-size: 16px; height:35px; width:70%; padding:0 5px 0 5px;" class="<?php if($_tmp=="12"){echo "black-button ";} ?>single_add_to_cart_button button alt"><?php echo $button_text; ?></button></a>

	 <script>    
       jQuery('#buy<?php echo $id.$value['variation_id']; ?>').click(function(e) {
          e.preventDefault();
          addToCartv(<?php echo $id; ?>,<?php echo $value['variation_id']; ?>,<?php echo $sales_price; ?>);
          return false;
       });    

       function addToCartv(p_id,v_id,s_id)
       {
           jQuery("#button"+p_id+v_id).html("Adding <i class='fas fa-spinner fa-spin'></i>");
           jQuery.get('/?post_type=product&add-to-cart=' + p_id + "&variation_id= " + v_id, function() {
           jQuery("#button" + p_id + v_id).html("Added!");

									var x = jQuery( ".count" ).first().text();
   								        var res = x.split(" ",1);
   								        res++;

									jQuery(".count").text(res);
                                    var cart = jQuery(".woocommerce-Price-amount").first().text();
                                    var cart = cart.replace("£", "");
                                    var middle = parseFloat(cart);
                                    var final = "£"+(middle+s_id).toFixed(2);
                                    jQuery(".woocommerce-Price-amount").first().html(final);

          });
       }
    </script>
    
	<?php } ?>	
	<?php } ?>	
	
	<?php if($_type=="mix-and-match"){?>	 
	
	<?php	$url = get_permalink( $item['product_id'] ) ; ?>
	<a href="<?php echo $url; ?>">
		<button style="letter-spacing: -0.5px; font-size: 16px; height:35px; width:70%; padding:0 5px 0 5px;" class="single_add_to_cart_button button alt">SELECT BOX</button>
	</a>
	<?php } ?>
  <?php } ?>
	
</li>


   

