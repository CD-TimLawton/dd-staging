<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

?>




<?php

$display = 0;
$christmas = date("d-m");
/* if($christmas=="28-12" ||  $christmas=="29-12" || $christmas=="30-12" ||$christmas=="31-12" || $christmas=="01-01" || $christmas=="25-12" || $christmas=="26-12"){$display=0;} */




if ($display==1){
?>

<h2>Your drinks should arrive on <span style="color:#6d6d6d;">

<?php



$_time = date('G');
$_day = date('N');


if($_time<15)
{
	$del_date = date('l, jS F', strtotime($stop_date . ' +1 day'));
}
else
{
	$del_date = date('l, jS F', strtotime($stop_date . ' +2 day'));
}

if($_day==4 && $_time>=15){	$del_date = date('l, jS F', strtotime($stop_date . ' +4 day'));}
if($_day==5 && $_time<15) {	$del_date = date('l, jS F', strtotime($stop_date . ' +3 day'));}
if($_day==5 && $_time>=15){	$del_date = date('l, jS F', strtotime($stop_date . ' +4 day'));}
if($_day==6){	$del_date = date('l, jS F', strtotime($stop_date . ' +3 day'));}
if($_day==7){	$del_date = date('l, jS F', strtotime($stop_date . ' +2 day'));}

if($christmas=="28-12" && $_time<14){$del_date = "Monday, 31st December";}
if($christmas=="29-12" || $christmas=="30-12"){$del_date = "Wednesday, 2nd January";}
if($christmas=="28-12" && $_time>=14){$del_date = "Wednesday, 2nd January";}
if($christmas=="31-12" || $christmas=="01-01"){$del_date = "Thursday, 3rd January";}

if($christmas=="21-12" || $christmas=="22-12" || $christmas=="23-12" || $christmas=="24-12"){$del_date = "Thursday, 27th December";}


echo $del_date;
?></span><span style="font-size:12px;">&nbsp;&nbsp;*&nbsp;UK Mainland</span>
</h2> <br />
<?php } ?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

	<?php if ( $checkout->get_checkout_fields() ) : ?>

		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

		<div class="col2-set" id="customer_details">
			<div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
			</div>

			<div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
			</div>
		</div>

		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

	<?php endif; ?>

	<h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>

	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	<div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
	</div>

	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
