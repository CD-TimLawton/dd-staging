<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>

<?php

$_okbeer=0;
$_okwine=0;
$_tmpbeers = array(23,29,30,31,32,33,34,35,37,38,42,43,46,88,124,126,128);
$_tmpwines = array(108,109,115,116,117);

$terms = get_the_terms( $product->ID, 'product_cat' );



foreach ($terms as $term) {
    $product_cat_id = $term->term_id;

    foreach ($_tmpbeers as $_tmp){
        if($product_cat_id==$_tmp){$_okbeer=1; break;}
    }
    foreach ($_tmpwines as $_tmp){
        if($product_cat_id==$_tmp){$_okwine=1; break;}
    }
}


?>


<div class="woocommerce-variation-add-to-cart variations_button">
	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	<?php
	do_action( 'woocommerce_before_add_to_cart_quantity' );

	woocommerce_quantity_input( array(
		'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
		'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
		'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
	) );

	do_action( 'woocommerce_after_add_to_cart_quantity' );
	?>

	<button type="submit" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

	<?php if($_okbeer==1){ ?><div><br /><h3>Enjoy more! UK Mainland delivery from £4.99 however big your order</h3></div><?php } ?>

	
	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
</div>
