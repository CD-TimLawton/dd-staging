=== WooCommerce Ajax add to cart ===
Contributors: quadlayers
Tags: woocommerce, woocommerce ajax, woocommerce ajax cart, add to cart
Requires at least: 4.8
Tested up to: 5.0
Stable tag: 1.0.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Ajax add to cart for WooCommerce Single products

== Description ==

The default add to cart button of WooCommerce for single products reloads the entire site each time a product is added to the cart. The more steps and time your e-commerce load, the more you reduce the chances of selling.

[WooCommerce Ajax add to cart](https://quadmenu.com/add-to-cart-with-woocommerce-and-ajax-step-by-step/) allows users to include single products or variable products in the cart without the need to reload the entire site each time.

== WooCommerce Express ==

If you're looking for an all in one solution we recommend you to install the [WooCommerce Express](https://wordpress.org/plugins/woo-express/) plugin. It allows you to simplify the checkout process by skipping the shopping cart page. 

The checkout options allow you to easily remove the unnecessary fields to reduce the user spend completing those fields. Also WooCommerce Express allows you to remove other unnecessary things in the checkout process like the order comments, shipping address, coupon form, policy text, and terms and conditions.

https://youtu.be/qeC9bVKFgL8

== Improvements ==

1. Use less bandwidth
2. Reduce server load
3. Speed user experience
4. Increase your sales!

== Changelog ==

= 1.0.5 =
* disabled button validation

= 1.0.4 =
* fixed ajax add to cart in variable products

= 1.0.3 =
* fixed when ajax add to cart in archives off

= 1.0.2 =
* class names woocommerce ajax add to cart updated

= 1.0.1 =
* include script only in products pages

= 1.0.0 =
* initial release of woocommerce ajax add to cart