(function ($) {

  $(document).on('click', '.single_add_to_cart_button', function (e) {

    var $button = $(this),
            $form = $button.closest('form.cart'),
            product_id = $form.find('input[name=add-to-cart]').val() || $button.val();

    if (!product_id)
      return;

    if ($button.is('.disabled'))
      return;

    e.preventDefault();

    var data = {
      action: 'woocommerce_ajax_add_to_cart',
      'add-to-cart': product_id,
    };

    $form.serializeArray().forEach(function (element) {
      data[element.name] = element.value;
    });

    $(document.body).trigger('adding_to_cart', [$button, data]);

    $.ajax({
      type: 'post',
      url: wc_add_to_cart_params.ajax_url,
      data: data,
      beforeSend: function (response) {
        $button.removeClass('added').addClass('loading');
      },
      complete: function (response) {
        $button.addClass('added').removeClass('loading');
      },
      success: function (response) {

        if (response.error & response.product_url) {
          window.location = response.product_url;
          return;
        } else {
          $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $button]);

          $('.woocommerce-notices-wrapper').empty().append(response.notices);

        }
      },
    });

    return false;

  });
})(jQuery);