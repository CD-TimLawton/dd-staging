<?php

/**
 * Plugin Name: WooCommerce Ajax add to cart
 * Plugin URI: https://quadmenu.com/add-to-cart-with-woocommerce-and-ajax-step-by-step/
 * Description: Ajax add to cart for WooCommerce Single products
 * Version: 1.0.5
 * Author: WooCommerce Ajax add to cart
 * Author URI: http://www.quadmenu.com
 */
if (!defined('ABSPATH')) {
  die('-1');
}

if (!class_exists('WATC')) {

  class WATC {

    function __construct() {
      add_action('wp_enqueue_scripts', array($this, 'enqueue'), 99);
      add_action('wp_ajax_woocommerce_ajax_add_to_cart', array($this, 'ajax_add_to_cart'));
      add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', array($this, 'ajax_add_to_cart'));
    }

    function enqueue() {
      if (function_exists('is_product') && is_product()) {
        wp_enqueue_script('woocommerce-ajax-add-to-cart', plugin_dir_url(__FILE__) . 'assets/ajax-add-to-cart.js', array('jquery', 'wc-add-to-cart'), '', true);
      }
    }

    function ajax_add_to_cart() {

      WC_AJAX::get_refreshed_fragments();

      wp_die();
    }

    function wc_get_notices() {

      ob_start();

      wc_print_notices();

      return ob_get_clean();
    }

  }

  new WATC();
}
