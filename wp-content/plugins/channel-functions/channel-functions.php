<?php
/*
Plugin Name: Channel Functions
Description: Functionality by Channel Digital
Version: 	 1.1
Author: 	 Channel Digital
Author URI:  https://www.channeldigital.co.uk
License: 	 GPLv2
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// A name and version number means the code was taken from a pre-existing plugin. If we have any issues / WP is updated, look to these plugins for updated code.

// Noindex attachment pages v1.0
add_action('wp_head', 'attachmentpages_noindex');
function attachmentpages_noindex(){
	if(is_attachment()){
		echo '<meta name="robots" content="noindex" />';
	}
}

// Disable emojis https://www.searchcandy.uk/blog/wordpress/how-to-disable-emojis-in-wordpress/
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );



// SAR Disable REST API v1.0 https://en-gb.wordpress.org/plugins/sar-disable-rest-api/
// Remove HTTP header and link tag, then disble the API
add_filter( 'init', 'sar_remove_rest_api_headers' );
function sar_remove_rest_api_headers() {
	remove_action( 'wp_head',                    'rest_output_link_wp_head', 10 );
	remove_action( 'template_redirect',          'rest_output_link_header', 11 );
}
add_filter('rest_enabled', '__return_false');
add_filter('rest_jsonp_enabled', '__return_false');

// Remove sorting dropdown
// https://businessbloomer.com/woocommerce-remove-default-sorting-dropdown/
add_action( 'init', 'bbloomer_delay_remove' ); 
function bbloomer_delay_remove() {
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 10 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 10 );
}



?>