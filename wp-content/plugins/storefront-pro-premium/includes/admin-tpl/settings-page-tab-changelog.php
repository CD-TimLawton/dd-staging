<?php
/**
 * Created by PhpStorm.
 * User: shramee
 * Date: 10/02/17
 * Time: 4:43 PM
 */
?>

<section class="half-width">
	<h4>Version 5.8.1 <small>2018-06-30</small></h4>
	<ul>
		<li>Fix - Error when WP_DEBUG is enabled</li>
	</ul>

	<h4>Version 5.8.0 <small>2018-06-29</small></h4>
	<ul>
		<li>New - Shop featured products slider hero</li>
		<li>New - Shop featured image hero</li>
		<li>New - Product category featured products slider hero</li>
		<li>New - Product category 'category image' hero</li>
		<li>New - Customizer slider controls supports putting in a number</li>
		<li>Fix - Console error when WooCommerce inactive</li>
		<li>Fix - WooCommerce Checkout customizations panel not showing</li>
		<li>Fix - WooCommerce quick view broken</li>
		<li>Fix - Mobile menu color default not working</li>
		<li>Fix - Product hero layout space under navigation</li>
		<li>Fix - Archive heading shown on blog page</li>
		<li>Tweak - Freemius SDK updated</li>
		<li>Dev - <code>storefront_pro_header_hero_slideshow_speed</code> filter to filter homepage slider speed.</li>
	</ul>

	<h4>Version 5.7.4 <small>2018-06-01</small></h4>
	<ul>
		<li>* New - Menu icon label</li>
		<li>* Fix - Mobile menu icon alignment broken in Storefront v2.3.2</li>
		<li>* Fix - Mobile menu positioned over menu icon in Storefront v2.3.2</li>
		<li>* Fix - Page builder gap above page builder content</li>
		<li>* Fix - Margin in left vertical navigation</li>
		<li>* Tweak - GDPR compliance</li>
		<li>* Tweak - Freemius SDK update</li>
	</ul>

	<h4>Version 5.7.3 <small>2018-05-13</small></h4>
	<ul>
		<li>New - Touch to hide/show mobile submenus</li>
		<li>Fix - Mobile menu icon position</li>
		<li>Fix - Mobile menu not working</li>
		<li>Fix - Footer bar loading page twice when background image not used</li>
		<li>Fix - Header cart link light grey background</li>
		<li>Fix - Primary navigation center style</li>
		<li>Fix - Primary navigation center logo in nav style</li>
		<li>Fix - Primary navigation left vertical style</li>
		<li>Fix - Primary navigation hamburger style</li>
		<li>Tweak - Use Storefront menu icon only</li>
		<li>Tweak - `Mobile menu > Hide logo image` and `Mobile menu > Remove page icons` options removed</li>
	</ul>

	<h4>Version 5.7.2 <small>2018-05-09</small></h4>
	<ul>
		<li>New - Variable product quick view image preview</li>
		<li>New - Storefront v2.3 compatibility</li>
		<li>Fix - Header bar loading page twice when background image not used</li>
		<li>Fix - Footer bar loading page twice when background image not used</li>
		<li>Fix - Footer bar loading page twice when background image not used</li>
		<li>Fix - PHP logged error on single post page</li>
	</ul>

</section>
<section class="half-width">

	<h4>Version 5.7.1 <small>2018-03-30</small></h4>
	<ul>
		<li>Fix - Header hero heading font not working</li>
	</ul>

	<h4>Version 5.7.0 <small>2018-03-30</small></h4>
	<ul>
		<li>New - Set CTA button text per slide</li>
		<li>New - Set CTA button link per site</li>
		<li>New - Header hero heading font</li>
		<li>New - Header hero heading size</li>
		<li>New - Header hero text size</li>
		<li>New - Live search bar compatibility with header hero</li>
		<li>Fix - Infinite scroll not working on mobile</li>
		<li>Fix - Header Slider hero breaking with hamburger menu</li>
	</ul>

	<h4>Version 5.6.0 <small>2018-03-09</small></h4>
	<ul>
		<li>New - Shop sidebar can now be displayed on fancy shop layouts</li>
		<li>Fix - Mobile menu primary menu flash</li>
		<li>Fix - Breadcrumbs not accessible from mobile</li>
		<li>Fix - Errors in Customizer (trying to edit section which don't exist)</li>
		<li>Tweak - Show excerpt on mobile for slider</li>
		<li>Tweak - Slider uses flexbox</li>
		<li>Tweak - Header hero bottom margin</li>
		<li>Tweak - Header hero sliders dynamically calculated whitespacing</li>
		<li>Tweak - Move import export dialog to admin footer</li>
	</ul>

</section>


</div>