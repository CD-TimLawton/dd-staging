<?php
/**
 * Created by PhpStorm.
 * User: shramee
 * Date: 18/11/16
 * Time: 12:56 PM
 */
?>
	<style>
		.videoWrapper {
			position: relative;
			padding-bottom: 56.5%;
			padding-top: 25px;
			height: 0;
			margin: 0 0 2.5em;
		}

		.videoWrapper iframe {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
		}

		h1 {
			font-weight: normal;
			margin: 1.6em 0;
		}

		h1 small {
			opacity: 0.7;
			float: right;
			font-size: 0.5em;
		}

		.sfp-wrap {
			width: 97%;
		}

		.sfp-wrap:after {
			display: block;
			content: '';
			clear: both;
		}

		.half-width {
			width: 50%;
			float: left;
		}
	</style>
	<div class="sfp-wrap">
	<h1>Storefront Pro
		<small>Version <?php echo SFP_VERSION ?></small>
	</h1>
	<?php

	$tabs = [ 'home', 'modules' ];

	$active_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : '';
	$active_tab = in_array( $active_tab, $tabs ) ? $active_tab : 'home';
	$href       = admin_url( 'themes.php?page=storefront-pro' );

	if ( ! empty( $_GET['notice'] ) ) {
		echo <<<HTML
<div class="notice notice-success is-dismissible" style="margin:7px 0">
	<p>$_GET[notice]</p>
</div>
HTML;

	}

	if ( ! empty( $_GET['error'] ) ) {
		echo <<<HTML
<div class="notice notice-error">
	<p>$_GET[error]</p>
</div>
HTML;

	}
	add_thickbox();
	?>
	<p>
		Storefront Pro let’s you easily customize the WooThemes Storefront theme. Storefront is the official WooCommerce
		theme.
	</p>
	<div class="videoWrapper">
		<iframe frameborder="0" src="https://player.vimeo.com/video/268133764"
						webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</div>
	<hr>
	<h1>Marketing apps included with your Storefront Pro subscription</h1>
	<?php
	require "settings-page-tab-modules.php";
	?>
	<br>
	<hr>
	<h1>Changelog</h1>

<?php
require "settings-page-tab-changelog.php";

