=== Storefront Pro ===
Contributors: pootlepress, jamiemarsland, shramee, freemius
Tags: storefront, woocommerce, storefront pro, ecommerce, e-commerce
Requires at least: 4.0
Tested up to: 4.9.5
Stable tag: 5.8.1
License: GPLv2 or later

Customize the design of every element of your Storefront website

== Description ==

Storefront Pro let’s you easily customize the WooThemes Storefront theme. Storefront is the official WooCommerce theme, built by WooThemes that has been downloaded 439,265 times.

Customize your website with live previews. Customize everything – website colors, fonts, the header, the menu, the footer, your blog, product layouts, your shop layout and more.

== Installation ==

Upload the Storefront Pro plugin to your blog, Activate it, then enter your API key.

== Changelog ==

Changelog for Storefront Pro is available at http://pootlepress.github.io/storefront-pro/changelog.html