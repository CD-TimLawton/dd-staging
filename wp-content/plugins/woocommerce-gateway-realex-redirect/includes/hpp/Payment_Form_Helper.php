<?php
/**
 * WooCommerce Global Payments HPP
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Global Payments HPP to newer
 * versions in the future. If you wish to customize WooCommerce Global Payments HPP for your
 * needs please refer to https://docs.woocommerce.com/document/woocommerce-global-payments/ for more information.
 *
 * @author      SkyVerge
 * @copyright   Copyright (c) 2012-2019, SkyVerge, Inc.
 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

namespace SkyVerge\WooCommerce\Global_Payments\HPP;

defined( 'ABSPATH' ) or exit;

use SkyVerge\WooCommerce\PluginFramework\v5_4_0 as Framework;

/**
 * Helper class to build HPP form data formatted for the Global Payments API.
 *
 * @since 2.2.0
 */
class Payment_Form_Helper {


	/**
	 * Gets HPP address params from an order.
	 *
	 * @since 2.2.0
	 *
	 * @param \WC_Order $order
	 * @param bool $ship_to_different_address whether the customer specified a different address at checkout
	 * @return array
	 */
	public static function get_address_params( \WC_Order $order, $ship_to_different_address = false ) {

		$params = self::build_address_params( $order );

		// add the shipping info if needed
		if ( Framework\SV_WC_Order_Compatibility::has_shipping_address( $order ) ) {

			// build the shipping params
			$shipping_params = self::build_address_params( $order, 'shipping' );

			// if the customer specifically indicated, or the addresses otherwise don't match, send the shipping info
			if ( $ship_to_different_address || array_values( $params ) !== array_values( $shipping_params ) ) {

				$params = array_merge( $params, $shipping_params );

				$params['HPP_ADDRESS_MATCH_INDICATOR'] = 'FALSE';

			// otherwise, indicate that the addresses match
			} else {

				$params['HPP_ADDRESS_MATCH_INDICATOR'] = 'TRUE';
			}

			// send these legacy params regardless
			$shipping_params['SHIPPING_CODE'] = Framework\SV_WC_Order_Compatibility::get_prop( $order, 'shipping_postcode' );
			$shipping_params['SHIPPING_CO']   = Framework\SV_WC_Order_Compatibility::get_prop( $order, 'shipping_country' );
		}

		return $params;
	}


	/**
	 * Builds HPP address params from an order.
	 *
	 * @since 2.2.0
	 *
	 * @param \WC_Order $order
	 * @param string $type address type
	 * @return array
	 */
	private static function build_address_params( \WC_Order $order, $type = 'billing' ) {

		$api_type = strtoupper( $type );

		$params = [
			"HPP_{$api_type}_STREET1" => Framework\SV_WC_Order_Compatibility::get_prop( $order, "{$type}_address_1" ),
			"HPP_{$api_type}_STREET2" => Framework\SV_WC_Order_Compatibility::get_prop( $order, "{$type}_address_2" ),
			"HPP_{$api_type}_CITY"    => Framework\SV_WC_Order_Compatibility::get_prop( $order, "{$type}_city" ),
			"HPP_{$api_type}_STATE"   => Framework\SV_WC_Order_Compatibility::get_prop( $order, "{$type}_state" ),
		];

		// the above fields share the same character limits
		foreach ( $params as $key => $value ) {
			$params[ $key ] = Framework\SV_WC_Helper::str_truncate( preg_replace( '/[^A-Za-z0-9-_+ \'".,]*/', '', $value ), 40 );
		}

		$params["HPP_{$api_type}_POSTALCODE"] = Framework\SV_WC_Helper::str_truncate( preg_replace( '/[^A-Za-z0-9-_]*/', '', Framework\SV_WC_Order_Compatibility::get_prop( $order, "{$type}_postcode" ) ), 30 );
		$params["HPP_{$api_type}_COUNTRY"]    = self::get_numeric_country_code( Framework\SV_WC_Order_Compatibility::get_prop( $order, "{$type}_country" ) );

		return $params;
	}


	/**
	 * Gets the customer params.
	 *
	 * @since 2.2.0
	 *
	 * @param \WC_Order $order order object
	 * @return array
	 */
	public static function get_customer_params( \WC_Order $order ) {

		// pre-formatted so we can replace strings
		$calling_code = self::get_country_calling_code( Framework\SV_WC_Order_Compatibility::get_prop( $order, 'billing_country' ) );
		$phone_number = Framework\SV_WC_Order_Compatibility::get_prop( $order, 'billing_phone' );

		// strip the calling code from the beginning if it exists
		if ( 0 === strpos( $phone_number, $calling_code ) ) {
			$phone_number = substr( $phone_number, strlen( $calling_code ) );
		}

		// format for the API
		$calling_code = preg_replace( '/\D/', '', $calling_code );
		$phone_number = preg_replace( '/\D/', '', $phone_number );

		$params = [
			'HPP_CUSTOMER_EMAIL'              => Framework\SV_WC_Order_Compatibility::get_prop( $order, 'billing_email' ),
			'HPP_CUSTOMER_FIRSTNAME'          => Framework\SV_WC_Helper::str_truncate( preg_replace( '/[^A-Za-z0-9-_+ \'".,]*/', '', Framework\SV_WC_Order_Compatibility::get_prop( $order, 'billing_first_name' ) ), 30, '' ),
			'HPP_CUSTOMER_LASTNAME'           => Framework\SV_WC_Helper::str_truncate( preg_replace( '/[^A-Za-z0-9-_+ \'".,]*/', '', Framework\SV_WC_Order_Compatibility::get_prop( $order, 'billing_last_name' ) ), 50, '' ),
			'HPP_CUSTOMER_PHONENUMBER'        => $phone_number,
			'HPP_CUSTOMER_PHONENUMBER_MOBILE' => "{$calling_code}|{$phone_number}",
		];

		return $params;
	}


	/**
	 * Gets a numeric country code for the alpha-2 code provided.
	 *
	 * @since 2.2.0
	 *
	 * @param string $country_code alpha-2 country code
	 * @return string
	 */
	private static function get_numeric_country_code( $country_code ) {

		$countries = [
			'AF' => '004',
			'AX' => '248',
			'AL' => '008',
			'DZ' => '012',
			'AS' => '016',
			'AD' => '020',
			'AO' => '024',
			'AI' => '660',
			'AQ' => '010',
			'AG' => '028',
			'AR' => '032',
			'AM' => '051',
			'AW' => '533',
			'AU' => '036',
			'AT' => '040',
			'AZ' => '031',
			'BS' => '044',
			'BH' => '048',
			'BD' => '050',
			'BB' => '052',
			'BY' => '112',
			'BE' => '056',
			'BZ' => '084',
			'BJ' => '204',
			'BM' => '060',
			'BT' => '064',
			'BO' => '068',
			'BQ' => '535',
			'BA' => '070',
			'BW' => '072',
			'BV' => '074',
			'BR' => '076',
			'IO' => '086',
			'BN' => '096',
			'BG' => '100',
			'BF' => '854',
			'BI' => '108',
			'KH' => '116',
			'CM' => '120',
			'CA' => '124',
			'CV' => '132',
			'KY' => '136',
			'CF' => '140',
			'TD' => '148',
			'CL' => '152',
			'CN' => '156',
			'CX' => '162',
			'CC' => '166',
			'CO' => '170',
			'KM' => '174',
			'CG' => '178',
			'CD' => '180',
			'CK' => '184',
			'CR' => '188',
			'CI' => '384',
			'HR' => '191',
			'CU' => '192',
			'CW' => '531',
			'CY' => '196',
			'CZ' => '203',
			'DK' => '208',
			'DJ' => '262',
			'DM' => '212',
			'DO' => '214',
			'EC' => '218',
			'EG' => '818',
			'SV' => '222',
			'GQ' => '226',
			'ER' => '232',
			'EE' => '233',
			'ET' => '231',
			'FK' => '238',
			'FO' => '234',
			'FJ' => '242',
			'FI' => '246',
			'FR' => '250',
			'GF' => '254',
			'PF' => '258',
			'TF' => '260',
			'GA' => '266',
			'GM' => '270',
			'GE' => '268',
			'DE' => '276',
			'GH' => '288',
			'GI' => '292',
			'GR' => '300',
			'GL' => '304',
			'GD' => '308',
			'GP' => '312',
			'GU' => '316',
			'GT' => '320',
			'GG' => '831',
			'GN' => '324',
			'GW' => '624',
			'GY' => '328',
			'HT' => '332',
			'HM' => '334',
			'VA' => '336',
			'HN' => '340',
			'HK' => '344',
			'HU' => '348',
			'IS' => '352',
			'IN' => '356',
			'ID' => '360',
			'IR' => '364',
			'IQ' => '368',
			'IE' => '372',
			'IM' => '833',
			'IL' => '376',
			'IT' => '380',
			'JM' => '388',
			'JP' => '392',
			'JE' => '832',
			'JO' => '400',
			'KZ' => '398',
			'KE' => '404',
			'KI' => '296',
			'KP' => '408',
			'KR' => '410',
			'KW' => '414',
			'KG' => '417',
			'LA' => '418',
			'LV' => '428',
			'LB' => '422',
			'LS' => '426',
			'LR' => '430',
			'LY' => '434',
			'LI' => '438',
			'LT' => '440',
			'LU' => '442',
			'MO' => '446',
			'MK' => '807',
			'MG' => '450',
			'MW' => '454',
			'MY' => '458',
			'MV' => '462',
			'ML' => '466',
			'MT' => '470',
			'MH' => '584',
			'MQ' => '474',
			'MR' => '478',
			'MU' => '480',
			'YT' => '175',
			'MX' => '484',
			'FM' => '583',
			'MD' => '498',
			'MC' => '492',
			'MN' => '496',
			'ME' => '499',
			'MS' => '500',
			'MA' => '504',
			'MZ' => '508',
			'MM' => '104',
			'NA' => '516',
			'NR' => '520',
			'NP' => '524',
			'NL' => '528',
			'NC' => '540',
			'NZ' => '554',
			'NI' => '558',
			'NE' => '562',
			'NG' => '566',
			'NU' => '570',
			'NF' => '574',
			'MP' => '580',
			'NO' => '578',
			'OM' => '512',
			'PK' => '586',
			'PW' => '585',
			'PS' => '275',
			'PA' => '591',
			'PG' => '598',
			'PY' => '600',
			'PE' => '604',
			'PH' => '608',
			'PN' => '612',
			'PL' => '616',
			'PT' => '620',
			'PR' => '630',
			'QA' => '634',
			'RE' => '638',
			'RO' => '642',
			'RU' => '643',
			'RW' => '646',
			'BL' => '652',
			'SH' => '654',
			'KN' => '659',
			'LC' => '662',
			'MF' => '663',
			'PM' => '666',
			'VC' => '670',
			'WS' => '882',
			'SM' => '674',
			'ST' => '678',
			'SA' => '682',
			'SN' => '686',
			'RS' => '688',
			'SC' => '690',
			'SL' => '694',
			'SG' => '702',
			'SX' => '534',
			'SK' => '703',
			'SI' => '705',
			'SB' => '090',
			'SO' => '706',
			'ZA' => '710',
			'GS' => '239',
			'SS' => '728',
			'ES' => '724',
			'LK' => '144',
			'SD' => '729',
			'SR' => '740',
			'SJ' => '744',
			'SZ' => '748',
			'SE' => '752',
			'CH' => '756',
			'SY' => '760',
			'TW' => '158',
			'TJ' => '762',
			'TZ' => '834',
			'TH' => '764',
			'TL' => '626',
			'TG' => '768',
			'TK' => '772',
			'TO' => '776',
			'TT' => '780',
			'TN' => '788',
			'TR' => '792',
			'TM' => '795',
			'TC' => '796',
			'TV' => '798',
			'UG' => '800',
			'UA' => '804',
			'AE' => '784',
			'GB' => '826',
			'US' => '840',
			'UM' => '581',
			'UY' => '858',
			'UZ' => '860',
			'VU' => '548',
			'VE' => '862',
			'VN' => '704',
			'VG' => '092',
			'VI' => '850',
			'WF' => '876',
			'EH' => '732',
			'YE' => '887',
			'ZM' => '894',
			'ZW' => '716',
		];

		return isset( $countries[ $country_code ] ) ? $countries[ $country_code ] : '';
	}


	/**
	 * Gets a calling code for the provided country.
	 *
	 * @since 2.2.0
	 *
	 * @param string $country_code country code
	 * @return string
	 */
	private static function get_country_calling_code( $country_code ) {

		// WC 3.6+ has this built in
		if ( Framework\SV_WC_Plugin_Compatibility::is_wc_version_gte( '3.6.0' ) ) {

			$calling_code = WC()->countries->get_country_calling_code( $country_code );

		} else {

			$calling_codes = [
				'BD' => '+880',
				'BE' => '+32',
				'BF' => '+226',
				'BG' => '+359',
				'BA' => '+387',
				'BB' => '+1246',
				'WF' => '+681',
				'BL' => '+590',
				'BM' => '+1441',
				'BN' => '+673',
				'BO' => '+591',
				'BH' => '+973',
				'BI' => '+257',
				'BJ' => '+229',
				'BT' => '+975',
				'JM' => '+1876',
				'BV' => '',
				'BW' => '+267',
				'WS' => '+685',
				'BQ' => '+599',
				'BR' => '+55',
				'BS' => '+1242',
				'JE' => '+441534',
				'BY' => '+375',
				'BZ' => '+501',
				'RU' => '+7',
				'RW' => '+250',
				'RS' => '+381',
				'TL' => '+670',
				'RE' => '+262',
				'TM' => '+993',
				'TJ' => '+992',
				'RO' => '+40',
				'TK' => '+690',
				'GW' => '+245',
				'GU' => '+1671',
				'GT' => '+502',
				'GS' => '',
				'GR' => '+30',
				'GQ' => '+240',
				'GP' => '+590',
				'JP' => '+81',
				'GY' => '+592',
				'GG' => '+441481',
				'GF' => '+594',
				'GE' => '+995',
				'GD' => '+1473',
				'GB' => '+44',
				'GA' => '+241',
				'SV' => '+503',
				'GN' => '+224',
				'GM' => '+220',
				'GL' => '+299',
				'GI' => '+350',
				'GH' => '+233',
				'OM' => '+968',
				'TN' => '+216',
				'JO' => '+962',
				'HR' => '+385',
				'HT' => '+509',
				'HU' => '+36',
				'HK' => '+852',
				'HN' => '+504',
				'HM' => '',
				'VE' => '+58',
				'PR' => [
					'+1787',
					'+1939',
				],
				'PS' => '+970',
				'PW' => '+680',
				'PT' => '+351',
				'SJ' => '+47',
				'PY' => '+595',
				'IQ' => '+964',
				'PA' => '+507',
				'PF' => '+689',
				'PG' => '+675',
				'PE' => '+51',
				'PK' => '+92',
				'PH' => '+63',
				'PN' => '+870',
				'PL' => '+48',
				'PM' => '+508',
				'ZM' => '+260',
				'EH' => '+212',
				'EE' => '+372',
				'EG' => '+20',
				'ZA' => '+27',
				'EC' => '+593',
				'IT' => '+39',
				'VN' => '+84',
				'SB' => '+677',
				'ET' => '+251',
				'SO' => '+252',
				'ZW' => '+263',
				'SA' => '+966',
				'ES' => '+34',
				'ER' => '+291',
				'ME' => '+382',
				'MD' => '+373',
				'MG' => '+261',
				'MF' => '+590',
				'MA' => '+212',
				'MC' => '+377',
				'UZ' => '+998',
				'MM' => '+95',
				'ML' => '+223',
				'MO' => '+853',
				'MN' => '+976',
				'MH' => '+692',
				'MK' => '+389',
				'MU' => '+230',
				'MT' => '+356',
				'MW' => '+265',
				'MV' => '+960',
				'MQ' => '+596',
				'MP' => '+1670',
				'MS' => '+1664',
				'MR' => '+222',
				'IM' => '+441624',
				'UG' => '+256',
				'TZ' => '+255',
				'MY' => '+60',
				'MX' => '+52',
				'IL' => '+972',
				'FR' => '+33',
				'IO' => '+246',
				'SH' => '+290',
				'FI' => '+358',
				'FJ' => '+679',
				'FK' => '+500',
				'FM' => '+691',
				'FO' => '+298',
				'NI' => '+505',
				'NL' => '+31',
				'NO' => '+47',
				'NA' => '+264',
				'VU' => '+678',
				'NC' => '+687',
				'NE' => '+227',
				'NF' => '+672',
				'NG' => '+234',
				'NZ' => '+64',
				'NP' => '+977',
				'NR' => '+674',
				'NU' => '+683',
				'CK' => '+682',
				'XK' => '',
				'CI' => '+225',
				'CH' => '+41',
				'CO' => '+57',
				'CN' => '+86',
				'CM' => '+237',
				'CL' => '+56',
				'CC' => '+61',
				'CA' => '+1',
				'CG' => '+242',
				'CF' => '+236',
				'CD' => '+243',
				'CZ' => '+420',
				'CY' => '+357',
				'CX' => '+61',
				'CR' => '+506',
				'CW' => '+599',
				'CV' => '+238',
				'CU' => '+53',
				'SZ' => '+268',
				'SY' => '+963',
				'SX' => '+599',
				'KG' => '+996',
				'KE' => '+254',
				'SS' => '+211',
				'SR' => '+597',
				'KI' => '+686',
				'KH' => '+855',
				'KN' => '+1869',
				'KM' => '+269',
				'ST' => '+239',
				'SK' => '+421',
				'KR' => '+82',
				'SI' => '+386',
				'KP' => '+850',
				'KW' => '+965',
				'SN' => '+221',
				'SM' => '+378',
				'SL' => '+232',
				'SC' => '+248',
				'KZ' => '+7',
				'KY' => '+1345',
				'SG' => '+65',
				'SE' => '+46',
				'SD' => '+249',
				'DO' => [
					'+1809',
					'+1829',
					'+1849',
				],
				'DM' => '+1767',
				'DJ' => '+253',
				'DK' => '+45',
				'VG' => '+1284',
				'DE' => '+49',
				'YE' => '+967',
				'DZ' => '+213',
				'US' => '+1',
				'UY' => '+598',
				'YT' => '+262',
				'UM' => '+1',
				'LB' => '+961',
				'LC' => '+1758',
				'LA' => '+856',
				'TV' => '+688',
				'TW' => '+886',
				'TT' => '+1868',
				'TR' => '+90',
				'LK' => '+94',
				'LI' => '+423',
				'LV' => '+371',
				'TO' => '+676',
				'LT' => '+370',
				'LU' => '+352',
				'LR' => '+231',
				'LS' => '+266',
				'TH' => '+66',
				'TF' => '',
				'TG' => '+228',
				'TD' => '+235',
				'TC' => '+1649',
				'LY' => '+218',
				'VA' => '+379',
				'VC' => '+1784',
				'AE' => '+971',
				'AD' => '+376',
				'AG' => '+1268',
				'AF' => '+93',
				'AI' => '+1264',
				'VI' => '+1340',
				'IS' => '+354',
				'IR' => '+98',
				'AM' => '+374',
				'AL' => '+355',
				'AO' => '+244',
				'AQ' => '',
				'AS' => '+1684',
				'AR' => '+54',
				'AU' => '+61',
				'AT' => '+43',
				'AW' => '+297',
				'IN' => '+91',
				'AX' => '+35818',
				'AZ' => '+994',
				'IE' => '+353',
				'ID' => '+62',
				'UA' => '+380',
				'QA' => '+974',
				'MZ' => '+258',
			];

			$calling_code = isset( $calling_codes[ $country_code ] ) ? $calling_codes[ $country_code ] : '';

			// we can't really know _which_ code is to be used, so use the first
			if ( is_array( $calling_code ) ) {
				$calling_code = $calling_code[0];
			}
		}

		return $calling_code;
	}


}
