=== WooCommerce Global Payments HPP ===
Author: skyverge, woocommerce
Tags: woocommerce
Requires PHP: 5.4
Requires at least: 4.4
Tested up to: 5.1.1

Adds Global Payments HPP to your WooCommerce website.

See https://docs.woocommerce.com/document/woocommerce-global-payments/ for full documentation.

== Installation ==

1. Upload the entire 'woocommerce-gateway-realex-redirect' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
