!function ($) {

  $(document).ready(function() {
    
    window.startTime = new Date().getTime();

    $.plModel.init()

    var end = new Date().getTime();
    var time = end - startTime;

    plTop()('.iframe-loading-overlay').removeClass('show-overlay')

    plTrigger('ready')

    
  })

  $.plModel = {

    init: function(){

      var that = this

      /** Set initial page data model */
      that.setMasterModel()

      $.plBinding.startUp()
      
      

    }, 

    /**
     * Get JSON in model (iframe) as if getting it from parent frame it wont work.
     * @return {json} model json string
     */
    getJSON: function(){

      return ko.toJSON( PLData.viewModel )
 
    }, 

    /**
     * Used for settings values when editing.
     */
    getData: function(){
 
      return ko.toJS( PLData.viewModel )
 
     }, 

    getSectionOption: function( UID, key ){

      return ( plIsset(PLData.viewModel[ UID ]) ) ? PLData.viewModel[ UID ][ key ]() : false

    }, 

    setSectionOption: function( UID, key, value ){

      PLData.viewModel[ UID ][ key ]( value )

    }, 

    getAllSectionData: function( UID ){

      var data = {}

      if( plIsset( PLData.viewModel[ UID ] ) ){

        $.each( PLData.viewModel[ UID ], function(key, val){



          var theValue = PLData.viewModel[ UID ][ key ]()



          if( plIsset(theValue) ){

            if( _.isArray(theValue) ){

              data[ key ] = {}

              $.each( theValue, function( ai, av ){

                data[ key ][ ai ] = {}

                $.each( av, function( arrayKey, arrayKeyObs ){

                  data[ key ][ ai ][ arrayKey ] = arrayKeyObs()

                })
                
              })

            }

            else{
              data[ key ] = theValue
            }
            
          }

          

        })
      
      }

      return data 
    }, 

     updateModelData: function(){

       var  that                = this,
            currentModelData   = that.getData()


         $.each( currentModelData, function(UID, keys){

           $.each( keys, function(key, val){

              if( ! plIsset( PLData.modelData[ UID ] ) ){
                PLData.modelData[ UID ] = { values: {} }
              }

             if( plIsset( val ) ){

              if( ! plIsset( PLData.modelData[ UID ].values[ key ] ) ){
                PLData.modelData[ UID ].values[ key ] = {}
              }

              PLData.modelData[ UID ].values[ key ].value = val

               // /** Standard data format */
               // if( plIsset(PLData.modelData[ UID ].values[ key ]) && plIsset(PLData.modelData[ UID ].values[ key ].value) ){
               //   PLData.modelData[ UID ].values[ key ].value = val
               // } 
               // /** For unknown options (js only) probably should be made consistent */
               // else {
               //   PLData.modelData[ UID ].values[ key ] = val

               // }

               
             }
           
           })

         })


     },


    

    

    /**
     * Loads all sections models based on page load.
     */
    setMasterModel: function(){

      var that     = this

      PLData.viewModel   = PLData.viewModel || {}

      $.each( PLData.modelData, function( UID, theData ) {
        
        that.loadSectionModel( UID, theData )  
        
      })

    

    },


    loadSectionModel: function( UID, theData ){

      var that = this

      /** If model entry exists, don't overwrite it */
      if( typeof PLData.viewModel[ UID ] == 'undefined' ){

        var mod = {}

        if(  !_.isEmpty( theData ) ){

          /** Set up basic observables */
          $.each( theData.values, function( key, info ) {

            if( ! plIsset( info ) ){
              console.log( 'View model for ' + key + ' is set incorrectly.')
            
            } 

            else {
              
              var val   = info.value, 
                  type   = info.type, 
                  opts   = info.opts
              

              
              mod = that.setObservables( mod, key, theData, type, opts)

            }

            
            
            

          })

          PLData.viewModel[ UID ] = mod


        } 



        /**
         * Standard settings
         */
        var standard = $.plStandardSettings.settingsArray()

        $.each( standard , function( title, panel ) {

          that.getSectionObservables( UID, panel.opts )

        })

      }


    }, 

    

    setObservables: function( mod, key, data, type, opts){

      var that = this, 
        val   = that.getValueFromKey( data, key )
      
        
      if( _.isArray( val ) || _.isObject( val ) ){

        mod[ key ] = that.makeObservableArray( val, type, opts )

      }

      else {
        mod[ key ] = ko.observable( val ).extend({ notify:'always' })
      }

      return mod

    },


    getValueFromKey: function( data, key ){

      return data.values[ key ].value;

    }, 

    asyncComputedArray: function(evaluator, async_function ) {
        var result = ko.observableArray();

        ko.computed( function() {
            // // Get the $.Deferred value, and then set up a callback so that when it's done,
            // // the output is transferred onto our "result" observable
            evaluator
              .call( async_function )
              .done( function( response ){

                var rsp  = response

                var theArray = []

                $.each( rsp.template, function(i, val){
                  theArray.push(val)
                })


                result(theArray)

              })
        });

        return result;
    },

    makeObservableArray: function( array, type, opts ) {

      
      var that = this,
        normalizedData = [];

      $.each( array, function (index, data) {

        /** Make sure all opts have observables */
        $.extend( opts, data )

        normalizedData.push( that.itemModel( opts ) );
      
      });


      return ko.observableArray(normalizedData);

    },

    itemModel: function( item ){

      var that  = this,
        model = {}


      $.each( item, function( key, val ){

        model[ key ] = ko.observable( val ).extend({ notify:'always' })

      })

      return model

    },

    

    getSectionObservables: function( UID, opts ){

      var that = this

      $.each( opts , function(i2, o) {

        /** If we have sub values, parse them */
        if( !_.isEmpty( o.opts ) ){

          that.getSectionObservables( UID, o.opts )

        } 

        /** If a key is set, then create observable */
        if( !_.isUndefined(o.key) && ! plIsset( PLData.viewModel[ UID ][ o.key ] ) ) {

          PLData.viewModel[ UID ][ o.key ] = ko.observable( that.getInitValue(UID, o.key) ).extend({ notify:'always' })

        }
        

      })


    },

    getInitValue: function( UID, key ){


      if( plIsset( PLData.modelData[ UID ] ) && plIsset( PLData.modelData[ UID ].values ) )
        return PLData.modelData[ UID ].values[ key ]
      else 
        return null
    }, 


    sortObservableArrayByArray: function( UID, key, sortArray ){

      var that     = this,
        newArray   = []

      theArray = PLData.viewModel[ UID ][ key ]()

      $.each( sortArray, function(i, val){

        newArray.push( theArray[ val ] )

      })      

      PLData.viewModel[ UID ][ key ].changeEvent = true

      PLData.viewModel[ UID ][ key ]( newArray )

    },

    RemoveItemByIndex: function( UID, key, index ){

      var that = this

      /** For triggers */
      PLData.changeEvent = true

      PLData.viewModel[ UID ][ key ].splice( index, 1 )

    }, 

    addItemToArray: function( UID, key, item ){

      var that = this

      /** For triggers */
      PLData.changeEvent = true

      observableItem = that.itemModel( item )

      PLData.viewModel[ UID ][ key ].push( observableItem )

    }, 

    setObservableArray: function( UID, key, array ){

      var that = this

      PLData.viewModel[ UID ][ key ] = ko.observableArray( array ).extend({ notify:'always' })

    },

    setNewObservable: function( UID, key, val){

      var that = this

      PLData.viewModel[ UID ][ key ] = ko.observable( val ).extend({ notify:'always' })



    }, 

  

  }



}(window.jQuery);


 /* -------------------- */ 

!function ($) {
  


  $.plAdd = {


    newSection: function( object, UID, newItem ){
      

      var that   = this, 
        args = {
          hook:     'load_section',
          object:   object,
          UID:       UID,
          query:     PLData.config.query,
          
          postSuccess: function( rsp ){

            var render    = PLData.config.tplRender,
                capture   = PLData.config.tplCapture, 
                bldAdd    = 'template'

            /** Add to Builder */
            $( newItem ).prependTo( plTop()( sprintf('.dd-list[data-region="%s"]', bldAdd) ) ).hide().fadeIn()

            /** Load template, add it after the loading section div */
            $( rsp.template ).prependTo( sprintf('.pl-region-wrap-%s', bldAdd ) )
            


            var newSection = $( sprintf('[data-clone="%s"]', UID))

            that.addScripts( rsp )

            $.plScrolling.scrollToSection( newSection )


            /** Reset D&D Interface */
            plTop().plEditing.reloadUI()

            $.plBinding.bindNewSection( UID, {
              object: object, 
              values: rsp.model
            })

            /** Update data to match the view */
            $.plModel.updateModelData()
            
            /** Update Map JSON */
            plTop().plBuilder.updateTemplateMap()

            plTop().plBuilder.doSortables()

          }
        }

      

      $.plServer.run( args )
    }, 

    /** 
     * Add Styles and Scripts.  
     * Must come after template is added for appropriate click event tracking.
     */
    addScripts: function( rsp ){

      var that = this

      /** Add CSS style.css */
      if( plIsset( rsp.css_style ) )
        $('head').append( sprintf('<link rel="stylesheet" href="%s?rand=%s" type="text/css" />', rsp.css_style, Math.floor(Math.random()*999999) ));

      /** Add CSS build.css */
      if( plIsset( rsp.css_build ) )
        $('head').append( sprintf('<link rel="stylesheet" href="%s?rand=%s" type="text/css" />', rsp.css_build , Math.floor(Math.random()*999999) ));

      /** Add JS to footer */
      if( plIsset( rsp.scripts ) && rsp.scripts.length != 0 ){

        $.each( rsp.scripts, function(index, value){
        
          $('body').append( sprintf('<script id="live-injection" src="%s"></script>', value ) );
        
        })
        
      }

    }

  }

  
}(window.jQuery);

 /* -------------------- */ 

!function ($) {
  
  $(window).on('pl_page_ready', function() {
    $.plAnimation.init()
  })

  $.plAnimation = {
      
    init: function(){
      
      var that = this 


      $('body').delegate('.pl-sn', 'template_ready', function(){

        $.plAnimation.doAnimation( $(this) )

      })
            
      
      
    },

    doAnimation: function( selector ){

      var selector = selector || $('body')
      
        
      selector.find('.pl-animation-group')
        .find('.pl-animation')
        .addClass('pla-group')
        
      selector.find('.pl-animation:not(.pla-group, .animation-loaded)').each(function(){

        var element = $(this)



        element.appear(function() {

            if( element.hasClass('pl-slidedown') ){

              var endHeight = element.find('.pl-end-height').outerHeight()
              
              element.css('height', endHeight)

            }


           $(this)
            .addClass('animation-loaded')
            .trigger('animation_loaded')

        })

      })
        


      selector.find('.pl-animation-group').each(function(){
        
        var element = $(this)

        element.imagesLoaded( function( instance ){

          element.appear(function() {
            
            var animationNum = $(this).find('.pl-animation').size()
            
            
            
               $(this)
              .find('.pl-animation:not(.animation-loaded)')
              .each( function(i){
                var element = $(this)
                
                setTimeout(
                  function(){ 
                    element
                      .addClass('animation-loaded hovered')
                    
                    setTimeout(function(){ 
                      element.removeClass('hovered');
                    }, 700); 
                  }
                  , (i * 200)
                );
              })

          })

        })

      })

      
      
      
    }
    
  }

  
}(window.jQuery);

 /* -------------------- */ 

 !function ($) {



  $.plBinding = {

    startUp: function(){

      var that = this

      that.plExtensions()


      /** Extend Knockout for PageLines functionality */
      that.extensions()

      $('body').trigger('pl_extend_bindings')

      /** Iterate through each section and apply observable bindings (model to view) */
      that.applyModel()
    }, 

   //   renderedItem: function( wrapper ){

    //   var that       = this,
    //       source     = wrapper.find( '.rendered-item' ),
    //       container = source.parent()

    //   /** if a clone is there, remove it. */
    //   container.find('.rendered').remove()

    //   * Clone the hidden KO version 
    //   rendered = source.clone().removeAttr('data-bind').removeClass('rendered-item')

    //   /** Add identification class, add and show */
    //   rendered.addClass('rendered').prependTo( container ).show()
      
    //   return rendered

    // }, 

    /** 
     * Trigger for other JS effects
     * NOTE: must come after binding so new values are in place
     */
    doTemplateTriggers: function( element, valueAccessor, allBindings ){

      var that   = this,
        value   = valueAccessor()

      
      if( PLData.changeEvent ){
      
        if( $(element).hasClass('pl-trigger-el') ){
          $(element).closest('.pl-trigger-container').trigger('template_updated')
        }

        else if( $(element).hasClass('pl-trigger') ){
          $(element).closest('.pl-sn').trigger('template_ready')
        }

        else 
          $(element).trigger( 'template_updated' )

      }


    },

    doTemplateAJAX: function( mode, args, element, valueAccessor, allBindings ){

      var that     = this,
          value     = ko.unwrap( valueAccessor() )

      
      if( plIsset( value ) && ( PLData.changeEvent || $( element ).hasClass('pl-load-lazy') ) ){
      
        var config = {
          hook:     'async_binding',
          value:     value,
          mode:     mode,
          args:     args, 
          postSuccess: function( rsp ){


            wrap = sprintf('<div class="new">%s</div>', rsp.template)

            $( element )
              .html( wrap )

            var UID       = $(element).closest('.pl-sn').trigger('template_ready').data('clone')
                newWrapper   = $( element ).find('.new')

            ko.applyBindings( PLData.viewModel[ UID ], newWrapper[0] );

            /** Remove temporary wrapper*/
            newWrapper.children().first().unwrap()
              
            // if( plIsset( rsp.engine ) ){
            //   var styles = '', 
            //       out     = ''

            //   if( _.isObject( rsp.engine['scripts'] ) ){
            //     $.each( rsp.engine['scripts'], function(i, sc){
            //       out += sc
            //     })
            //   }
              
            //   if( _.isObject( rsp.engine['styles'] ) ){
            //     $.each( rsp.engine['styles'], function(i, sc){
            //       styles += sc
            //     })

            //     out += sprintf('<style>%s</style>', styles)
            //   }
              
            //   $('body').append(out)

            // }

            that.doTemplateTriggers( element, valueAccessor, allBindings )
            

          }
        }

        $.plServer.run( config )


      }



    },

    /**
     * This class is primarily used to set class names to option values 
     * It saves the previous or original value as a data attribute, so it can be specifically removed
     *
     */
    doClass: function( element, value, index ){

      var index = index || '', 
          saved = '__pl__previousClassValue__' + index

        
      if (element[ saved ]) {
          $(element).removeClass(element[ saved ]);
      }

      $(element).addClass(value);
    
      element[ saved ] = value;
    
    }, 

    plExtensions: function(){

      var that = this


      ko.bindingHandlers.plnav = {

          update: function( element, valueAccessor, allBindings ) {


              var value   = ko.unwrap( valueAccessor() ),
                    el       = $(element),
                  mode     = el.data('mode'), 
                  clss     = el.data('class')


              that.doTemplateAJAX( 'menu', { menu_class: clss, mode: mode }, element, valueAccessor, allBindings )
            


          }
      };

      ko.bindingHandlers.plsidebar = {

          update: function( element, valueAccessor, allBindings ) {
       
              var value   = ko.unwrap( valueAccessor() ),
                    el       = $(element)

              that.doTemplateAJAX( 'sidebar', { }, element, valueAccessor, allBindings )
  
          }
      };

      ko.bindingHandlers.plcallback = {

          update: function( element, valueAccessor, allBindings ) {

              var value   = ko.unwrap( valueAccessor() ),
                    el       = $(element),
                  mode     = el.data('callback')

              

              that.doTemplateAJAX( mode, { mode: mode }, element, valueAccessor, allBindings )
            


          }
      };

      ko.bindingHandlers.plshortcode = {

          update: function( element, valueAccessor, allBindings ) {


              var value = ko.unwrap( valueAccessor() );

              if( that.isset( value ) && value != '' ){

                
                /** Look for modify event because we dont change out these on load... */
                if(  valueAccessor().modifyEvent  )
                  $(element).html( value ).removeClass('js-unset')

              } 
              else 
                $(element).addClass('js-unset')

            
            that.doTemplateAJAX( 'shortcodes', {}, element, valueAccessor, allBindings )

          }
      };

      ko.bindingHandlers.plbtn = {

          update: function( element, valueAccessor, allBindings ) {
 
              /** Get Option Key */
              var value     = ko.unwrap( valueAccessor() ),
                    el         = $(element),
                  model     = ko.dataFor( element )

              

              var txt       = model[ value + '_text' ]()   || 'Button', 
                  clss       = model[ value + '_style' ]() || 'default', 
                  size       = model[ value + '_size' ]()   || 'st', 
                  link       = model[ value ]()

              


               el.html( txt )
                
             
              if( ! that.isset( link ) || link == '' )
                el.addClass('js-unset')
              else
                el.attr( 'href', link ).removeClass('js-unset')
                

            
              that.doClass( element, 'pl-btn-' + clss, 'style' )
               that.doClass( element, 'pl-btn-' + size, 'size' )


          }
      };


      /**
       * Use this binding to set an option value as a classname
       * Multiple classnames can be binded if an array is used [ key, key ]
       */
      ko.bindingHandlers.plclassname = {
            'update': function( element, valueAccessor, allBindings ) {

              var value = ko.unwrap( valueAccessor() );


              if( _.isArray( value ) ){

                $.each( value, function( i, val ){

                  that.doClass( element, val, i )
                })

              }

              else 
                that.doClass( element, value )

              that.doTemplateTriggers( element, valueAccessor, allBindings )
          }
      };
      

      /**
       * This binding allows you set an arbitrary css class for an element. It requires jQuery.
       * https://github.com/knockout/knockout/wiki/Bindings---class
       */
      ko.bindingHandlers['class'] = {
            'update': function( element, valueAccessor, allBindings ) {

              var value = ko.unwrap( valueAccessor() );

              that.doClass( element, value )

              that.doTemplateTriggers( element, valueAccessor, allBindings )
          }
      };

      


      /** Set Background Images... */
      ko.bindingHandlers.plbg = {
          update: function( element, valueAccessor, allBindings ) {

            var value     = ko.unwrap( valueAccessor() ), 
                args       = allBindings.get( 'args' )    || {},
                url       = ''


            if( that.isset( value ) ){

              url = pl_do_shortcode( value )            

              $(element).css( 'backgroundImage', sprintf( 'url(%s)', url ) )
            
            }
            
            else
              $(element).css( 'backgroundImage', '' )

            

            that.doTemplateTriggers( element, valueAccessor, allBindings )

          }
      };

      /**
       * Handler for Images SRC.
       * Will apply on load if the value of the image src is not empty.
       * If it is empty, it will add the js-unset class which will hide the element.
       */
      ko.bindingHandlers.plimg = {
          update: function( element, valueAccessor, allBindings ) {

            var value = ko.unwrap( valueAccessor() )

            if( that.isset( value ) && value != '' )
              $(element).attr('src', pl_do_shortcode( value ) ).removeClass('js-unset')
            else 
              $(element).removeAttr('src').addClass('js-unset')

            /** do triggers on elements for redraw */
            that.doTemplateTriggers( element, valueAccessor, allBindings )

          }
      };


      /**
       * Handler for text and HTML
       * Will replace contents of the element on load if the value is not empty.
       * If it is empty, it will add the js-unset class which will hide the element.
       */
      ko.bindingHandlers.pltext = {
          update: function(element, valueAccessor, allBindings) {

            var value = ko.unwrap( valueAccessor() );

            

            if( that.isset( value ) && '' !== value ){

              $(element).html( value ).removeClass('js-unset')
  
            } 

            else 
              $(element).addClass('js-unset')

            that.doTemplateTriggers( element, valueAccessor, allBindings )

          }
      };

      /**
       * Set or remove HREF attribute for selected styling ([href])
       */
      ko.bindingHandlers.plhref = {
          update: function( element, valueAccessor, allBindings ) {


          var value = ko.unwrap( valueAccessor() );

          if( that.isset( value ) && value !== '' ){
            $(element).attr('href', pl_do_shortcode( value ) )
          }

          else
            $(element).removeAttr('href')

          that.doTemplateTriggers( element, valueAccessor, allBindings )

          }
      };




    }, 

    isset: function( value ){

      if( typeof value !== 'undefined' && value !== null ){
        return true; 
      }

      else
        return false

    }, 


    applyModel: function(){
      

      var that = this
      
      var count   = $('.pl-sn').length, 
          initial = 1


    
      $('.pl-sn').each( function(i) {
        

        var UID         = $(this).data('clone'), 
            theSection   = $(this),
            addPosts     = false


        if( plIsset(UID) ){

          

          setTimeout(function () {

              ko.applyBindings( PLData.viewModel[ UID ], theSection[0] );

              that.startSection( theSection )             
           
           });

          

        }

    

      })

      // $('.static-template-content').each( function(i){

      //   var UID   = PLData.config.editID, 
      //     theTpl  = $(this)

        

      //   ko.applyBindings( PLData.viewModel[ UID ], theTpl[0] );

      // })
      


    },

    startSection: function( theSection ){
      
      /** 
       * IF a temporary wrapper is used, we should remove now.
       * These are useful to prevent sub binds, better than comments which error out 
       */
      if( theSection.parent().hasClass('temp-wrap') ){
        theSection.unwrap()
      }

      /** Show the section and trigger ready event so we can do actions like animate. */
      theSection
        .trigger( 'template_ready' )
        .addClass( 'js-loaded' )
  
        

    }, 

    bindNewSection: function( UID, theData ){

      var that     = this, 
          theSection   = $( sprintf('[data-clone="%s"]', UID ) )

      /** In case new bindings are added. */
      $('body').trigger('pl_extend_bindings')


      if( theSection.length ){

        /** Create observables */
        $.plModel.loadSectionModel( UID, theData )

        $.each( PLData.viewModel[ UID ], function(key, func){
          PLData.viewModel[ UID ][ key ].modifyEvent = true
        })

        /** Apply binding */
        that.applySectionBinds( PLData.viewModel[ UID ], theSection )

      } else {
        console.log('Error: A bindable section was not found.')
      }

      


    }, 

    applySectionBinds: function( model, section ){

      var that = this

      ko.applyBindings( model, section[0] );

      that.startSection( section )

    }, 

    

    extensions: function(){

      var that = this


      /**
       * Main value replacement binding
       * Optionally pulls information from server based on input
       */
      // ko.bindingHandlers.plsync = {

      //     update: function( element, valueAccessor, allBindings ) {

    
      //       var runQuery = false 


      //         // First get the latest data that we're bound to
      //         var value = valueAccessor();

      //         // Next, whether or not the supplied model property is observable, get its current value
      //         var valueUnwrapped = ko.unwrap( value );

      //         /** The type of AJAX request to make, passed as sync_mode: [type] */
      //         var mode     = allBindings.get( 'sync_mode' )   || 'shortcodes', 
      //           args     = allBindings.get( 'args' )    || {}, 
      //           runOnStart   = args.runStart         || false 

      //         if( typeof valueUnwrapped !== 'undefined' && valueUnwrapped !== ''){

                

      //           /** Change Event value only triggers AJAX on change events not keyups or fast moving changes */
      //            if( PLData.changeEvent ){
                  
      //              runQuery = true

      //            } 

      //            * If shortcodes or autop (text), set value as we type 
      //            else if( value.modifyEvent && ( mode == 'shortcodes' || mode == 'autop') ) {

      //              $(element).html( valueUnwrapped )

      //            }

      //         } 

      //         else {
            
      //       valueUnwrapped = $(element).html()

      //         }

      //         /** Run server query */
      //         if( runQuery ){


      //           var config = {
      //            hook:   'async_binding',
      //            value:   valueUnwrapped,
      //            mode:   mode, 
      //            args:   args, 
      //           postSuccess: function( rsp ){

      //             value.changeEvent = false

      //             /** Trigger an event on the element so we can do more JS */
      //             trigger = rsp.trigger || 'sync'

      //             /** Set the AJAX response template into the element */
      //             $( element )
      //               .html( rsp.template )
      //               .trigger( trigger )
                
                  
    
      //           }
      //         }

      //       $.plServer.run( config )

            
      //       /** Trigger template change event */
      //       that.doTemplateTriggers( element, valueAccessor, allBindings )

      //         }
       
      

      //     }
      // };

      /**
       * Extends the KO foreach binding and triggers an event on the element whenever the obs is updated.
       */
      ko.bindingHandlers.plstyle = {
      
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
          
          var value           = valueAccessor(), 
            valueUnwrapped    = ko.utils.unwrapObservable( value );
          
          ko.bindingHandlers.style.update(element, valueAccessor, allBindings, viewModel, bindingContext);

          /** do triggers on elements for redraw */
          that.doTemplateTriggers( element, valueAccessor, allBindings )

          return { controlsDescendantBindings: true };

        }
      }

      /**
       * Extends the KO foreach binding and triggers an event on the element whenever the obs is updated.
       */
      ko.bindingHandlers.plforeach = {
        init: function(element, valueAccessor) {

          return ko.bindingHandlers.foreach.init(element, valueAccessor);

        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
          
          /** Note value must be unwrapped for this to be triggered. */
          var value       = valueAccessor(), 
            valueUnwrapped   = ko.utils.unwrapObservable( value );
          
          ko.bindingHandlers.foreach.update(element, valueAccessor, allBindings, viewModel, bindingContext);

          /** do triggers on elements for redraw */
          that.doTemplateTriggers( element, valueAccessor, allBindings )

          return { controlsDescendantBindings: true };

        }
      }

      /**
       * Extends the KO template binding and triggers an event on the element
       */
      ko.bindingHandlers.pltemplate = {
        init: function(element, valueAccessor) {
          return ko.bindingHandlers.template.init(element, valueAccessor);
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

    
          var value = valueAccessor()
          
                if( _.isEmpty( value.foreach ))
                  return 
                

          ko.bindingHandlers.template.update(element, valueAccessor, allBindings, viewModel, bindingContext);

          /** do triggers on elements for redraw */
          that.doTemplateTriggers( element, valueAccessor, allBindings )

          return { controlsDescendantBindings: true };

        }
      }

      /**
       * Extends CSS binding to add an event
       */
      ko.bindingHandlers.plcss = {
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

          var value = valueAccessor()
          
                ko.bindingHandlers.css.update(element, valueAccessor, allBindings, viewModel, bindingContext);

          /** do triggers on elements for redraw */
          that.doTemplateTriggers( element, valueAccessor, allBindings )

          return { controlsDescendantBindings: true };

        }
      }


      ko.bindingHandlers.plvisible = {
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

          var value = valueAccessor()
          
          ko.bindingHandlers.visible.update(element, valueAccessor, allBindings, viewModel, bindingContext);

          /** do triggers on elements for redraw */
          that.doTemplateTriggers( element, valueAccessor, allBindings )

          return { controlsDescendantBindings: true };

        }
      }




      /**
       * This prevents Knockout from removing non knockout bindings when we use ApplyBindings
       * http://knockoutjs.com/documentation/custom-bindings-disposal.html#overriding-the-clean-up-of-external-data
       */
      ko.utils.domNodeDisposal.cleanExternalData = function () {
          // Do nothing. Now any jQuery data associated with elements will
          // not be cleaned up when the elements are removed from the DOM.
          // 
      };

      

      /**
       * Take a \n based entry and split into icon/urls 
       * Used in sections like footer, sociallinks, mobilemenu (megamenu)
       */
      ko.bindingHandlers.plicons = {
          update: function( element, valueAccessor, allBindings ) {


            var value     = ko.unwrap( valueAccessor() ),
                items     = value.split("\n");

            var theList = ''


            if( typeof value !== 'undefined' && value !== '' ){              
              
              $.each(items, function(i, item){

                pieces   = item.split(" "), 
                icon     = ( plIsset(pieces[0]) ) ? pieces[0] : '',
                url     = ( plIsset(pieces[1]) ) ? pieces[1] : ''

                if( '' != icon ){
                  theList += sprintf('<a class="iconlist-link" href="%s"><i class="pl-icon pl-icon-%s"></i></a>', url, icon )
                }

              })              
              
            }

            $(element)
              .html(theList)

            /** Trigger template change event */
            that.doTemplateTriggers( element, valueAccessor, allBindings )

          }
      };
      

      /**
       * Take a \n based entry and create a list markup
       */
      ko.bindingHandlers.pllist = {
          update: function( element, valueAccessor, allBindings ) {


          var value     = ko.unwrap( valueAccessor() ),
              flag     = allBindings.get('flag') || false,
              items     = value.split("\n");



          if( typeof value !== 'undefined' && value !== '' ){

            var theList = ''
            
            $.each(items, function(i, item){

              var emph = ''

              if( item.substring(0, 1) == '*' ){

                emph = 'emphasis'
                item = item.substring(1)

              }
              
              if(item != '')
                theList += sprintf('<li class="pl-border %s">%s</li>', emph, item )

            })

            $(element)
              .html(theList)

            
            /** Trigger template change event */
            that.doTemplateTriggers( element, valueAccessor, allBindings )
          }

          }
      };

      

      
      /**
       * The main section editing binding.
       * 
       */
      ko.bindingHandlers.pledit = {
        init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {

          /** Do it on the parent so no conflicts */
          $(element).data( 'start_classes', $(element).attr('class') )

        },
        update: function( element ) {
     
          var model       = ko.dataFor( element ), 
              value       = '', 
              classEl     = $(element).parent(), 
              wrapperEl   = classEl.parent()

    
          /** Background Image */
          if( plIsset( model.background() ) ){

            value = sprintf( 'url(%s)', pl_do_shortcode( model.background() ) )

             classEl.css( 'backgroundImage', value)
          } 

          /** Background Color */
          if( plIsset( model.color() ) ){

               classEl.css( 'backgroundColor', model.color())
          } 

          /** Background Color */
          if( plIsset( model.textcolor() ) ){

               classEl.css( 'color', model.textcolor())

          } 

          /** Background Position */
          var xPos = model.bgxpos(),
              yPos = model.bgypos()

          if( plIsset(xPos) || plIsset(yPos) ){

                
            xPos = ( ! plIsset(xPos) || xPos == '') ? 0 : xPos
            yPos = ( ! plIsset(yPos) || yPos == '') ? 0 : yPos                

            classEl.css( 'backgroundPosition', xPos + "% " + yPos + "%" ) 

          } 

             
          /** Background Size */
          var bgWidth   = model.bgwidth(),
              bgHeight   = model.bgheight(),
              bgCover   = model.bgcover(), 
              sizeValue   = ''

          if( plIsset(bgCover) && bgCover != '' ){

            classEl.css( 'backgroundSize', bgCover ) 

          } 

          else if( plIsset(bgWidth) || plIsset(bgHeight) ){

            bgWidth   = ( ! plIsset(bgWidth)  || bgWidth == '' ) ? 'auto' : bgWidth + 'px'
            bgHeight   = ( ! plIsset(bgHeight) || bgHeight == '') ? 'auto' : bgHeight + 'px'            

            classEl.css( 'backgroundSize', bgWidth + ' ' + bgHeight ) 

          }

          

          /** Background Tile */
          if( plIsset( model.bgrepeat() ) ){
               classEl.css( 'backgroundRepeat', model.bgrepeat() ) 
          } 

          /** Text/Element Scheme */

          /** Remove Current Scheme Classes */
          that.doClass( classEl[0], model.theme(), 'scheme' )
    
          /** Scrolling and Sizing Effects */
          that.doClass( classEl[0], model.effects(), 'effect' )

          /** Minimum Height */
          if( plIsset( model.minheight() ) && '' != model.minheight() ){
            $(element).css( 'min-height', model.minheight() + 'vw' ) 
          } 
          else {
            $(element).css( 'min-height', '' ) 
          }


          /**
           * Columns / Offsets
           * Only applies to sections within the grid area.
           */
          
          var theSpan = ( plIsset( model.col() ) && model.col() != '' )   ? model.col()     : '12', 
              theOff   = ( plIsset( model.offset() ) && model.col() != '' )  ? model.offset()   : '0'

          that.doClass( wrapperEl[0], sprintf('pl-col-sm-%s pl-col-sm-offset-%s', theSpan, theOff ), 'grid' )

          
          /**
           * Layout Content MAX Width
           */
          if( plIsset( model.contentwidth() ) && '' !==  model.contentwidth() ){

            classEl.find('.pl-content-area').css('max-width', model.contentwidth() + 'px')
          } 
          else{
            classEl.find('.pl-content-area').css('max-width', '')
          }
           
          /** Section Alignment Format */
          that.doClass( classEl[0], model.alignment(), 'align' )

          /** Font Size */
          if( plIsset( model.font_size() ) ){
             
               $(element).css( 'fontSize', model.font_size() + 'em' ) 
          } 
        
          /** Special custom classes */
          that.doClass( wrapperEl[0], model.special_classes(), 'special' )


          /** Hiding on specific pages */
          if( plIsset( model.hide_on() ) ){

            var classes = '', 
                pageIDs = model.hide_on().split(',')

            $.each(pageIDs, function( i, PID ){
              if( PID == PLData.config.pageID ){
                classes += 'hide-on-page'
              }
            
            })

            that.doClass( wrapperEl[0], classes, 'hide' )
          }

          /** Background Video */
              if( plIsset( model.video() ) && model.video() != ''){

                var fieldValue   = pl_do_shortcode( model.video() ), 
                  
                format1 = fieldValue.substr( (fieldValue.lastIndexOf('.') +1) );
                    

                var videos   = sprintf('<source src="%s" type="video/%s">', fieldValue, format1),
                  HTML   = sprintf('<div class="pl-bg-video-container"><video class="pl-bg-video" preload autoplay loop muted>%s</video></div>', videos)
                  

                if( classEl.children('.pl-bg-video-container').length == 0 ){
                  classEl.prepend( HTML )
                } else {
                  
                  classEl.children('.pl-bg-video-container video').html( videos )
                }
          
          } else {
                classEl.children('.pl-bg-video-container').remove()
              }

              /** Background Overlay */
              if( plIsset( model.overlay() ) ){

                value = sprintf( 'url(%s)', pl_do_shortcode( model.overlay() ) ) 

               var HTML   = '<div class="pl-bg-overlay" ></div>'

               if( classEl.children('.pl-bg-overlay').length == 0 ){
                  classEl.prepend( HTML )
                } 

               classEl.find('.pl-bg-overlay').css( 'backgroundImage', value )

          } else {
            classEl.children('.pl-bg-overlay').remove()
          }



              /** Padding / Margin */

          var spUnit       = 'vw', 
              directions   = ['top', 'right', 'bottom', 'left'], 
              modes       = {
                  padding:   '',
                  margin:   ''
                }
              
              $.each( modes, function( mode, obj ) {

                $.each( directions, function( i, dir ) {

                  var spacingVal   = model[ mode + '_' + dir ](),
                      spacing     = parseFloat( spacingVal ) 


        
                  if( ! isNaN(spacing) && typeof spacing !== 'undefined'  ){
                    modes[ mode ] += spacing + spUnit + ' '
    
                  }

                  else 
                    modes[ mode ] += '0 '

                })


                /** If there is no */
                if( modes[ mode ].lastIndexOf(spUnit) != -1 ){

                  modes[ mode ] = modes[ mode ].substring( 0, modes[ mode ].lastIndexOf(spUnit) + 2 )

                  $(element).css( mode, modes[ mode ])  


                } 

                else{
                  $(element).css( mode, '')
                }
                


              })

             


          }
      };
    
      /**
       * Better attribute setter with event triggering and better zero state
       */
      ko.bindingHandlers.plattr = {
          update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {

              var value = valueAccessor();

              var valueUnwrapped = ko.unwrap( value );

              var unit  = allBindings.get('unit')   || false

              if( typeof valueUnwrapped !== 'undefined' && valueUnwrapped !== '' ){

                var setValue = {}

                $.each(valueUnwrapped, function(attr, val){

                  if( unit == 'percent' ){
                    newVal = val * 100
                    newVal = newVal + '%'
                  } else {
                    newVal = val
                  }
                  
                  setValue[attr] = newVal

                })
          
            /** Do normal attr binding */
            ko.bindingHandlers.attr.update(element, function () { return setValue; }, allBindings, viewModel, bindingContext);


            /** do triggers on elements for redraw */
            that.doTemplateTriggers( element, valueAccessor, allBindings )

            

            
              }

          }
      };

      

      /**
       * Class changing binding.
       */
      ko.bindingHandlers.plclass = {
          update: function(element, valueAccessor, allBindings) {

            
            var value   = valueAccessor();

              var val   = ko.unwrap( valueAccessor() )
                
              
              if( _.isObject( val ) || _.isArray( val ) ){

                $.each( val, function(){

                  if( that.isset( val ) ){

                    var set   = $(this)[0],
                      partial = set.partial   ||   'pl-control-', 
                      wlabel   = set.wlabel   ||   [], 
                      child   = set.child   ||  false, 
                      dflt    = set.dflt    || 'default', 
                      tag   = String(set.tag)


                    tag = ( tag == '' ) ? dflt : tag;


                    tag = tag.replace( partial, '')
                      
                    

                    if( child ){

                      $(element).find( child ).each( function(){

                        pl_remove_class_partial( $(this), partial, wlabel)

                        $(this).addClass( partial + tag )

                      })

                    } else {

                      pl_remove_class_partial( $(element), partial, wlabel)

                      $(element).addClass( partial + tag )

                    }

                     

                  }

                })

              } 

              else if( typeof val !== 'undefined' ){

                var partial = allBindings.get('partial')   || 'pl-control-',
                  wlabel  = allBindings.get('wlabel')   || [], 
                  dflt    = allBindings.get('dflt')     || 'default', 
                  val   = ( val == '' ) ? dflt : val,
                  classes = val.split(" "), 
                  child   = allBindings.get('child')     ||  false


                if( child ){

                  $(element).find( child ).each( function(){

                    var theChild = $(this)

                    pl_remove_class_partial( theChild, partial, wlabel )

                    $.each(classes, function(i, theClass){
                      theClass = theClass.replace( partial, '')
                      theChild.addClass( partial + theClass ) 
                    })
                    

                  })

                } else {

            
                  pl_remove_class_partial( $(element), partial, wlabel)

                  $.each(classes, function(i, theClass){
                    theClass = theClass.replace( partial, '')
                    $(element).addClass( partial + theClass ) 
                  })

                }
                    
                
                
                 

              }

              that.doTemplateTriggers( element, valueAccessor, allBindings )

        

          }
      };


      

      // ko.bindingHandlers.href = {
      //     update: function (element, valueAccessor) {
      //         ko.bindingHandlers.attr.update(element, function () {
      //             return { href: valueAccessor()}
      //         });
      //     }
      // };

      // ko.bindingHandlers.src = {
      //     update: function (element, valueAccessor) {
      //         ko.bindingHandlers.attr.update(element, function () {
                
      //             return { src: valueAccessor()}
      //         });
      //     }
      // };

      // ko.bindingHandlers.hidden = {
      //     update: function (element, valueAccessor) {
      //         var value = ko.utils.unwrapObservable(valueAccessor());
      //         ko.bindingHandlers.visible.update(element, function () { return !value; });
      //     }
      // };

      ko.bindingHandlers.instantValue = {
          init: function (element, valueAccessor, allBindings) {
              var newAllBindings = function(){
                  // for backwards compatibility w/ knockout  < 3.0
                  return ko.utils.extend(allBindings(), { valueUpdate: 'afterkeydown' });
              };
              newAllBindings.get = function(a){
                  return a === 'valueupdate' ? 'afterkeydown' : allBindings.get(a);
              };
              newAllBindings.has = function(a){
                  return a === 'valueupdate' || allBindings.has(a);
              };
              ko.bindingHandlers.value.init(element, valueAccessor, newAllBindings);
          },
          update: ko.bindingHandlers.value.update
      };

      ko.bindingHandlers.toggle = {
          init: function (element, valueAccessor) {
              var value = valueAccessor();
              ko.applyBindingsToNode(element, {
                  click: function () {
                      value(!value());
                  }
              });
          }
      };

      ko.bindingHandlers.toJSON = {
          update: function(element, valueAccessor){
              return ko.bindingHandlers.text.update(element,function(){
                  return ko.toJSON(valueAccessor(), null, 2);
              });
          }
      };

      ko.virtualElements.allowedBindings.stopBinding = true;
      ko.bindingHandlers.stopBinding = {
          init: function () {
              return { controlsDescendantBindings: true };
          }
      };      



    }
  }

}(window.jQuery);

 /* -------------------- */ 

!function ($) {

  // --> Initialize
  $(window).on('pl_page_ready', function() {

    $.plStandard.init()

    $.plStandard.handleSearchfield()

    $(document).trigger( 'plReady' )

    $(window).trigger('resize')

  })

  window.$pl = function() {

    return window.PLData
  
  }

  window.$plServer = function() {
  
    return $.plServer
  
  }


  window.plTop = function(){

    return window.parent.jQuery;
  }

  window.plAddNewLinks = function(){

    if( typeof plTop().plFrame != 'undefined' )
      plTop().plFrame.handleFrameLinks()
  }


  window.plTrigger = function( type ){

    var type = type || 'ready'

    if( type == 'ready' )
      $(window).trigger('pl_page_ready')

    else if ( type == 'change' )
      $('body').trigger('pl_page_change')

    /** Always trigger window resize */
    $(window).trigger( 'resize' )
  }

  


  /**
   * plReady action fires on page load and after every successive section load
   */
  $( 'body' ).on('pl_load_event', function( e, location ){

    var location = location || 'None'

    //console.log('Page Load Event. Trigger Location: ' + location)

    plAddNewLinks()

  })



  $.plScrolling = {

    scrollToSection: function( section ){

      var that = this

      if( section.length == 0 )
        return

      section.addClass('section-glow')



      $('body').animate({
            scrollTop: section.offset().top - 40
          },
          1000, 
          'swing', 
          function(){

            setTimeout( function(){
              section.removeClass('section-glow')
            }, 1000)
            
          });

    }, 

  }
  



  $.plStandard = {

    init: function(){
      var that = this

          
      plAdjustAdminBar()

    }, 


    handleSearchfield: function(){
      
    
      
      $('.searchfield').on('focus', function(){
        
        $(this).parent().parent().addClass('has-focus')
          
      }).on( 'blur', function(){
        
        $(this).parent().parent().removeClass('has-focus')
      
      })
      
      $('.pl-searcher').on('click touchstart', function(e){
        
        e.stopPropagation()
        
        var searchForm = $(this), 
            container  = searchForm.parent().parent()
        
        $(this).addClass('has-focus').parent().find( '.search-field' ).focus()

        container.addClass('showing-search')
        
        $('body').on('click touchstart', function(){
          searchForm.removeClass('has-focus')

          container.removeClass('showing-search')
          
        })
      })
      
    }

  }

}(window.jQuery);

 /* -------------------- */ 

!function ($) {


  /*
   * AJAX Actions
   */
  $.plServer = {

    run: function( opts ){
      
      var  that = this,
        config = {
            action:       'pl_server',               // tied to hook wp_ajax_pl_server
            hook:         '',                        // the PL hook to run. Ie: pl_server_ . $hook
            pageID:       PLData.config.pageID,       // ID for the current page
            typeID:       PLData.config.typeID,       // ID for current page type
            editID:       PLData.config.editID,       // ID currently being edited
            editslug:     PLData.config.editslug,       // ID for current page type
            tplMode:       PLData.config.tplMode,      // Data mode for page
            tplActive:     PLData.config.tplActive,    // Active Layout (if any)... 
            tplCapture:   PLData.config.tplCapture,    // Capture static template
            nonce:         PLData.config.nonce,
            beforeSend:   '',
            postSuccess:   '',
            args:         {}

        }

      /** merge args into config, overwriting config w/ opts */
      $.extend( config, opts )
      
      if( ! plIsset( config.hook ) ){

        console.log('No hook set for AJAX server request.');

        return false
      
      } else {

        return that.doAJAX( config )

      }

      
      

    },

    doAJAX: function( config ){

      var that = this

      /** Remove the callbacks from the data sent. Create a new object using extend */
      theData = $.extend({}, config)
      delete theData.beforeSend
      delete theData.postSuccess

      

      var args = {
          type:   'POST',         // POST request type
          url:   PLData.urls.ajaxURL,    // ajaxurl standard WP ajax URL
          data:   theData,         // the data we're sending
          beforeSend: function(){
              
            /** Call the beforeSend callback */      
            if ( $.isFunction( config.beforeSend ) )
              config.beforeSend.call( this )

          }, 
        
          success: function( response ){

            console.log(response)
            
            /** Turn response JSON string into JS object */      
            var rsp  = response
            
            console.log(rsp)

            /** Call the postSuccess callback */    
            if ( $.isFunction( config.postSuccess ) )
              config.postSuccess.call( this, rsp )

          },
          
          error: function( jqXHR, status, error ){
            
            console.log('------ AJAX Error ------')
            console.log( jqXHR )
            console.log( status )
            console.log( error )

          }
        }

      /** 
       * Do AJAX Request
       * NOTE that nested objects must be of consistent type, mixed numeric/associative objects cannot be passed.
       */
      return $.ajax( args )

    }


  }
}(window.jQuery);
