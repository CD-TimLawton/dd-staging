!function ($) {



  // --> Initialize
  $(document).ready(function() {


    $.plAdmin.init()


    $.plOptions.init()

    $.plCustomizer.init()

    $.plShortcodesEngine.init()

    $.plAdminNotifications.init()


  })

  $.plAdminNotifications = {

    init: function(){

      $( '.pl-notice.pl-is-dismissible' ).each( function() {

        var notice = $( this );
        
        if ( notice.find( '.pl-notice-dismiss').empty() ) {

          var btn = $( '<div class="pl-notice-dismiss"><i class="pl-icon pl-icon-remove"></i></div>' );

          notice.find('p').prepend( btn );    

        }

      });

      $( '.pl-notice-dismiss').click( function() {

        var notice = $( this ).closest('.pl-notice')

        var theData = {
                id:       notice.data('id'), 
                exp:      notice.data('exp'),
                action:  'pl_admin_notice'
            }

        notice.remove()

        var args = {
            type:   'POST',
            url:     PLAdmin.ajaxurl,
            
            data:   theData,
            success: function( response ){

              var rsp  = response

            }

          }


        $.ajax( args )


      });



    }

  }


  $.plAdmin = {

    init: function(){

      var that = this,
          pagebase      = window.location.pathname.match(/.*\/([^/]+)\.([^?]+)/i)
          queryString   = window.location.href,
          urlVars       = that.getUrlVars( queryString ),
          refresh       = false, // defualt is no refresh of localstorage
          refresh_array = [ 'plugins', 'themes', 'update', 'update-core' ],
          doc           = ( null !== pagebase ) ? pagebase[1] : 'unknown'

      // possibly clear local storage, we want to flush it when the following occurs:
      // 1. User clicks a refresh button
      // 2. User installs/deletes a plugin or theme
      // 3. User is on the updates page.

      // user click refresh
      if( urlVars.refresh == 1 ){
        refresh = true
        delete urlVars.refresh
      }
      
      // matched pages that need to clear localstorage
      if( $.inArray( doc, refresh_array ) !== -1 ) {
        refresh = true
      }
      
      if( true === refresh ) {
        localStorage.clear();
      }

      if( $('.pl-cards').length > 0 ){

        that.doCardSetup( $('.pl-cards') )

      }

    },

    doCardSetup: function( cards ){

      var that = this
      that.doCardFilterLink( window.location.href )

      $('body').delegate('.pl-filter-links a', 'click', function(e){

        e.preventDefault()

        var href = $(this).attr('href')

        window.history.pushState( "", "", href );

        that.doCardFilterLink( href )

      })
    },

    doCardFilterLink: function( queryString ){

      var that = this,
          hook = $('.pl-cards').data('hook') || 'extend'


      var urlVars = that.getUrlVars( queryString )

      that.cardRequest({
        hook:   hook,
        query:   queryString
      })


    },

    cardRequest: function( config ){

      var that         = this,
          queryString = config.query || window.location.href,
          urlVars     = that.getUrlVars( queryString )

      // Remove wp admin page var  (not needed)
      delete urlVars.page
      delete urlVars.tab

      if( urlVars.refresh == 1 ){
        that.cardCacheReset()
        delete urlVars.refresh
      }

      if( $.isEmptyObject(urlVars) && $('.pl-filter-links a').first().length > 0 ){

        queryString = $('.pl-filter-links a').first().attr('href')

        urlVars = that.getUrlVars( queryString )


      }

      that.doCardTitle( urlVars )



      var dflt = {
          queryVars:  urlVars,
          key:         queryString,
          beforeSend: function(){
            $('.pl-cards').html('<div class="pl-loading-banner"><div class="ic"><i class="pl-icon pl-icon-cog pl-icon-spin"></i></div><div>Loading</div></div>')
          },
          postSuccess: function(r){


            that.doProductCards(r.cards, config.key)

            that.cardCache( config.key, r.cards )
          }
        }

      config = $.extend(dflt, config)

      var cached = that.cardCache( config.key )

      if( cached ){
        that.doProductCards( cached, config.key )

      }

      else{

        that.request( config )
      }


    },



    request: function( config ){

      var that       = this,
          theData   = $.extend({ action: 'pl_platform' }, config )
      
      theData.nonce = PLAdmin.security
        
      delete theData.beforeSend
      delete theData.postSuccess

      var args = {
          type:   'POST',
          url:     PLAdmin.ajaxurl,
          data:   theData,
          beforeSend: function(){

            /** Call the beforeSend callback */
            if ( $.isFunction( config.beforeSend ) )
              config.beforeSend.call( this )

          },

          success: function( response ){

            var rsp  = response

            if ( $.isFunction( config.postSuccess ) )
              config.postSuccess.call( this, rsp )

          }

        }


      $.ajax( args )

    },

    cardCacheReset: function(){
      localStorage.setItem( PLAdmin.cachekey, JSON.stringify({}) )
    },

    cardCache: function( key, data ){

      var that   = this,
          data   = data || false

      /** Set the cache if data var is set */
      if( data !== false ){

        cardCache = JSON.parse( localStorage.getItem( PLAdmin.cachekey ) )


        cardCache = ( typeof(cardCache) !== "undefined" && cardCache !== null) ? cardCache : {}


        cardCache[ key ] = data

        cardCache.timestamp = new Date().getTime().toString()

        localStorage.setItem( PLAdmin.cachekey, JSON.stringify(cardCache) )



        return true

      }

      /** Read the cache */
      else {

        cardCache = JSON.parse( localStorage.getItem( PLAdmin.cachekey) ) || {};

        var dateString   = cardCache.timestamp,
            now         = new Date().getTime().toString(),
            diff         = (now - dateString) / (1000 * 60 * 60 ) // diff in hours

        /** Expire cache after 8 hours */

        return ( diff >= 8 || cardCache[ key ] == null ) ? false : cardCache[ key ]

      }


    },


    doCardTitle: function( urlVars ){

      var that   = this,
          title = 'PageLines Store'

      if( typeof urlVars.s != 'undefined' )
        title = 'Search: ' + urlVars.s

      else if( $('.'+urlVars.navitem).length > 0 ){
        title = $('.'+urlVars.navitem).attr('title')

      }

      $('.pl-filter-links a').removeClass('current')

      $('.'+urlVars.navitem).addClass('current')

      $('.pl-store-title').html(title)

    },



    doProductCards: function(data, queryString){

      var that          = this,
          out           = '',
          pagination    = '', 
          wrapCards     = true


      if( $.isEmptyObject(data) ){

        out += that.banner({
          img:       '',
          header:   'Nothing Found',
          subhead:   'Nothing was found for the selected query.',
          content:   ''
        })
      }

      else if( typeof data.html != 'undefined' ){

        wrapCards = false

        out += data.html
        
      }

      else if( typeof data.error != 'undefined' ){

        out += that.banner({
          img:       '',
          header:   'Error Loading Data',
          subhead:   'If the problem persists please contact PageLines support.',
          content:   sprintf( '<a class="button button-refresh" href="%s"><i class="pl-icon pl-icon-refresh"></i> Refresh User Data</a>', PLAdmin.refreshURL )
        })
        console.log( 'Error: ' + data.error )
        console.log( 'Description: ' + data.error_description )
      }

      else {


        var numPages     = data[0].total_pages || 1,
            pagination   = that.pagination(numPages, queryString)

        $.each(data, function(i, card){

          var thumb        = sprintf( '<a target="_blank" href="%s" class="pl-product-thumb"><img src="%s"></a>', card.product_link, card.thumb),
              desc         = sprintf( '<div class="desc column-description"><p>%s</p></div>', card.post_excerpt),
              name         = sprintf( '<div class="name column-name"><h4><a href="%s">%s</a></h4>%s</div>', card.product_link, card.post_title, that.getCardMetahead(card))
              action       = sprintf( '<div class="action-links">%s</div>', card.actionlink)
              tags         = sprintf( '<div class="card-tags pl-filter-links">Tags: %s</div>', that.getCardTags( card ) ),
              modified     = sprintf( '<div class="modified">Updated: %s ago</div>', card.modified )
              meta         = sprintf( '<div class="product-meta">%s %s %s</div>', that.getCardMeta(card), tags, modified),
              content      = sprintf( '<div class="card-content">%s %s %s</div>%s', name, desc, action, meta)
          out += sprintf('<div class="pl-product-card pl-col-sm-3"><div class="pl-product-card-pad">%s %s</div></div>', thumb, content )
        })

      }




      var wrap = ( wrapCards ) ? sprintf('<div class="pl-row">%s</div>%s', out, pagination ) : sprintf('%s', out)

      $('.pl-cards').html( wrap )

      $('.pl-product-card').each(function(i){

        var element = $(this)

        setTimeout(
          function(){
            element
              .addClass('animation-loaded hovered')

            setTimeout(function(){
              element.removeClass('hovered');
            }, 700);
          }
          , (i * 100)
        );
      })

      $('.pl-cards-nav, .pl-cards-sidebar').addClass('loaded')

    },

    pagination: function( numPages, queryString ){

      var that         = this,
          out         = '',
          currentPage = that.getQueryVar('getpaged') || 1

      for( var i = 1; i <= numPages; i++){

        var cls = ( currentPage == i ) ? 'current' : ''

        out += sprintf('<a class="%s" href="%s&getpaged=%s">%s</a>', cls, queryString, i, i)
      }

      return sprintf('<div class="pl-store-pagination">%s</div>', out)

    },

    getCardMetahead: function( card ){

      var that = this
          meta = []

      if( card.slug.indexOf("pl-framework") > -1 ){

        if( card.slug.indexOf("pl-framework-") > -1 ){
          meta.push( 'Framework Child Theme' )
        }

        else{
          meta.push( 'Framework Theme' )
        }
      }

      else if( card.slug.indexOf("pl-plugin") > -1 ){

        meta.push( 'PageLines Plugin' )

      }

      else if( card.slug.indexOf("pl-section") > -1 ){

        meta.push( 'Drag &amp; Drop Section' )

      }

      else {
        meta.push( 'WordPress Plugin' )
      }


      return sprintf('<div class="metabar">%s%s</div>', meta.join(', '), card.version_html )
    },

    getCardTags: function( card ){


      var that = this,
          href = $('.pl-cards-ui').data('baseurl'),
          meta = []

      $.each( card.tags, function(i, tg ){

        meta.push( sprintf('<a href="%s&product_tag=%s">%s</a>', href, tg.slug, tg.name) )

      })

      return meta.join('<span class="comma">, </span>')

    },

    getCardMeta: function( card ){

      var that = this,
          meta = []

        if( card.product_link )
          meta.push( sprintf( '<a target="_blank" href="%s" class="demo-btn">View</a>', card.product_link ) )

        if( card.reviews > 0 )
          meta.push( sprintf( '<span class="star-rating">%s <span class="num-ratings">(%s)</span></span>', that.getStarRating(card.rating), card.reviews) )

        if( card.demo )
          meta.push( sprintf( '<a target="_blank" href="%s" class="demo-btn">Demo</a>', card.demo ) )

        // if( card.sales > 0 )
        //   meta.push( sprintf( '<span class="num-downloads"> %s sales </span>', card.sales ) )

        if( card.download_count > 0 ) {
          var cnt = card.download_count
          var txt = ( cnt > 1 ) ? 'Downloads' : 'Download'
          meta.push( sprintf( '<span class="num-downloads"> %s %s </span>', card.download_count, txt ) )
        }
        
      return meta.join('<span class="divider">|</span>')
    },

    getStarRating: function( rating ){

      var starRating   = '',
          ratingNum   = Math.round( rating * 2 )/2,
          cls         = 'star'

      for (i = 1; i <= 5; i++) {

          if( ratingNum <= 0 )
            cls = 'star-o'
          else if( ratingNum < 1 )
            cls = 'star-half-full'

          starRating += sprintf( '<i class="pl-icon pl-icon-%s"></i>', cls )

          ratingNum--
      }

      return starRating

    },

    getActionLink: function(card){

      return '<a href="#" class="button button-primary">Link</a>'
    },

    banner: function( args ){

      var that   = this

      banner = sprintf('<div class="pl-col-sm-12"><div class="pl-platform-banner"><div class="pl-platform-banner-inner">%s<div class="banner-body"><div class="banner-body-pad"><h2 class="banner-header" >%s</h2><div class="banner-subheader">%s</div><div class="pl-platform-banner-content">%s</div></div></div><div class="clear"></div></div></div></div>', args.img, args.header, args.subhead, args.content)

      return banner
    },

    getQueryVar: function(theVar) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === theVar) {
                return sParameterName[1];
            }
        }
    },

    getUrlVars: function( url ){

      var that = this,
          vars = {},
          hash


      var hashes = url.slice( url.indexOf('?') + 1).split('&')

      for( var i = 0; i < hashes.length; i++){

        hash = hashes[i].split('=')

        vars[ hash[0] ] = hash[1]
      }

      return vars
    },

    isset: function( variable ){
        if(typeof(variable) !== "undefined" && variable !== null)
            return true
        else
            return false
    }
  }

  $.plCustomizer = {

    init: function(){
      var that = this

      if( ! $('body').hasClass('wp-customizer') )
        return

      that.scriptOptions()

      that.editUI()
    },

    editUI: function(){

      var that         = this,
          api         = wp.customize


      $(api).on('ready', function(){

        $('.customize-control-description')
          .addClass('pl-hide')
          .parent()
          .find('.customize-control-title')
            .append('<span class="pl-more-info">About <i class="pl-icon pl-icon-caret-down"></i><i class="pl-icon pl-icon-caret-up"></i></span>')



        $('.pl-more-info').on('click', function(e){

          e.stopPropagation() // inside label
          e.preventDefault() // inside label

          if( $(this).hasClass('show-info') ){

            $(this)
              .removeClass('show-info')
              .closest('label')
              .find('.customize-control-description')
                .addClass('pl-hide')


          }

          else{

            $(this)
              .addClass('show-info')
              .closest('label')
              .find('.customize-control-description')
                .removeClass('pl-hide')

          }

        })
      })




    },

    scriptOptions: function(){

      var that         = this,
          codemirrors = {},
          api         = wp.customize


      $(api).on('ready', function(){

        /**
         * Deal With Codemirror/ Script Options
         */

        $('.pl-code-editor').each( function(i){

          var cm_mode       = $(this).data('mode'),
              cm_config     = $.extend( {}, cm_base_config, { mode : cm_mode } )


          codemirrors[ 'item' + i ] = CodeMirror.fromTextArea($(this).get(0), cm_config)

          codemirrors[ 'item' + i ].el = $(this)

          $(this).parent().addClass('is-ready')

        })

        $( api ).on('expanded', function(){

          $.each( codemirrors, function(i, cm){
            cm.refresh()
          })
        })

        $.each( codemirrors, function(i, cm){

          cm.on('change', function(){

            var value = cm.getValue()

            cm.el.parent().find('.the-value').val(value).trigger('change')

          })

        })



      })



    },
  }







  /** Main Options Panel */
  $.plOptions = {

    init: function(){

      that = this


      /** We are on a PL options page */
      if( $('.pl-admin-settings').length > 0 )
        that.specialOptions()

    },




    specialOptions: function(){

      var that     = this,
          codemirrors = {}


      // Use WP image upload script
      that.imageUploaders()

      // color pickers, using WP colorpicker API
      $('.pl-colorpicker').wpColorPicker().addClass('is-ready')

      // use hidden inputs for checkboxes (0 or 1)
      $('.checkbox-input').on('change', function(){

        var checkToggle = $(this).prev()

        if( $(this).is(':checked') )
            checkToggle.val(1)
        else
            checkToggle.val(0)
      })


      if( $('.pl-settings-tabs').length > 0 ){

        /** Selected tab tracked with hidden input, added as data on wrapper */
        var selTabIndex = $('.pl-settings-tabs').data('selected')

        if( selTabIndex != '' && selTabIndex != 'default' ){
          var selTab = $( '#' + selTabIndex )
        }
        else
          var selTab = $('.pl-tab-panel').first()


        selTab.addClass('selected')


        $( sprintf('[href="#%s"]', selTab.attr('id') ) ).addClass('selected')

        $('.pl-settings-tabs').find('.pl-settings-nav a').on('click', function(e){

          e.preventDefault()

          $('.pl-tab-panel').removeClass('selected')

          $('.pl-settings-nav a').removeClass('selected')

          var panel = $(this).attr('href')

          $( panel ).addClass('selected')

          $(this).addClass('selected')

          if( $(panel).hasClass('tab-disabled') ){
            $(panel).find('.pl-opt').attr('disabled', true)
          }

          /** Refresh */
          $.each( codemirrors, function(i, cm){
            cm.refresh()
          })

          $('.selected_tab_input').val($( panel ).attr('id'))


        })


        $('.pl-settings-tabs').addClass('loaded')



        /** CodeMirror Syntax Highlighted Textareas */
        $('.pl-code-editor').each( function(i){

          var cm_mode     = $(this).data('mode'),
              cm_config     = $.extend( {}, cm_base_config, { mode : cm_mode } )


          codemirrors[ 'item' + i ] = CodeMirror.fromTextArea($(this).get(0), cm_config)

          $(this).parent().addClass('is-ready')

        })

      }

    },

    imageUploaders: function(){

        var custom_uploader

        $('.image_upload_button').on('click', function(e) {

          e.preventDefault()

          var button       = $(this),
              theOption   = button.closest('.image_uploader')
              mode         = button.data('mode'),
              handling    = button.data('handling')


          var upArgs = {
            multiple:   false,
            library: {
               type: mode //Only allow mode
             },
          }

          //Extend the wp.media object
          custom_uploader = wp.media.frames.file_frame = wp.media( upArgs );

          //When a file is selected, grab the URL and set it as the text field's value
          custom_uploader.on('select', function() {

            attachment = custom_uploader.state().get('selection').first().toJSON()

            if( handling == 'id' ){
              var setvalue = attachment.id
            } else {
              var setvalue = attachment.url
            }


            theOption
              .find('.upload_image_option')
              .val( setvalue )

            theOption
              .find('.the_preview_image')
              .attr('src', attachment.url)


          })

          //Open the uploader dialog
          custom_uploader.open()

        });

    }
  }


}(window.jQuery);


 /* -------------------- */ 

!function ($) {

$.plShortcodesEngine = {

    init: function(){

      var that = this



      that.bindUIActions(); 


    }, 

    bindUIActions: function(){

      var that = this 

      /** Set height on click because its super tall if not, and not working as documented */
      $('body').delegate('.pl-shortcode-tb', 'click', function(){
        setTimeout( function(){
          $('#TB_ajaxContent').css('height', 'auto')
        }, 30)
            
      })

      $('body').delegate('.plsc-show-list', 'click', function(){
        that.showList()
      })

      $('body').delegate('.btn-add-shortcode', 'click', function(){

        var theBox  = $(this).closest('#TB_window'),
            key      = $(this).data('key'),
            out     = ''

        that.showEngine()

        $('#TB_ajaxContent').css('height', 'auto')

        /** Install Sprintf extension */

        var data = PLAdmin.shortcodes[key]

        var settings = ''

        if( ! $.isEmptyObject( data.settings ) ){
          $.each( data.settings, function(i, o){

            o.key = i

            settings += that.engine(o) 

          })
        }

        else
          settings += '<div class="plsc-option">No settings for this shortcode.</div>'
        

        var backBtn = '<a href="#" class="button plsc-show-list"><i class="pl-icon pl-icon-chevron-left"></i> Back to list</a>'

        out += sprintf( '<div class="plsc-iframe-label" ><span><i class="pl-icon pl-icon-%s"></i> %s Shortcode Setup</span>%s</div>', data.icon, data.title, backBtn)

        out += sprintf( '<div class="plsc-settings" data-key="%s">%s</div>', key, settings)

        $('.plsc-options-container').html(out)

      })

      $('.plsc').on('click', '.plsc-insert', function(e) {

        e.preventDefault()

        var shortcode = that.parseShortcode()

        window.wp.media.editor.insert(shortcode);
      })

    },

    showEngine: function(){
      $('.plsc').addClass('show-engine')
    }, 

    showList: function(){
      $('.plsc').removeClass('show-engine')
    },

    parseShortcode: function(){

      var theShortcodeName = $('.plsc-settings').data('key')

      var theShortcodeAtts = ''

      var theShortcodeContent = ''

      $('.plsc-settings').find('.plsc-input').each( function(){

        var val = $(this).val(), 
            key = $(this).data('key')

        if( val != '' && key != 'content'){
          theShortcodeAtts += sprintf(' %s="%s"', key, val )
        }

        if( key == 'content' ){
          theShortcodeContent += val
        }


      })

      var sc =  sprintf('[%s%s]', theShortcodeName, theShortcodeAtts );

      if( '' != theShortcodeContent )
        sc += sprintf('%s[/%s]', theShortcodeContent, theShortcodeName)
     
      return sc

    }, 

    /** Parses the shortcode options array and creates options */
    engine: function( o ){

      var out = sprintf('<label class="plsc-option-label">%s</label>', o.label)

      var defaultContent = o.default || ''

      if( 'text' == o.type )  {

        // text option
        out += sprintf('<input type="text" class="plsc-input" data-key="%s" value="%s" placeholder="%s" />', o.key, defaultContent, o.place)

      }

      else if( 'textarea' == o.type ){

        out += sprintf('<textarea rows="4" cols="50" class="plsc-input" data-key="%s" placeholder="%s">%s</textarea>', o.key, o.place, defaultContent )
      }

      else if( 'select' == o.type || 'select_same' == o.type || 'count_select' == o.type || 'select_section' == o.type ){

        //First Item is default
        //var options = '<option value="">Select...</option>'
        var options = ''

        if( 'count_select' == o.type ){

          o.opts = {}
                    
          for( i = o.count_start; i <= o.count_end; i++){
            o.opts[i] = i;
          }
                    
        }

        if( 'select_section' == o.type ){

          o.opts = {}
                    
          $.each( PLAdmin.sections, function(i, s){
            o.opts[i] = i;
          })
                    
        }


        $.each( o.opts, function( key, name ){

          var val

          if( 'select_same' == o.type ){
            val = name
          }
          else 
            val = key

          options += sprintf('<option value="%s">%s</option>', val, name)
        })

        out += sprintf('<select class="plsc-input" data-key="%s">%s</select>', o.key, options )

      }

      else{

        out += sprintf('"%s" shortcode option type is missing.', o.type)
      }

      if( o.desc ){
        out += sprintf('<span class="plsc-option-description">%s</span>', o.desc)
      }

      return sprintf('<div class="plsc-option">%s</div>', out);

    }

}

}(window.jQuery);

 /* -------------------- */ 

/*! sprintf-js | Alexandru Marasteanu <hello@alexei.ro> (http://alexei.ro/) | BSD-3-Clause */

(function(window) {
    var re = {
        not_string: /[^s]/,
        number: /[dief]/,
        text: /^[^\x25]+/,
        modulo: /^\x25{2}/,
        placeholder: /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fiosuxX])/,
        key: /^([a-z_][a-z_\d]*)/i,
        key_access: /^\.([a-z_][a-z_\d]*)/i,
        index_access: /^\[(\d+)\]/,
        sign: /^[\+\-]/
    }

    function sprintf() {
        var key = arguments[0], cache = sprintf.cache
        if (!(cache[key] && cache.hasOwnProperty(key))) {
            cache[key] = sprintf.parse(key)
        }
        return sprintf.format.call(null, cache[key], arguments)
    }

    sprintf.format = function(parse_tree, argv) {
        var cursor = 1, tree_length = parse_tree.length, node_type = "", arg, output = [], i, k, match, pad, pad_character, pad_length, is_positive = true, sign = ""
        for (i = 0; i < tree_length; i++) {
            node_type = get_type(parse_tree[i])
            if (node_type === "string") {
                output[output.length] = parse_tree[i]
            }
            else if (node_type === "array") {
                match = parse_tree[i] // convenience purposes only
                if (match[2]) { // keyword argument
                    arg = argv[cursor]
                    for (k = 0; k < match[2].length; k++) {
                        if (!arg.hasOwnProperty(match[2][k])) {
                            throw new Error(sprintf("[sprintf] property '%s' does not exist", match[2][k]))
                        }
                        arg = arg[match[2][k]]
                    }
                }
                else if (match[1]) { // positional argument (explicit)
                    arg = argv[match[1]]
                }
                else { // positional argument (implicit)
                    arg = argv[cursor++]
                }

                if (get_type(arg) === "function") {
                    arg = arg()
                }

                if (re.not_string.test(match[8]) && (get_type(arg) !== "number" && isNaN(arg))) {
                    throw new TypeError(sprintf("[sprintf] expecting number but found %s", get_type(arg)))
                }

                if (re.number.test(match[8])) {
                    is_positive = arg >= 0
                }

                switch (match[8]) {
                    case "b":
                        arg = arg.toString(2)
                    break
                    case "c":
                        arg = String.fromCharCode(arg)
                    break
                    case "d":
                    case "i":
                        arg = parseInt(arg, 10)
                    break
                    case "e":
                        arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential()
                    break
                    case "f":
                        arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg)
                    break
                    case "o":
                        arg = arg.toString(8)
                    break
                    case "s":
                        arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg)
                    break
                    case "u":
                        arg = arg >>> 0
                    break
                    case "x":
                        arg = arg.toString(16)
                    break
                    case "X":
                        arg = arg.toString(16).toUpperCase()
                    break
                }
                if (re.number.test(match[8]) && (!is_positive || match[3])) {
                    sign = is_positive ? "+" : "-"
                    arg = arg.toString().replace(re.sign, "")
                }
                else {
                    sign = ""
                }
                pad_character = match[4] ? match[4] === "0" ? "0" : match[4].charAt(1) : " "
                pad_length = match[6] - (sign + arg).length
                pad = match[6] ? (pad_length > 0 ? str_repeat(pad_character, pad_length) : "") : ""
                output[output.length] = match[5] ? sign + arg + pad : (pad_character === "0" ? sign + pad + arg : pad + sign + arg)
            }
        }
        return output.join("")
    }

    sprintf.cache = {}

    sprintf.parse = function(fmt) {
        var _fmt = fmt, match = [], parse_tree = [], arg_names = 0
        while (_fmt) {
            if ((match = re.text.exec(_fmt)) !== null) {
                parse_tree[parse_tree.length] = match[0]
            }
            else if ((match = re.modulo.exec(_fmt)) !== null) {
                parse_tree[parse_tree.length] = "%"
            }
            else if ((match = re.placeholder.exec(_fmt)) !== null) {
                if (match[2]) {
                    arg_names |= 1
                    var field_list = [], replacement_field = match[2], field_match = []
                    if ((field_match = re.key.exec(replacement_field)) !== null) {
                        field_list[field_list.length] = field_match[1]
                        while ((replacement_field = replacement_field.substring(field_match[0].length)) !== "") {
                            if ((field_match = re.key_access.exec(replacement_field)) !== null) {
                                field_list[field_list.length] = field_match[1]
                            }
                            else if ((field_match = re.index_access.exec(replacement_field)) !== null) {
                                field_list[field_list.length] = field_match[1]
                            }
                            else {
                                throw new SyntaxError("[sprintf] failed to parse named argument key")
                            }
                        }
                    }
                    else {
                        throw new SyntaxError("[sprintf] failed to parse named argument key")
                    }
                    match[2] = field_list
                }
                else {
                    arg_names |= 2
                }
                if (arg_names === 3) {
                    throw new Error("[sprintf] mixing positional and named placeholders is not (yet) supported")
                }
                parse_tree[parse_tree.length] = match
            }
            else {
                throw new SyntaxError("[sprintf] unexpected placeholder")
            }
            _fmt = _fmt.substring(match[0].length)
        }
        return parse_tree
    }

    var vsprintf = function(fmt, argv, _argv) {
        _argv = (argv || []).slice(0)
        _argv.splice(0, 0, fmt)
        return sprintf.apply(null, _argv)
    }

    /**
     * helpers
     */
    function get_type(variable) {
        return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase()
    }

    function str_repeat(input, multiplier) {
        return new Array(multiplier + 1).join(input)
    }

    /**
     * export to either browser or node.js
     */
    if (typeof exports !== "undefined") {
        exports.sprintf = sprintf
        exports.vsprintf = vsprintf
    }
    else {
        window.sprintf = sprintf
        window.vsprintf = vsprintf

        if (typeof define === "function" && define.amd) {
            define(function() {
                return {
                    sprintf: sprintf,
                    vsprintf: vsprintf
                }
            })
        }
    }
})(typeof window === "undefined" ? this : window);
