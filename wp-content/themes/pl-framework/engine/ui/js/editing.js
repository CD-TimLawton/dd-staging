// PageLines Tools Initializer

!function ($) {
   



  $.plBuilder = {

    /**
     * Gets the list of sections available of a certain type.
     * @param  {string} type    type of area, full width area or content
     * @param  {object} section the section where the 'add' as clicked
     * @return {string} html output for placement in panel
     */
    init: function( clicked ){

      var that     = this,
          clicked = clicked || $('body'),
          uid     = clicked.data('clone') || false,
          config = {
            name:     plTranslate( 'page_builder' ),
            panels:   that.thePanels(), 
            key:       'builder',
            call: function(){
              that.bindListActions( uid )
            }
          }
          
      $.plEditing.sidebarEngine( config )

    }, 

    doSortables: function(){

      var that = this

      $('.dd-sort').pagesort({

        group:       'builder', 
        animation:     250,
        draggable:     '.dd-item',
        onAdd: function(){
          that.dragDropUpdate()
        }, 
        onUpdate: function(evt){
          that.dragDropUpdate()
        }, 
            
        onEnd: function(){
          $('.pl-builder-list').find('.dd-item').css('transform', '')
        }
      })


      $('.dd-sub-sort').pagesort({
        group:       'builder', 
        animation:     250,
        draggable:     '.dd-item'
      })

    
    }, 


    bindListActions: function( uid ){

      var that = this

      that.doSortables()
    
      if( uid ){

        var el = $('.pl-builder-list').find( sprintf( '[data-clone="%s"]', uid ) )

        el.addClass('item-highlight')

        setTimeout( function(){
          el.removeClass('item-highlight')
        }, 2000 )

      }

      $('.pl-builder-list').delegate('.dd-control', 'click', function(e){

        e.stopPropagation()

        var functionName = 'control_' + $(this).data('tool')

        if ( $.isFunction( that[ functionName ] ) ){
          that[ functionName ].call( that, $(this) )
      }

      })

      $('.dd-builder li.dd-item .dd-handle').each( function( e, i ) {
        $(i).on( 'dblclick', function(){
          btn = $(this)
          $.plBuilder.control_options(btn)
        })
      })
  
      $('.select-new-add-item').on( 'click', function(){

        var theItem   = $(this), 
            object     = theItem.data('class'), 
            loading   = theItem.data('loading'),
            UID       = plUniqueID(), 
            section   = that.getSection( object ),
            name       = section.name,
            hassub     = ( section.contain == 1 ) ? 'parent-item' : '', 
            sublist   = ( section.contain == 1 ) ? that.getListWrap() : '', 
            newItem   = that.getListElement({ object: object, clone: UID, name: name, parentCl: hassub, sublist: sublist})
          
        /** Add to page using GET and refresh */
        if( loading == 'refresh' ){

          $.plFrame.reloadFrame( { addSections: JSON.stringify( [ object ] ) } )

        } 

        /** Add to Page using AJAX */
        else{

          $jq().plAdd.newSection( object, UID, newItem )
        
        }
        
      })



    }, 

    getSection: function( object ){

      var that = this

      section   =  ( plIsset( PLWorkarea.factory[ object ] )) ? PLWorkarea.factory[ object ] : false

      return section

    }, 



    control_grid: function( btn ){

      var that     = this, 
        section   = btn.closest('.dd-item'), 
        object     = section.data('object'), 
        UID     = section.data('clone'),
        current   = that.getColumnSize( section ),
        currentOff   = that.getOffsetSize( section ), 
        action     = btn.data('action')


        if( action == 'increase' || action == 'decrease' ){

          section
            .removeClass( current[0] )

          if( action == 'decrease'){
            section.addClass( current[2] )
            $plModel().setSectionOption( UID, 'col', current[5] )
          }

          else if (action == 'increase'){
            section.addClass( current[1] )
            $plModel().setSectionOption( UID, 'col', current[4] )
          }

        }

        else if( action == 'offmore' || action == 'offless' ){

          section
            .removeClass( currentOff[0] )

      
          if (action == 'offless'){
            section.addClass( currentOff[2] )
            $plModel().setSectionOption( UID, 'offset', currentOff[5] )
          }

          else if (action == 'offmore'){
            section.addClass( currentOff[1] )
            $plModel().setSectionOption( UID, 'offset', currentOff[4] )
          }

        }
        

        

      $.plEditing.setNeedsSave()
    }, 

    control_dropdown: function( btn ){

      var that     = this, 
          section   = btn.closest('.dd-item'), 
          container   = section.parent(),
          controls   = ''

        

        if( section.find('.dd-dropdown').length > 0 ){

          section.removeClass('dd-show').find('.dd-dropdown').remove()
        
        }

        else {

          $('.dd-dropdown').remove() 
          $('.dd-show').removeClass('dd-show')
        
          /** If a missing section, dont create controls as they can error out the page */
          if( btn.attr('title').indexOf("Missing") == -1 ){
            
            controls +=  '<div class="dd-li"><span class="a">'
            controls += '<span class="dd-control dd-in" data-tool="grid" data-action="decrease"><i class="pl-icon pl-icon-caret-left"></i></span>'
            controls += '<span class="dd-control dd-in" data-tool="grid" data-action="increase"><i class="pl-icon pl-icon-caret-right"></i></span>'
            controls += sprintf('%s</span></div>', plTranslate( 'cols' ))

            controls +=  '<div class="dd-li"><span class="a">'
            controls += '<span class="dd-control dd-in" data-tool="grid" data-action="offless"><i class="pl-icon pl-icon-caret-left"></i></span>'
            controls += '<span class="dd-control dd-in" data-tool="grid" data-action="offmore"><i class="pl-icon pl-icon-caret-right"></i></span>'
            controls += sprintf('%s</span></div>',plTranslate( 'offset' ))

          
            controls += sprintf( '<div class="dd-li"><a class="dd-control" data-tool="options" ><i class="pl-icon pl-icon-pencil"></i> %s</a></div>', plTranslate( 'edit' ))
            controls += sprintf( '<div class="dd-li"><a class="dd-control" data-tool="show" ><i class="pl-icon pl-icon-eye"></i> %s </a></div>', plTranslate( 'show' ))
            controls +=  sprintf( '<div class="dd-li"><a class="dd-control" data-tool="clone" ><i class="pl-icon pl-icon-file-text"></i> %s</a></div>', plTranslate( 'clone' ))
          }
          
          controls += sprintf('<div class="dd-li"><a class="dd-control" data-tool="delete" ><i class="pl-icon pl-icon-remove"></i> %s</a></div>',plTranslate( 'delete' ))

          section
            .addClass('dd-show')
            .children('.dd-item-wrap')
            .children('.dd-title')
            .after( sprintf( '<div class="dd-dropdown">%s</div>', controls ) )

        }
        

        

      $('body').on( 'click.closeDDDropdown', function(){ 
        
        $('.dd-dropdown').remove()

        $('.dd-show').removeClass('dd-show')

        $(this).unbind( 'click.closeDDDropdown' )

      });
    
    }, 

    control_options: function( btn ){

      var clone   = btn.closest('.dd-item').data('clone'),
          section = $i( sprintf('[data-clone="%s"]', clone) ) 

      $( '.tool-active' ).removeClass( 'tool-active' )  

      $jq().plScrolling.scrollToSection( section )

      $.plEditing.loadSectionOptions( section )


    }, 

    control_delete: function( btn ){

      var that     = this, 
        dd         = btn.closest('.dd-item'),
        clone     = dd.data('clone')

        $.plEditing.deleteSection( btn, clone )

  
    },

    control_clone: function( btn ){

      var that      = this, 
          dd        = btn.closest('.dd-item'),
          clone     = dd.data('clone'),
          section   = $i( sprintf('[data-clone="%s"]', clone) ).first(),
          ddClone   = dd.clone(),
          elClone   = section.clone()

      elClone
        .insertAfter(section)
        .hide()
        .fadeIn()
      
      ddClone
        .insertAfter( dd )
        .hide()
        .fadeIn()

      /** Remove stuff by click action */
      $('body').click()

      that.updateCloneData( ddClone, elClone )

      that.updateTemplateMap()

      $.plEditing.reloadUI()

      $iWindow().plTrigger('ready')

    },

    /**
     * RECURSIVE 
     * Creates a new UID for element and recursively for sub elements. 
     * The data from these elements is added to master list object.
     */
    updateCloneData: function( ddClone, elClone ){

      var that        = this,
          oldUID       = ddClone.data('clone'),
          newUID       = plUniqueID(),
          theModel     = {}
      
      elClone
        .attr('data-clone', newUID)
        .data('clone', newUID)

      ddClone
        .attr('data-clone', newUID)
        .data('clone', newUID)



      if( plIsset( $pl().modelData[ oldUID ] ) ){

        console.log($pl().modelData[ oldUID ])


        /**
         * We have to remove all elClones inside template and foreach, or it will multiply
         */
        elClone.find('[data-bind]').each( function(){
          var binding = $(this).attr('data-bind')

          /** If foreach binding remove all but first */
          if ( binding.indexOf('foreach') >= 0 ){
            
            $(this).children("*:gt(0)").remove()

          } 

          /** If template binding remove all */
          else if ( binding.indexOf('template') >= 0 ){
            
            $(this).empty()
          }


        })

        
        /** Update data to match the view */
        $plModel().updateModelData()

        /** Get the cloned sections data */
        $pl().modelData[ newUID ] = $.extend( {}, $pl().modelData[ oldUID ] )

        /** Bind data to new section */
        $plBinding().bindNewSection( newUID, $pl().modelData[ newUID ] )


        

      }

      else{
        console.log('model data not set')
      }

      console.log('go here')

      /** Recursion */
      ddClone.find("[data-clone]").each(function(){

        var ddNext     = $(this),
            UIDNext    = $(this).data('clone'), 
            elNext     = elClone.find( sprintf( '[data-clone="%s"]', UIDNext ) )


        console.log(UIDNext)
        console.log(elNext)
        
        that.updateCloneData( ddNext, elNext )
        
      })
      


      

    },


    control_show: function( btn ){


      var clone   = btn.closest('.dd-item').data('clone'),
        section = $i( sprintf('[data-clone="%s"]', clone) ) 

      $jq().plScrolling.scrollToSection( section )

      
    }, 



    /**
     * Gets the amound of column offsets from the classname
     */
    getOffsetSize: function( column, defaultValue ) {

      var that   = this,
        max   = 10,
        min   = 0,
        sizes   = that.getColumnSize( column ),
        avail   = 12 - sizes[3],
        data   = [], 
        offset   = 'pl-col-sm-offset-'

      for( i = min; i <= max; i++){

          next = ( i === avail ) ? min : i+1

          prev = ( i <= min ) ? avail : i-1

          

          if( column.hasClass( offset + i ) ){
            data = new Array( offset + i,  offset +next,  offset + prev, i, next, prev )
          }

      }

      if(data.length === 0 || defaultValue)
        return new Array( offset + "0",  offset + "0",  offset + "0", '0 Off', i)
      else
        return data

    }, 


    /** 
     * Gets the amount of grid columns for the section
     */
    getColumnSize: function(column, defaultValue) {

      var that   = this,
        max   = 12,
        min   = 3,
        data   = [], 
        col   = 'pl-col-sm-'

      for( i = min; i <= max; i++){

          next = ( i === max ) ? min : i+1

          prev = ( i === min ) ? max : i-1

          if(column.hasClass(col + i))
            data = new Array( col + i, col + next, col + prev, i, next, prev )

      }

      if(data.length === 0 || defaultValue)
        return new Array( col + "12", col + "1",  col + "11", "12 Cols", 12)
      else
        return data

      

    }, 


    updateTemplateMap: function(){

      var that = this

      $pl().config.tplMap = that.getLevelMap($('.pl-builder-list'), -1)

      $.plEditing.setNeedsSave()

      $('.pl-builder-list').find('.dd-item').css('transform', '')
    }, 


    dragDropUpdate: function(){

  
      var that   = this

      that.updateTemplateMap()

      that.reDrawPage( $pl().config.tplMap, -1, $i('body'))

    }, 

    reDrawPage: function( map, level, container ){

      var that = this
      
      $.each( map , function( index, section ){

        
        var theElement     = $i( sprintf('[data-clone="%s"]', section.clone) ), 
            oldLevel       = theElement.data('level'),
            nextLevel     = level + 1


          
        if( level != -1 ){



          /** Set to new level */
          theElement.data('level', level).attr('data-level', level)

          /** Move the section */
          theElement
            .detach()
            .appendTo( container )

          
        }


        if( !_.isEmpty( section.content ) ){

          var nextContainer  = theElement.find('[data-contains-level]').first()

          if( !_.isEmpty( nextContainer ) )
            that.reDrawPage( section.content, nextLevel, nextContainer )

        }
  

      })

    }, 



    getLevelMap: function( container, level ){

      var that   = this, 
        level   = level || 0, 
        map   = {}


        
      container.children('.dd-item-wrap').children('.itemset').children('.dd-list').each( function( theListIndex ){ 

        var theList   = $(this),
            listMap   = {}




        $(this).children('.dd-item').each( function( listItemIndex ){ 

          var theListItem   = $(this), 
            clone       = $(this).data('clone'),
            index       = $(this).data('index') || listItemIndex



          theListItem.children('.dd-item-wrap').children('.itemset').children('.dd-list').attr( 'data-level', level + 1 ).data('level', level + 1)

          
          listMap[ index ] = {
            clone:     theListItem.data('clone'), 
            object:   theListItem.data('object'),
            content:   that.getLevelMap( theListItem, level + 1 )
          }

        })
  
        if( theList.hasClass('no-list') ){
          map = $pl().config.tplMap[ theList.data('region') ].content
        }
        else{
          map = listMap
        }

      })




      return map
    }, 

    opt_type_add_sections: function(){

      var that   = this,
        addList = that.builderAddList()
  

      return sprintf( '%s', addList )


    }, 



    opt_type_builder: function(){

      var that   = this, 
        list   = '', 
        tabs   = ''

        list = that.builderList( $pl().config.tplMap, -1)




      return sprintf( '<div class="dd-builder"><div class="pl-builder-list dd-list-container"><div class="dd-item-wrap">%s</div></div></div>', list )

    }, 

    opt_type_scope: function(){

      var that   = this,
          out   = ''

      var scopeOptions = {} 

      scopeOptions.type = sprintf('%s: "%s"', plTranslate( 'all_of_type'), $pl().config.typename)

      if( $pl().config.pageID != $pl().config.typeID )
        scopeOptions.local = sprintf('%s: "%s"', plTranslate( 'current_page_only' ), $pl().config.currentPageName)

      if( $pl().config.termID != $pl().config.pageID )
        scopeOptions.term = sprintf( '%s: "%s"', plTranslate('taxonomy_archive' ), $pl().config.currentTaxonomy )

      out = $.engineOpts.selectOption( scopeOptions, $pl().config.tplMode, 'tpl_scope')

      return out

    }, 

    opt_type_paste_sections: function(){

      var that    = this,
          out     = ''

      out += sprintf('<div class="paste-section"><input id="spaste" class="paste-section-data pl-form-control" type="text" placeholder="Paste section data here..." /><span class="pl-btn pl-btn-primary pl-btn-xs submit-paste-section"><i class="pl-icon pl-icon-plus"></i> Add Section</span></div>')

     $.engineOpts.optScripts.paste_section = function(){

        $('.submit-paste-section').on('click', function(){

          var pasteContainer  = $(this).parent(), 
              pasteInput      = pasteContainer.find('.paste-section-data')

          $.plFrame.reloadFrame( { loadMap: encodeURIComponent( pasteInput.val() ) } )

        })
        

      }

      

      return out 

    }, 

    builderList: function( map, level, region ){

      var that     = this,
        level     = level || 0,
        title     = ( level == 0 ) ? sprintf('<div class="filter-title item-toggle">%s</div>', region) : '',
        list       = '', 
        region     = region || '', 
        sort       = '', 
        listWrap   = '', 
        staticTpl = '',
        render     = $pl().config.tplRender,
        capture   = $pl().config.tplCapture, 
        dynamic   = true, 
        doList     = true, 
        nameAft   = '',
        select

      

      level = parseInt( level )

      dynamic = ( level === 0 && ! plIsset( render[ region ] ) ) ? false : true 

      
      if( ! dynamic ){

        doList = false


        
        if( region == 'template' ){

          doList = true

        }

        staticTpl = sprintf('<div class="pl-alert pl-alert-workarea"><strong class="subtle">%s %s.</strong></div>', plTranslate( 'using_a_theme'), region) 

      }

      

      if( doList ) {
        

        $.each( map, function( i, item ){

          var object        = item.object,
              clone         = item.clone,
              content       = ( plIsset( item.content) ) ? item.content : [],
              section       = ( plIsset( PLWorkarea.factory[ object ] )) ? PLWorkarea.factory[ object ] : false,
              name          = ( section ) ? sectionNameFromObject( object ) : sprintf('(Missing: %s)', item.object),
              cName         = $plModel().getSectionOption( clone, 'custom_name' ) || false,
              name          = ( cName ) ? sprintf( '%s %s', cName, name ) : name,
              canContain    = ( ( section && section.contain == 1 ) || level == -1 ) ? true : false,
              sublist       = ( canContain ) ? that.builderList( content, level + 1, i ) : '', 
              hassub        = ( canContain ) ? 'parent-item' : '', 
              col           = $plModel().getSectionOption( clone, 'col' ),
              offset        = $plModel().getSectionOption( clone, 'offset' ), 
              hideOn        = $plModel().getSectionOption( clone, 'hide_on' ), 
              colClass      = ( plIsset( col ) && col != '') ? col : '12',
              offClass      = ( plIsset( offset )  && offset != '' ) ? offset : '0'

          /** If hidden on page */
          if( hideOn ){

            var hideClass = ''

            $.each(hideOn.split(','), function( i, PID ){

              if( PID == $pl().config.pageID ){
                hideClass = 'hide-on-page'
              }
            
            })

          }

          if( level == -1 ){

            list += sprintf('<div class="dd-item" data-clone="%s" data-object="%s" data-index="%s"><div class="dd-item-wrap">%s</div></div>', clone, object, object, sublist )
          } 

          else {

            var config = {
                col:         colClass, 
                offset:     offClass, 
                hide:       hideClass,
                parentCl:   hassub, 
                clone:       clone, 
                object:     object, 
                name:       name, 
                sublist:     sublist
            }

            list += that.getListElement( config )


          }
          

        })

      } 
      
    

      var listWrap = that.getListWrap({
          title:       title,
          region:     region, 
          level:       level, 
          list:       list, 
          staticTpl:   staticTpl, 
          doList:     doList
      })


      return listWrap

    }, 

    getListWrap: function( config ){

      var that     = this, 
        classes    = '',
        defaults   = {
          title:     '',
          region:   '', 
          level:     '1', 
          list:      '', 
          staticTpl:   '', 
          doList:     true
        }

      config = $.extend( defaults, config )

      region = (config.region != '') ? sprintf('data-region="%s"', config.region) : ''

      if( config.level == 0 )
        classes += 'dd-sort' 
      else if( config.level > 0 )
        classes += 'dd-sub-sort'

      if( ! config.doList )
        classes += ' no-list'

      return sprintf('<div class="itemset">%s<ol class="item-contents dd-list fix %s" %s data-level="%s">%s</ol>%s</div>', config.title, classes, region, config.level, config.list, config.staticTpl)

    }, 

    getListElement: function( config ){

      var that     = this, 
          defaults   = {
            col:       '12', 
            offset:   '0', 
            parentCl:   '', 
            clone:      '', 
            object:   '', 
            name:     '', 
            sublist:   '', 
            hide:       ''
          }

      config = $.extend( defaults, config )

      if( config.hide != '' ){
        sName = config.name + ' ' + plTranslate( '(Hidden)' )
      }
      else{
        sName = config.name
      }

      var reorder   = $.plEditing.reorderIcon(), 
          title     = sprintf( '<div class="dd-el dd-name dd-control" title="%s Section" data-tool="dropdown">%s <i class="pl-icon pl-icon-caret-down"></i></div>', config.name, sName ) 


      return sprintf( '<li class="dd-item pl-col-sm-%s pl-col-sm-offset-%s %s %s" data-clone="%s" data-object="%s" ><div class="dd-item-wrap"><div class="dd-title dd-handle clearfix">%s %s</div>%s</div></li>', config.col, config.offset, config.parentCl, config.hide, config.clone, config.object, title, reorder, config.sublist )


    }, 

    
    sectionFilters: function(){

      var filters = {
            basic:        plTranslate( 'basic' ), 
            content:      plTranslate( 'content_formats' ), 
            layout:       plTranslate( 'layout_containers' ), 
            component:    plTranslate( 'components' ),
            nav:          plTranslate( 'navigation_menus' ),
            carousel:     plTranslate( 'carousel' ), 
            gallery:      plTranslate( 'gallery' ), 
            slider:       plTranslate( 'sliders_features' ), 
            wordpress:    'WordPress', 
            localsocial:  plTranslate( 'social_local' ), 
            widgetized:   plTranslate( 'widgets_sidebar' ), 
            advanced:     plTranslate( 'advanced' )
          }

      return filters
    
    }, 


    builderAddList: function( ){

      var that     = this, 
          out     = '', 
          panels   = ''


      $.each( that.sectionFilters(), function( tag, title ){

        var theSections =  that.getFilterSections( tag )

        if( theSections != '' )
          panels += sprintf( '<div class="itemset"><div class="filter-title item-toggle">%s</div> %s</div>', title, that.getFilterSections( tag ) );

        

      })

      var additional = sprintf('<div class="dd-additional"><a class="pl-btn pl-btn-default" href="%s"><i class="pl-icon pl-icon-plug"></i> &nbsp; %s</a></div>', PLWorkarea.extendURL, plTranslate( 'download_new_sections'))


      return sprintf('<div id="tab-add" class="pl-builder-add dd-list-container">%s%s</div>', additional, panels)

    }, 

    getFilterSections: function( tag ){

      var that   = this, 
          out   = ''
        
      $.each( PLWorkarea.factory, function(){

        var section     = $(this)[0], 
            txtfilter   = (section.filter == '') ? 'advanced' : section.filter,
            filter       = txtfilter.split(','), 
            hasFilter   = $.inArray( tag, filter ) > -1      


        if( hasFilter ){

          var refreshIcon = ( 'refresh' == section.loading ) ? sprintf('<span class="sicons"><i class="pl-icon pl-icon-refresh"></i></span>') : ''

          out += sprintf('<li title="%s" class="dd-item select-new-add-item fix" data-class="%s" data-loading="%s">%s<div class="list-icon" style="background-image: url(%s)"></div><div class="list-content"><div class="title">%s</div></div></li>', section.desc, section.class, section.loading, refreshIcon, section.icon, section.name )
        }
        
        

      })


      return ( out != '' ) ? sprintf( '<ul class="item-contents list-grid">%s</ul>', out ) : ''

    }, 





    thePanels: function(){

      var that   = this,
        panels = {
          scope: {
            title:  plTranslate( 'scope' ),
            opts:   [
              {
                type:     'scope',
                callback:   that
              }
            ]

          }, 
          builder:  {
            title:     plTranslate( 'page_layout' ), 
            format:   'full',  
            opts:   [
              {
                type:     'builder',
                callback:   that
              }
            ]
          },
          add:  {
            title:     plTranslate( 'add_sections_to_page' ), 
            format:   'full',  
            opts:   [
              {
                type:     'add_sections',
                callback:   that
              }
            ]
          },
          paste:  {
            title:     plTranslate( 'section_copy_paste' ), 
            opts:   [
              {
                type:     'paste_sections',
                callback:   that
              }
            ]
          },

        }
      return panels
    }
  }
}(window.jQuery);


 /* -------------------- */ 

// PageLines Tools Initializer

!function ($) {

  // --> Initialize
  $(document).ready(function() {
  
    $.plEditing.init()

    $.plEditing.bindIFrame()
  })




  /**
   * The master controller class for editing tools
   * Controls Sidebar, tool activation, standard UI bindings and loading
   * 
   */
  $.plEditing = {

    /**
     * Initialize editing on parent document ready
     */
    init: function(){

      var that = this

      that.setupWPAdminBar()

      /** Attach UI bindings */
      that.bindUIActions()

      /**  An attribute that gets an array of functions to run when a tool is closed. */
      that.closeScripts = {};


    }, 

    bindIFrame: function(){

      var that = this,
          tool = 'plBuilder'

      $('iframe.site-frame').on("load", function () {
        
        that.startUI()

        /** Get outta my house with the admin bar in the frame */
        if( $i('#wpadminbar').length > 0 ){
          $i('#wpadminbar').hide()
          $i('html').css('cssText', 'margin-top: 0px !important;')
        }


        if( $('.tool-active').first().length > 0 && ! true == $('.pl-workarea-sidebar').data('persist') ){

          that.editingAction( $('.tool-active').first().attr('rel') )

        } 
        else if( getURLParameter('start') == 'yes' ){

        

          if( getURLParameter('pl_tool') ){
            tool = getURLParameter('pl_tool')
          }

          
          that.editingAction( tool, false, $( sprintf('[rel="%s"]', tool ) ).first() )

        }

      
      
      })

    },

    /** 
     * Bind UI Interactions and events
     */
    bindUIActions: function(){

      var that = this,
          tool = 'plBuilder'

      that.doToolbarBinding()

    }, 

    setupWPAdminBar: function(){

      var that = this


      $('.pl-ab-save > a').addClass('toolbar-save').attr('data-action', 'save').data('action', 'save')

  
    },

    doToolbarBinding: function(){

      var that = this


      $('.pl-ab-item > a').on('click', function(e){

        e.preventDefault();

        $('body').click()

        var clicked = $(this)

        that.clickTool( clicked )
        
      })

    }, 

    startUI: function(){

      var that = this




      /** Marker Class */
      $i('body')
        .addClass('pldd-active')

      /** Add section edit buttons */
      $i('.pl-sn').each( function(){

        $(this)
          .children('.pl-sn-wrap')
          .prepend( that.getSectionToolbar( $(this) ) ) 
        
      })

      /** Bind section edit buttons */
      $i('.pldd-control')
        .on( 'click.pldd', function(e){

          e.preventDefault();
          e.stopPropagation();
          
          $i('body').click()

          that.clickTool( $(this) );
          
        })



    }, 

    reloadUI: function(  ){

      var that = this


      that.closeTools()
      that.shutdownEditing()

      that.startUI()

      

    }, 

    /** 
     * Calls a function based on the tool being loaded and activated
     */
    clickTool: function( clicked ){

      var that   = this

      that.editingAction( clicked.attr('rel'), clicked.data('action'), clicked )

    },

    editingAction: function( tool, action, clicked ){

      var that           = this, 
          action         = action   || false, 
          tool           = tool     || false, 
          clicked       = clicked  || false,
          prefixTool     = 'toolbar_', 
          prefixAction  = 'action_', 
          activeClass   = 'tool-active'
        
        if( action ){

          that.callFunction( prefixAction + action, clicked )

        }

        if( tool ){

          $('.'+activeClass).removeClass( activeClass )

          $( sprintf('[rel="%s"]', tool ) ).addClass( activeClass )

          
          that.callFunction( 'init', clicked, tool )

        }

    }, 

    callFunction: function( functionName, clicked, object ){

      var that           = this
      
      var functionObj = ( plIsset(object) ) ? $[ object ] : that


      if ( plIsset(functionObj) && $.isFunction( functionObj[ functionName ] ) ){
        functionObj[ functionName ].call( functionObj, clicked )
      }

      else 
        console.log('PageLines: ' + object + ':' + functionName + ' does not exist.')

    }, 

    



    shutdownEditing: function( ){


      /** Remove editing classes on body */
      $i( 'body' )
        .removeClass('pldd-active pldd-editing')

      /** Remove added in page items */
      $i( "[class*=pldd-]" ).remove()
      

    },

    

    /**
     * Uses array set by the tools and iterates through performing the close operations
     * @return {action} runs close actions
     */
    closeTools: function( ){

      $.each( this.closeScripts, function(index, callback){

        if ( $.isFunction( callback ) )
          callback.call( this )

      })

      this.closeScripts = {};

    },

    



    /** SIDEBAR  ********************************************************/

    /**
     * Load the editor sidebar with HTML content and other configuration 
     * @param  {object} config sidebar configuration
     * @return {action} Draws and shows sidebar
     */
    loadSidebar: function( config ){

      var defaults = {
            title:     '', 
            header:   '',
            sub:       '', 
            cont:     '', 
            key:       '', 
            persist:   0
          }

      config = $.extend( defaults, config ) 
      
      var that     = this, 
          sub     = ( config.sub    !== '' ) ? sprintf('<div class="sb-sub">%s</div>', config.sub) : '',
          title   = ( config.title !== '' ) ? sprintf('<div class="sb-title"><div class="the-title">%s</div>%s</div>', config.title, sub) : '', 
          header   = ( config.header !== '' ) ? sprintf('<div class="sb-header"><div class="the-header fix">%s</div></div>', config.header) : '', 
          content = header + title + config.cont

      $('body').addClass('pl-has-sidebar')

      $('.pl-workarea-sidebar')
        .html( content )
        .data('key', config.key )
        .attr('data-key', config.key )
        .data('persist', config.persist )
        .attr('data-persist', config.persist )

      PLWorkarea.iframe.contents().bind( 'click.sidebarClose', that.closeSidebar);

    },

    itemToggles: function(){

      

       /** Add toggle icons */
      $('.item-toggle')
        .not('.loaded')
        .each( function(){

          var theItem = $(this).parent()

          $(this).append(' <i class="pl-icon pl-icon-caret-down"></i><i class="pl-icon pl-icon-angle-up"></i>')

          var key   = $(this).text(),
              state = plGetTabState( key )

          

          if( state == 'open' ){
              theItem
                .addClass('item-open')
                .removeClass('item-closed')
          } 

          else if( state == 'closed' ){

            theItem
              .addClass('item-closed')
              .removeClass('item-open')

          }

        })
        


      

       /** Accordion style panel titles */
      $('.item-toggle')
        .not('.loaded')
        .on('click', function(){
        
          var theItem = $(this).parent(),
              key     = $(this).text()

          if( theItem.hasClass('item-closed') ){

            theItem
              .addClass('item-open')
              .removeClass('item-closed')

            plSetTabState( key, 'open' )
          
          } 

          else {
            theItem
              .addClass('item-closed')
              .removeClass('item-open')

            plSetTabState( key, 'closed' )
          }
          
        }).addClass('loaded')

    }, 

    /**
     * Bind standard sidebar UX elements after render.
     */
    bindSidebar: function(){

      var that = this


       /** Reference on click */
       $('.btn-ref').on('click.ref', function(){

        var theRef   = $(this).parent()
        ,  help   = $(this).next()

        if( theRef.hasClass('ref-open') ){
          theRef.removeClass('ref-open')
          help.slideUp()
        } else {
          theRef.addClass('ref-open')
          help.slideDown()
        }

      })


     /** Standard Dropdown Handling */
     $('.pl-dropdown-toggle').on('click', function(){

       var theDropdown = $(this).parent().find('.pl-dropdown-menu')

       if( theDropdown.hasClass('show') ){
         theDropdown.removeClass('show')
       }
       else{
         theDropdown.addClass('show')
       }

     })

      that.itemToggles()

       


      $('.pl-tools-list').on( 'click', '.tools-bar', function(){

        var theItem = $(this).parent()

        if( ! theItem.hasClass('item-open') ){

          $('.pl-tools-list li')
            .removeClass('item-open')

          $('.pl-tools-list').find('.tools-panel').hide()


          theItem
            .addClass('item-open')

          theItem.find('.tools-panel').fadeIn()
        
        } 

        else {

          theItem.removeClass('item-open')

          $('.pl-tools-list').find('.tools-panel').hide()

        }
        
        

      })

      /** Generalized Action Select */
      $('.select-action').on('change', function(){


        var functionName   = 'action_' + $(this).data('action')
        
        if ( $.isFunction( that[ functionName ] ) )
          that[ functionName ].call( that, $(this) )
        else
          console.log('Action not found: '+functionName)

        /** Set back to default option */
        $(this).val('')

        return false;
      })

      /** Generalized Action Select */
      $('.tool-action').on('click', function(){

        var action       = $(this).data('action'),
          functionName   = 'toolbar_' + action

        /** Activate proper highlights */
        $('.tool-active').removeClass('tool-active')

        $( sprintf('[data-action="%s"]', action) ).addClass('tool-active')

        
        if ( $.isFunction( that[ functionName ] ) )
          that[ functionName ].call( that, $(this) )
        else
          console.log('Action not found: '+functionName)

  
      })

      


    },   

    /** Closes sidebar from view. */
    closeSidebar: function( e ){

      var that   = this, 
        e     = e || false
        target  = (e) ? e.target : false

      /** We check the target to make sure its not an element that shouldn't close sidebar */
      if( ! target || $(target).closest('.dropdown').length == 0 ){

        PLWorkarea.iframe.contents().unbind('click.sidebarClose');

        /** Remove the active class from Workarea nav item, disabling sidebar always turns off */
        $('.tool-active').removeClass('tool-active')


        $i('.editing-section').removeClass('editing-section')
        
        $('body').removeClass('pl-has-sidebar')

      } 
      
    },

    sidebarEngine: function( config ){

      var that     = this, 
          HTML     = '', 
          cnt     = 1, 
          panels   = config.panels, 
          call     = config.call


      var defaults = {
            header: '', 
            closed: 'none'
          }

      config = $.extend( defaults, config )

      var sbConfig = {
            title:       sprintf( '<i class="pl-icon pl-icon-pencil"></i> %s', config.name),
            header:     config.header,
            cont:       sprintf( '<div class="workarea-opts-form" data-level="%s"><div class="pl-loader"><i class="pl-icon pl-icon-spin pl-icon-cog"></i></div></div>', config.level, 'HTML' ),
            key:         config.key,
            persist:    config.persist
          };

      $.plEditing.loadSidebar( sbConfig )
      
      setTimeout(function(){

        /** Initialize the option engine, only do this once */
        $.engineOpts.init()


        /** Get HTML for all options related to section */
        HTML += sprintf("<div class='panel-accordion'>");

        $.each( panels , function( index, panel ) {

          

          var closeClass   = ( (config.closed == "all" && cnt !== 1) ) ? 'item-closed' : '', 
              format       = panel.format || 'options'

          HTML += sprintf( '<div class="opt-panel itemset %s"><div class="panel-title item-toggle">%s</div>', closeClass, panel.title );

          if( typeof panel.opts == 'undefined' || $.isEmptyObject( panel.opts ) )
            panel.opts = [{ label: plTranslate('no_custom_options'), help: plTranslate('no_custom_options_added') , type: "help" }];
        
          HTML += sprintf('<div class="panel-opts item-contents format-%s">%s</div></div>', format, $.toolEngine.runEngine( panel.opts ) );

          cnt++

        })

        HTML += sprintf("</div>");


        $('.workarea-opts-form').html( HTML )


        if( $.isFunction( call ) ){
          call.call( that )
        }

        /** Run options JS */
        $.engineOpts.runScripts()

        /** Bind standard sidebar UX elements */
        that.bindSidebar()

      }, 150)
      

    }, 

    
    /**
     * Show notifications from PHP actions
     */
    // loadNotifications: function(){

    //   var that = this

    //   if( !_.isEmpty( PLWorkarea.notifications )){

    //     $.each( PLWorkarea.notifications, function(i, item){
        
    //       that.showNotification( item )

    //     } )

    //   }

      

    // }, 

    /** TOOLS  ********************************************************/
    


    action_dropdown: function( btn ){

      var that     = this, 
        container   = btn.parent()

      that.removeLogoDrop()

      if( ! container.hasClass('show-drop') )
        container.addClass('show-drop')
      else{
        btn.removeClass('tool-active')
        container.removeClass('show-drop')
      }

      PLWorkarea.iframe.contents().on('click.logoDropClose', function(){
        that.removeLogoDrop()
      })

      

      $('body').on( 'click.logoDropClose', function(e){

        var e     = e || false
          target  = (e) ? e.target : false

        if( ! target || $(target).closest( '.logo-dropdown' ).length == 0 )
          that.removeLogoDrop()

      });

    },

    removeLogoDrop: function(){

      $('.show-drop')
        .removeClass('show-drop')
        .find('.tool-active')
          .removeClass('tool-active')

      $('body').unbind('click.logoDropClose');

      PLWorkarea.iframe.contents().unbind('click.logoDropClose');

    }, 


    /**
     * Save data from page. 
     * @return {action} saves page map configuration and section data.
     */
    action_save: function(){

      var that     = this

      that.savePage()
        

    },

    savePage: function( config ){

      var that         = this, 
          config       = config || {},
          modelJSON   = $plModel().getJSON(),
          mapData     = $pl().config.tplMap

      
      config = $.extend( { map: mapData, model: modelJSON }, config ) 

      /** Allow plugins to add data to be saved through adding that data to the extraData variable. */
      config = $.extend( $pl().extraData, config ) 

      that.savePageConfig( config );

    }, 

    /**
     * Saves the pages data based on a map/model
     * @param  {array} map   the map of the page sections
     * @param  {array} model the page data model from knockout
     */
    savePageConfig: function( config ){

      var that = this, 
        args = {
          hook:   'save_page',
          postSuccess: function( rsp ){

            that.setNeedsSave( 'no' )            

            $.plEditing.showNotification('saved!')

          },
          beforeSend: function( ){

            that.setNeedsSave( 'active' )

          }
        }

      config = $.extend( args, config ) 

      $plServer().run( config )

    }, 

    /**
     * Sets save button state when save is required, active or not...
     */
    setNeedsSave: function( state ){

      var state = state || 'yes' 

      /** Doesn't need save */
      if ( state == 'no' ){

        window.needsSave = false 

        /** Close save notification */
        $('.toolbar-save')
          .removeClass('needs-save')
          .html('<i class="pl-icon pl-icon-ok"></i> Page Saved')
          .parent()
            .removeClass('show-save')

        $("body").css("cursor", "default");

        $iWindow().onbeforeunload = null;

      }

      /** Currently saving */
      else if ( state == 'active' ){

        $('.toolbar-save')
          .html('<i class="pl-icon pl-icon-cog pl-icon-spin"></i> Saving Changes')
          .parent()
            .addClass('show-save')

        $("body").css("cursor", "progress");

      }

      /** Needs saving. */
      else {

        window.needsSave = true 

        $('.toolbar-save')
          .addClass('needs-save')
          .html(sprintf( '<i class="pl-icon pl-icon-upload"></i> %s', plTranslate( 'save_changes' ) ) )
          .parent()
            .addClass('show-save')

        $iWindow().onbeforeunload = function() { 
          
          return sprintf('Save your changes! %sYou will lose your unsaved edits if you continue.', "\n"); 
        }

      } 
      

    }, 

    /**
     * Does the page need a save?
     */
    needsSave: function(){

      if( $('.toolbar-save').hasClass('needs-save') )
        return true; 
      else
        return false;

    }, 

    /**
     * Shows a temporary notification in the workarea header
     * @param  {html} html the string/html for the notification
     * @param  {int} time the time in ms it should show for...
     */
    showNotification: function( html, time){

      var time = time || 1000

      $('.toolbar-notification')
        .html( html )
        .addClass('active')

      setTimeout(function() {
          $('.toolbar-notification')
            .removeClass('active')
      }, time);


    }, 



    /**
     * Activate templates controls
     */
    toolbar_layouts: function( clicked ){
      
      var that = this

      $.plTemplates.init( clicked )

    },

    /**
     * Activate template builder controls
     */
    toolbar_builder: function( clicked ){
      
      var that = this

      $.plBuilder.init( clicked )

    },


    

    

    toolbar_code: function( clicked ){

      var that   = this


      $.plCode.init( clicked )
    },


    toolbar_static: function( ){
      
      var that     = this, 
        template  = $i('.static-template')

      $('body').click()
      
      var config = {
          UID:     template.data('clone'),
          theClass:   'template', 
          level:     0
      }

    
      $.toolEngine.render( config )
    
    },




    

    getSectionToolbar: function( section ){

      var that  = this, set, type, name
      var clone = section.data('clone')
      var title = false
      var cName = $plModel().getSectionOption( clone, 'custom_name' ) || false
      name  = name || sectionNameFromObject( section.data('object') )
    
      name = ( cName ) ? sprintf( '%s %s', cName, name ) : name
   
      return sprintf('<div class="pldd-section-bar pldd-bar pl-border"><div class="pldd-bar-tools pldd-section-tools"><a class="bar-control pldd-control" data-action="options"><i class="pl-icon pl-icon-pencil"></i>%s</a></div></div>', name )
    },

    
    
    action_options: function( btn ){
      
      
      var that     = this, 
          section = btn.closest(".pl-sn")

  
      that.loadSectionOptions( section )
      
    },

    loadSectionOptions: function( section, config ){

      var config = config || {}

      $i('.editing-section').removeClass('editing-section')

      section.addClass('editing-section')

      header = sprintf('<div class="tool-action" data-action="builder_show"><i class="pl-icon pl-icon-caret-left"></i> &nbsp;%s</div>', plTranslate( 'show_in_builder' ) )
      header += sprintf('<div class="tool-action send-right" data-action="delete"><i class="pl-icon pl-icon-remove"></i> %s</div>', plTranslate( 'delete'))

      mode = ( section.hasClass('pl-sn-static-content') ) ? 'static' : 'standard';
      
      var configEngine = {
            UID:         section.data('clone'),
            theClass:   section.data('object'), 
            level:       section.data('level'), 
            header:     header, 
            mode:       mode
      }

      config = $.extend( {}, config, configEngine )

      
      $.toolEngine.render( config )

    }, 

    
    action_tpl_scope: function( select ){

      var that     = this, 
          selText   = select.find('option:selected').text(), 
          selVal     = select.val()

      $.plFrame.reloadFrame( { tplScope: selVal } )
    }, 

    

    action_tpl_capture: function( select ){

      var that     = this, 
        selText   = select.find('option:selected').text(), 
        selVal     = select.val()
  
      $.plFrame.reloadFrame( { tplCapture: selVal } )

      //$.plEditing.setNeedsSave()

    }, 


    reorderIcon: function(){
      return '<i class="pl-icon pl-icon-reorder dd-reorder"></i>'
    },

    deleteSection: function( clicked, clone, callback ){

      var that       = this, 
          section   = $i( sprintf('[data-clone="%s"]', clone) ), 
          listitem   = $('.pl-builder-list').find( sprintf( '[data-clone="%s"]', clone ) )

      plConfirm( clicked, {
        subhead: plTranslate( 'remove_from_page' ), 
        callback: function(){

        //  $.plEditing.sectionDelete( section ) // recursive function

          $pl().config.tplMap = that.parseMap( function( set ){

            

            if( clone == set.meta.clone || $.inArray( clone, set.path ) !== -1 ){


              if( plIsset( $pl().modelData[ set.meta.clone ] ) )
                delete $pl().modelData[ set.meta.clone ]

              delete set.map[ set.index ]

            }

            return set.map

          })

          $.plEditing.setNeedsSave()


          $('.pl-builder-list').find( sprintf( '[data-clone="%s"]', clone ) ).slideUp('slow', function(){
            $(this).remove()
          })

          section.slideUp( 'slow', function(){
            $(this).remove()
          })

          if( $.isFunction( callback ) ){
      
            callback.call( that, clicked, section )
          }
        } 
      })

    },

    toolbar_builder_show: function( clicked ){

      var that       = this, 
          clone     = clicked.closest('.pl-workarea-sidebar').data('key'),
          section   = $i( sprintf('[data-clone="%s"]', clone) ) 

          clicked.data( 'clone', clone )

          $jq().plScrolling.scrollToSection( section )

          that.editingAction('plBuilder', false, clicked )

    }, 

    toolbar_delete: function( clicked ){

      var that       = this, 
          clone     = clicked.closest('.pl-workarea-sidebar').data('key')

          that.deleteSection( clicked, clone, function(){ that.toolbar_builder( ) })


    }, 


  
    parseMap: function( callback, path, level, map ){

      var that       = this,
          map       = map || $pl().config.tplMap,
          path       = path || [], 
          level     = ( plIsset( level ) ) ? level : -1, 
          nextLevel = level + 1

        $.each( map, function( index, meta ){

          // This is a weird function. It stays linked to the variable, 
          // so we have to slice it e.g. duplicate the array and send that to the call back.
          path.splice( nextLevel, 100, meta.clone)
          
          var currentPath = path.slice()

          

          map[ index ].content = that.parseMap( callback, path, nextLevel, meta.content )
        
  

          if( $.isFunction( callback ) ){
      
            var set = {
              map:     map, 
              index:   index, 
              meta:   meta, 
              path:   currentPath, 
              level:   level
            }
            map = callback.call( that, set )
          }

        })

        return map

    },


  }





}(window.jQuery);


 /* -------------------- */ 

 !function ($) {

  $.toolEngine = {

    defaults: {
      theClass:  '', 
      UID:     '', 
    }, 

    render: function( config ) {

      var that = this

      that.config   = $.extend({}, that.defaults, config)

      that.UID       = that.config.UID

      that.level     = that.config.level

      that.factory  = PLWorkarea.factory

      that.object   = that.config.theClass

      that.name      = that.factory[ that.object ].name


      that.master   = that.createMasterOptionsArray( config.mode )

      that.drawSectionOptions( config )

      //that.inputListener()

    }, 

    createMasterOptionsArray: function( mode ){

      var that   = this,
        master   = {}

      if( mode == 'static' ){

        var sectionOptions = {
              sections: {
                title:   'Content Settings', 
                opts:   $pl().config.templateOpts
              }
            }
      }

      else {

        var sectionOptions = {
              sections: {
                title:   'Configuration', 
                opts:   that.factory[ that.config.theClass ].opts
              }
            }

        

      }

      master = $.extend({}, sectionOptions, $.plStandardSettings.settingsArray())
      
      return master;

    }, 

      
    drawSectionOptions: function( config ) {

      var that   = this

      var configSidebar = {
            name:     that.name, 
            panels:   that.master, 
            key:       that.UID, 
            level:     that.level, 
            header:   '', 
            closed:   'all',
            call: function(){
              that.inputListener()
            }
          }

      config = $.extend( configSidebar, config )

      $.plEditing.sidebarEngine( config )

    }, 

    runEngine: function( opts ){

    

      var that = this,
        HTML = ''

      $.each( opts , function(index, o) {
        HTML += that.optEngine( o )
      })
      
      return HTML
    

    }, 

    sanitizeValue: function( val ){

      var that = this

      if( _.isObject(val) || _.isArray(val) ){

        $.each(val, function( i, v ){
          val[i] = that.sanitizeValue( v )
        })
      }

      else {
        val = pl_html_input( val )
      }

      return val 
    }, 

    optGetValue: function( key ){

      var that     = this,
          pageData   = $plModel().getData(),
          value     = ''

  
      if( pageData[ that.UID ] && pageData[ that.UID ][ key ] ){
        
        value = that.sanitizeValue( pageData[ that.UID ][ key ] )

      }
      

      return value

    }, 

    optAddMeta: function( o, option_meta ) {

      var that           = this,
          option_meta   = option_meta || {}
      
      /** If we are in an accordion, handle accordingly using option_meta */
      if( typeof option_meta.key !== 'undefined' ){
        
        o.ind     = option_meta.ind
        o.value   =  ( option_meta.vals[ o.key ] != '' ) ? option_meta.vals[ o.key ] : ''
        key       = sprintf('%s__%s__%s', option_meta.key, option_meta.ind, o.key)

      }

      else{
        key = o.key
      }

      var meta = {
          place:       '',
          classes:     '',
          name:       key, 
          value:       that.optGetValue( key ), 
          inputID:     key, 
          callback:   that, 
          label:       ''
      }
      
      return $.extend( meta, o );

    },

    

    optEngine: function( o, option_meta ) {

      var that           = this,
          out           = '',
          option_meta   = option_meta || {}, 


    
      o = that.optAddMeta( o, option_meta )

      if( 
        !_.isEmpty( o.opts ) && 
        o.type != 'accordion' && 
        o.type.indexOf("select") < 0 && 
        o.type.indexOf("radio") < 0 && 
        o.type != 'dragger' 
      ){
      
        $.each( o.opts , function( index, osub ) {

          out += that.optEngine( osub, option_meta ) // recursive

        })
      }

      if( 
        typeof o.type == 'undefined' || 
        o.type == 'multi' || 
        o.type == 'get_posts' 
      ){
      
        // do nothing.

      }else if( o.type === 'accordion' ){

        

        out += $.engineOpts.accordion( o );



      }

      else if( o.type === 'text' || o.type === 'text_small' ){
        
        out += $.engineOpts.textoption( o );

      }

      else if( o.type === 'richtext'){
        
        out += $.engineOpts.richtext( o );

      }

      else if( o.type === 'textarea' || o.type === 'html' ){

        out += $.engineOpts.textarea( o );

      }

      else if( o.type === 'check' ){

        out += $.engineOpts.checkoption( o );

      }

      else if( o.type === 'image_upload' ){

        out += $.engineOpts.imagedropzone( o );

      }

      else if( o.type === 'color' ){

        out += $.engineOpts.coloroption( o );

      }

      else if( o.type === 'media_select_video' ){

        out += $.engineOpts.videooption( o );

      }

      else if( o.type === 'edit_post' ){

        out += sprintf('<a href="%s" class="pl-btn pl-btn-primary pl-btn-xs %s" >%s</a>', PLWorkarea.editPost, o.classes, o.label )

      }

      else if ( o.type === 'link' ){

        var target = o.target || '_blank'

        out += sprintf('<div><a href="%s" class="pl-btn pl-btn-default pl-btn-xs %s" target="%s">%s</a></div>', o.url, o.classes, target, o.label )
      }

      else if ( o.type === 'button_link' ){

        out += $.engineOpts.buttonLink( o );
        
        
      }

      else if (
        o.type === 'select' ||
        o.type === 'count_select' ||
        o.type === 'count_select_same' ||
        o.type === 'select_pixels' ||
        o.type === 'select_vw' ||
        o.type === 'select_percent' ||
        o.type === 'select_proportion' ||
        o.type === 'select_same' ||
        o.type === 'select_taxonomy' ||
        o.type === 'select_term' ||
        o.type === 'select_wp_tax' ||
        o.type === 'select_icon' ||
        o.type === 'select_animation' ||
        o.type === 'select_multi' ||
        o.type === 'select_button' ||
        o.type === 'select_button_size' ||
        o.type === 'select_theme' ||
        o.type === 'select_sidebar' ||
        o.type === 'select_padding' ||
        o.type === 'select_imagesizes' ||
        o.type === 'select_menu'
      ){

        out += $.engineOpts.selectoption( o );

      }

      else if (
        o.type === 'radio'
      ){

        out += $.engineOpts.radioOption( o );

      }

      else if (
        o.type === 'dragger'
      ){

        out += $.engineOpts.dragInput( o, option_meta );

      }

      // Adds the help attr as text only, nothing else  
      else if( o.type === 'help'  || o.type === 'help_important' ){

        out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )
        
        
      }



      // Hook type 
      else if(  $.isFunction( o.callback[ 'opt_type_' + o.type ] ) ){

        out += o.callback[ 'opt_type_' + o.type ].call( o.callback, o )
      
      }

      else {
        out = sprintf('<div class="pl-alert pl-alert-warning">Could not find option type: <strong>%s</strong></div>', o.type); 
      }

      var help     = ( typeof o.help     !== 'undefined' )   ? sprintf( '<div class="help-block %s">%s</div>', o.type, o.help)  : '',
          title   = ( typeof o.title     !== 'undefined' )   ? sprintf( '<div class="opt-title item-toggle">%s</div>',   o.title) : '',
          guide   = ( typeof o.guide     !== 'undefined' )   ? sprintf( '<div class="opt-guide">%s</div>',   o.guide) : '',
          ref     = ( typeof o.ref       !== 'undefined' )   ? that.doReference( plTranslate('reference'), o.ref ) : '', 
          stylize = ( typeof o.stylize   !== 'undefined' )   ? o.stylize : '', 
          toggle   = ( typeof o.toggle   !== 'undefined' )   ? 'item-' + o.toggle : ''


      return sprintf('<div class="pl-form-group itemset %s type-%s %s">%s%s<div class="item-contents">%s</div>%s%s</div>', stylize, o.type, toggle, title, guide, out, help, ref);

    },

    doReference: function( title, text ){


      return sprintf( '<div class="pl-dropdown ref-opt"><span class="pl-btn pl-btn-success pl-btn-xs pl-dropdown-toggle" >%s <i class="pl-icon pl-icon-angle-down"></i></span><div class="pl-dropdown-menu dd-ref">%s</div></div>', title, text )
    },

    resetListenerBinding: function(){

      var that = this

      $('.lstn').off('keyup.optlstn blur.optlstn change.optlstn paste.optlstn')

      that.inputListener()
    },

    inputListener: function(){

      $pl().modifyEvent = false

      $('.lstn').on('keyup.optlstn blur.optlstn change.optlstn paste.optlstn', function( e ){
      
        var UID           = $('.pl-workarea-sidebar').data('key'),
            theInput       = $(this),
            theInputType   = theInput.getInputType(),
            theInputID     = theInput.attr('name'),
            theValue       = theInput.val()


        $.plEditing.setNeedsSave( 'yes' )

        console.log( theInput.val() )

        /** Certain actions should only be triggered on more occasional change events, as opposed to keyups, etc.. */
        if( e.type === 'blur' || ( e.type === 'change' && ( theInputType === 'checkbox' || theInputType === 'select' || theInputType === 'radio' || theInputType === 'hidden' ) ) ){
          $pl().changeEvent = true; 
          changeEvent = true
        }

        else {
          $pl().changeEvent = false; 
          changeEvent = false
        }

        modifyEvent = true
        $pl().modifyEvent = true;

        // if has __ then we are in an accordion
        if( theInputID.indexOf("__") >= 0 ){

          var arr         = theInputID.split("__"), 
              accordionID = arr[0], 
              suboptionID = arr[2], 
              indexID     = arr[1] //theInput.closest('.opt-group').data('num')


          $pl().viewModel[ UID ][ accordionID ]()[ indexID ][ suboptionID ]( theValue )

        }

        /** We are doing a regular option type */
        else{

          
          if( ! plIsset( $pl().viewModel[ UID ][ theInputID ] ) ){

            console.log('*** Unset Model Value: ' + theInputID)

            $plModel().setNewObservable(UID, theInputID, theValue)
            
          }



          /** Set change event flag on observable so we can fire some actions only occasionally */
          
          /** This must be set BEFORE the value or there is a one cycle delay */
          $pl().viewModel[ UID ][ theInputID ].changeEvent = changeEvent

          $pl().viewModel[ UID ][ theInputID ].modifyEvent = modifyEvent

          $pl().viewModel[ UID ][ theInputID ]( theValue )    

        

        }    

        /** Allows elements to recompute and animate */
        if( changeEvent ){
          $iWindow().plTrigger( 'change' )
        }

      })

    }, 
 

     
    

  }



}(window.jQuery);


 /* -------------------- */ 

!function ($) {

  /** Set Squire richtext editor global variable */
  window.squire = {}
  window.lastClicked = false

  /** The Options ENGINE */
  $.engineOpts = {

    init: function(){

      // Run these on load and again after anything is regenerated.
      this.optScripts = {};

    

    },

    /**
     * Run scripts from options
     */
    runScripts: function(){

      
      $.each( this.optScripts, function(index, optEvent){

        if ( $.isFunction( optEvent ) ){
          optEvent.call( this )
        }

      })

    },

    specialOption: function( config ){

      var that = this,
        defaults = {
          title:     '', 
          message:   '', 
          val:     '', 
          valLabel:   'Current:',
          option:   ''
        }


      config = $.extend( defaults, config )  

      //var value = (config.val !== '') ? sprintf('<div class="special-option-value"><label>%s</label><div class="val">%s</div></div>', config.valLabel, config.val ) : ''

      out = sprintf('<div class="alert-help pl-form-group"><div class="itemset item-closed alert-content"><div class="item-toggle"><strong class="alert-title">%s</strong></div><div class="item-contents">%s</div></div> %s</div>', config.title, config.message, config.option )

      return out
    }, 

    selectOption: function( options, value, action, dtext ){

        var dtext     = dtext || 'Select...',
            opts      = sprintf( '<option value="">%s</option>', dtext ),
            disabled   = ( Object.keys(options).length <= 1 ) ? 'disabled' : ''

        $.each( options, function( val, name ){

            var selected = ( val == value ) ? 'selected' : ''

            opts += sprintf('<option value="%s" %s>%s</option>', val, selected, name)
        })

        return sprintf('<select class="pl-form-control select-action" data-action="%s" %s>%s</select>', action, disabled, opts)

  
        
    },

    videooption: function( o ){


      var that   = this, 
        out   = '', 
        o2     = $.toolEngine.optAddMeta( {key: o.key+'_2'} );

      out += '<div class="video-upload-inputs option-group">'
        
      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )

      out += sprintf( '<div class="video-inputs clearfix">' )
          
      out +=  that.addVideoOption( o, "Select MP4 Video")

      out += sprintf( '</div>' )
      

      out += '</div>'

      return out;

      

    }, 

    addVideoOption: function( o, label){
      
      var out = ''
      
      out += '<div class="upload-box media-select-video">'
      
      out += sprintf('<label for="%s">%s</label>', o.inputID, label )

      out += sprintf('<input id="%1$s" name="%2$s" type="text" class="lstn pl-form-control upload-input" placeholder="" value="%3$s" />', o.inputID, o.name, o.value )
      
      out += '<a class="pl-btn pl-btn-xs pl-btn-primary pl-load-media-lib" data-mimetype="video"><i class="pl-icon pl-icon-edit"></i> Select</a> '

      out += sprintf(' <a class="pl-btn pl-btn-default pl-btn-xs" href="%s" target="_blank"><i class="pl-icon pl-icon-upload"></i> Upload</a> <div class="pl-btn pl-btn-default pl-btn-xs pl-image-remove"><i class="pl-icon pl-icon-remove"></i></div>', PLWorkarea.addMediaURL)
      
      out += '</div>'
      
      return out
    }, 

    textoption: function( o ){

      var out = '';

      out += sprintf( '<label for="%s">%s</label>', o.inputID, o.label );
      out += sprintf( '<input type="text" id="%s" name="%s" class="%s lstn pl-form-control" placeholder="%s" value="%s" />', o.inputID, o.name, o.classes, o.place, o.value, o.inputID);

      return out;
    }, 

    textarea: function( o ){

      var out     = '', 
        special   = ( o.type == 'html' ) ? 'html-textarea' : '' 

      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )
      out += sprintf('<textarea id="%s" name="%s" class="%s lstn pl-form-control %s" placeholder="%s">%s</textarea>', o.inputID, o.name, o.classes, special, o.place, o.value )

      this.optScripts.textarea = function(){
        
        
      }
    
      return out;
    }, 

    richtext: function( o ){

      var out = '';



      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )

      out += sprintf('<div class="richtext-controls noselect" data-id="%s">', o.inputID)
        out += '<span class="ctrl" data-action="bold" data-tag="b"><i class="pl-icon pl-icon-bold"></i></span>'
        out += '<span class="ctrl" data-action="italic" data-tag="i"><i class="pl-icon pl-icon-italic"></i></span>'
        out += '<span class="ctrl" data-action="underline" data-tag="u"><i class="pl-icon pl-icon-underline"></i></span>'
        out += '<span class="ctrl" data-action="align" data-tag="left"><i class="pl-icon pl-icon-align-left"></i></span>'
        out += '<span class="ctrl" data-action="align" data-tag="center"><i class="pl-icon pl-icon-align-center"></i></span>'
        out += '<span class="ctrl" data-action="align" data-tag="right"><i class="pl-icon pl-icon-align-right"></i></span>'

        out += '<span class="pl-dropdown">'
          out += '<span class="ctrl pl-dropdown-toggle"><i class="pl-icon pl-icon-chevron-up"></i></span>'
          out += '<div class="pl-dropdown-menu" >'
            out += '<span class="ctrl" data-action="format" data-tag="h1">H1</span> <span class="ctrl" data-action="format" data-tag="h2">H2</span> <span class="ctrl" data-action="format" data-tag="h3">H3</span><span class="ctrl" data-action="format" data-tag="h4">H4</span><span class="ctrl" data-action="format" data-tag="p">P</span>'
        out += '</div></span>'
      out += '</div>'

      /** Richtext loads in an iframe, on load we get a top level variable editor[ inputID ] to use for API  */
      out += sprintf('<iframe class="richtext-frame noselect" src="%s/plugins/squire/document.html" data-inputid="%s"></iframe>', PLWorkarea.PLUI, o.inputID);  

      out += sprintf('<textarea id="%s" name="%s" class="%s richtext-textarea lstn pl-form-control"  style="">%s</textarea>', o.inputID, o.name, o.classes, o.value )

      out += '<div class="richtext-toggle pl-btn pl-btn-default pl-btn-xs">Toggle <span class="sel-richtext">Rich Text</span><span class="sel-rawtext">Raw Text</span></div>'

        
      this.optScripts.richtext = function(){

        $('.richtext-frame').on('load', function(){

          var inputID   = $(this).data('inputid'), 
            textInput   = $(this).next()

          

          top.squire[ inputID ] = this.contentWindow.editor

          top.squire[ inputID ].setHTML(textInput.val())

          $(top.squire[ inputID ]).on('input', function(){

            textInput.val(  top.squire[ inputID ].getHTML() ).trigger('change')
          })

          textInput.on('keyup', function(){

            top.squire[ inputID ].setHTML( $(this).val() )

          })


          /** 
           * Dont trigger blur event if they are clicking one of the richtext controls
           * There is no standard way to do this so we set a global and check if they click the controls 
           */

          $('.richtext-controls').on('click', function(e) {
            
            lastClicked = true
          });

          $(top.squire[ inputID ]).on('blur', function(e){

            setTimeout(function(){


              if( ! lastClicked ){
                textInput.trigger('blur')
                

              }

              lastClicked = false
              
              
             }, 200)
    
          })

        })

        // $('.pl-dropdown-toggle').on('click', function(){

        //   var theDropdown = $(this).parent().find('.pl-dropdown-menu')

        //   if( theDropdown.hasClass('show') ){
        //     theDropdown.removeClass('show')
        //   }
        //   else{
        //     theDropdown.addClass('show')
        //   }

        // })
        
        /**
         * Toggle editor from rich to raw
         */
        $('.richtext-toggle').on('click', function() {
          var textarea = $(this).parent().find('.richtext-textarea')
          var controls = $(this).parent().find('.richtext-controls')
          var frame    = $(this).parent().find('.richtext-frame')
          var sel_rich = $(this).find('.sel-richtext')
          var sel_raw  = $(this).find('.sel-rawtext')
                    
          $([textarea, controls, frame, sel_raw, sel_rich ]).each( function(){
            $(this).toggle()
          })
        })
        
        $('.richtext-controls [data-action]').on('click', function(e){

      
          var optID   = $(this).closest('.richtext-controls').data('id'),
            id     = $(this).data('action'),
            tag   = $(this).data('tag') || false,
            editor   = squire[ optID ],
              value


          if( id == 'format' ){

            if ( id && editor ) {

              editor.changeFormat({
                  tag: 'SPAN',
                  attributes: {
                      'data-type'  : 'size',
                      'class'    : tag
                  }
              }, {
                  tag: 'SPAN',
                  attributes: { 'data-type': 'size' }
              })

              //editor.setFontSize( tag )

              

            } 
          } 

          else {

            if( id == 'align' ){
              id     = 'setTextAlignment'
              value   = tag
            }  


            else if(tag && editor.hasFormat( tag ))
              id = 'remove' + capitalizeFirstLetter( id )



            if ( id && editor && editor[ id ] ) {

              editor[ id ]( value );
            }


          }

          
          

        })

      }

      return out;
    }, 

    
    dragInput: function( o, option_meta ){


      var that   = this,
          out   = '', 
          unit   = o.unit || 'px',
          scale = o.scale || 1

      /**
       * The basic handling of drag inputs
       */
      out += sprintf( '<label class="label-block" for="%s">%s <a class="dragger-reset">reset</a></label>', o.inputID, o.label );

      out += sprintf( '<div class="form-inline dragger-option">');

      _.each( o.opts, function( field ) {


        field = $.toolEngine.optAddMeta( field, option_meta )

        var max   = field.max   || 1000,
            min   = field.min   || 0,
            def   = field.def   || 0
          
        
        unit   = ( plIsset( field.unit) ) ? field.unit : unit

        
        out += sprintf( '<div class="pl-input-group"  ><div class="pl-input-group-addon"><i class="pl-icon pl-icon-%s"></i></div><input id="%s" name="%s" type="text" class="pl-form-control dragger lstn" placeholder="" value="%s" data-max="%s" data-min="%s" data-default="%s" data-scale="%s"><span class="pl-input-group-addon">%s</span></div>',  field.icon, field.inputID, field.inputID, field.value, max, min, def, scale, unit);

      })

      
      this.optScripts.dragInput = function(){
        

        $('.dragger').not('.loaded').on('change', function(e){

          var value         = $(this).val(),
              checkedValue   = (value != '' ) ? parseFloat( value ) : ''; 
              max           = $(this).data('max'),
              min           = $(this).data('min')

              //.replace("[^\\d.]", "") 
          if( checkedValue > max )
            checkedValue = max
          else if( checkedValue < min && checkedValue !== '' )
            checkedValue = min
          else if( _.isNaN(checkedValue) ){
            checkedValue = ''
          }

          
          /** Round to 1 decimal */
        //  checkedValue = Math.round( checkedValue * 10 ) / 10

          if( value != checkedValue )
            $(this).val( checkedValue )
        
        }).addClass('loaded')

        /** Reset back to default */
        $('.dragger-reset').on('click', function(){

          $(this).parent().parent().find('.dragger').val('').trigger('keyup').trigger('change')

        }).addClass('loaded')

        /** Drag to change value */
        $(".dragger-option .pl-input-group")
          .not('.loaded')
          .on( 'mousedown.dragdistance', function(e) {

            var that           = this,
                startingTop   = e.pageY, 
                theInput       = $(that).find('.dragger'),
                 preVal         = theInput.val(), 
                 throttle       = 2, 
                 scale         = theInput.data('scale'), 
                 toFixed       = (scale >= 1) ? 0 : 1

            preVal = ( preVal === '' ) ? theInput.data('default') : preVal
             
              $(document).on( 'mousemove.dragdistance', function(e2) {
                 
                 var   math       = Math.round( ( startingTop - e2.clientY ) / throttle ) * scale,
                       newVal     = Number(1 * preVal + math).toFixed(toFixed)      // multiply by one to give it an integer value w/o dealing with NaN 
                  
                 theInput.val( newVal ).trigger('keyup');

              });

          }).addClass('loaded')

        // $('.dragger').bind('keydown', 'up', function(e){

        //   e.preventDefault()

        //   var val   = parseFloat($(this).val()), 
        //       scale = $(this).data('scale')

        //       console.log(val + scale + 'with scale')

        //   $(this).val( val + scale )
         
        // });
        // $('.dragger').bind('keydown', 'down', function(e){

        //   e.preventDefault()
          
        //   var val   = parseFloat($(this).val()),
        //       scale = $(this).data('scale')
          
        //   $(this).val( val - scale )
        
        // });

        /** On mouseup make sure to remove the event from both parent and iframe */
          $(document).on( 'mouseup.dragdistance', function() {
            $(document).off("mousemove.dragdistance");
          });

          /** This should probably be generalized to all iframes */
          $('.richtext-frame').on('load', function(){
            $( $('iframe.richtext-frame').contents() ).on( 'mouseup.dragdistance', function() {
              $(document).off("mousemove.dragdistance");
            });
          })
          

          $iDocument().on( 'mouseup.dragdistance', function() {
                $(document).off("mousemove.dragdistance");
            });
      }
      
    
      out += sprintf( '</div>');

      return out;


    }, 


    coloroption: function( o ){

      var out     = '', 
          theID   = o.inputID

      out += sprintf('<label for="%s">%s</label>', theID, o.label )
      out += sprintf('<div class="coloroption"><input id="%s" class="pl-colorpicker  lstn" type="text" name="%s" value="%s"/></div>', theID, o.name, o.value )

      this.optScripts.coloroption = function(){

        

        // color pickers, using WP colorpicker API
        $('.pl-colorpicker').not('.loaded').each( function(){

          

          $(this).wpColorPicker({
            change: function(e, ui){ 
          
              /** Have to set value manually because apparently this fires before the input value is set. Thus causes a one-click delay if we dont. */
              $(this).val( $(this).iris('color') ).trigger('keyup')
          
            }
          }).addClass('is-ready loaded')

        }) 

        

        /** If cleared, trigger keyup event so obs are notified */
        $('.wp-picker-clear').not('.loaded').on('click', function(){ 

          $(this).prev().trigger('keyup')

        }).addClass('loaded')
      }

      return out;
    }, 

    checkoption: function( o ){

      var out     = '',
        value     = ( o.value == 'true' ) ? 1 : o.value,
        value     = parseInt(value),
        checked   = (!value || value === 0 || value === '') ? '' : 'checked',
        toggleValue = (checked === 'checked') ? 1 : 0,
        aux     = sprintf('<input class="checkbox-toggle lstn" id="%s"  name="%s" type="hidden" value="%s" />', o.inputID, o.name, toggleValue )

      //out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )
      out += sprintf('<div class="checkbox checkbox-group"><label><input id="%s" name="%s" class="checkbox-input" type="checkbox" %s> %s</label>%s</div>', o.inputID, o.name, checked, o.label, aux)
      

      /**
       * Checkbox Toggle Hidden Input (0 or 1)
       */
      this.optScripts.checkbox = function(){
        
        $('.checkbox-input:not(.loaded)').on('change', function(){

          var checkToggle = $(this).parent().next()

          if( $(this).is(':checked') ){
              $(this).val(1)
              checkToggle.val(1)
          } else{
              $(this).val(0)
              checkToggle.val(0)
          }

          checkToggle.trigger('change')

        }).addClass('loaded')
      }
      
      return out;
    }, 

    _createAccordion: function( o ){

      var  that           = this, 
          out           = '',
          optionArray   = $.toolEngine.optGetValue( o.key ), 
          itemType       = 'Item',
          itemNumber     = 0,
          totalNum       = optionArray.length || Object.keys(optionArray).length,
          removeShow     = ( totalNum <= 1 ) ? 'display: none;' : '', 
          reorder       = $.plEditing.reorderIcon()


      $.each( optionArray, function( ind, vals ){ 


        o.itemNumber = 'item'+itemNumber

        out += sprintf("<div class='opt-group itemset item-closed' data-num='%s'><div class='opt-name item-toggle'>%s<span class='bar-title'>%s %s</span> <span class='pl-btn pl-btn-xs remove-item' style='%s'><i class='pl-icon pl-icon-remove'></i></span></div><div class='opt-accordion-opts item-contents'>", ind,  reorder, itemType, itemNumber + 1, removeShow )

        /** Add the array index and all values for the index to option meta for engine */
        o.ind = ind
        o.vals = vals

        if( o.opts ){
          $.each( o.opts , function(index, osub) {

            out += $.toolEngine.optEngine( osub, o ) 
            
          })
        }

        // adds a hidden input set to true, so that the item doesn't disappear
        out += that.addHiddenInput( o.key, o.itemNumber )
        
        out += sprintf("</div></div>")

        itemNumber++
      })

      return out

    }, 



    _redoAccordion: function( theAccordion, o ){

      var that = this

      
      
      newAccordion = that._createAccordion( o )

      theAccordion.html( newAccordion )

    
      theAccordion
        .pagesort({
          
          handle:     ".opt-name",
          animation:     150,
          draggable:     '.opt-group',
          onUpdate: function(evt){

            that._resortAccordion( theAccordion, o )
        
            $.plEditing.setNeedsSave()
        
          }
        })

      $.plEditing.itemToggles()

      /** Run option scripts again, except for accordion */
      $.engineOpts.runScripts()

      $.toolEngine.resetListenerBinding()
      
      

    }, 

    _resortAccordion: function( theAccordion, o ){

      var that = this, 
        sortArray = []

      theAccordion.find('.opt-group').each( function(){
        sortArray.push( parseInt( $(this).attr('data-num') ) ) 
        
      })
    
      $plModel().sortObservableArrayByArray( $.toolEngine.UID, o.key, sortArray )

      that._redoAccordion( theAccordion, o )
    }, 

    accordion: function( o ){

      var that = this, 
        out = ''

      var opts = sprintf( '<script type="application/json" class="accordion-options">%s</script>', JSON.stringify( o ) )

      out += sprintf('<div id="%s" name="%s" class="opt-accordion toolbox-sortable">', o.inputID, o.name)

      out += that._createAccordion( o )
      
      var tools = sprintf('<span class="pl-btn pl-btn-default pl-btn-xs add-accordion-item" >+ Add Item</span>')

      

      out += sprintf("</div><div class='accordion-tools'>%s%s</div>", opts, tools)





      that.optScripts.accordion = function(){ 

        $('.workarea-opts-form .opt-accordion').not('.loaded').each( function(){

          var theAccordion   = $(this), 
              theOpt         = $(this).closest('.pl-form-group'),
              o             = JSON.parse( theOpt.find('.accordion-options').html() )

          theAccordion
            .pagesort({
              
              handle:     ".opt-name",
              animation:     250,
              draggable:     '.opt-group',
              onUpdate: function(evt){
                that._resortAccordion( theAccordion, o )

                $.plEditing.setNeedsSave()
              }, 
              
            })

        }).addClass('loaded')

        /** Remove accordion item, redraws accordion */
        $('.workarea-opts-form .opt-accordion').not('.del-loaded').delegate( '.opt-name .remove-item', 'click', function () {

          var theOpt         = $(this).closest('.pl-form-group'),
              theAccordion   = theOpt.find('.opt-accordion'), 
              theGroup       = $(this).closest('.opt-group'), 
              o             = JSON.parse( theOpt.find('.accordion-options').html() )

        
          /** Don't let them remove last one */
          if( theAccordion.find('.opt-group').length <= 2 ){ theAccordion.find('.remove-item').hide() }


          var index = theGroup.data('num')

          $plModel().RemoveItemByIndex( $.toolEngine.UID, o.key, index )

          /** Remove the item html */
          theGroup.remove()

          /** Redraw the accordion */
          that._redoAccordion( theAccordion, o )
          
          $.plEditing.setNeedsSave( 'yes' )

          }).addClass('del-loaded')

        /** Event to add new accordion items, redraws accordion */
        $('.add-accordion-item').not('.loaded').on('click', function () {

          var theOpt       = $(this).closest('.pl-form-group'),
            theAccordion   = theOpt.find('.opt-accordion'), 
            o         = JSON.parse( theOpt.find('.accordion-options').html() ),
            item = {}

          /** Parse the options array */
          $.each( o.opts, function( i, option ){ 

            if( plIsset(option.key) )
              item[ option.key ] = '' 

            if( typeof option.opts != 'undefined' ){

              $.each( option.opts, function( i2, option2 ){ 

                if( plIsset(option2.key) )
                  item[ option2.key ] = ''

              })
            }
          
          })

          $plModel().addItemToArray( $.toolEngine.UID, o.key, item )

          that._redoAccordion( theAccordion, o )

          $.plEditing.setNeedsSave( 'yes' )


          }).addClass('loaded')


      }


      return out 
    }, 

    addHiddenInput: function( key, itemNumber ){

      var that = this
      
      return sprintf( '<input type="hidden" class="lstn dont-change" id="%s_%s_showitem" name="%s[%s][%s][showitem]" value="1" />', key, itemNumber, that.uniqueID, key, itemNumber)

    }, 

    imagedropzone: function( o ){

      var that        = this,
          theClass    = sprintf('pl-dropzone-%s', o.inputID),
          size        = o.size || '',
          out         = ''
      
      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label );

      /** Note: need dropzone class for CSS styling. */
      out += sprintf('<div class="dropzone dropzone-option upload-box clearfix select-%s" data-select="%s" data-size="%s" >', o.name, o.name, size);

      out += sprintf('<div class="pl-dropzone-actions ">');
      
      out += sprintf( '<input type="text" id="%s" name="%s" class="%s lstn pl-form-control upload-input" placeholder="%s" value="%s" />', o.inputID, o.name, o.classes, o.place, o.value);

      out += sprintf('<span class="pl-dropzone %s pl-btn pl-btn-primary pl-btn-xs">Upload</span>', theClass);
      out += sprintf(' <span class="pl-load-media-lib pl-btn pl-btn-default pl-btn-xs">Media</span>', o.value);
      out += sprintf(' <span class="pl-image-remove pl-btn pl-btn-default pl-btn-xs"><i class="pl-icon pl-icon-remove"></i></span>', o.value);

      out += sprintf('</div>'); // actions

      
      out += sprintf('<div class="pl-dropzone-preview %s"></div>', o.inputID);

      out += sprintf('</div>'); // dropzone

      var template = '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-meta"><div class="dz-filename"><span data-dz-name></span></div><div class="dz-size" data-dz-size></div></div><img data-dz-thumbnail /></div><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-success-mark"><i class="pl-icon pl-icon-ok"></i></div><div class="dz-error-mark"><i class="pl-icon pl-icon-remove"></i></div></div>';


      

      var myDropzones = {}


      /** Runonly once for all image uploaders */
      that.optScripts.dropzoneUpload = function(){



        $( '.dropzone-option' ).not('.loaded').each( function(){

          theDZ     = $(this),
          theSelector = '.' + $(this).data('select'), 
          theInput   = theDZ.find('.upload-input'), 
          thePreview   = theDZ.find('.pl-dropzone-preview')

          
          if( theInput.val() != '' ){
          
            thePreview.html( sprintf('<div class="dz-preview dz-file-preview"><img src="%s" /></div>', pl_do_shortcode(theInput.val())) )

          }

          theDZ.find('.pl-dropzone').dropzone({ 
            url:         PLWorkarea.ajaxURL,
            previewsContainer:   thePreview[0],
            previewTemplate:   template,
            sending: function (file, xhr, formData) {

              formData.append( "action", "pl_server"); 
              formData.append( "hook",   "pl_image_upload");
              formData.append( "nonce",   PLWorkarea.security );

              var DZ = $( $( this )[0].element )

              formData.append( "size", DZ.closest('.dropzone-option').data('size') ); 

              // Remove old image in preview
              DZ.parent().next().find('.dz-preview:not(:last)').remove()
            }, 
            
            success: function (file, response) {

              var rsp  = response

              var DZ = $( $( this )[0].element )


              DZ.parent().find('.upload-input').val( rsp.url ).trigger('blur')

            }
          });

        }).addClass('loaded')

        

        /** Remove Image */
        $('.pl-image-remove').not('.loaded').on('click', function(){

          $(this)
            .closest('.upload-box')
            .find('.lstn')
            .val('')
            .trigger('blur')
            .end()
            .find('.dz-preview').fadeOut()

        }).addClass('loaded')

        /** Get image from media library */
        $('.pl-load-media-lib').not('.loaded').on('click', function(){

          var mediaFrame

          if( $(this).data('mimetype') === 'video' )
            mediaFrame = PLWorkarea.mediaLibraryVideo
          else 
            mediaFrame = PLWorkarea.mediaLibrary

          var theInput = $(this).closest('.upload-box').find('.upload-input'),
            optionID = theInput.attr('id')
          
          mediaFrame + '&oid=' + optionID

          PLWorkarea.iframeSelector = optionID

          bootbox.dialog({
            title:     "Media Library",
              message:   sprintf('<iframe class="modal-iframe" src="%s"></iframe>', mediaFrame),
              animate: false,
            className: 'pl-modal modal-media',
            backdrop: true,
            onEscape: true

          })


          $('.bootbox').on('hidden.mediaDialog', function () {
            
            theInput.trigger('blur').closest('.ui-accordion').accordion('refresh')
            
            $('.bootbox').off('hidden.mediaDialog')

          })

        }).addClass('loaded')


      }

      
      return out;
    }, 


    radioOption: function( o ){

      var that = this,
        out = ''

      
      
    
      
      if(o.opts){

        var radioButtons = ''

        $.each(o.opts, function(key, s){

          var optValue   = s.val,
              optName     = ( o.type === 'select_same'   ) ? s : s.name,
              selected     = ( o.value == optValue     ) ? 'checked' : '',
              txt         = ( typeof s.txt !== 'undefined') ? ' ' + s.txt : '', 
              theID       = o.inputID + key,
              hoverTitle  = s.hover || '', 
              icon         = ( plIsset(s.icon) ) ? sprintf('<i class="pl-icon pl-icon-%s"></i>', s.icon) : ''


          
          radioButtons += sprintf('<label for="%s" class="pl-btn pl-btn-default pl-btn-radio %s" title="%s">%s%s<input type="radio" class="input-radio lstn" name="%s" id="%s" value="%s" %s></label>', theID, selected, hoverTitle, icon, txt, o.inputID, theID, optValue, selected)

        })
      }

      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )
      out += sprintf('<div class="pl-btn-group pl-btn-radios">%s</div>', radioButtons)

      // Add Once Off Scripts
      that.optScripts.radioSet = function(){  


        $('.input-radio').not('.loaded').on('click', function(){

          var clicked = $(this), 
              group   = clicked.parent().parent()
          
          group.find('.pl-btn-radio').removeClass('checked')

          group.find('.input-radio:checked').parent().addClass('checked')

    
        }).addClass('loaded')

        $('.pl-btn-radios .input-radio:checked').parent().addClass('checked')

      
      }

      return out;

    },
    

    selectoption: function( o ){

      var that = this,
        out = '',
        defaultValue = o.default || '',
        select_opts = (o.type !== 'select_multi') ? sprintf( '<option value="%s" >Select</option>', defaultValue)  : ''
        
      if ( 
          o.type === 'count_select'     || 
          o.type === 'count_select_same'   || 
          o.type === 'select_pixels'    || 
          o.type === 'select_vw'        || 
          o.type === 'select_percent'   ||
          o.type === 'select_proportion'  
        ) {

      
        if( o.type == 'select_pixels' ){
          
          var cnt_start       = parseInt(o.count_start)   || 0, 
              cnt_num         = parseInt(o.count_number)   || 500, 
              cnt_multiple     = parseInt(o.count_mult)     || 25,
              suffix           = 'px',
              key_suffix       = 'px'

        } 

        else if( o.type == 'select_vw' ){
          
          var cnt_start     = parseInt(o.count_start)   || 10, 
            cnt_num         = parseInt(o.count_number)   || 100, 
            cnt_multiple     = parseInt(o.count_mult)   || 5,
            suffix           = 'vw',
            key_suffix       = 'vw'

        } 

        else if( o.type == 'select_proportion' || o.type == 'select_percent' ){
          
          var cnt_start       = parseInt(o.count_start)   || 0, 
              cnt_num         = parseInt(o.count_number)   || 100, 
              cnt_multiple     = parseInt(o.count_mult)   || 10,
              suffix           = '%',
              key_suffix       = (o.type == 'select_proportion') ? '' : '%'

        } 

        else {

          var cnt_start     = parseInt(o.count_start)   || 0, 
            cnt_num     = parseInt(o.count_number)   || 10, 
            cnt_multiple   = parseInt(o.count_mult)   || 1, 
            suffix       = o.suffix           || '',
            key_suffix     = ( o.type === 'count_select_same' ) ? o.suffix : ''

        }

        o.opts = {}

        for ( i = cnt_start; i <= cnt_num; i+=cnt_multiple ) {

          var selectOptionValue = ( o.type == 'select_proportion' ) ? i / 100 : i + key_suffix

          o.opts[ selectOptionValue ] = { name: i+suffix }

        }

        
      }

        
      if(o.type === 'select_wp_tax'){

        var taxes = PLWorkarea.taxes
        o.opts = {}
        $.each(taxes, function(key, s){
          o.opts[ s ] = {name: s}
        })

      }

      else if(o.type === 'select_term'){

      
        $.engineOpts.optScripts.selectTerm = function(){

          /** If the trigger option changes we need new terms via ajax */
          $('#' + o.trigger ).on('change load', function(e){

            console.log(e)

            var triggerEl = $(this)

            /** If value unchanged */
            if( $(this).val() == $(this).data('init') && e.type != 'load' ){
              return ;
            }

            var args = {
                  hook:   'select_term',
                  pt:     $(this).val(),
                  postSuccess: function( rsp ){ 

                    newopts =  sprintf('<option value="">Select</option>')

                    $.each(rsp.opts, function(key, s){

                      newopts += sprintf('<option value="%s">%s</option>', key, s.name)
                      newopts[ key ] = {name: s.name}
                    })

                    $('#' + o.key ).html( newopts )

                    triggerEl.data('init', triggerEl.val()).attr('data-init', triggerEl.val())
                    
                  },
                }

            
            $plServer().run( args )

          })

          $('#' + o.trigger ).trigger('load')

        }

      } 

      else if(o.type === 'select_icon'){

        icons = PLWorkarea.icons

        o.opts = {}
        $.each(icons, function(key, s){
          o.opts[ s ] = {name: s}
        })
      } 

      else if( o.type === 'select_animation' ){

        var anims = PLWorkarea.animations

        o.opts = {}
        $.each(anims, function(key, s){
          o.opts[ key ] = {name: s}
        })

      } 

      else if( o.type === 'select_button' || o.type === 'select_button_size'){

        if( o.type === 'select_button' )
          var btns = PLWorkarea.btns
        else
          var btns = PLWorkarea.btnSizes


        o.opts = {}
        $.each(btns, function(key, s){

          // we cant use a string of '0' as a default key here, as it stops the option engine from using a default :/
          if( '0' === key )
            key = ''

          o.opts[ key ] = {name: s}
        })

      } 



      else if( o.type === 'select_sidebar' ){

        var sbs = PLWorkarea.sidebars



        o.opts = {}
        $.each(sbs, function(key, s){
          o.opts[ key ] = {name: s}
        })

      } 

      else if( o.type === 'select_menu' ){

        var obs = PLWorkarea.menus

        o.opts = {}
        $.each( obs, function(key, s){
          o.opts[ key ] = {name: s}
        })

      } 

      else if( o.type === 'select_imagesizes' ){

        var sizes = PLWorkarea.imgSizes

        o.opts = {}
        $.each(sizes, function(key, s){
          o.opts[ s ] = {name: s}
        })

      }

      
      if(o.opts){

        $.each(o.opts, function(key, s){

          var optValue = (o.type === 'select_same') ? s : key
          ,  optName = (o.type === 'select_same') ? s : s.name
          ,  selected = ''

          // Multi Select
          if(typeof o.value === 'object'){
            $.each(o.value, function(k, val){
              if(optValue === val)
                selected = 'selected'
            })

          } else {

            if(o.value !== '')
              selected = (o.value === optValue) ? 'selected' : ''
            else if( plIsset(o.default) )
              selected = (o.default === optValue) ? 'selected' : ''

          }

          select_opts += sprintf('<option value="%s" %s >%s</option>', optValue, selected, optName)

        })
      }


      var multi = (o.type === 'select_multi') ? 'multiple' : ''


      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )
      out += sprintf('<select id="%s" name="%s" class="%s pl-form-control lstn" data-type="%s" data-init="%s" %s>%s</select>', o.inputID, o.name, o.classes, o.type, o.value, multi, select_opts)

      
      if(o.type === 'select_taxonomy' && o.post_type){
        
        out += sprintf(
          '<div style="margin-bottom: 10px;"><a href="%sedit.php?post_type=%s" target="_blank" class="pl-btn pl-btn-xs pl-btn-info"><i class="pl-icon pl-icon-edit"></i> %s</a></div>',
          PLWorkarea.adminURL,
          o.post_type,
          plTranslate( 'edit_sets' )
        )
      }
      

      return out;

    }

    
  }

}(window.jQuery);


 /* -------------------- */ 

!function ($) {
   

  window.$i = function(element) {

    if ( typeof PLWorkarea.iframe == 'undefined' || typeof PLWorkarea.iframe.contents() == 'undefined' )
      return $();

    return PLWorkarea.iframe.contents().find(element);

  }

  window.$iDocument = function() {

    return $(PLWorkarea.iframe.contents());

  }

  window.$iWindow = function() {

    return $('iframe.site-frame')[0].contentWindow;
  
  }

  window.$jq = function() {

    return $iWindow().jQuery
  
  }

  

  window.$pl = function() {

    return $iWindow().PLData
  
  }

  window.$plModel = function() {

    return $iWindow().jQuery.plModel
  
  }

  window.$plBinding = function() {

    return $iWindow().jQuery.plBinding
  
  }

  window.$plServer = function() {

    return $iWindow().jQuery.plServer
  
  }

  window.$iWindowEl = function() {

    return $( $('iframe.site-frame')[0].contentWindow );
  
  }



  // --> Initialize
  $(document).ready(function() {
  
    
    $.plFrame.init()
    
  })
  
  $.plFrame = {

    init: function(){

      var that = this

      /** Assign global for iFrame */
      PLWorkarea.iframe = $('iframe.site-frame');


      PLWorkarea.iframe.on("load", function () {

  
        $('title').html('Editing: ' + pl_strip_html( $pl().config.currentPageName ) );
        

          /** Add events to links to maintain editing state */
          $.plFrame.handleFrameLinks()

          if( $pl().config.needsave == 1 ) 
            $.plEditing.setNeedsSave()
          else
          $.plEditing.setNeedsSave('no')


        /** Deal with edit page link...  */
        
        if( $pl().urls.editPost == '' )
          $('.editlink').addClass('disable-link').removeAttr('href').append('<span class="na"> (N/A)</span>')
        else
          $('#wp-admin-bar-edit > a').removeClass('disable-link').attr( 'href', $pl().urls.editPost ).find('.na').remove()


            
         
      });

      
      /** Changing to a different area of the site, show overlay */
      PLWorkarea.iframe.on("unload", function () {
        $('.iframe-loading-overlay').addClass('show-overlay')
      });


      plAdjustAdminBar()
      

    },

    

    reloadFrame: function( params ){

      var url = PLWorkarea.iframe.attr('src')

      params.needsave = 1
  
      $.each( params, function( key, value ){

        url = updateQueryStringParameter( url, key, value );

      })

      PLWorkarea.iframe.attr('src', url);

    }, 

    /** Deal with iFrame link clicking. */
    handleFrameLinks: function(){

        $i('body').delegate('a', 'click', function(e){

            var link       = $(this),
                url        = $( this ).attr("href"),
                hostname   = new RegExp( location.host ), 
                ignore     = false,
                Protocol   = $.plFrame.getProtocol( url ),
                iProtocol  = $.plFrame.getProtocol( PLWorkarea.iframe.attr('src') ),
                mismatch   = false

          
            /** Pretty Photo (woocommerce) */
            if( link.data('rel') || link.hasClass('prettyPhoto') || link.hasClass('iframe-ignore-link') || link.hasClass('modal') || '_blank' == link.attr('target')){
              ignore = true;
            }
              
            if( Protocol !== iProtocol ) {
              console.log( 'Protocals are mismatched. Removing iFrame.')
              mismatch = true
            }
              

              
            if( typeof url !== 'undefined' && url !== '' && ! ignore ){

                // Do default on anchor
                if( url.slice(0, 1) != "#" ){

                  e.preventDefault()

                
                  /** Local link, keep the editing stuff unless ADMIN or a protocol missmatch*/
                  if( hostname.test( url ) && url.indexOf('wp-admin') < 0 && ! mismatch ){

                  
                    // Remove any anchors
                    url = url.split("#")[0]

                    /* Push New URL to browser */
                    browserURL = updateQueryStringParameter(url, 'pl_edit', 'on' );

                    window.history.pushState( "", "", browserURL );
                    
                    
                    url = updateQueryStringParameter( url, 'iframe', 'true' );



                    PLWorkarea.iframe.attr('src', url);

                  }
                  
                  /** Link in new window */
                  else if( '_blank' == $(this).attr('target') ){
                    window.open( url );
                  } 

                  /** Load external link or admin in parent */
                  else {
                   
                    location.href = url;             
                  }
                }

            }
            

          })
    },
    
    // get protocol from url as a string, returns http: or https:
    getProtocol: function( url ) {
      var link = document.createElement('a')
      link.setAttribute( 'href', url )
      return link.protocol
    },

    loadNew: function( url ){

      var that = this 

      if ( typeof url == 'undefined' || ! url)
        var url = PLWorkarea.homeURL;

      /* Choose contents iframe or preview iframe depending on argument */
        var iframe = PLWorkarea.iframe;


      
      /** Trigger unload event on iFrame */
      iframe.trigger('unload')

      iframe.fadeOut(300, function(){

        /* Build the URL */
        iframeURL = url;
        iframeURL = updateQueryStringParameter(iframeURL, 'iframe', 'true');
        iframeURL = updateQueryStringParameter(iframeURL, 'rand', Math.floor(Math.random() * 100000001));

      /* Clear out existing iframe contents */
        

        iframe.contents().find('*')
          .unbind()
          .remove();


        iframe[0].src = iframeURL;

        PLWorkarea.iframe.fadeIn()
      
        //that.loadCallback( callback, iframe );

      })
      

    }, 

  }

  


}(window.jQuery);


 /* -------------------- */ 

(function (factory) {
  "use strict";

  if (typeof define === "function" && define.amd) {
    define(["jquery"], factory);
  }
  else {
    /* jshint sub:true */
    factory(jQuery);
  }
})(function ($) {
  "use strict";

  /* CODE */



  /**
   * jQuery plugin for Sortable
   * @param   {Object|String} options
   * @param   {..*}           [args]
   * @returns {jQuery|*}
   */
  $.fn.pagesort = function (options) {
    var retVal;

    this.each(function () {
      var $el = $(this),
        sortable = $el.data('sortable');

      if (!sortable && (options instanceof Object || !options)) {
        sortable = new Sortable(this, options);
        $el.data('sortable', sortable);
      }

      if (sortable) {
        if (options === 'widget') {
          return sortable;
        }
        else if (options === 'destroy') {
          sortable.destroy();
          $el.removeData('sortable');
        }
        else if (options in sortable) {
          retVal = sortable[sortable].apply(sortable, [].slice.call(arguments, 1));
        }
      }
    });

    return (retVal === void 0) ? this : retVal;
  };
});


/**!
 * Sortable
 * @author  RubaXa   <trash@rubaxa.org>
 * @license MIT
 */


(function (factory) {
  "use strict";

  if (typeof define === "function" && define.amd) {
    define(factory);
  }
  else if (typeof module != "undefined" && typeof module.exports != "undefined") {
    module.exports = factory();
  }
  else if (typeof Package !== "undefined") {
    Sortable = factory();  // export for Meteor.js
  }
  else {
    /* jshint sub:true */
    window["Sortable"] = factory();
  }
})(function () {
  "use strict";

  var dragEl,
    ghostEl,
    cloneEl,
    rootEl,
    scrollEl,
    nextEl,

    lastEl,
    lastCSS,

    oldIndex,
    newIndex,

    activeGroup,
    autoScroll = {},

    tapEvt,
    touchEvt,

    expando = 'Sortable' + (new Date).getTime(),

    win = window,
    document = win.document,
    parseInt = win.parseInt,
    supportIEdnd = !!document.createElement('div').dragDrop,

    _silent = false,

    _dispatchEvent = function (rootEl, name, targetEl, fromEl, startIndex, newIndex) {
      var evt = document.createEvent('Event');

      evt.initEvent(name, true, true);

      evt.item = targetEl || rootEl;
      evt.from = fromEl || rootEl;
      evt.clone = cloneEl;

      evt.oldIndex = startIndex;
      evt.newIndex = newIndex;

      rootEl.dispatchEvent(evt);
    },

    _customEvents = 'onAdd onUpdate onRemove onStart onEnd onFilter onSort'.split(' '),

    noop = function () {},

    abs = Math.abs,
    slice = [].slice,

    touchDragOverListeners = []
  ;



  /**
   * @class  Sortable
   * @param  {HTMLElement}  el
   * @param  {Object}       [options]
   */
  function Sortable(el, options) {
    this.el = el; // root element
    this.options = options = (options || {});


    // Default options
    var defaults = {
      group: Math.random(),
      sort: true,
      disabled: false,
      store: null,
      handle: null,
      scroll: true,
      scrollSensitivity: 30,
      scrollSpeed: 10,
      draggable: /[uo]l/i.test(el.nodeName) ? 'li' : '>*',
      ghostClass: 'sortable-ghost',
      ignore: 'a, img',
      filter: null,
      animation: 0,
      setData: function (dataTransfer, dragEl) {
        dataTransfer.setData('Text', dragEl.textContent);
      },
      dropBubble: false,
      dragoverBubble: false
    };


    // Set default options
    for (var name in defaults) {
      !(name in options) && (options[name] = defaults[name]);
    }


    var group = options.group;

    if (!group || typeof group != 'object') {
      group = options.group = { name: group };
    }


    ['pull', 'put'].forEach(function (key) {
      if (!(key in group)) {
        group[key] = true;
      }
    });


    // Define events
    _customEvents.forEach(function (name) {
      options[name] = _bind(this, options[name] || noop);
      _on(el, name.substr(2).toLowerCase(), options[name]);
    }, this);


    // Export group name
    el[expando] = group.name + ' ' + (group.put.join ? group.put.join(' ') : '');


    // Bind all private methods
    for (var fn in this) {
      if (fn.charAt(0) === '_') {
        this[fn] = _bind(this, this[fn]);
      }
    }


    // Bind events
    _on(el, 'mousedown', this._onTapStart);
    _on(el, 'touchstart', this._onTapStart);
    supportIEdnd && _on(el, 'selectstart', this._onTapStart);

    _on(el, 'dragover', this._onDragOver);
    _on(el, 'dragenter', this._onDragOver);

    touchDragOverListeners.push(this._onDragOver);

    // Restore sorting
    options.store && this.sort(options.store.get(this));
  }


  Sortable.prototype = /** @lends Sortable.prototype */ {
    constructor: Sortable,


    _dragStarted: function () {
      // Apply effect
      _toggleClass(dragEl, this.options.ghostClass, true);

      Sortable.active = this;

      // Drag start event
      _dispatchEvent(rootEl, 'start', dragEl, rootEl, oldIndex);
    },


    _onTapStart: function (/**Event|TouchEvent*/evt) {
      var type = evt.type,
        touch = evt.touches && evt.touches[0],
        target = (touch || evt).target,
        originalTarget = target,
        options =  this.options,
        el = this.el,
        filter = options.filter;

      if (type === 'mousedown' && evt.button !== 0 || options.disabled) {
        return; // only left button or enabled
      }

      if (options.handle) {
        target = _closest(target, options.handle, el);
      }

      target = _closest(target, options.draggable, el);

      // get the index of the dragged element within its parent
      oldIndex = _index(target);

      // Check filter
      if (typeof filter === 'function') {
        if (filter.call(this, evt, target, this)) {
          _dispatchEvent(originalTarget, 'filter', target, el, oldIndex);
          evt.preventDefault();
          return; // cancel dnd
        }
      }
      else if (filter) {
        filter = filter.split(',').some(function (criteria) {
          criteria = _closest(originalTarget, criteria.trim(), el);

          if (criteria) {
            _dispatchEvent(criteria, 'filter', target, el, oldIndex);
            return true;
          }
        });

        if (filter) {
          evt.preventDefault();
          return; // cancel dnd
        }
      }

      // Prepare `dragstart`
      if (target && !dragEl && (target.parentNode === el)) {
        // IE 9 Support
        (type === 'selectstart') && target.dragDrop();

        tapEvt = evt;

        rootEl = this.el;
        dragEl = target;
        nextEl = dragEl.nextSibling;
        activeGroup = this.options.group;

        dragEl.draggable = true;

        // Disable "draggable"
        options.ignore.split(',').forEach(function (criteria) {
          _find(target, criteria.trim(), _disableDraggable);
        });

        if (touch) {
          // Touch device support
          tapEvt = {
            target: target,
            clientX: touch.clientX,
            clientY: touch.clientY
          };

          this._onDragStart(tapEvt, true);
          evt.preventDefault();
        }

        _on(document, 'mouseup', this._onDrop);
        _on(document, 'touchend', this._onDrop);
        _on(document, 'touchcancel', this._onDrop);

        _on(dragEl, 'dragend', this);
        _on(rootEl, 'dragstart', this._onDragStart);

        _on(document, 'dragover', this);


        try {
          if (document.selection) {
            document.selection.empty();
          } else {
            window.getSelection().removeAllRanges();
          }
        } catch (err) {
        }
      }
    },

    _emulateDragOver: function () {
      if (touchEvt) {
        _css(ghostEl, 'display', 'none');

        var target = document.elementFromPoint(touchEvt.clientX, touchEvt.clientY),
          parent = target,
          groupName = this.options.group.name,
          i = touchDragOverListeners.length;

        if (parent) {
          do {
            if ((' ' + parent[expando] + ' ').indexOf(groupName) > -1) {
              while (i--) {
                touchDragOverListeners[i]({
                  clientX: touchEvt.clientX,
                  clientY: touchEvt.clientY,
                  target: target,
                  rootEl: parent
                });
              }

              break;
            }

            target = parent; // store last element
          }
          /* jshint boss:true */
          while (parent = parent.parentNode);
        }

        _css(ghostEl, 'display', '');
      }
    },


    _onTouchMove: function (/**TouchEvent*/evt) {
      if (tapEvt) {
        var touch = evt.touches[0],
          dx = touch.clientX - tapEvt.clientX,
          dy = touch.clientY - tapEvt.clientY,
          translate3d = 'translate3d(' + dx + 'px,' + dy + 'px,0)';

        touchEvt = touch;

        _css(ghostEl, 'webkitTransform', translate3d);
        _css(ghostEl, 'mozTransform', translate3d);
        _css(ghostEl, 'msTransform', translate3d);
        _css(ghostEl, 'transform', translate3d);

        this._onDrag(touch);
        evt.preventDefault();
      }
    },


    _onDragStart: function (/**Event*/evt, /**boolean*/isTouch) {
      var dataTransfer = evt.dataTransfer,
        options = this.options;

      this._offUpEvents();

      if (activeGroup.pull == 'clone') {
        cloneEl = dragEl.cloneNode(true);
        _css(cloneEl, 'display', 'none');
        rootEl.insertBefore(cloneEl, dragEl);
      }

      if (isTouch) {
        var rect = dragEl.getBoundingClientRect(),
          css = _css(dragEl),
          ghostRect;

        ghostEl = dragEl.cloneNode(true);

        _css(ghostEl, 'top', rect.top - parseInt(css.marginTop, 10));
        _css(ghostEl, 'left', rect.left - parseInt(css.marginLeft, 10));
        _css(ghostEl, 'width', rect.width);
        _css(ghostEl, 'height', rect.height);
        _css(ghostEl, 'opacity', '0.8');
        _css(ghostEl, 'position', 'fixed');
        _css(ghostEl, 'zIndex', '100000');

        rootEl.appendChild(ghostEl);

        // Fixing dimensions.
        ghostRect = ghostEl.getBoundingClientRect();
        _css(ghostEl, 'width', rect.width * 2 - ghostRect.width);
        _css(ghostEl, 'height', rect.height * 2 - ghostRect.height);

        // Bind touch events
        _on(document, 'touchmove', this._onTouchMove);
        _on(document, 'touchend', this._onDrop);
        _on(document, 'touchcancel', this._onDrop);

        this._loopId = setInterval(this._emulateDragOver, 150);
      }
      else {
        if (dataTransfer) {
          dataTransfer.effectAllowed = 'move';
          options.setData && options.setData.call(this, dataTransfer, dragEl);
        }

        _on(document, 'drop', this);
      }

      scrollEl = options.scroll;

      if (scrollEl === true) {
        scrollEl = rootEl;

        do {
          if ((scrollEl.offsetWidth < scrollEl.scrollWidth) ||
            (scrollEl.offsetHeight < scrollEl.scrollHeight)
          ) {
            break;
          }
        /* jshint boss:true */
        } while (scrollEl = scrollEl.parentNode);
      }

      setTimeout(this._dragStarted, 0);
    },

    _onDrag: _throttle(function (/**Event*/evt) {
      // Bug: https://bugzilla.mozilla.org/show_bug.cgi?id=505521
      if (rootEl && this.options.scroll) {
        var el,
          rect,
          options = this.options,
          sens = options.scrollSensitivity,
          speed = options.scrollSpeed,

          x = evt.clientX,
          y = evt.clientY,

          winWidth = window.innerWidth,
          winHeight = window.innerHeight,

          vx = (winWidth - x <= sens) - (x <= sens),
          vy = (winHeight - y <= sens) - (y <= sens)
        ;

        if (vx || vy) {
          el = win;
        }
        else if (scrollEl) {
          el = scrollEl;
          rect = scrollEl.getBoundingClientRect();
          vx = (abs(rect.right - x) <= sens) - (abs(rect.left - x) <= sens);
          vy = (abs(rect.bottom - y) <= sens) - (abs(rect.top - y) <= sens);
        }

        if (autoScroll.vx !== vx || autoScroll.vy !== vy || autoScroll.el !== el) {
          autoScroll.el = el;
          autoScroll.vx = vx;
          autoScroll.vy = vy;

          clearInterval(autoScroll.pid);

          if (el) {
            autoScroll.pid = setInterval(function () {
              if (el === win) {
                win.scrollTo(win.scrollX + vx * speed, win.scrollY + vy * speed);
              } else {
                vy && (el.scrollTop += vy * speed);
                vx && (el.scrollLeft += vx * speed);
              }
            }, 24);
          }
        }
      }
    }, 30),


    _onDragOver: function (/**Event*/evt) {
      var el = this.el,
        target,
        dragRect,
        revert,
        options = this.options,
        group = options.group,
        groupPut = group.put,
        isOwner = (activeGroup === group),
        canSort = options.sort;

      if (evt.preventDefault !== void 0) {
        evt.preventDefault();
        !options.dragoverBubble && evt.stopPropagation();
      }

      if (!_silent && activeGroup &&
        (isOwner
          ? canSort || (revert = !rootEl.contains(dragEl))
          : activeGroup.pull && groupPut && (
            (activeGroup.name === group.name) || // by Name
            (groupPut.indexOf && ~groupPut.indexOf(activeGroup.name)) // by Array
          )
        ) &&
        (evt.rootEl === void 0 || evt.rootEl === this.el)
      ) {
        target = _closest(evt.target, options.draggable, el);
        dragRect = dragEl.getBoundingClientRect();


        if (revert) {
          _cloneHide(true);

          if (cloneEl || nextEl) {
            rootEl.insertBefore(dragEl, cloneEl || nextEl);
          }
          else if (!canSort) {
            rootEl.appendChild(dragEl);
          }

          return;
        }


        if ((el.children.length === 0) || (el.children[0] === ghostEl) ||
          (el === evt.target) && (target = _ghostInBottom(el, evt))
        ) {
          if (target) {
            if (target.animated) {
              return;
            }
            targetRect = target.getBoundingClientRect();
          }

          _cloneHide(isOwner);

          try{
            el.appendChild(dragEl);
          } catch( err ){
            
          }

          
          this._animate(dragRect, dragEl);
          target && this._animate(targetRect, target);
        }
        else if (target && !target.animated && target !== dragEl && (target.parentNode[expando] !== void 0)) {
          if (lastEl !== target) {
            lastEl = target;
            lastCSS = _css(target);
          }


          var targetRect = target.getBoundingClientRect(),
            width = targetRect.right - targetRect.left,
            height = targetRect.bottom - targetRect.top,
            floating = /left|right|inline/.test(lastCSS.cssFloat + lastCSS.display),
            isWide = (target.offsetWidth > dragEl.offsetWidth),
            isLong = (target.offsetHeight > dragEl.offsetHeight),
            halfway = (floating ? (evt.clientX - targetRect.left) / width : (evt.clientY - targetRect.top) / height) > 0.5,
            nextSibling = target.nextElementSibling,
            after
          ;

          _silent = true;
          setTimeout(_unsilent, 30);

          _cloneHide(isOwner);

          if (floating) {
            after = (target.previousElementSibling === dragEl) && !isWide || halfway && isWide;
          } else {
            after = (nextSibling !== dragEl) && !isLong || halfway && isLong;
          }

          if (after && !nextSibling) {

            try {
              el.appendChild(dragEl);
            } catch( err ){
              //console.log(err)
            }
          } else {

            try {
              target.parentNode.insertBefore(dragEl, after ? nextSibling : target);
            } catch (err) {
              //console.log(err)
            }
            
          }

          this._animate(dragRect, dragEl);
          this._animate(targetRect, target);
        }
      }
    },

    _animate: function (prevRect, target) {
      var ms = this.options.animation;

      if (ms) {
        var currentRect = target.getBoundingClientRect();

        _css(target, 'transition', 'none');
        _css(target, 'transform', 'translate3d('
          + (prevRect.left - currentRect.left) + 'px,'
          + (prevRect.top - currentRect.top) + 'px,0)'
        );

        target.offsetWidth; // repaint

        _css(target, 'transition', 'all ' + ms + 'ms');
        _css(target, 'transform', 'translate3d(0,0,0)');

        clearTimeout(target.animated);
        target.animated = setTimeout(function () {
          _css(target, 'transition', '');
          target.animated = false;
        }, ms);
      }
    },

    _offUpEvents: function () {
      _off(document, 'mouseup', this._onDrop);
      _off(document, 'touchmove', this._onTouchMove);
      _off(document, 'touchend', this._onDrop);
      _off(document, 'touchcancel', this._onDrop);
    },

    _onDrop: function (/**Event*/evt) {
      var el = this.el,
        options = this.options;

      clearInterval(this._loopId);
      clearInterval(autoScroll.pid);

      // Unbind events
      _off(document, 'drop', this);
      _off(document, 'dragover', this);

      _off(el, 'dragstart', this._onDragStart);

      this._offUpEvents();

      if (evt) {
        evt.preventDefault();
        !options.dropBubble && evt.stopPropagation();

        ghostEl && ghostEl.parentNode.removeChild(ghostEl);

        if (dragEl) {
          _off(dragEl, 'dragend', this);

          _disableDraggable(dragEl);
          _toggleClass(dragEl, this.options.ghostClass, false);

          if (rootEl !== dragEl.parentNode) {
            newIndex = _index(dragEl);

            // drag from one list and drop into another
            _dispatchEvent(dragEl.parentNode, 'sort', dragEl, rootEl, oldIndex, newIndex);
            _dispatchEvent(rootEl, 'sort', dragEl, rootEl, oldIndex, newIndex);

            // Add event
            _dispatchEvent(dragEl, 'add', dragEl, rootEl, oldIndex, newIndex);

            // Remove event
            _dispatchEvent(rootEl, 'remove', dragEl, rootEl, oldIndex, newIndex);
          }
          else {
            // Remove clone
            cloneEl && cloneEl.parentNode.removeChild(cloneEl);

            if (dragEl.nextSibling !== nextEl) {
              // Get the index of the dragged element within its parent
              newIndex = _index(dragEl);

              // drag & drop within the same list
              _dispatchEvent(rootEl, 'update', dragEl, rootEl, oldIndex, newIndex);
              _dispatchEvent(rootEl, 'sort', dragEl, rootEl, oldIndex, newIndex);
            }
          }

          // Drag end event
          Sortable.active && _dispatchEvent(rootEl, 'end', dragEl, rootEl, oldIndex, newIndex);
        }

        // Set NULL
        rootEl =
        dragEl =
        ghostEl =
        nextEl =
        cloneEl =

        tapEvt =
        touchEvt =

        lastEl =
        lastCSS =

        activeGroup =
        Sortable.active = null;

        // Save sorting
        this.save();
      }
    },


    handleEvent: function (/**Event*/evt) {
      var type = evt.type;

      if (type === 'dragover') {
        this._onDrag(evt);
        _globalDragOver(evt);
      }
      else if (type === 'drop' || type === 'dragend') {
        this._onDrop(evt);
      }
    },


    /**
     * Serializes the item into an array of string.
     * @returns {String[]}
     */
    toArray: function () {
      var order = [],
        el,
        children = this.el.children,
        i = 0,
        n = children.length;

      for (; i < n; i++) {
        el = children[i];
        if (_closest(el, this.options.draggable, this.el)) {
          order.push(el.getAttribute('data-id') || _generateId(el));
        }
      }

      return order;
    },


    /**
     * Sorts the elements according to the array.
     * @param  {String[]}  order  order of the items
     */
    sort: function (order) {
      var items = {}, rootEl = this.el;

      this.toArray().forEach(function (id, i) {
        var el = rootEl.children[i];

        if (_closest(el, this.options.draggable, rootEl)) {
          items[id] = el;
        }
      }, this);

      order.forEach(function (id) {
        if (items[id]) {
          rootEl.removeChild(items[id]);
          rootEl.appendChild(items[id]);
        }
      });
    },


    /**
     * Save the current sorting
     */
    save: function () {
      var store = this.options.store;
      store && store.set(this);
    },


    /**
     * For each element in the set, get the first element that matches the selector by testing the element itself and traversing up through its ancestors in the DOM tree.
     * @param   {HTMLElement}  el
     * @param   {String}       [selector]  default: `options.draggable`
     * @returns {HTMLElement|null}
     */
    closest: function (el, selector) {
      return _closest(el, selector || this.options.draggable, this.el);
    },


    /**
     * Set/get option
     * @param   {string} name
     * @param   {*}      [value]
     * @returns {*}
     */
    option: function (name, value) {
      var options = this.options;

      if (value === void 0) {
        return options[name];
      } else {
        options[name] = value;
      }
    },


    /**
     * Destroy
     */
    destroy: function () {
      var el = this.el, options = this.options;

      _customEvents.forEach(function (name) {
        _off(el, name.substr(2).toLowerCase(), options[name]);
      });

      _off(el, 'mousedown', this._onTapStart);
      _off(el, 'touchstart', this._onTapStart);
      _off(el, 'selectstart', this._onTapStart);

      _off(el, 'dragover', this._onDragOver);
      _off(el, 'dragenter', this._onDragOver);

      //remove draggable attributes
      Array.prototype.forEach.call(el.querySelectorAll('[draggable]'), function (el) {
        el.removeAttribute('draggable');
      });

      touchDragOverListeners.splice(touchDragOverListeners.indexOf(this._onDragOver), 1);

      this._onDrop();

      this.el = null;
    }
  };


  function _cloneHide(state) {
    if (cloneEl && (cloneEl.state !== state)) {
      _css(cloneEl, 'display', state ? 'none' : '');
      !state && cloneEl.state && rootEl.insertBefore(cloneEl, dragEl);
      cloneEl.state = state;
    }
  }


  function _bind(ctx, fn) {
    var args = slice.call(arguments, 2);
    return  fn.bind ? fn.bind.apply(fn, [ctx].concat(args)) : function () {
      return fn.apply(ctx, args.concat(slice.call(arguments)));
    };
  }


  function _closest(/**HTMLElement*/el, /**String*/selector, /**HTMLElement*/ctx) {
    if (el) {
      ctx = ctx || document;
      selector = selector.split('.');

      var tag = selector.shift().toUpperCase(),
        re = new RegExp('\\s(' + selector.join('|') + ')\\s', 'g');

      do {
        if (
          (tag === '>*' && el.parentNode === ctx) || (
            (tag === '' || el.nodeName.toUpperCase() == tag) &&
            (!selector.length || ((' ' + el.className + ' ').match(re) || []).length == selector.length)
          )
        ) {
          return el;
        }
      }
      while (el !== ctx && (el = el.parentNode));
    }

    return null;
  }


  function _globalDragOver(/**Event*/evt) {
    evt.dataTransfer.dropEffect = 'move';
    evt.preventDefault();
  }


  function _on(el, event, fn) {
    el.addEventListener(event, fn, false);
  }


  function _off(el, event, fn) {
    el.removeEventListener(event, fn, false);
  }


  function _toggleClass(el, name, state) {
    if (el) {
      if (el.classList) {
        el.classList[state ? 'add' : 'remove'](name);
      }
      else {
        var className = (' ' + el.className + ' ').replace(/\s+/g, ' ').replace(' ' + name + ' ', '');
        el.className = className + (state ? ' ' + name : '');
      }
    }
  }


  function _css(el, prop, val) {
    var style = el && el.style;

    if (style) {
      if (val === void 0) {
        if (document.defaultView && document.defaultView.getComputedStyle) {
          val = document.defaultView.getComputedStyle(el, '');
        }
        else if (el.currentStyle) {
          val = el.currentStyle;
        }

        return prop === void 0 ? val : val[prop];
      }
      else {
        if (!(prop in style)) {
          prop = '-webkit-' + prop;
        }

        style[prop] = val + (typeof val === 'string' ? '' : 'px');
      }
    }
  }


  function _find(ctx, tagName, iterator) {
    if (ctx) {
      var list = ctx.getElementsByTagName(tagName), i = 0, n = list.length;

      if (iterator) {
        for (; i < n; i++) {
          iterator(list[i], i);
        }
      }

      return list;
    }

    return [];
  }


  function _disableDraggable(el) {
    el.draggable = false;
  }


  function _unsilent() {
    _silent = false;
  }


  /** @returns {HTMLElement|false} */
  function _ghostInBottom(el, evt) {
    var lastEl = el.lastElementChild, rect = lastEl.getBoundingClientRect();
    return (evt.clientY - (rect.top + rect.height) > 5) && lastEl; // min delta
  }


  /**
   * Generate id
   * @param   {HTMLElement} el
   * @returns {String}
   * @private
   */
  function _generateId(el) {
    var str = el.tagName + el.className + el.src + el.href + el.textContent,
      i = str.length,
      sum = 0;

    while (i--) {
      sum += str.charCodeAt(i);
    }

    return sum.toString(36);
  }

  /**
   * Returns the index of an element within its parent
   * @param el
   * @returns {number}
   * @private
   */
  function _index(/**HTMLElement*/el) {
    var index = 0;
    while (el && (el = el.previousElementSibling) && (el.nodeName.toUpperCase() !== 'TEMPLATE')) {
      index++;
    }
    return index;
  }

  function _throttle(callback, ms) {
    var args, _this;

    return function () {
      if (args === void 0) {
        args = arguments;
        _this = this;

        setTimeout(function () {
          if (args.length === 1) {
            callback.call(_this, args[0]);
          } else {
            callback.apply(_this, args);
          }

          args = void 0;
        }, ms);
      }
    };
  }


  // Export utils
  Sortable.utils = {
    on: _on,
    off: _off,
    css: _css,
    find: _find,
    bind: _bind,
    is: function (el, selector) {
      return !!_closest(el, selector, el);
    },
    throttle: _throttle,
    closest: _closest,
    toggleClass: _toggleClass,
    dispatchEvent: _dispatchEvent,
    index: _index
  };


  Sortable.version = '1.0.1';


  /**
   * Create sortable instance
   * @param {HTMLElement}  el
   * @param {Object}      [options]
   */
  Sortable.create = function (el, options) {
    return new Sortable(el, options);
  };

  // Export
  return Sortable;
});
