// PageLines Tools Initializer

!function ($) {

  // --> Initialize
  $(document).ready(function() {
  
    $.plEditing.init()

    $.plEditing.bindIFrame()
  })




  /**
   * The master controller class for editing tools
   * Controls Sidebar, tool activation, standard UI bindings and loading
   * 
   */
  $.plEditing = {

    /**
     * Initialize editing on parent document ready
     */
    init: function(){

      var that = this

      that.setupWPAdminBar()

      /** Attach UI bindings */
      that.bindUIActions()

      /**  An attribute that gets an array of functions to run when a tool is closed. */
      that.closeScripts = {};


    }, 

    bindIFrame: function(){

      var that = this,
          tool = 'plBuilder'

      $('iframe.site-frame').on("load", function () {
        
        that.startUI()

        /** Get outta my house with the admin bar in the frame */
        if( $i('#wpadminbar').length > 0 ){
          $i('#wpadminbar').hide()
          $i('html').css('cssText', 'margin-top: 0px !important;')
        }


        if( $('.tool-active').first().length > 0 && ! true == $('.pl-workarea-sidebar').data('persist') ){

          that.editingAction( $('.tool-active').first().attr('rel') )

        } 
        else if( getURLParameter('start') == 'yes' ){

        

          if( getURLParameter('pl_tool') ){
            tool = getURLParameter('pl_tool')
          }

          
          that.editingAction( tool, false, $( sprintf('[rel="%s"]', tool ) ).first() )

        }

      
      
      })

    },

    /** 
     * Bind UI Interactions and events
     */
    bindUIActions: function(){

      var that = this,
          tool = 'plBuilder'

      that.doToolbarBinding()

    }, 

    setupWPAdminBar: function(){

      var that = this


      $('.pl-ab-save > a').addClass('toolbar-save').attr('data-action', 'save').data('action', 'save')

  
    },

    doToolbarBinding: function(){

      var that = this


      $('.pl-ab-item > a').on('click', function(e){

        e.preventDefault();

        $('body').click()

        var clicked = $(this)

        that.clickTool( clicked )
        
      })

    }, 

    startUI: function(){

      var that = this




      /** Marker Class */
      $i('body')
        .addClass('pldd-active')

      /** Add section edit buttons */
      $i('.pl-sn').each( function(){

        $(this)
          .children('.pl-sn-wrap')
          .prepend( that.getSectionToolbar( $(this) ) ) 
        
      })

      /** Bind section edit buttons */
      $i('.pldd-control')
        .on( 'click.pldd', function(e){

          e.preventDefault();
          e.stopPropagation();
          
          $i('body').click()

          that.clickTool( $(this) );
          
        })



    }, 

    reloadUI: function(  ){

      var that = this


      that.closeTools()
      that.shutdownEditing()

      that.startUI()

      

    }, 

    /** 
     * Calls a function based on the tool being loaded and activated
     */
    clickTool: function( clicked ){

      var that   = this

      that.editingAction( clicked.attr('rel'), clicked.data('action'), clicked )

    },

    editingAction: function( tool, action, clicked ){

      var that           = this, 
          action         = action   || false, 
          tool           = tool     || false, 
          clicked       = clicked  || false,
          prefixTool     = 'toolbar_', 
          prefixAction  = 'action_', 
          activeClass   = 'tool-active'
        
        if( action ){

          that.callFunction( prefixAction + action, clicked )

        }

        if( tool ){

          $('.'+activeClass).removeClass( activeClass )

          $( sprintf('[rel="%s"]', tool ) ).addClass( activeClass )

          
          that.callFunction( 'init', clicked, tool )

        }

    }, 

    callFunction: function( functionName, clicked, object ){

      var that           = this
      
      var functionObj = ( plIsset(object) ) ? $[ object ] : that


      if ( plIsset(functionObj) && $.isFunction( functionObj[ functionName ] ) ){
        functionObj[ functionName ].call( functionObj, clicked )
      }

      else 
        console.log('PageLines: ' + object + ':' + functionName + ' does not exist.')

    }, 

    



    shutdownEditing: function( ){


      /** Remove editing classes on body */
      $i( 'body' )
        .removeClass('pldd-active pldd-editing')

      /** Remove added in page items */
      $i( "[class*=pldd-]" ).remove()
      

    },

    

    /**
     * Uses array set by the tools and iterates through performing the close operations
     * @return {action} runs close actions
     */
    closeTools: function( ){

      $.each( this.closeScripts, function(index, callback){

        if ( $.isFunction( callback ) )
          callback.call( this )

      })

      this.closeScripts = {};

    },

    



    /** SIDEBAR  ********************************************************/

    /**
     * Load the editor sidebar with HTML content and other configuration 
     * @param  {object} config sidebar configuration
     * @return {action} Draws and shows sidebar
     */
    loadSidebar: function( config ){

      var defaults = {
            title:     '', 
            header:   '',
            sub:       '', 
            cont:     '', 
            key:       '', 
            persist:   0
          }

      config = $.extend( defaults, config ) 
      
      var that     = this, 
          sub     = ( config.sub    !== '' ) ? sprintf('<div class="sb-sub">%s</div>', config.sub) : '',
          title   = ( config.title !== '' ) ? sprintf('<div class="sb-title"><div class="the-title">%s</div>%s</div>', config.title, sub) : '', 
          header   = ( config.header !== '' ) ? sprintf('<div class="sb-header"><div class="the-header fix">%s</div></div>', config.header) : '', 
          content = header + title + config.cont

      $('body').addClass('pl-has-sidebar')

      $('.pl-workarea-sidebar')
        .html( content )
        .data('key', config.key )
        .attr('data-key', config.key )
        .data('persist', config.persist )
        .attr('data-persist', config.persist )

      PLWorkarea.iframe.contents().bind( 'click.sidebarClose', that.closeSidebar);

    },

    itemToggles: function(){

      

       /** Add toggle icons */
      $('.item-toggle')
        .not('.loaded')
        .each( function(){

          var theItem = $(this).parent()

          $(this).append(' <i class="pl-icon pl-icon-caret-down"></i><i class="pl-icon pl-icon-angle-up"></i>')

          var key   = $(this).text(),
              state = plGetTabState( key )

          

          if( state == 'open' ){
              theItem
                .addClass('item-open')
                .removeClass('item-closed')
          } 

          else if( state == 'closed' ){

            theItem
              .addClass('item-closed')
              .removeClass('item-open')

          }

        })
        


      

       /** Accordion style panel titles */
      $('.item-toggle')
        .not('.loaded')
        .on('click', function(){
        
          var theItem = $(this).parent(),
              key     = $(this).text()

          if( theItem.hasClass('item-closed') ){

            theItem
              .addClass('item-open')
              .removeClass('item-closed')

            plSetTabState( key, 'open' )
          
          } 

          else {
            theItem
              .addClass('item-closed')
              .removeClass('item-open')

            plSetTabState( key, 'closed' )
          }
          
        }).addClass('loaded')

    }, 

    /**
     * Bind standard sidebar UX elements after render.
     */
    bindSidebar: function(){

      var that = this


       /** Reference on click */
       $('.btn-ref').on('click.ref', function(){

        var theRef   = $(this).parent()
        ,  help   = $(this).next()

        if( theRef.hasClass('ref-open') ){
          theRef.removeClass('ref-open')
          help.slideUp()
        } else {
          theRef.addClass('ref-open')
          help.slideDown()
        }

      })


     /** Standard Dropdown Handling */
     $('.pl-dropdown-toggle').on('click', function(){

       var theDropdown = $(this).parent().find('.pl-dropdown-menu')

       if( theDropdown.hasClass('show') ){
         theDropdown.removeClass('show')
       }
       else{
         theDropdown.addClass('show')
       }

     })

      that.itemToggles()

       


      $('.pl-tools-list').on( 'click', '.tools-bar', function(){

        var theItem = $(this).parent()

        if( ! theItem.hasClass('item-open') ){

          $('.pl-tools-list li')
            .removeClass('item-open')

          $('.pl-tools-list').find('.tools-panel').hide()


          theItem
            .addClass('item-open')

          theItem.find('.tools-panel').fadeIn()
        
        } 

        else {

          theItem.removeClass('item-open')

          $('.pl-tools-list').find('.tools-panel').hide()

        }
        
        

      })

      /** Generalized Action Select */
      $('.select-action').on('change', function(){


        var functionName   = 'action_' + $(this).data('action')
        
        if ( $.isFunction( that[ functionName ] ) )
          that[ functionName ].call( that, $(this) )
        else
          console.log('Action not found: '+functionName)

        /** Set back to default option */
        $(this).val('')

        return false;
      })

      /** Generalized Action Select */
      $('.tool-action').on('click', function(){

        var action       = $(this).data('action'),
          functionName   = 'toolbar_' + action

        /** Activate proper highlights */
        $('.tool-active').removeClass('tool-active')

        $( sprintf('[data-action="%s"]', action) ).addClass('tool-active')

        
        if ( $.isFunction( that[ functionName ] ) )
          that[ functionName ].call( that, $(this) )
        else
          console.log('Action not found: '+functionName)

  
      })

      


    },   

    /** Closes sidebar from view. */
    closeSidebar: function( e ){

      var that   = this, 
        e     = e || false
        target  = (e) ? e.target : false

      /** We check the target to make sure its not an element that shouldn't close sidebar */
      if( ! target || $(target).closest('.dropdown').length == 0 ){

        PLWorkarea.iframe.contents().unbind('click.sidebarClose');

        /** Remove the active class from Workarea nav item, disabling sidebar always turns off */
        $('.tool-active').removeClass('tool-active')


        $i('.editing-section').removeClass('editing-section')
        
        $('body').removeClass('pl-has-sidebar')

      } 
      
    },

    sidebarEngine: function( config ){

      var that     = this, 
          HTML     = '', 
          cnt     = 1, 
          panels   = config.panels, 
          call     = config.call


      var defaults = {
            header: '', 
            closed: 'none'
          }

      config = $.extend( defaults, config )

      var sbConfig = {
            title:       sprintf( '<i class="pl-icon pl-icon-pencil"></i> %s', config.name),
            header:     config.header,
            cont:       sprintf( '<div class="workarea-opts-form" data-level="%s"><div class="pl-loader"><i class="pl-icon pl-icon-spin pl-icon-cog"></i></div></div>', config.level, 'HTML' ),
            key:         config.key,
            persist:    config.persist
          };

      $.plEditing.loadSidebar( sbConfig )
      
      setTimeout(function(){

        /** Initialize the option engine, only do this once */
        $.engineOpts.init()


        /** Get HTML for all options related to section */
        HTML += sprintf("<div class='panel-accordion'>");

        $.each( panels , function( index, panel ) {

          

          var closeClass   = ( (config.closed == "all" && cnt !== 1) ) ? 'item-closed' : '', 
              format       = panel.format || 'options'

          HTML += sprintf( '<div class="opt-panel itemset %s"><div class="panel-title item-toggle">%s</div>', closeClass, panel.title );

          if( typeof panel.opts == 'undefined' || $.isEmptyObject( panel.opts ) )
            panel.opts = [{ label: plTranslate('no_custom_options'), help: plTranslate('no_custom_options_added') , type: "help" }];
        
          HTML += sprintf('<div class="panel-opts item-contents format-%s">%s</div></div>', format, $.toolEngine.runEngine( panel.opts ) );

          cnt++

        })

        HTML += sprintf("</div>");


        $('.workarea-opts-form').html( HTML )


        if( $.isFunction( call ) ){
          call.call( that )
        }

        /** Run options JS */
        $.engineOpts.runScripts()

        /** Bind standard sidebar UX elements */
        that.bindSidebar()

      }, 150)
      

    }, 

    
    /**
     * Show notifications from PHP actions
     */
    // loadNotifications: function(){

    //   var that = this

    //   if( !_.isEmpty( PLWorkarea.notifications )){

    //     $.each( PLWorkarea.notifications, function(i, item){
        
    //       that.showNotification( item )

    //     } )

    //   }

      

    // }, 

    /** TOOLS  ********************************************************/
    


    action_dropdown: function( btn ){

      var that     = this, 
        container   = btn.parent()

      that.removeLogoDrop()

      if( ! container.hasClass('show-drop') )
        container.addClass('show-drop')
      else{
        btn.removeClass('tool-active')
        container.removeClass('show-drop')
      }

      PLWorkarea.iframe.contents().on('click.logoDropClose', function(){
        that.removeLogoDrop()
      })

      

      $('body').on( 'click.logoDropClose', function(e){

        var e     = e || false
          target  = (e) ? e.target : false

        if( ! target || $(target).closest( '.logo-dropdown' ).length == 0 )
          that.removeLogoDrop()

      });

    },

    removeLogoDrop: function(){

      $('.show-drop')
        .removeClass('show-drop')
        .find('.tool-active')
          .removeClass('tool-active')

      $('body').unbind('click.logoDropClose');

      PLWorkarea.iframe.contents().unbind('click.logoDropClose');

    }, 


    /**
     * Save data from page. 
     * @return {action} saves page map configuration and section data.
     */
    action_save: function(){

      var that     = this

      that.savePage()
        

    },

    savePage: function( config ){

      var that         = this, 
          config       = config || {},
          modelJSON   = $plModel().getJSON(),
          mapData     = $pl().config.tplMap

      
      config = $.extend( { map: mapData, model: modelJSON }, config ) 

      /** Allow plugins to add data to be saved through adding that data to the extraData variable. */
      config = $.extend( $pl().extraData, config ) 

      that.savePageConfig( config );

    }, 

    /**
     * Saves the pages data based on a map/model
     * @param  {array} map   the map of the page sections
     * @param  {array} model the page data model from knockout
     */
    savePageConfig: function( config ){

      var that = this, 
        args = {
          hook:   'save_page',
          postSuccess: function( rsp ){

            that.setNeedsSave( 'no' )            

            $.plEditing.showNotification('saved!')

          },
          beforeSend: function( ){

            that.setNeedsSave( 'active' )

          }
        }

      config = $.extend( args, config ) 

      $plServer().run( config )

    }, 

    /**
     * Sets save button state when save is required, active or not...
     */
    setNeedsSave: function( state ){

      var state = state || 'yes' 

      /** Doesn't need save */
      if ( state == 'no' ){

        window.needsSave = false 

        /** Close save notification */
        $('.toolbar-save')
          .removeClass('needs-save')
          .html('<i class="pl-icon pl-icon-ok"></i> Page Saved')
          .parent()
            .removeClass('show-save')

        $("body").css("cursor", "default");

        $iWindow().onbeforeunload = null;

      }

      /** Currently saving */
      else if ( state == 'active' ){

        $('.toolbar-save')
          .html('<i class="pl-icon pl-icon-cog pl-icon-spin"></i> Saving Changes')
          .parent()
            .addClass('show-save')

        $("body").css("cursor", "progress");

      }

      /** Needs saving. */
      else {

        window.needsSave = true 

        $('.toolbar-save')
          .addClass('needs-save')
          .html(sprintf( '<i class="pl-icon pl-icon-upload"></i> %s', plTranslate( 'save_changes' ) ) )
          .parent()
            .addClass('show-save')

        $iWindow().onbeforeunload = function() { 
          
          return sprintf('Save your changes! %sYou will lose your unsaved edits if you continue.', "\n"); 
        }

      } 
      

    }, 

    /**
     * Does the page need a save?
     */
    needsSave: function(){

      if( $('.toolbar-save').hasClass('needs-save') )
        return true; 
      else
        return false;

    }, 

    /**
     * Shows a temporary notification in the workarea header
     * @param  {html} html the string/html for the notification
     * @param  {int} time the time in ms it should show for...
     */
    showNotification: function( html, time){

      var time = time || 1000

      $('.toolbar-notification')
        .html( html )
        .addClass('active')

      setTimeout(function() {
          $('.toolbar-notification')
            .removeClass('active')
      }, time);


    }, 



    /**
     * Activate templates controls
     */
    toolbar_layouts: function( clicked ){
      
      var that = this

      $.plTemplates.init( clicked )

    },

    /**
     * Activate template builder controls
     */
    toolbar_builder: function( clicked ){
      
      var that = this

      $.plBuilder.init( clicked )

    },


    

    

    toolbar_code: function( clicked ){

      var that   = this


      $.plCode.init( clicked )
    },


    toolbar_static: function( ){
      
      var that     = this, 
        template  = $i('.static-template')

      $('body').click()
      
      var config = {
          UID:     template.data('clone'),
          theClass:   'template', 
          level:     0
      }

    
      $.toolEngine.render( config )
    
    },




    

    getSectionToolbar: function( section ){

      var that  = this, set, type, name
      var clone = section.data('clone')
      var title = false
      var cName = $plModel().getSectionOption( clone, 'custom_name' ) || false
      name  = name || sectionNameFromObject( section.data('object') )
    
      name = ( cName ) ? sprintf( '%s %s', cName, name ) : name
   
      return sprintf('<div class="pldd-section-bar pldd-bar pl-border"><div class="pldd-bar-tools pldd-section-tools"><a class="bar-control pldd-control" data-action="options"><i class="pl-icon pl-icon-pencil"></i>%s</a></div></div>', name )
    },

    
    
    action_options: function( btn ){
      
      
      var that     = this, 
          section = btn.closest(".pl-sn")

  
      that.loadSectionOptions( section )
      
    },

    loadSectionOptions: function( section, config ){

      var config = config || {}

      $i('.editing-section').removeClass('editing-section')

      section.addClass('editing-section')

      header = sprintf('<div class="tool-action" data-action="builder_show"><i class="pl-icon pl-icon-caret-left"></i> &nbsp;%s</div>', plTranslate( 'show_in_builder' ) )
      header += sprintf('<div class="tool-action send-right" data-action="delete"><i class="pl-icon pl-icon-remove"></i> %s</div>', plTranslate( 'delete'))

      mode = ( section.hasClass('pl-sn-static-content') ) ? 'static' : 'standard';
      
      var configEngine = {
            UID:         section.data('clone'),
            theClass:   section.data('object'), 
            level:       section.data('level'), 
            header:     header, 
            mode:       mode
      }

      config = $.extend( {}, config, configEngine )

      
      $.toolEngine.render( config )

    }, 

    
    action_tpl_scope: function( select ){

      var that     = this, 
          selText   = select.find('option:selected').text(), 
          selVal     = select.val()

      $.plFrame.reloadFrame( { tplScope: selVal } )
    }, 

    

    action_tpl_capture: function( select ){

      var that     = this, 
        selText   = select.find('option:selected').text(), 
        selVal     = select.val()
  
      $.plFrame.reloadFrame( { tplCapture: selVal } )

      //$.plEditing.setNeedsSave()

    }, 


    reorderIcon: function(){
      return '<i class="pl-icon pl-icon-reorder dd-reorder"></i>'
    },

    deleteSection: function( clicked, clone, callback ){

      var that       = this, 
          section   = $i( sprintf('[data-clone="%s"]', clone) ), 
          listitem   = $('.pl-builder-list').find( sprintf( '[data-clone="%s"]', clone ) )

      plConfirm( clicked, {
        subhead: plTranslate( 'remove_from_page' ), 
        callback: function(){

        //  $.plEditing.sectionDelete( section ) // recursive function

          $pl().config.tplMap = that.parseMap( function( set ){

            

            if( clone == set.meta.clone || $.inArray( clone, set.path ) !== -1 ){


              if( plIsset( $pl().modelData[ set.meta.clone ] ) )
                delete $pl().modelData[ set.meta.clone ]

              delete set.map[ set.index ]

            }

            return set.map

          })

          $.plEditing.setNeedsSave()


          $('.pl-builder-list').find( sprintf( '[data-clone="%s"]', clone ) ).slideUp('slow', function(){
            $(this).remove()
          })

          section.slideUp( 'slow', function(){
            $(this).remove()
          })

          if( $.isFunction( callback ) ){
      
            callback.call( that, clicked, section )
          }
        } 
      })

    },

    toolbar_builder_show: function( clicked ){

      var that       = this, 
          clone     = clicked.closest('.pl-workarea-sidebar').data('key'),
          section   = $i( sprintf('[data-clone="%s"]', clone) ) 

          clicked.data( 'clone', clone )

          $jq().plScrolling.scrollToSection( section )

          that.editingAction('plBuilder', false, clicked )

    }, 

    toolbar_delete: function( clicked ){

      var that       = this, 
          clone     = clicked.closest('.pl-workarea-sidebar').data('key')

          that.deleteSection( clicked, clone, function(){ that.toolbar_builder( ) })


    }, 


  
    parseMap: function( callback, path, level, map ){

      var that       = this,
          map       = map || $pl().config.tplMap,
          path       = path || [], 
          level     = ( plIsset( level ) ) ? level : -1, 
          nextLevel = level + 1

        $.each( map, function( index, meta ){

          // This is a weird function. It stays linked to the variable, 
          // so we have to slice it e.g. duplicate the array and send that to the call back.
          path.splice( nextLevel, 100, meta.clone)
          
          var currentPath = path.slice()

          

          map[ index ].content = that.parseMap( callback, path, nextLevel, meta.content )
        
  

          if( $.isFunction( callback ) ){
      
            var set = {
              map:     map, 
              index:   index, 
              meta:   meta, 
              path:   currentPath, 
              level:   level
            }
            map = callback.call( that, set )
          }

        })

        return map

    },


  }





}(window.jQuery);
