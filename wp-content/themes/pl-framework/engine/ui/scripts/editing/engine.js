 !function ($) {

  $.toolEngine = {

    defaults: {
      theClass:  '', 
      UID:     '', 
    }, 

    render: function( config ) {

      var that = this

      that.config   = $.extend({}, that.defaults, config)

      that.UID       = that.config.UID

      that.level     = that.config.level

      that.factory  = PLWorkarea.factory

      that.object   = that.config.theClass

      that.name      = that.factory[ that.object ].name


      that.master   = that.createMasterOptionsArray( config.mode )

      that.drawSectionOptions( config )

      //that.inputListener()

    }, 

    createMasterOptionsArray: function( mode ){

      var that   = this,
        master   = {}

      if( mode == 'static' ){

        var sectionOptions = {
              sections: {
                title:   'Content Settings', 
                opts:   $pl().config.templateOpts
              }
            }
      }

      else {

        var sectionOptions = {
              sections: {
                title:   'Configuration', 
                opts:   that.factory[ that.config.theClass ].opts
              }
            }

        

      }

      master = $.extend({}, sectionOptions, $.plStandardSettings.settingsArray())
      
      return master;

    }, 

      
    drawSectionOptions: function( config ) {

      var that   = this

      var configSidebar = {
            name:     that.name, 
            panels:   that.master, 
            key:       that.UID, 
            level:     that.level, 
            header:   '', 
            closed:   'all',
            call: function(){
              that.inputListener()
            }
          }

      config = $.extend( configSidebar, config )

      $.plEditing.sidebarEngine( config )

    }, 

    runEngine: function( opts ){

    

      var that = this,
        HTML = ''

      $.each( opts , function(index, o) {
        HTML += that.optEngine( o )
      })
      
      return HTML
    

    }, 

    sanitizeValue: function( val ){

      var that = this

      if( _.isObject(val) || _.isArray(val) ){

        $.each(val, function( i, v ){
          val[i] = that.sanitizeValue( v )
        })
      }

      else {
        val = pl_html_input( val )
      }

      return val 
    }, 

    optGetValue: function( key ){

      var that     = this,
          pageData   = $plModel().getData(),
          value     = ''

  
      if( pageData[ that.UID ] && pageData[ that.UID ][ key ] ){
        
        value = that.sanitizeValue( pageData[ that.UID ][ key ] )

      }
      

      return value

    }, 

    optAddMeta: function( o, option_meta ) {

      var that           = this,
          option_meta   = option_meta || {}
      
      /** If we are in an accordion, handle accordingly using option_meta */
      if( typeof option_meta.key !== 'undefined' ){
        
        o.ind     = option_meta.ind
        o.value   =  ( option_meta.vals[ o.key ] != '' ) ? option_meta.vals[ o.key ] : ''
        key       = sprintf('%s__%s__%s', option_meta.key, option_meta.ind, o.key)

      }

      else{
        key = o.key
      }

      var meta = {
          place:       '',
          classes:     '',
          name:       key, 
          value:       that.optGetValue( key ), 
          inputID:     key, 
          callback:   that, 
          label:       ''
      }
      
      return $.extend( meta, o );

    },

    

    optEngine: function( o, option_meta ) {

      var that           = this,
          out           = '',
          option_meta   = option_meta || {}, 


    
      o = that.optAddMeta( o, option_meta )

      if( 
        !_.isEmpty( o.opts ) && 
        o.type != 'accordion' && 
        o.type.indexOf("select") < 0 && 
        o.type.indexOf("radio") < 0 && 
        o.type != 'dragger' 
      ){
      
        $.each( o.opts , function( index, osub ) {

          out += that.optEngine( osub, option_meta ) // recursive

        })
      }

      if( 
        typeof o.type == 'undefined' || 
        o.type == 'multi' || 
        o.type == 'get_posts' 
      ){
      
        // do nothing.

      }else if( o.type === 'accordion' ){

        

        out += $.engineOpts.accordion( o );



      }

      else if( o.type === 'text' || o.type === 'text_small' ){
        
        out += $.engineOpts.textoption( o );

      }

      else if( o.type === 'richtext'){
        
        out += $.engineOpts.richtext( o );

      }

      else if( o.type === 'textarea' || o.type === 'html' ){

        out += $.engineOpts.textarea( o );

      }

      else if( o.type === 'check' ){

        out += $.engineOpts.checkoption( o );

      }

      else if( o.type === 'image_upload' ){

        out += $.engineOpts.imagedropzone( o );

      }

      else if( o.type === 'color' ){

        out += $.engineOpts.coloroption( o );

      }

      else if( o.type === 'media_select_video' ){

        out += $.engineOpts.videooption( o );

      }

      else if( o.type === 'edit_post' ){

        out += sprintf('<a href="%s" class="pl-btn pl-btn-primary pl-btn-xs %s" >%s</a>', PLWorkarea.editPost, o.classes, o.label )

      }

      else if ( o.type === 'link' ){

        var target = o.target || '_blank'

        out += sprintf('<div><a href="%s" class="pl-btn pl-btn-default pl-btn-xs %s" target="%s">%s</a></div>', o.url, o.classes, target, o.label )
      }

      else if ( o.type === 'button_link' ){

        out += $.engineOpts.buttonLink( o );
        
        
      }

      else if (
        o.type === 'select' ||
        o.type === 'count_select' ||
        o.type === 'count_select_same' ||
        o.type === 'select_pixels' ||
        o.type === 'select_vw' ||
        o.type === 'select_percent' ||
        o.type === 'select_proportion' ||
        o.type === 'select_same' ||
        o.type === 'select_taxonomy' ||
        o.type === 'select_term' ||
        o.type === 'select_wp_tax' ||
        o.type === 'select_icon' ||
        o.type === 'select_animation' ||
        o.type === 'select_multi' ||
        o.type === 'select_button' ||
        o.type === 'select_button_size' ||
        o.type === 'select_theme' ||
        o.type === 'select_sidebar' ||
        o.type === 'select_padding' ||
        o.type === 'select_imagesizes' ||
        o.type === 'select_menu'
      ){

        out += $.engineOpts.selectoption( o );

      }

      else if (
        o.type === 'radio'
      ){

        out += $.engineOpts.radioOption( o );

      }

      else if (
        o.type === 'dragger'
      ){

        out += $.engineOpts.dragInput( o, option_meta );

      }

      // Adds the help attr as text only, nothing else  
      else if( o.type === 'help'  || o.type === 'help_important' ){

        out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )
        
        
      }



      // Hook type 
      else if(  $.isFunction( o.callback[ 'opt_type_' + o.type ] ) ){

        out += o.callback[ 'opt_type_' + o.type ].call( o.callback, o )
      
      }

      else {
        out = sprintf('<div class="pl-alert pl-alert-warning">Could not find option type: <strong>%s</strong></div>', o.type); 
      }

      var help     = ( typeof o.help     !== 'undefined' )   ? sprintf( '<div class="help-block %s">%s</div>', o.type, o.help)  : '',
          title   = ( typeof o.title     !== 'undefined' )   ? sprintf( '<div class="opt-title item-toggle">%s</div>',   o.title) : '',
          guide   = ( typeof o.guide     !== 'undefined' )   ? sprintf( '<div class="opt-guide">%s</div>',   o.guide) : '',
          ref     = ( typeof o.ref       !== 'undefined' )   ? that.doReference( plTranslate('reference'), o.ref ) : '', 
          stylize = ( typeof o.stylize   !== 'undefined' )   ? o.stylize : '', 
          toggle   = ( typeof o.toggle   !== 'undefined' )   ? 'item-' + o.toggle : ''


      return sprintf('<div class="pl-form-group itemset %s type-%s %s">%s%s<div class="item-contents">%s</div>%s%s</div>', stylize, o.type, toggle, title, guide, out, help, ref);

    },

    doReference: function( title, text ){


      return sprintf( '<div class="pl-dropdown ref-opt"><span class="pl-btn pl-btn-success pl-btn-xs pl-dropdown-toggle" >%s <i class="pl-icon pl-icon-angle-down"></i></span><div class="pl-dropdown-menu dd-ref">%s</div></div>', title, text )
    },

    resetListenerBinding: function(){

      var that = this

      $('.lstn').off('keyup.optlstn blur.optlstn change.optlstn paste.optlstn')

      that.inputListener()
    },

    inputListener: function(){

      $pl().modifyEvent = false

      $('.lstn').on('keyup.optlstn blur.optlstn change.optlstn paste.optlstn', function( e ){
      
        var UID           = $('.pl-workarea-sidebar').data('key'),
            theInput       = $(this),
            theInputType   = theInput.getInputType(),
            theInputID     = theInput.attr('name'),
            theValue       = theInput.val()


        $.plEditing.setNeedsSave( 'yes' )

        console.log( theInput.val() )

        /** Certain actions should only be triggered on more occasional change events, as opposed to keyups, etc.. */
        if( e.type === 'blur' || ( e.type === 'change' && ( theInputType === 'checkbox' || theInputType === 'select' || theInputType === 'radio' || theInputType === 'hidden' ) ) ){
          $pl().changeEvent = true; 
          changeEvent = true
        }

        else {
          $pl().changeEvent = false; 
          changeEvent = false
        }

        modifyEvent = true
        $pl().modifyEvent = true;

        // if has __ then we are in an accordion
        if( theInputID.indexOf("__") >= 0 ){

          var arr         = theInputID.split("__"), 
              accordionID = arr[0], 
              suboptionID = arr[2], 
              indexID     = arr[1] //theInput.closest('.opt-group').data('num')


          $pl().viewModel[ UID ][ accordionID ]()[ indexID ][ suboptionID ]( theValue )

        }

        /** We are doing a regular option type */
        else{

          
          if( ! plIsset( $pl().viewModel[ UID ][ theInputID ] ) ){

            console.log('*** Unset Model Value: ' + theInputID)

            $plModel().setNewObservable(UID, theInputID, theValue)
            
          }



          /** Set change event flag on observable so we can fire some actions only occasionally */
          
          /** This must be set BEFORE the value or there is a one cycle delay */
          $pl().viewModel[ UID ][ theInputID ].changeEvent = changeEvent

          $pl().viewModel[ UID ][ theInputID ].modifyEvent = modifyEvent

          $pl().viewModel[ UID ][ theInputID ]( theValue )    

        

        }    

        /** Allows elements to recompute and animate */
        if( changeEvent ){
          $iWindow().plTrigger( 'change' )
        }

      })

    }, 
 

     
    

  }



}(window.jQuery);
