!function ($) {

  /** Set Squire richtext editor global variable */
  window.squire = {}
  window.lastClicked = false

  /** The Options ENGINE */
  $.engineOpts = {

    init: function(){

      // Run these on load and again after anything is regenerated.
      this.optScripts = {};

    

    },

    /**
     * Run scripts from options
     */
    runScripts: function(){

      
      $.each( this.optScripts, function(index, optEvent){

        if ( $.isFunction( optEvent ) ){
          optEvent.call( this )
        }

      })

    },

    specialOption: function( config ){

      var that = this,
        defaults = {
          title:     '', 
          message:   '', 
          val:     '', 
          valLabel:   'Current:',
          option:   ''
        }


      config = $.extend( defaults, config )  

      //var value = (config.val !== '') ? sprintf('<div class="special-option-value"><label>%s</label><div class="val">%s</div></div>', config.valLabel, config.val ) : ''

      out = sprintf('<div class="alert-help pl-form-group"><div class="itemset item-closed alert-content"><div class="item-toggle"><strong class="alert-title">%s</strong></div><div class="item-contents">%s</div></div> %s</div>', config.title, config.message, config.option )

      return out
    }, 

    selectOption: function( options, value, action, dtext ){

        var dtext     = dtext || 'Select...',
            opts      = sprintf( '<option value="">%s</option>', dtext ),
            disabled   = ( Object.keys(options).length <= 1 ) ? 'disabled' : ''

        $.each( options, function( val, name ){

            var selected = ( val == value ) ? 'selected' : ''

            opts += sprintf('<option value="%s" %s>%s</option>', val, selected, name)
        })

        return sprintf('<select class="pl-form-control select-action" data-action="%s" %s>%s</select>', action, disabled, opts)

  
        
    },

    videooption: function( o ){


      var that   = this, 
        out   = '', 
        o2     = $.toolEngine.optAddMeta( {key: o.key+'_2'} );

      out += '<div class="video-upload-inputs option-group">'
        
      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )

      out += sprintf( '<div class="video-inputs clearfix">' )
          
      out +=  that.addVideoOption( o, "Select MP4 Video")

      out += sprintf( '</div>' )
      

      out += '</div>'

      return out;

      

    }, 

    addVideoOption: function( o, label){
      
      var out = ''
      
      out += '<div class="upload-box media-select-video">'
      
      out += sprintf('<label for="%s">%s</label>', o.inputID, label )

      out += sprintf('<input id="%1$s" name="%2$s" type="text" class="lstn pl-form-control upload-input" placeholder="" value="%3$s" />', o.inputID, o.name, o.value )
      
      out += '<a class="pl-btn pl-btn-xs pl-btn-primary pl-load-media-lib" data-mimetype="video"><i class="pl-icon pl-icon-edit"></i> Select</a> '

      out += sprintf(' <a class="pl-btn pl-btn-default pl-btn-xs" href="%s" target="_blank"><i class="pl-icon pl-icon-upload"></i> Upload</a> <div class="pl-btn pl-btn-default pl-btn-xs pl-image-remove"><i class="pl-icon pl-icon-remove"></i></div>', PLWorkarea.addMediaURL)
      
      out += '</div>'
      
      return out
    }, 

    textoption: function( o ){

      var out = '';

      out += sprintf( '<label for="%s">%s</label>', o.inputID, o.label );
      out += sprintf( '<input type="text" id="%s" name="%s" class="%s lstn pl-form-control" placeholder="%s" value="%s" />', o.inputID, o.name, o.classes, o.place, o.value, o.inputID);

      return out;
    }, 

    textarea: function( o ){

      var out     = '', 
        special   = ( o.type == 'html' ) ? 'html-textarea' : '' 

      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )
      out += sprintf('<textarea id="%s" name="%s" class="%s lstn pl-form-control %s" placeholder="%s">%s</textarea>', o.inputID, o.name, o.classes, special, o.place, o.value )

      this.optScripts.textarea = function(){
        
        
      }
    
      return out;
    }, 

    richtext: function( o ){

      var out = '';



      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )

      out += sprintf('<div class="richtext-controls noselect" data-id="%s">', o.inputID)
        out += '<span class="ctrl" data-action="bold" data-tag="b"><i class="pl-icon pl-icon-bold"></i></span>'
        out += '<span class="ctrl" data-action="italic" data-tag="i"><i class="pl-icon pl-icon-italic"></i></span>'
        out += '<span class="ctrl" data-action="underline" data-tag="u"><i class="pl-icon pl-icon-underline"></i></span>'
        out += '<span class="ctrl" data-action="align" data-tag="left"><i class="pl-icon pl-icon-align-left"></i></span>'
        out += '<span class="ctrl" data-action="align" data-tag="center"><i class="pl-icon pl-icon-align-center"></i></span>'
        out += '<span class="ctrl" data-action="align" data-tag="right"><i class="pl-icon pl-icon-align-right"></i></span>'

        out += '<span class="pl-dropdown">'
          out += '<span class="ctrl pl-dropdown-toggle"><i class="pl-icon pl-icon-chevron-up"></i></span>'
          out += '<div class="pl-dropdown-menu" >'
            out += '<span class="ctrl" data-action="format" data-tag="h1">H1</span> <span class="ctrl" data-action="format" data-tag="h2">H2</span> <span class="ctrl" data-action="format" data-tag="h3">H3</span><span class="ctrl" data-action="format" data-tag="h4">H4</span><span class="ctrl" data-action="format" data-tag="p">P</span>'
        out += '</div></span>'
      out += '</div>'

      /** Richtext loads in an iframe, on load we get a top level variable editor[ inputID ] to use for API  */
      out += sprintf('<iframe class="richtext-frame noselect" src="%s/plugins/squire/document.html" data-inputid="%s"></iframe>', PLWorkarea.PLUI, o.inputID);  

      out += sprintf('<textarea id="%s" name="%s" class="%s richtext-textarea lstn pl-form-control"  style="">%s</textarea>', o.inputID, o.name, o.classes, o.value )

      out += '<div class="richtext-toggle pl-btn pl-btn-default pl-btn-xs">Toggle <span class="sel-richtext">Rich Text</span><span class="sel-rawtext">Raw Text</span></div>'

        
      this.optScripts.richtext = function(){

        $('.richtext-frame').on('load', function(){

          var inputID   = $(this).data('inputid'), 
            textInput   = $(this).next()

          

          top.squire[ inputID ] = this.contentWindow.editor

          top.squire[ inputID ].setHTML(textInput.val())

          $(top.squire[ inputID ]).on('input', function(){

            textInput.val(  top.squire[ inputID ].getHTML() ).trigger('change')
          })

          textInput.on('keyup', function(){

            top.squire[ inputID ].setHTML( $(this).val() )

          })


          /** 
           * Dont trigger blur event if they are clicking one of the richtext controls
           * There is no standard way to do this so we set a global and check if they click the controls 
           */

          $('.richtext-controls').on('click', function(e) {
            
            lastClicked = true
          });

          $(top.squire[ inputID ]).on('blur', function(e){

            setTimeout(function(){


              if( ! lastClicked ){
                textInput.trigger('blur')
                

              }

              lastClicked = false
              
              
             }, 200)
    
          })

        })

        // $('.pl-dropdown-toggle').on('click', function(){

        //   var theDropdown = $(this).parent().find('.pl-dropdown-menu')

        //   if( theDropdown.hasClass('show') ){
        //     theDropdown.removeClass('show')
        //   }
        //   else{
        //     theDropdown.addClass('show')
        //   }

        // })
        
        /**
         * Toggle editor from rich to raw
         */
        $('.richtext-toggle').on('click', function() {
          var textarea = $(this).parent().find('.richtext-textarea')
          var controls = $(this).parent().find('.richtext-controls')
          var frame    = $(this).parent().find('.richtext-frame')
          var sel_rich = $(this).find('.sel-richtext')
          var sel_raw  = $(this).find('.sel-rawtext')
                    
          $([textarea, controls, frame, sel_raw, sel_rich ]).each( function(){
            $(this).toggle()
          })
        })
        
        $('.richtext-controls [data-action]').on('click', function(e){

      
          var optID   = $(this).closest('.richtext-controls').data('id'),
            id     = $(this).data('action'),
            tag   = $(this).data('tag') || false,
            editor   = squire[ optID ],
              value


          if( id == 'format' ){

            if ( id && editor ) {

              editor.changeFormat({
                  tag: 'SPAN',
                  attributes: {
                      'data-type'  : 'size',
                      'class'    : tag
                  }
              }, {
                  tag: 'SPAN',
                  attributes: { 'data-type': 'size' }
              })

              //editor.setFontSize( tag )

              

            } 
          } 

          else {

            if( id == 'align' ){
              id     = 'setTextAlignment'
              value   = tag
            }  


            else if(tag && editor.hasFormat( tag ))
              id = 'remove' + capitalizeFirstLetter( id )



            if ( id && editor && editor[ id ] ) {

              editor[ id ]( value );
            }


          }

          
          

        })

      }

      return out;
    }, 

    
    dragInput: function( o, option_meta ){


      var that   = this,
          out   = '', 
          unit   = o.unit || 'px',
          scale = o.scale || 1

      /**
       * The basic handling of drag inputs
       */
      out += sprintf( '<label class="label-block" for="%s">%s <a class="dragger-reset">reset</a></label>', o.inputID, o.label );

      out += sprintf( '<div class="form-inline dragger-option">');

      _.each( o.opts, function( field ) {


        field = $.toolEngine.optAddMeta( field, option_meta )

        var max   = field.max   || 1000,
            min   = field.min   || 0,
            def   = field.def   || 0
          
        
        unit   = ( plIsset( field.unit) ) ? field.unit : unit

        
        out += sprintf( '<div class="pl-input-group"  ><div class="pl-input-group-addon"><i class="pl-icon pl-icon-%s"></i></div><input id="%s" name="%s" type="text" class="pl-form-control dragger lstn" placeholder="" value="%s" data-max="%s" data-min="%s" data-default="%s" data-scale="%s"><span class="pl-input-group-addon">%s</span></div>',  field.icon, field.inputID, field.inputID, field.value, max, min, def, scale, unit);

      })

      
      this.optScripts.dragInput = function(){
        

        $('.dragger').not('.loaded').on('change', function(e){

          var value         = $(this).val(),
              checkedValue   = (value != '' ) ? parseFloat( value ) : ''; 
              max           = $(this).data('max'),
              min           = $(this).data('min')

              //.replace("[^\\d.]", "") 
          if( checkedValue > max )
            checkedValue = max
          else if( checkedValue < min && checkedValue !== '' )
            checkedValue = min
          else if( _.isNaN(checkedValue) ){
            checkedValue = ''
          }

          
          /** Round to 1 decimal */
        //  checkedValue = Math.round( checkedValue * 10 ) / 10

          if( value != checkedValue )
            $(this).val( checkedValue )
        
        }).addClass('loaded')

        /** Reset back to default */
        $('.dragger-reset').on('click', function(){

          $(this).parent().parent().find('.dragger').val('').trigger('keyup').trigger('change')

        }).addClass('loaded')

        /** Drag to change value */
        $(".dragger-option .pl-input-group")
          .not('.loaded')
          .on( 'mousedown.dragdistance', function(e) {

            var that           = this,
                startingTop   = e.pageY, 
                theInput       = $(that).find('.dragger'),
                 preVal         = theInput.val(), 
                 throttle       = 2, 
                 scale         = theInput.data('scale'), 
                 toFixed       = (scale >= 1) ? 0 : 1

            preVal = ( preVal === '' ) ? theInput.data('default') : preVal
             
              $(document).on( 'mousemove.dragdistance', function(e2) {
                 
                 var   math       = Math.round( ( startingTop - e2.clientY ) / throttle ) * scale,
                       newVal     = Number(1 * preVal + math).toFixed(toFixed)      // multiply by one to give it an integer value w/o dealing with NaN 
                  
                 theInput.val( newVal ).trigger('keyup');

              });

          }).addClass('loaded')

        // $('.dragger').bind('keydown', 'up', function(e){

        //   e.preventDefault()

        //   var val   = parseFloat($(this).val()), 
        //       scale = $(this).data('scale')

        //       console.log(val + scale + 'with scale')

        //   $(this).val( val + scale )
         
        // });
        // $('.dragger').bind('keydown', 'down', function(e){

        //   e.preventDefault()
          
        //   var val   = parseFloat($(this).val()),
        //       scale = $(this).data('scale')
          
        //   $(this).val( val - scale )
        
        // });

        /** On mouseup make sure to remove the event from both parent and iframe */
          $(document).on( 'mouseup.dragdistance', function() {
            $(document).off("mousemove.dragdistance");
          });

          /** This should probably be generalized to all iframes */
          $('.richtext-frame').on('load', function(){
            $( $('iframe.richtext-frame').contents() ).on( 'mouseup.dragdistance', function() {
              $(document).off("mousemove.dragdistance");
            });
          })
          

          $iDocument().on( 'mouseup.dragdistance', function() {
                $(document).off("mousemove.dragdistance");
            });
      }
      
    
      out += sprintf( '</div>');

      return out;


    }, 


    coloroption: function( o ){

      var out     = '', 
          theID   = o.inputID

      out += sprintf('<label for="%s">%s</label>', theID, o.label )
      out += sprintf('<div class="coloroption"><input id="%s" class="pl-colorpicker  lstn" type="text" name="%s" value="%s"/></div>', theID, o.name, o.value )

      this.optScripts.coloroption = function(){

        

        // color pickers, using WP colorpicker API
        $('.pl-colorpicker').not('.loaded').each( function(){

          

          $(this).wpColorPicker({
            change: function(e, ui){ 
          
              /** Have to set value manually because apparently this fires before the input value is set. Thus causes a one-click delay if we dont. */
              $(this).val( $(this).iris('color') ).trigger('keyup')
          
            }
          }).addClass('is-ready loaded')

        }) 

        

        /** If cleared, trigger keyup event so obs are notified */
        $('.wp-picker-clear').not('.loaded').on('click', function(){ 

          $(this).prev().trigger('keyup')

        }).addClass('loaded')
      }

      return out;
    }, 

    checkoption: function( o ){

      var out     = '',
        value     = ( o.value == 'true' ) ? 1 : o.value,
        value     = parseInt(value),
        checked   = (!value || value === 0 || value === '') ? '' : 'checked',
        toggleValue = (checked === 'checked') ? 1 : 0,
        aux     = sprintf('<input class="checkbox-toggle lstn" id="%s"  name="%s" type="hidden" value="%s" />', o.inputID, o.name, toggleValue )

      //out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )
      out += sprintf('<div class="checkbox checkbox-group"><label><input id="%s" name="%s" class="checkbox-input" type="checkbox" %s> %s</label>%s</div>', o.inputID, o.name, checked, o.label, aux)
      

      /**
       * Checkbox Toggle Hidden Input (0 or 1)
       */
      this.optScripts.checkbox = function(){
        
        $('.checkbox-input:not(.loaded)').on('change', function(){

          var checkToggle = $(this).parent().next()

          if( $(this).is(':checked') ){
              $(this).val(1)
              checkToggle.val(1)
          } else{
              $(this).val(0)
              checkToggle.val(0)
          }

          checkToggle.trigger('change')

        }).addClass('loaded')
      }
      
      return out;
    }, 

    _createAccordion: function( o ){

      var  that           = this, 
          out           = '',
          optionArray   = $.toolEngine.optGetValue( o.key ), 
          itemType       = 'Item',
          itemNumber     = 0,
          totalNum       = optionArray.length || Object.keys(optionArray).length,
          removeShow     = ( totalNum <= 1 ) ? 'display: none;' : '', 
          reorder       = $.plEditing.reorderIcon()


      $.each( optionArray, function( ind, vals ){ 


        o.itemNumber = 'item'+itemNumber

        out += sprintf("<div class='opt-group itemset item-closed' data-num='%s'><div class='opt-name item-toggle'>%s<span class='bar-title'>%s %s</span> <span class='pl-btn pl-btn-xs remove-item' style='%s'><i class='pl-icon pl-icon-remove'></i></span></div><div class='opt-accordion-opts item-contents'>", ind,  reorder, itemType, itemNumber + 1, removeShow )

        /** Add the array index and all values for the index to option meta for engine */
        o.ind = ind
        o.vals = vals

        if( o.opts ){
          $.each( o.opts , function(index, osub) {

            out += $.toolEngine.optEngine( osub, o ) 
            
          })
        }

        // adds a hidden input set to true, so that the item doesn't disappear
        out += that.addHiddenInput( o.key, o.itemNumber )
        
        out += sprintf("</div></div>")

        itemNumber++
      })

      return out

    }, 



    _redoAccordion: function( theAccordion, o ){

      var that = this

      
      
      newAccordion = that._createAccordion( o )

      theAccordion.html( newAccordion )

    
      theAccordion
        .pagesort({
          
          handle:     ".opt-name",
          animation:     150,
          draggable:     '.opt-group',
          onUpdate: function(evt){

            that._resortAccordion( theAccordion, o )
        
            $.plEditing.setNeedsSave()
        
          }
        })

      $.plEditing.itemToggles()

      /** Run option scripts again, except for accordion */
      $.engineOpts.runScripts()

      $.toolEngine.resetListenerBinding()
      
      

    }, 

    _resortAccordion: function( theAccordion, o ){

      var that = this, 
        sortArray = []

      theAccordion.find('.opt-group').each( function(){
        sortArray.push( parseInt( $(this).attr('data-num') ) ) 
        
      })
    
      $plModel().sortObservableArrayByArray( $.toolEngine.UID, o.key, sortArray )

      that._redoAccordion( theAccordion, o )
    }, 

    accordion: function( o ){

      var that = this, 
        out = ''

      var opts = sprintf( '<script type="application/json" class="accordion-options">%s</script>', JSON.stringify( o ) )

      out += sprintf('<div id="%s" name="%s" class="opt-accordion toolbox-sortable">', o.inputID, o.name)

      out += that._createAccordion( o )
      
      var tools = sprintf('<span class="pl-btn pl-btn-default pl-btn-xs add-accordion-item" >+ Add Item</span>')

      

      out += sprintf("</div><div class='accordion-tools'>%s%s</div>", opts, tools)





      that.optScripts.accordion = function(){ 

        $('.workarea-opts-form .opt-accordion').not('.loaded').each( function(){

          var theAccordion   = $(this), 
              theOpt         = $(this).closest('.pl-form-group'),
              o             = JSON.parse( theOpt.find('.accordion-options').html() )

          theAccordion
            .pagesort({
              
              handle:     ".opt-name",
              animation:     250,
              draggable:     '.opt-group',
              onUpdate: function(evt){
                that._resortAccordion( theAccordion, o )

                $.plEditing.setNeedsSave()
              }, 
              
            })

        }).addClass('loaded')

        /** Remove accordion item, redraws accordion */
        $('.workarea-opts-form .opt-accordion').not('.del-loaded').delegate( '.opt-name .remove-item', 'click', function () {

          var theOpt         = $(this).closest('.pl-form-group'),
              theAccordion   = theOpt.find('.opt-accordion'), 
              theGroup       = $(this).closest('.opt-group'), 
              o             = JSON.parse( theOpt.find('.accordion-options').html() )

        
          /** Don't let them remove last one */
          if( theAccordion.find('.opt-group').length <= 2 ){ theAccordion.find('.remove-item').hide() }


          var index = theGroup.data('num')

          $plModel().RemoveItemByIndex( $.toolEngine.UID, o.key, index )

          /** Remove the item html */
          theGroup.remove()

          /** Redraw the accordion */
          that._redoAccordion( theAccordion, o )
          
          $.plEditing.setNeedsSave( 'yes' )

          }).addClass('del-loaded')

        /** Event to add new accordion items, redraws accordion */
        $('.add-accordion-item').not('.loaded').on('click', function () {

          var theOpt       = $(this).closest('.pl-form-group'),
            theAccordion   = theOpt.find('.opt-accordion'), 
            o         = JSON.parse( theOpt.find('.accordion-options').html() ),
            item = {}

          /** Parse the options array */
          $.each( o.opts, function( i, option ){ 

            if( plIsset(option.key) )
              item[ option.key ] = '' 

            if( typeof option.opts != 'undefined' ){

              $.each( option.opts, function( i2, option2 ){ 

                if( plIsset(option2.key) )
                  item[ option2.key ] = ''

              })
            }
          
          })

          $plModel().addItemToArray( $.toolEngine.UID, o.key, item )

          that._redoAccordion( theAccordion, o )

          $.plEditing.setNeedsSave( 'yes' )


          }).addClass('loaded')


      }


      return out 
    }, 

    addHiddenInput: function( key, itemNumber ){

      var that = this
      
      return sprintf( '<input type="hidden" class="lstn dont-change" id="%s_%s_showitem" name="%s[%s][%s][showitem]" value="1" />', key, itemNumber, that.uniqueID, key, itemNumber)

    }, 

    imagedropzone: function( o ){

      var that        = this,
          theClass    = sprintf('pl-dropzone-%s', o.inputID),
          size        = o.size || '',
          out         = ''
      
      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label );

      /** Note: need dropzone class for CSS styling. */
      out += sprintf('<div class="dropzone dropzone-option upload-box clearfix select-%s" data-select="%s" data-size="%s" >', o.name, o.name, size);

      out += sprintf('<div class="pl-dropzone-actions ">');
      
      out += sprintf( '<input type="text" id="%s" name="%s" class="%s lstn pl-form-control upload-input" placeholder="%s" value="%s" />', o.inputID, o.name, o.classes, o.place, o.value);

      out += sprintf('<span class="pl-dropzone %s pl-btn pl-btn-primary pl-btn-xs">Upload</span>', theClass);
      out += sprintf(' <span class="pl-load-media-lib pl-btn pl-btn-default pl-btn-xs">Media</span>', o.value);
      out += sprintf(' <span class="pl-image-remove pl-btn pl-btn-default pl-btn-xs"><i class="pl-icon pl-icon-remove"></i></span>', o.value);

      out += sprintf('</div>'); // actions

      
      out += sprintf('<div class="pl-dropzone-preview %s"></div>', o.inputID);

      out += sprintf('</div>'); // dropzone

      var template = '<div class="dz-preview dz-file-preview"><div class="dz-details"><div class="dz-meta"><div class="dz-filename"><span data-dz-name></span></div><div class="dz-size" data-dz-size></div></div><img data-dz-thumbnail /></div><div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div><div class="dz-success-mark"><i class="pl-icon pl-icon-ok"></i></div><div class="dz-error-mark"><i class="pl-icon pl-icon-remove"></i></div></div>';


      

      var myDropzones = {}


      /** Runonly once for all image uploaders */
      that.optScripts.dropzoneUpload = function(){



        $( '.dropzone-option' ).not('.loaded').each( function(){

          theDZ     = $(this),
          theSelector = '.' + $(this).data('select'), 
          theInput   = theDZ.find('.upload-input'), 
          thePreview   = theDZ.find('.pl-dropzone-preview')

          
          if( theInput.val() != '' ){
          
            thePreview.html( sprintf('<div class="dz-preview dz-file-preview"><img src="%s" /></div>', pl_do_shortcode(theInput.val())) )

          }

          theDZ.find('.pl-dropzone').dropzone({ 
            url:         PLWorkarea.ajaxURL,
            previewsContainer:   thePreview[0],
            previewTemplate:   template,
            sending: function (file, xhr, formData) {

              formData.append( "action", "pl_server"); 
              formData.append( "hook",   "pl_image_upload");
              formData.append( "nonce",   PLWorkarea.security );

              var DZ = $( $( this )[0].element )

              formData.append( "size", DZ.closest('.dropzone-option').data('size') ); 

              // Remove old image in preview
              DZ.parent().next().find('.dz-preview:not(:last)').remove()
            }, 
            
            success: function (file, response) {

              var rsp  = response

              var DZ = $( $( this )[0].element )


              DZ.parent().find('.upload-input').val( rsp.url ).trigger('blur')

            }
          });

        }).addClass('loaded')

        

        /** Remove Image */
        $('.pl-image-remove').not('.loaded').on('click', function(){

          $(this)
            .closest('.upload-box')
            .find('.lstn')
            .val('')
            .trigger('blur')
            .end()
            .find('.dz-preview').fadeOut()

        }).addClass('loaded')

        /** Get image from media library */
        $('.pl-load-media-lib').not('.loaded').on('click', function(){

          var mediaFrame

          if( $(this).data('mimetype') === 'video' )
            mediaFrame = PLWorkarea.mediaLibraryVideo
          else 
            mediaFrame = PLWorkarea.mediaLibrary

          var theInput = $(this).closest('.upload-box').find('.upload-input'),
            optionID = theInput.attr('id')
          
          mediaFrame + '&oid=' + optionID

          PLWorkarea.iframeSelector = optionID

          bootbox.dialog({
            title:     "Media Library",
              message:   sprintf('<iframe class="modal-iframe" src="%s"></iframe>', mediaFrame),
              animate: false,
            className: 'pl-modal modal-media',
            backdrop: true,
            onEscape: true

          })


          $('.bootbox').on('hidden.mediaDialog', function () {
            
            theInput.trigger('blur').closest('.ui-accordion').accordion('refresh')
            
            $('.bootbox').off('hidden.mediaDialog')

          })

        }).addClass('loaded')


      }

      
      return out;
    }, 


    radioOption: function( o ){

      var that = this,
        out = ''

      
      
    
      
      if(o.opts){

        var radioButtons = ''

        $.each(o.opts, function(key, s){

          var optValue   = s.val,
              optName     = ( o.type === 'select_same'   ) ? s : s.name,
              selected     = ( o.value == optValue     ) ? 'checked' : '',
              txt         = ( typeof s.txt !== 'undefined') ? ' ' + s.txt : '', 
              theID       = o.inputID + key,
              hoverTitle  = s.hover || '', 
              icon         = ( plIsset(s.icon) ) ? sprintf('<i class="pl-icon pl-icon-%s"></i>', s.icon) : ''


          
          radioButtons += sprintf('<label for="%s" class="pl-btn pl-btn-default pl-btn-radio %s" title="%s">%s%s<input type="radio" class="input-radio lstn" name="%s" id="%s" value="%s" %s></label>', theID, selected, hoverTitle, icon, txt, o.inputID, theID, optValue, selected)

        })
      }

      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )
      out += sprintf('<div class="pl-btn-group pl-btn-radios">%s</div>', radioButtons)

      // Add Once Off Scripts
      that.optScripts.radioSet = function(){  


        $('.input-radio').not('.loaded').on('click', function(){

          var clicked = $(this), 
              group   = clicked.parent().parent()
          
          group.find('.pl-btn-radio').removeClass('checked')

          group.find('.input-radio:checked').parent().addClass('checked')

    
        }).addClass('loaded')

        $('.pl-btn-radios .input-radio:checked').parent().addClass('checked')

      
      }

      return out;

    },
    

    selectoption: function( o ){

      var that = this,
        out = '',
        defaultValue = o.default || '',
        select_opts = (o.type !== 'select_multi') ? sprintf( '<option value="%s" >Select</option>', defaultValue)  : ''
        
      if ( 
          o.type === 'count_select'     || 
          o.type === 'count_select_same'   || 
          o.type === 'select_pixels'    || 
          o.type === 'select_vw'        || 
          o.type === 'select_percent'   ||
          o.type === 'select_proportion'  
        ) {

      
        if( o.type == 'select_pixels' ){
          
          var cnt_start       = parseInt(o.count_start)   || 0, 
              cnt_num         = parseInt(o.count_number)   || 500, 
              cnt_multiple     = parseInt(o.count_mult)     || 25,
              suffix           = 'px',
              key_suffix       = 'px'

        } 

        else if( o.type == 'select_vw' ){
          
          var cnt_start     = parseInt(o.count_start)   || 10, 
            cnt_num         = parseInt(o.count_number)   || 100, 
            cnt_multiple     = parseInt(o.count_mult)   || 5,
            suffix           = 'vw',
            key_suffix       = 'vw'

        } 

        else if( o.type == 'select_proportion' || o.type == 'select_percent' ){
          
          var cnt_start       = parseInt(o.count_start)   || 0, 
              cnt_num         = parseInt(o.count_number)   || 100, 
              cnt_multiple     = parseInt(o.count_mult)   || 10,
              suffix           = '%',
              key_suffix       = (o.type == 'select_proportion') ? '' : '%'

        } 

        else {

          var cnt_start     = parseInt(o.count_start)   || 0, 
            cnt_num     = parseInt(o.count_number)   || 10, 
            cnt_multiple   = parseInt(o.count_mult)   || 1, 
            suffix       = o.suffix           || '',
            key_suffix     = ( o.type === 'count_select_same' ) ? o.suffix : ''

        }

        o.opts = {}

        for ( i = cnt_start; i <= cnt_num; i+=cnt_multiple ) {

          var selectOptionValue = ( o.type == 'select_proportion' ) ? i / 100 : i + key_suffix

          o.opts[ selectOptionValue ] = { name: i+suffix }

        }

        
      }

        
      if(o.type === 'select_wp_tax'){

        var taxes = PLWorkarea.taxes
        o.opts = {}
        $.each(taxes, function(key, s){
          o.opts[ s ] = {name: s}
        })

      }

      else if(o.type === 'select_term'){

      
        $.engineOpts.optScripts.selectTerm = function(){

          /** If the trigger option changes we need new terms via ajax */
          $('#' + o.trigger ).on('change load', function(e){

            console.log(e)

            var triggerEl = $(this)

            /** If value unchanged */
            if( $(this).val() == $(this).data('init') && e.type != 'load' ){
              return ;
            }

            var args = {
                  hook:   'select_term',
                  pt:     $(this).val(),
                  postSuccess: function( rsp ){ 

                    newopts =  sprintf('<option value="">Select</option>')

                    $.each(rsp.opts, function(key, s){

                      newopts += sprintf('<option value="%s">%s</option>', key, s.name)
                      newopts[ key ] = {name: s.name}
                    })

                    $('#' + o.key ).html( newopts )

                    triggerEl.data('init', triggerEl.val()).attr('data-init', triggerEl.val())
                    
                  },
                }

            
            $plServer().run( args )

          })

          $('#' + o.trigger ).trigger('load')

        }

      } 

      else if(o.type === 'select_icon'){

        icons = PLWorkarea.icons

        o.opts = {}
        $.each(icons, function(key, s){
          o.opts[ s ] = {name: s}
        })
      } 

      else if( o.type === 'select_animation' ){

        var anims = PLWorkarea.animations

        o.opts = {}
        $.each(anims, function(key, s){
          o.opts[ key ] = {name: s}
        })

      } 

      else if( o.type === 'select_button' || o.type === 'select_button_size'){

        if( o.type === 'select_button' )
          var btns = PLWorkarea.btns
        else
          var btns = PLWorkarea.btnSizes


        o.opts = {}
        $.each(btns, function(key, s){

          // we cant use a string of '0' as a default key here, as it stops the option engine from using a default :/
          if( '0' === key )
            key = ''

          o.opts[ key ] = {name: s}
        })

      } 



      else if( o.type === 'select_sidebar' ){

        var sbs = PLWorkarea.sidebars



        o.opts = {}
        $.each(sbs, function(key, s){
          o.opts[ key ] = {name: s}
        })

      } 

      else if( o.type === 'select_menu' ){

        var obs = PLWorkarea.menus

        o.opts = {}
        $.each( obs, function(key, s){
          o.opts[ key ] = {name: s}
        })

      } 

      else if( o.type === 'select_imagesizes' ){

        var sizes = PLWorkarea.imgSizes

        o.opts = {}
        $.each(sizes, function(key, s){
          o.opts[ s ] = {name: s}
        })

      }

      
      if(o.opts){

        $.each(o.opts, function(key, s){

          var optValue = (o.type === 'select_same') ? s : key
          ,  optName = (o.type === 'select_same') ? s : s.name
          ,  selected = ''

          // Multi Select
          if(typeof o.value === 'object'){
            $.each(o.value, function(k, val){
              if(optValue === val)
                selected = 'selected'
            })

          } else {

            if(o.value !== '')
              selected = (o.value === optValue) ? 'selected' : ''
            else if( plIsset(o.default) )
              selected = (o.default === optValue) ? 'selected' : ''

          }

          select_opts += sprintf('<option value="%s" %s >%s</option>', optValue, selected, optName)

        })
      }


      var multi = (o.type === 'select_multi') ? 'multiple' : ''


      out += sprintf('<label for="%s">%s</label>', o.inputID, o.label )
      out += sprintf('<select id="%s" name="%s" class="%s pl-form-control lstn" data-type="%s" data-init="%s" %s>%s</select>', o.inputID, o.name, o.classes, o.type, o.value, multi, select_opts)

      
      if(o.type === 'select_taxonomy' && o.post_type){
        
        out += sprintf(
          '<div style="margin-bottom: 10px;"><a href="%sedit.php?post_type=%s" target="_blank" class="pl-btn pl-btn-xs pl-btn-info"><i class="pl-icon pl-icon-edit"></i> %s</a></div>',
          PLWorkarea.adminURL,
          o.post_type,
          plTranslate( 'edit_sets' )
        )
      }
      

      return out;

    }

    
  }

}(window.jQuery);
