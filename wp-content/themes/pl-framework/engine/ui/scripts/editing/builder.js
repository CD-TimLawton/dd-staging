// PageLines Tools Initializer

!function ($) {
   



  $.plBuilder = {

    /**
     * Gets the list of sections available of a certain type.
     * @param  {string} type    type of area, full width area or content
     * @param  {object} section the section where the 'add' as clicked
     * @return {string} html output for placement in panel
     */
    init: function( clicked ){

      var that     = this,
          clicked = clicked || $('body'),
          uid     = clicked.data('clone') || false,
          config = {
            name:     plTranslate( 'page_builder' ),
            panels:   that.thePanels(), 
            key:       'builder',
            call: function(){
              that.bindListActions( uid )
            }
          }
          
      $.plEditing.sidebarEngine( config )

    }, 

    doSortables: function(){

      var that = this

      $('.dd-sort').pagesort({

        group:       'builder', 
        animation:     250,
        draggable:     '.dd-item',
        onAdd: function(){
          that.dragDropUpdate()
        }, 
        onUpdate: function(evt){
          that.dragDropUpdate()
        }, 
            
        onEnd: function(){
          $('.pl-builder-list').find('.dd-item').css('transform', '')
        }
      })


      $('.dd-sub-sort').pagesort({
        group:       'builder', 
        animation:     250,
        draggable:     '.dd-item'
      })

    
    }, 


    bindListActions: function( uid ){

      var that = this

      that.doSortables()
    
      if( uid ){

        var el = $('.pl-builder-list').find( sprintf( '[data-clone="%s"]', uid ) )

        el.addClass('item-highlight')

        setTimeout( function(){
          el.removeClass('item-highlight')
        }, 2000 )

      }

      $('.pl-builder-list').delegate('.dd-control', 'click', function(e){

        e.stopPropagation()

        var functionName = 'control_' + $(this).data('tool')

        if ( $.isFunction( that[ functionName ] ) ){
          that[ functionName ].call( that, $(this) )
      }

      })

      $('.dd-builder li.dd-item .dd-handle').each( function( e, i ) {
        $(i).on( 'dblclick', function(){
          btn = $(this)
          $.plBuilder.control_options(btn)
        })
      })
  
      $('.select-new-add-item').on( 'click', function(){

        var theItem   = $(this), 
            object     = theItem.data('class'), 
            loading   = theItem.data('loading'),
            UID       = plUniqueID(), 
            section   = that.getSection( object ),
            name       = section.name,
            hassub     = ( section.contain == 1 ) ? 'parent-item' : '', 
            sublist   = ( section.contain == 1 ) ? that.getListWrap() : '', 
            newItem   = that.getListElement({ object: object, clone: UID, name: name, parentCl: hassub, sublist: sublist})
          
        /** Add to page using GET and refresh */
        if( loading == 'refresh' ){

          $.plFrame.reloadFrame( { addSections: JSON.stringify( [ object ] ) } )

        } 

        /** Add to Page using AJAX */
        else{

          $jq().plAdd.newSection( object, UID, newItem )
        
        }
        
      })



    }, 

    getSection: function( object ){

      var that = this

      section   =  ( plIsset( PLWorkarea.factory[ object ] )) ? PLWorkarea.factory[ object ] : false

      return section

    }, 



    control_grid: function( btn ){

      var that     = this, 
        section   = btn.closest('.dd-item'), 
        object     = section.data('object'), 
        UID     = section.data('clone'),
        current   = that.getColumnSize( section ),
        currentOff   = that.getOffsetSize( section ), 
        action     = btn.data('action')


        if( action == 'increase' || action == 'decrease' ){

          section
            .removeClass( current[0] )

          if( action == 'decrease'){
            section.addClass( current[2] )
            $plModel().setSectionOption( UID, 'col', current[5] )
          }

          else if (action == 'increase'){
            section.addClass( current[1] )
            $plModel().setSectionOption( UID, 'col', current[4] )
          }

        }

        else if( action == 'offmore' || action == 'offless' ){

          section
            .removeClass( currentOff[0] )

      
          if (action == 'offless'){
            section.addClass( currentOff[2] )
            $plModel().setSectionOption( UID, 'offset', currentOff[5] )
          }

          else if (action == 'offmore'){
            section.addClass( currentOff[1] )
            $plModel().setSectionOption( UID, 'offset', currentOff[4] )
          }

        }
        

        

      $.plEditing.setNeedsSave()
    }, 

    control_dropdown: function( btn ){

      var that     = this, 
          section   = btn.closest('.dd-item'), 
          container   = section.parent(),
          controls   = ''

        

        if( section.find('.dd-dropdown').length > 0 ){

          section.removeClass('dd-show').find('.dd-dropdown').remove()
        
        }

        else {

          $('.dd-dropdown').remove() 
          $('.dd-show').removeClass('dd-show')
        
          /** If a missing section, dont create controls as they can error out the page */
          if( btn.attr('title').indexOf("Missing") == -1 ){
            
            controls +=  '<div class="dd-li"><span class="a">'
            controls += '<span class="dd-control dd-in" data-tool="grid" data-action="decrease"><i class="pl-icon pl-icon-caret-left"></i></span>'
            controls += '<span class="dd-control dd-in" data-tool="grid" data-action="increase"><i class="pl-icon pl-icon-caret-right"></i></span>'
            controls += sprintf('%s</span></div>', plTranslate( 'cols' ))

            controls +=  '<div class="dd-li"><span class="a">'
            controls += '<span class="dd-control dd-in" data-tool="grid" data-action="offless"><i class="pl-icon pl-icon-caret-left"></i></span>'
            controls += '<span class="dd-control dd-in" data-tool="grid" data-action="offmore"><i class="pl-icon pl-icon-caret-right"></i></span>'
            controls += sprintf('%s</span></div>',plTranslate( 'offset' ))

          
            controls += sprintf( '<div class="dd-li"><a class="dd-control" data-tool="options" ><i class="pl-icon pl-icon-pencil"></i> %s</a></div>', plTranslate( 'edit' ))
            controls += sprintf( '<div class="dd-li"><a class="dd-control" data-tool="show" ><i class="pl-icon pl-icon-eye"></i> %s </a></div>', plTranslate( 'show' ))
            controls +=  sprintf( '<div class="dd-li"><a class="dd-control" data-tool="clone" ><i class="pl-icon pl-icon-file-text"></i> %s</a></div>', plTranslate( 'clone' ))
          }
          
          controls += sprintf('<div class="dd-li"><a class="dd-control" data-tool="delete" ><i class="pl-icon pl-icon-remove"></i> %s</a></div>',plTranslate( 'delete' ))

          section
            .addClass('dd-show')
            .children('.dd-item-wrap')
            .children('.dd-title')
            .after( sprintf( '<div class="dd-dropdown">%s</div>', controls ) )

        }
        

        

      $('body').on( 'click.closeDDDropdown', function(){ 
        
        $('.dd-dropdown').remove()

        $('.dd-show').removeClass('dd-show')

        $(this).unbind( 'click.closeDDDropdown' )

      });
    
    }, 

    control_options: function( btn ){

      var clone   = btn.closest('.dd-item').data('clone'),
          section = $i( sprintf('[data-clone="%s"]', clone) ) 

      $( '.tool-active' ).removeClass( 'tool-active' )  

      $jq().plScrolling.scrollToSection( section )

      $.plEditing.loadSectionOptions( section )


    }, 

    control_delete: function( btn ){

      var that     = this, 
        dd         = btn.closest('.dd-item'),
        clone     = dd.data('clone')

        $.plEditing.deleteSection( btn, clone )

  
    },

    control_clone: function( btn ){

      var that      = this, 
          dd        = btn.closest('.dd-item'),
          clone     = dd.data('clone'),
          section   = $i( sprintf('[data-clone="%s"]', clone) ).first(),
          ddClone   = dd.clone(),
          elClone   = section.clone()

      elClone
        .insertAfter(section)
        .hide()
        .fadeIn()
      
      ddClone
        .insertAfter( dd )
        .hide()
        .fadeIn()

      /** Remove stuff by click action */
      $('body').click()

      that.updateCloneData( ddClone, elClone )

      that.updateTemplateMap()

      $.plEditing.reloadUI()

      $iWindow().plTrigger('ready')

    },

    /**
     * RECURSIVE 
     * Creates a new UID for element and recursively for sub elements. 
     * The data from these elements is added to master list object.
     */
    updateCloneData: function( ddClone, elClone ){

      var that        = this,
          oldUID       = ddClone.data('clone'),
          newUID       = plUniqueID(),
          theModel     = {}
      
      elClone
        .attr('data-clone', newUID)
        .data('clone', newUID)

      ddClone
        .attr('data-clone', newUID)
        .data('clone', newUID)



      if( plIsset( $pl().modelData[ oldUID ] ) ){

        console.log($pl().modelData[ oldUID ])


        /**
         * We have to remove all elClones inside template and foreach, or it will multiply
         */
        elClone.find('[data-bind]').each( function(){
          var binding = $(this).attr('data-bind')

          /** If foreach binding remove all but first */
          if ( binding.indexOf('foreach') >= 0 ){
            
            $(this).children("*:gt(0)").remove()

          } 

          /** If template binding remove all */
          else if ( binding.indexOf('template') >= 0 ){
            
            $(this).empty()
          }


        })

        
        /** Update data to match the view */
        $plModel().updateModelData()

        /** Get the cloned sections data */
        $pl().modelData[ newUID ] = $.extend( {}, $pl().modelData[ oldUID ] )

        /** Bind data to new section */
        $plBinding().bindNewSection( newUID, $pl().modelData[ newUID ] )


        

      }

      else{
        console.log('model data not set')
      }

      console.log('go here')

      /** Recursion */
      ddClone.find("[data-clone]").each(function(){

        var ddNext     = $(this),
            UIDNext    = $(this).data('clone'), 
            elNext     = elClone.find( sprintf( '[data-clone="%s"]', UIDNext ) )


        console.log(UIDNext)
        console.log(elNext)
        
        that.updateCloneData( ddNext, elNext )
        
      })
      


      

    },


    control_show: function( btn ){


      var clone   = btn.closest('.dd-item').data('clone'),
        section = $i( sprintf('[data-clone="%s"]', clone) ) 

      $jq().plScrolling.scrollToSection( section )

      
    }, 



    /**
     * Gets the amound of column offsets from the classname
     */
    getOffsetSize: function( column, defaultValue ) {

      var that   = this,
        max   = 10,
        min   = 0,
        sizes   = that.getColumnSize( column ),
        avail   = 12 - sizes[3],
        data   = [], 
        offset   = 'pl-col-sm-offset-'

      for( i = min; i <= max; i++){

          next = ( i === avail ) ? min : i+1

          prev = ( i <= min ) ? avail : i-1

          

          if( column.hasClass( offset + i ) ){
            data = new Array( offset + i,  offset +next,  offset + prev, i, next, prev )
          }

      }

      if(data.length === 0 || defaultValue)
        return new Array( offset + "0",  offset + "0",  offset + "0", '0 Off', i)
      else
        return data

    }, 


    /** 
     * Gets the amount of grid columns for the section
     */
    getColumnSize: function(column, defaultValue) {

      var that   = this,
        max   = 12,
        min   = 3,
        data   = [], 
        col   = 'pl-col-sm-'

      for( i = min; i <= max; i++){

          next = ( i === max ) ? min : i+1

          prev = ( i === min ) ? max : i-1

          if(column.hasClass(col + i))
            data = new Array( col + i, col + next, col + prev, i, next, prev )

      }

      if(data.length === 0 || defaultValue)
        return new Array( col + "12", col + "1",  col + "11", "12 Cols", 12)
      else
        return data

      

    }, 


    updateTemplateMap: function(){

      var that = this

      $pl().config.tplMap = that.getLevelMap($('.pl-builder-list'), -1)

      $.plEditing.setNeedsSave()

      $('.pl-builder-list').find('.dd-item').css('transform', '')
    }, 


    dragDropUpdate: function(){

  
      var that   = this

      that.updateTemplateMap()

      that.reDrawPage( $pl().config.tplMap, -1, $i('body'))

    }, 

    reDrawPage: function( map, level, container ){

      var that = this
      
      $.each( map , function( index, section ){

        
        var theElement     = $i( sprintf('[data-clone="%s"]', section.clone) ), 
            oldLevel       = theElement.data('level'),
            nextLevel     = level + 1


          
        if( level != -1 ){



          /** Set to new level */
          theElement.data('level', level).attr('data-level', level)

          /** Move the section */
          theElement
            .detach()
            .appendTo( container )

          
        }


        if( !_.isEmpty( section.content ) ){

          var nextContainer  = theElement.find('[data-contains-level]').first()

          if( !_.isEmpty( nextContainer ) )
            that.reDrawPage( section.content, nextLevel, nextContainer )

        }
  

      })

    }, 



    getLevelMap: function( container, level ){

      var that   = this, 
        level   = level || 0, 
        map   = {}


        
      container.children('.dd-item-wrap').children('.itemset').children('.dd-list').each( function( theListIndex ){ 

        var theList   = $(this),
            listMap   = {}




        $(this).children('.dd-item').each( function( listItemIndex ){ 

          var theListItem   = $(this), 
            clone       = $(this).data('clone'),
            index       = $(this).data('index') || listItemIndex



          theListItem.children('.dd-item-wrap').children('.itemset').children('.dd-list').attr( 'data-level', level + 1 ).data('level', level + 1)

          
          listMap[ index ] = {
            clone:     theListItem.data('clone'), 
            object:   theListItem.data('object'),
            content:   that.getLevelMap( theListItem, level + 1 )
          }

        })
  
        if( theList.hasClass('no-list') ){
          map = $pl().config.tplMap[ theList.data('region') ].content
        }
        else{
          map = listMap
        }

      })




      return map
    }, 

    opt_type_add_sections: function(){

      var that   = this,
        addList = that.builderAddList()
  

      return sprintf( '%s', addList )


    }, 



    opt_type_builder: function(){

      var that   = this, 
        list   = '', 
        tabs   = ''

        list = that.builderList( $pl().config.tplMap, -1)




      return sprintf( '<div class="dd-builder"><div class="pl-builder-list dd-list-container"><div class="dd-item-wrap">%s</div></div></div>', list )

    }, 

    opt_type_scope: function(){

      var that   = this,
          out   = ''

      var scopeOptions = {} 

      scopeOptions.type = sprintf('%s: "%s"', plTranslate( 'all_of_type'), $pl().config.typename)

      if( $pl().config.pageID != $pl().config.typeID )
        scopeOptions.local = sprintf('%s: "%s"', plTranslate( 'current_page_only' ), $pl().config.currentPageName)

      if( $pl().config.termID != $pl().config.pageID )
        scopeOptions.term = sprintf( '%s: "%s"', plTranslate('taxonomy_archive' ), $pl().config.currentTaxonomy )

      out = $.engineOpts.selectOption( scopeOptions, $pl().config.tplMode, 'tpl_scope')

      return out

    }, 

    opt_type_paste_sections: function(){

      var that    = this,
          out     = ''

      out += sprintf('<div class="paste-section"><input id="spaste" class="paste-section-data pl-form-control" type="text" placeholder="Paste section data here..." /><span class="pl-btn pl-btn-primary pl-btn-xs submit-paste-section"><i class="pl-icon pl-icon-plus"></i> Add Section</span></div>')

     $.engineOpts.optScripts.paste_section = function(){

        $('.submit-paste-section').on('click', function(){

          var pasteContainer  = $(this).parent(), 
              pasteInput      = pasteContainer.find('.paste-section-data')

          $.plFrame.reloadFrame( { loadMap: encodeURIComponent( pasteInput.val() ) } )

        })
        

      }

      

      return out 

    }, 

    builderList: function( map, level, region ){

      var that     = this,
        level     = level || 0,
        title     = ( level == 0 ) ? sprintf('<div class="filter-title item-toggle">%s</div>', region) : '',
        list       = '', 
        region     = region || '', 
        sort       = '', 
        listWrap   = '', 
        staticTpl = '',
        render     = $pl().config.tplRender,
        capture   = $pl().config.tplCapture, 
        dynamic   = true, 
        doList     = true, 
        nameAft   = '',
        select

      

      level = parseInt( level )

      dynamic = ( level === 0 && ! plIsset( render[ region ] ) ) ? false : true 

      
      if( ! dynamic ){

        doList = false


        
        if( region == 'template' ){

          doList = true

        }

        staticTpl = sprintf('<div class="pl-alert pl-alert-workarea"><strong class="subtle">%s %s.</strong></div>', plTranslate( 'using_a_theme'), region) 

      }

      

      if( doList ) {
        

        $.each( map, function( i, item ){

          var object        = item.object,
              clone         = item.clone,
              content       = ( plIsset( item.content) ) ? item.content : [],
              section       = ( plIsset( PLWorkarea.factory[ object ] )) ? PLWorkarea.factory[ object ] : false,
              name          = ( section ) ? sectionNameFromObject( object ) : sprintf('(Missing: %s)', item.object),
              cName         = $plModel().getSectionOption( clone, 'custom_name' ) || false,
              name          = ( cName ) ? sprintf( '%s %s', cName, name ) : name,
              canContain    = ( ( section && section.contain == 1 ) || level == -1 ) ? true : false,
              sublist       = ( canContain ) ? that.builderList( content, level + 1, i ) : '', 
              hassub        = ( canContain ) ? 'parent-item' : '', 
              col           = $plModel().getSectionOption( clone, 'col' ),
              offset        = $plModel().getSectionOption( clone, 'offset' ), 
              hideOn        = $plModel().getSectionOption( clone, 'hide_on' ), 
              colClass      = ( plIsset( col ) && col != '') ? col : '12',
              offClass      = ( plIsset( offset )  && offset != '' ) ? offset : '0'

          /** If hidden on page */
          if( hideOn ){

            var hideClass = ''

            $.each(hideOn.split(','), function( i, PID ){

              if( PID == $pl().config.pageID ){
                hideClass = 'hide-on-page'
              }
            
            })

          }

          if( level == -1 ){

            list += sprintf('<div class="dd-item" data-clone="%s" data-object="%s" data-index="%s"><div class="dd-item-wrap">%s</div></div>', clone, object, object, sublist )
          } 

          else {

            var config = {
                col:         colClass, 
                offset:     offClass, 
                hide:       hideClass,
                parentCl:   hassub, 
                clone:       clone, 
                object:     object, 
                name:       name, 
                sublist:     sublist
            }

            list += that.getListElement( config )


          }
          

        })

      } 
      
    

      var listWrap = that.getListWrap({
          title:       title,
          region:     region, 
          level:       level, 
          list:       list, 
          staticTpl:   staticTpl, 
          doList:     doList
      })


      return listWrap

    }, 

    getListWrap: function( config ){

      var that     = this, 
        classes    = '',
        defaults   = {
          title:     '',
          region:   '', 
          level:     '1', 
          list:      '', 
          staticTpl:   '', 
          doList:     true
        }

      config = $.extend( defaults, config )

      region = (config.region != '') ? sprintf('data-region="%s"', config.region) : ''

      if( config.level == 0 )
        classes += 'dd-sort' 
      else if( config.level > 0 )
        classes += 'dd-sub-sort'

      if( ! config.doList )
        classes += ' no-list'

      return sprintf('<div class="itemset">%s<ol class="item-contents dd-list fix %s" %s data-level="%s">%s</ol>%s</div>', config.title, classes, region, config.level, config.list, config.staticTpl)

    }, 

    getListElement: function( config ){

      var that     = this, 
          defaults   = {
            col:       '12', 
            offset:   '0', 
            parentCl:   '', 
            clone:      '', 
            object:   '', 
            name:     '', 
            sublist:   '', 
            hide:       ''
          }

      config = $.extend( defaults, config )

      if( config.hide != '' ){
        sName = config.name + ' ' + plTranslate( '(Hidden)' )
      }
      else{
        sName = config.name
      }

      var reorder   = $.plEditing.reorderIcon(), 
          title     = sprintf( '<div class="dd-el dd-name dd-control" title="%s Section" data-tool="dropdown">%s <i class="pl-icon pl-icon-caret-down"></i></div>', config.name, sName ) 


      return sprintf( '<li class="dd-item pl-col-sm-%s pl-col-sm-offset-%s %s %s" data-clone="%s" data-object="%s" ><div class="dd-item-wrap"><div class="dd-title dd-handle clearfix">%s %s</div>%s</div></li>', config.col, config.offset, config.parentCl, config.hide, config.clone, config.object, title, reorder, config.sublist )


    }, 

    
    sectionFilters: function(){

      var filters = {
            basic:        plTranslate( 'basic' ), 
            content:      plTranslate( 'content_formats' ), 
            layout:       plTranslate( 'layout_containers' ), 
            component:    plTranslate( 'components' ),
            nav:          plTranslate( 'navigation_menus' ),
            carousel:     plTranslate( 'carousel' ), 
            gallery:      plTranslate( 'gallery' ), 
            slider:       plTranslate( 'sliders_features' ), 
            wordpress:    'WordPress', 
            localsocial:  plTranslate( 'social_local' ), 
            widgetized:   plTranslate( 'widgets_sidebar' ), 
            advanced:     plTranslate( 'advanced' )
          }

      return filters
    
    }, 


    builderAddList: function( ){

      var that     = this, 
          out     = '', 
          panels   = ''


      $.each( that.sectionFilters(), function( tag, title ){

        var theSections =  that.getFilterSections( tag )

        if( theSections != '' )
          panels += sprintf( '<div class="itemset"><div class="filter-title item-toggle">%s</div> %s</div>', title, that.getFilterSections( tag ) );

        

      })

      var additional = sprintf('<div class="dd-additional"><a class="pl-btn pl-btn-default" href="%s"><i class="pl-icon pl-icon-plug"></i> &nbsp; %s</a></div>', PLWorkarea.extendURL, plTranslate( 'download_new_sections'))


      return sprintf('<div id="tab-add" class="pl-builder-add dd-list-container">%s%s</div>', additional, panels)

    }, 

    getFilterSections: function( tag ){

      var that   = this, 
          out   = ''
        
      $.each( PLWorkarea.factory, function(){

        var section     = $(this)[0], 
            txtfilter   = (section.filter == '') ? 'advanced' : section.filter,
            filter       = txtfilter.split(','), 
            hasFilter   = $.inArray( tag, filter ) > -1      


        if( hasFilter ){

          var refreshIcon = ( 'refresh' == section.loading ) ? sprintf('<span class="sicons"><i class="pl-icon pl-icon-refresh"></i></span>') : ''

          out += sprintf('<li title="%s" class="dd-item select-new-add-item fix" data-class="%s" data-loading="%s">%s<div class="list-icon" style="background-image: url(%s)"></div><div class="list-content"><div class="title">%s</div></div></li>', section.desc, section.class, section.loading, refreshIcon, section.icon, section.name )
        }
        
        

      })


      return ( out != '' ) ? sprintf( '<ul class="item-contents list-grid">%s</ul>', out ) : ''

    }, 





    thePanels: function(){

      var that   = this,
        panels = {
          scope: {
            title:  plTranslate( 'scope' ),
            opts:   [
              {
                type:     'scope',
                callback:   that
              }
            ]

          }, 
          builder:  {
            title:     plTranslate( 'page_layout' ), 
            format:   'full',  
            opts:   [
              {
                type:     'builder',
                callback:   that
              }
            ]
          },
          add:  {
            title:     plTranslate( 'add_sections_to_page' ), 
            format:   'full',  
            opts:   [
              {
                type:     'add_sections',
                callback:   that
              }
            ]
          },
          paste:  {
            title:     plTranslate( 'section_copy_paste' ), 
            opts:   [
              {
                type:     'paste_sections',
                callback:   that
              }
            ]
          },

        }
      return panels
    }
  }
}(window.jQuery);
