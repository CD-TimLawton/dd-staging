/**
 * Handling functions for iFrame editing and transport
 */
!function ($) {
   

  window.$i = function(element) {

    if ( typeof PLWorkarea.iframe == 'undefined' || typeof PLWorkarea.iframe.contents() == 'undefined' )
      return $();

    return PLWorkarea.iframe.contents().find(element);

  }

  window.$iDocument = function() {

    return $(PLWorkarea.iframe.contents());

  }

  window.$iWindow = function() {

    return $('iframe.site-frame')[0].contentWindow;
  
  }

  window.$jq = function() {

    return $iWindow().jQuery
  
  }

  

  window.$pl = function() {

    return $iWindow().PLData
  
  }

  window.$plModel = function() {

    return $iWindow().jQuery.plModel
  
  }

  window.$plBinding = function() {

    return $iWindow().jQuery.plBinding
  
  }

  window.$plServer = function() {

    return $iWindow().jQuery.plServer
  
  }

  window.$iWindowEl = function() {

    return $( $('iframe.site-frame')[0].contentWindow );
  
  }



  // --> Initialize
  $(document).ready(function() {
  
    
    $.plFrame.init()
    
  })
  
  $.plFrame = {

    init: function(){

      var that = this

      /** Assign global for iFrame */
      PLWorkarea.iframe = $('iframe.site-frame');


      PLWorkarea.iframe.on("load", function () {

  
        $('title').html('Editing: ' + pl_strip_html( $pl().config.currentPageName ) );
        

          /** Add events to links to maintain editing state */
          $.plFrame.handleFrameLinks()

          if( $pl().config.needsave == 1 ) 
            $.plEditing.setNeedsSave()
          else
          $.plEditing.setNeedsSave('no')


        /** Deal with edit page link...  */
        
        if( $pl().urls.editPost == '' )
          $('.editlink').addClass('disable-link').removeAttr('href').append('<span class="na"> (N/A)</span>')
        else
          $('#wp-admin-bar-edit > a').removeClass('disable-link').attr( 'href', $pl().urls.editPost ).find('.na').remove()


            
         
      });

      
      /** Changing to a different area of the site, show overlay */
      PLWorkarea.iframe.on("unload", function () {
        $('.iframe-loading-overlay').addClass('show-overlay')
      });


      plAdjustAdminBar()
      

    },

    

    reloadFrame: function( params ){

      var url = PLWorkarea.iframe.attr('src')

      params.needsave = 1
  
      $.each( params, function( key, value ){

        url = updateQueryStringParameter( url, key, value );

      })

      PLWorkarea.iframe.attr('src', url);

    }, 

    /** Deal with iFrame link clicking. */
    handleFrameLinks: function(){

        $i('body').delegate('a', 'click', function(e){

            var link       = $(this),
                url        = $( this ).attr("href"),
                hostname   = new RegExp( location.host ), 
                ignore     = false,
                Protocol   = $.plFrame.getProtocol( url ),
                iProtocol  = $.plFrame.getProtocol( PLWorkarea.iframe.attr('src') ),
                mismatch   = false

          
            /** Pretty Photo (woocommerce) */
            if( link.data('rel') || link.hasClass('prettyPhoto') || link.hasClass('iframe-ignore-link') || link.hasClass('modal') || '_blank' == link.attr('target')){
              ignore = true;
            }
              
            if( Protocol !== iProtocol ) {
              console.log( 'Protocals are mismatched. Removing iFrame.')
              mismatch = true
            }
              

              
            if( typeof url !== 'undefined' && url !== '' && ! ignore ){

                // Do default on anchor
                if( url.slice(0, 1) != "#" ){

                  e.preventDefault()

                
                  /** Local link, keep the editing stuff unless ADMIN or a protocol missmatch*/
                  if( hostname.test( url ) && url.indexOf('wp-admin') < 0 && ! mismatch ){

                  
                    // Remove any anchors
                    url = url.split("#")[0]

                    /* Push New URL to browser */
                    browserURL = updateQueryStringParameter(url, 'pl_edit', 'on' );

                    window.history.pushState( "", "", browserURL );
                    
                    
                    url = updateQueryStringParameter( url, 'iframe', 'true' );



                    PLWorkarea.iframe.attr('src', url);

                  }
                  
                  /** Link in new window */
                  else if( '_blank' == $(this).attr('target') ){
                    window.open( url );
                  } 

                  /** Load external link or admin in parent */
                  else {
                   
                    location.href = url;             
                  }
                }

            }
            

          })
    },
    
    // get protocol from url as a string, returns http: or https:
    getProtocol: function( url ) {
      var link = document.createElement('a')
      link.setAttribute( 'href', url )
      return link.protocol
    },

    loadNew: function( url ){

      var that = this 

      if ( typeof url == 'undefined' || ! url)
        var url = PLWorkarea.homeURL;

      /* Choose contents iframe or preview iframe depending on argument */
        var iframe = PLWorkarea.iframe;


      
      /** Trigger unload event on iFrame */
      iframe.trigger('unload')

      iframe.fadeOut(300, function(){

        /* Build the URL */
        iframeURL = url;
        iframeURL = updateQueryStringParameter(iframeURL, 'iframe', 'true');
        iframeURL = updateQueryStringParameter(iframeURL, 'rand', Math.floor(Math.random() * 100000001));

      /* Clear out existing iframe contents */
        

        iframe.contents().find('*')
          .unbind()
          .remove();


        iframe[0].src = iframeURL;

        PLWorkarea.iframe.fadeIn()
      
        //that.loadCallback( callback, iframe );

      })
      

    }, 

  }

  


}(window.jQuery);
