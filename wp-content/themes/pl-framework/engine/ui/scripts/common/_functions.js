

/**
 * Aliased $ for JQuery, apply all functions to window element
 */
!function ($) {

  plAdjustAdminBar = function(){

    if( $('#wpadminbar').length > 0 ){

      var url = window.location.href,

      urlStop = updateQueryStringParameter( url, 'pl_edit', 'off')

      $('[rel="_plDeactivate"]').attr('href', urlStop )

      urlStart = updateQueryStringParameter( url, 'pl_edit', 'on')
      urlStart = updateQueryStringParameter( urlStart, 'pl_start', 'yes')

      $('[rel="plBuilder"]').attr('href', urlStart )

    }
  }

  /**
   * Confirmation modal wrapper
   */
  window.plConfirm = function( clicked, config ){

    var defaults = {
        header:         plTranslate( 'are_you_sure' ),
        subhead:        '',
        details:        '',
        callback:       function(){},
        dontConfirm:    false
    }

    config = $.extend( defaults, config )

    var details = ( config.details != '' ) ? sprintf('<div class="pl-modal-details">%s</div>', config.details) : '',
        subhead = ( config.subhead != '' ) ? sprintf('<div class="pl-modal-subhead">%s</div>', config.subhead) : ''

    var message = sprintf('<h2 class="pl-modal-head">%s</h2>%s%s', config.header, subhead, details)

    /** If set to true, just do the callback. */
    if( config.dontConfirm == true ){

        if ( $.isFunction( config.callback ) )
           config.callback.call( clicked, config )

    }

    else {

        bootbox.setLocale( PLWorkarea.locale )

        bootbox.confirm( message, function( result ){

            if(result === true){

                if ( $.isFunction( config.callback ) )
                    config.callback.call( clicked, config )


            }

            else
                return


        })

    }


  }

    /**
     * Alert modal wrapper
     */
    // window.plAlert = function( config ){

    //     var defaults = {
    //         header:     'Are you sure?',
    //         subhead:    '',
    //         details:    '',
    //         callback:   function(){}
    //     }

    //     config = $.extend( defaults, config )

    //     var details = ( config.details != '' ) ? sprintf('<div class="pl-modal-details">%s</div>', config.details) : '',
    //         subhead = ( config.subhead != '' ) ? sprintf('<div class="pl-modal-subhead">%s</div>', config.subhead) : ''

    //     var message = sprintf('<h2 class="pl-modal-head">%s</h2>%s%s', config.header, subhead, details)

    //     bootbox.alert( message, function( ){

    //        if ( $.isFunction( config.callback ) )
    //             config.callback.call( this, config )


    //     })

    // }

    window.GetUIDs = function( ){

        var uids = []

        jQuery('.site-wrap').find("[data-clone]").each(function(){

            uids.push( $(this).data('clone') )

        })

        return uids

    }

    window.plRenderItem = function( wrapper ){

      var that      = this,
          source    = wrapper.find( '.pl-render-item' ),
          container = source.parent()

      /** if a clone is there, remove it. */
      container.find('.pl-rendered').remove()

      /** Clone the hidden KO version */
      rendered = source.clone().removeAttr('data-bind').removeClass('pl-render-item')

      /** Add identification class, add and show */
      rendered.addClass('pl-rendered').prependTo( container ).show()

      return rendered

    }
    
    /**
     * See if there is a translatable string in the PLWorkarea dataset, if not just pass back str
     */
    window.plTranslate = function( str ){

      if( typeof PLWorkarea != 'undefined' ){
        var key = str.replace(/ /gi,'_').toLowerCase().replace(/(<([^>]+)>)/ig,'')
        if( typeof PLWorkarea.translate[ key ] != 'undefined' ) {
          return PLWorkarea.translate[ key ]
        } else {
          console.log( sprintf( 'no translation string for [%s], needs to be added to engine/lib/i18n.php as [%s] ', str, key ) )
          return str
        }
      }
      else{
        
        return key
      }
    }
}(window.jQuery);

/**
 * Remove classes based on a partial substring
 */
function pl_remove_class_partial( element, partial, wlabel ) {

    var wlabel = wlabel || []

    element.removeClass ( function (index, css) {

        var toRemove = []

        classArray = jQuery(this).attr('class').match(/\S+/g);

        jQuery.each(classArray, function(i, theClass){

            /** Remove if it has the partial and isn't white labeled */
            if( theClass.indexOf( partial ) !== -1 && jQuery.inArray( theClass, wlabel ) == -1 )
                toRemove.push( theClass )

        })

        return toRemove.join(' ')

    });
}


/**
 * Determine if a variable is unset or null
 * @param  {all} variable variable to check
 * @return {bool}          is set or not
 */
function plIsset( variable ){
    if(typeof(variable) !== "undefined" && variable !== null)
        return true
    else
        return false
}

function plPrint( variable ){ // jshint ignore:line
    if( plIsset(PLData) && plIsset(PLData.config) && true === PLData.config.debug )
        console.log( variable )
}

function plIsEmailFormat(email) { // jshint ignore:line
  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}




function shuffle(o){ //v1.0
  for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
  return o;
}

function plRandSort(c) {
    var o = [];
    for (var i = 0; i < c; i++) {
    o.push(i);

    }
    return shuffle(o);
}

function removeQueryStringParameter(url, key) {
    //prefer to use l.search if you have a location/link object
    var urlparts= url.split('?');
    if (urlparts.length>=2) {

        var prefix= encodeURIComponent(key)+'=';
        var pars= urlparts[1].split(/[&;]/g);

        //reverse iteration as may be destructive
        for (var i= pars.length; i-- > 0;) {
            //idiom for string.startsWith
            if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                pars.splice(i, 1);
            }
        }

        url= urlparts[0]+'?'+pars.join('&');
        return url;
    } else {
        return url;
    }
}

function updateQueryStringParameter(uri, key, value) {

    var re = new RegExp("([?|&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";

    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        return uri + separator + key + "=" + value;
    }

}

var encodeHtmlEntity = function(str) {
  var buf = [];
  for (var i=str.length-1;i>=0;i--) {
    buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
  }
  return buf.join('');
};

/* Simple Shortcode System
 * tied to pl_shortcode_url PHP function
 * =============================================
 */
function pl_do_shortcode( opt ) {

    /* If we are not a string, bail out! */
    if( 'string' !== typeof opt || ! opt.length )
        return opt

    var match = opt.match( /\[([^\]]*)/ ) || false
    var shortcode = (match) ? match[1] : false

    if( ! shortcode )
        return opt

    var thePaths = ( typeof PLWorkarea != 'undefined' ) ? PLWorkarea.urls : PLData.urls


    switch(shortcode) {
        case 'pl_child_url':
            opt = opt.replace(/\[pl_child_url\]/g, thePaths.ChildStyleSheetURL) // should link to child theme root
            break;
        case 'pl_parent_url':
            opt = opt.replace(/\[pl_parent_url\]/g, thePaths.CoreURL) // should always be dms root
            break;
        case 'pl_image_url':
            opt = opt.replace(/\[pl_image_url\]/g, thePaths.CoreURL + '/ui/images') // should always be dms root
            break;
        case 'pl_site_url':
            opt = opt.replace(/\[pl_site_url\]/g, thePaths.siteURL) // site root url
            break;
        case 'pl_theme_url':
            opt = opt.replace(/\[pl_theme_url\]/g, thePaths.ParentStyleSheetURL) // parent theme
            break;
        case 'pl_logo_url':
            opt = opt.replace(/\[pl_logo_url\]/g, thePaths.logoURL) // parent theme
            break;
    }
    return opt
}

/*jshint -W098 */

function sectionNameFromObject( object ){

    if( object == 'PL_Content' && ! plIsset( $pl().config.tplRender.template ) ) {
      var name = $pl().config.themename + " Content"
    }

    else if( plIsset(PLWorkarea.factory[ object ]) ) {
      var name = PLWorkarea.factory[ object ].name
    }

    else{
      var name = 'Missing Section'
    }


    return name
}

function plGetTabState( keyText ){

  var key = keyText.replace(/[^A-Z0-9]/ig, "_");

  tabMemory = JSON.parse( localStorage.getItem('plTabMemory') ) || {};

  return tabMemory[ key ] || false

}

function plSetTabState( keyText, state ){

  var key = keyText.replace(/[^A-Z0-9]/ig, "_");

  tabMemory = JSON.parse( localStorage.getItem('plTabMemory') )

  tabMemory = ( ! _.isObject(tabMemory) ) ? {} : tabMemory

  tabMemory[ key ] = state

  localStorage.setItem('plTabMemory', JSON.stringify(tabMemory) )

}

function plItemScope( item ){

    return ( item.parents(".pl-region-wrap-template").length === 1 ) ? 'local' : 'global'
}

function plCallWhenSet( flag, callback, flip ){

    flip = flip || false
    var flagVal = (flip) ? !PLData.flags[flag] : PLData.flags[flag]


    if( ! flagVal ){
        setTimeout(function() {
            plCallWhenSet( flag, callback, flip )
        }, 150)

    } else {
        plPrint('call function')
        callback.call( this )
    }
}

function plUniqueID( length ) {
    length = length || 6

  // Math.random should be unique because of its seeding algorithm.
  // Convert it to base 36 (numbers + letters), and grab the first 9 characters
  // after the decimal.
  return 'u' + Math.random().toString(36).substr(2, length);
}



/* Data cleanup and handling
 * ============================================= */
function pl_html_input( text ) {

    if( typeof text !== 'string')
        return text
    else
        return jQuery.trim( pl_htmlEntities( pl_stripSlashes( pl_urldecode( text ) ) ) )
}


function getURLParameter(name) {

    var val = decodeURI(
        (new RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]
    );


    if( ! plIsset(val) || val == 'null' )
      return false
    else
      return val
}

function pl_stripSlashes (str) {

  return (str + '').replace(/\\(.?)/g, function (s, n1) {
    switch (n1) {
    case '\\':
      return '\\';
    case '0':
      return '\u0000';
    case '':
      return '';
    default:
      return n1;
    }
  });
}

function pl_htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function isset () {

  var a = arguments,
    l = a.length,
    i = 0,
    undef;

  if (l === 0) {
    throw new Error('Empty isset');
  }

  while (i !== l) {
    if (a[i] === undef || a[i] === null) {
      return false;
    }
    i++;
  }
  return true;
}

function basename (path, suffix) {

  var b = path.replace(/^.*[\/\\]/g, '');

  if (typeof(suffix) === 'string' && b.substr(b.length - suffix.length) === suffix) {
    b = b.substr(0, b.length - suffix.length);
  }

  return b;
}




/* Page refresh function with optional timeout.
 * =============================================
 */
function pl_url_refresh(url,timeout){
    if( !url )
        url = window.location.href;
    if(!timeout)
        timeout = 0
    setTimeout(function() {
      window.location.href = url;
    }, timeout);
}

jQuery.fn.getInputType = function(){ return this[0].tagName === "INPUT" ? jQuery(this[0]).attr("type").toLowerCase() : this[0].tagName.toLowerCase(); }

function localStorageSpace() {
    var allStrings = '';
    for(var key in window.localStorage){
        if(window.localStorage.hasOwnProperty(key)){
            allStrings += window.localStorage[key]
        }
    }
    return allStrings ? 3 + ((allStrings.length*16)/(8*1024)).toFixed(2) + ' KB' : 'Empty (0 KB)';
}

function pl_urldecode(str) {
    return unescape(str)
}

function strpos(haystack, needle, offset) {
  //  discuss at: http://phpjs.org/functions/strpos/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Onno Marsman
  // improved by: Brett Zamir (http://brett-zamir.me)
  // bugfixed by: Daniel Esteban
  //   example 1: strpos('Kevin van Zonneveld', 'e', 5);
  //   returns 1: 14

  var i = (haystack + '')
    .indexOf(needle, (offset || 0));
  return i === -1 ? false : i;
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1];
        }
    }
}

function pl_strip_html(html){
   var tmp = document.createElement("DIV");
   tmp.innerHTML = html;
   return tmp.textContent || tmp.innerText || "";
}

function capitalizeFirstLetter( capitalizeMe ){

  return capitalizeMe.charAt(0).toUpperCase() + capitalizeMe.substring(1);

}
