/**
 * Aliased $ for JQuery, apply all functions to window element
 */
!function ($) {

  $( document ).on('ready', function(){


    $('.pl-standard-form').on('submit', function(e){

      e.preventDefault()

      var theForm   = $(this), 
          formdata  = {}, 
          callback  = theForm.data('callback')


      $(this).find('.pl-opt').each(function(){

        var id = $(this).attr('id')

        formdata[ id ] = $(this).val()

      })

     
      var args = {
            hook:         callback,
            formdata:     formdata,
            postSuccess: function( rsp ){

              theForm.attr('data-state', 'sent')
            
            },

            beforeSend: function( ){ 

              theForm.attr('data-state', 'sending')
            }
          }

      
      $plServer().run( args )

      return false;

    })


    $('.pl-form-image-upload').on('click', function(e){

      e.preventDefault()

      var button       = $(this),
          theOption   = button.closest('.image_uploader')
          mode         = button.data('mode'),
          handling    = button.data('handling')


      var upArgs = {
        multiple:   false,
        library: {
           type: mode //Only allow mode
         },
      }

      //Extend the wp.media object
      custom_uploader = wp.media.frames.file_frame = wp.media( upArgs );

      //When a file is selected, grab the URL and set it as the text field's value
      custom_uploader.on('select', function() {

        attachment = custom_uploader.state().get('selection').first().toJSON()

        if( handling == 'id' ){
          var setvalue = attachment.id
        } else {
          var setvalue = attachment.url
        }


        theOption
          .find('.upload_image_option')
          .val( setvalue )

        theOption
          .find('.the_preview_image')
          .attr('src', attachment.url)


      })

      //Open the uploader dialog
      custom_uploader.open()

    })

  })

  

}(window.jQuery);