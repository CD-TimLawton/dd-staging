!function ($) {

  $.plStandardSettings = {

    actionsArray: function(){

      var that = this

      var panels = {
        actions:  {
          title:     'Actions', 
          opts:   [
            {
              type:     'section_actions',
              callback:   that
            }
          ]
        },
      }

      return panels

    },

    settingsArray: function(){

      var that = this

      var settings = {
        spacing: {
          title:  plTranslate( 'padding_margin' ), 
          type:   'multi',
          opts:   that.spacingFields()
        },
        arrangement: {
          title:  plTranslate( 'grid_and_sizing' ), 
          type:   'multi',
          opts:   that.arrangementFields()
        },
        typography: {
          title:  plTranslate( 'font_size_and_alignment' ), 
          type:   'multi',
          opts:   that.typography()
        }, 
        background: {
          title:  plTranslate( 'background_and_color' ), 
          type:   'multi',
          opts:   that.backgroundFields()
        }, 
        advanced: {
          title:  plTranslate( 'advanced' ), 
          type:   'multi',
          opts:   that.advanced()
        }
      }

      return settings

    }, 

    getClasses: function(){

      var that     = this

      $.each( that.settingsArray, function( i, panel ){


        $.each(panel.opts, function(index, opt){

          $.each(opt.opts, function(index, opt){
            
          })

        })
      })

    }, 

    opt_type_section_actions: function(){

      var that       = this,
          out       = '',
          options   = {}

          options.show     = plTranslate( 'show_in_builder' )
          options.del      = plTranslate( 'delete_section' )

      out += $.engineOpts.selectOption( options, '', 'section_actions', 'Select Action...')

      return sprintf('<div class="section-actions">%s</div>', out);
    }, 

    typography: function(){

      var fields = [
              
              { 
                type:   'radio', 
                key:    'alignment',
                label:  plTranslate( 'text_element_align' ), 
                opts: [
                  { hover: plTranslate( 'default' ),     icon: 'minus',          val: '' },
                  { hover: plTranslate( 'left' ),        icon: 'align-left',     val: 'pl-alignment-left' },
                  { hover: plTranslate( 'center' ),      icon: 'align-center',   val: 'pl-alignment-center' },
                  { hover: plTranslate( 'right' ),       icon: 'align-right',    val: 'pl-alignment-right' },
                ]
              },
              

              { 
                type:         'select_proportion', 
                key:          'font_size',
                label:        sprintf( '%s <small>%s</small>', plTranslate( 'font_size' ), plTranslate( 'relative_to_base' ) ), 
                count_start:  50, 
                count_number: 300, 

              },
            ]


      return fields

    }, 

    advanced: function(){

      var that = this

      var fields = [
              { 
                type:   'text', 
                key:    'special_classes',
                label:  plTranslate( 'additional_section_classes' ), 
                help:   sprintf( '%s: " "', plTranslate( 'seperate_space' ) )
              },
              { 
                type:   'text', 
                key:    'hide_on',
                label:  plTranslate( 'hide_on_pages' ), 
                help:   sprintf('<p>%s: ",".</p>', plTranslate( 'hide_with_comma' ) ) 
              },
              {
                type:   'text',
                key:    'custom_name',
                label:  'Custom Name', 
                help:   plTranslate( 'add_custom_section_name' )
              },
              { 
                type:   'section_info', 
                key:    'section_info',
                callback: that,
                label:  plTranslate( 'section_info' ), 
                help:   plTranslate( 'section_info_help' )
              },
              { 
                type:     'section_utilities', 
                key:      'section_utilities',
                callback: that,
                label:    plTranslate( 'utilities' ),
              },
              
            ]
      return fields

    }, 

    opt_type_section_info: function( o ){

      var Class = $.toolEngine.object
      var id    = sprintf( '%s_%s', $.toolEngine.factory[Class].name.toLowerCase().replace(' ','-'), $.toolEngine.UID )

      return sprintf( 'Unique Section ID: %s', id );

    }, 

    opt_type_section_utilities: function( o ){

      var that  = this, 
          clone = $.toolEngine.UID

      var btn   = sprintf('<span class="pl-btn pl-btn-primary clipboard-section"> <i class="pl-icon pl-icon-clone"></i> &nbsp; Copy Section To Clipboard</span>'), 
          conf  = sprintf('<span class="pl-btn clipboard-confirm"> <i class="pl-icon pl-icon-ok"></i> Copied!</span>')

      $.engineOpts.optScripts.utilities = function(){

        new Clipboard('.clipboard-section', {
            text: function(trigger) {

                var map = that.getSectionMap( clone )

                $('.section-copy').addClass('copied')

                setTimeout(function() {
                  $('.section-copy').removeClass('copied')
                }, 3000)

                return JSON.stringify( map );
            }
        });

      }

      return sprintf('<div class="section-copy"><label>Section Cut / Paste</label> %s %s</div>', btn, conf)

    }, 

    getSectionMap: function( clone ){

      var that    = this, 
          theMap  = {}

      theMap = that.getObjectMap( $pl().config.tplMap, clone )

      theMap = that.getTplData( theMap )

      return theMap

    }, 

    getTplData: function( tpl ){

      var that    = this, 
          data    = {}


      tpl.settings = $plModel().getAllSectionData( tpl.clone ) 

    
      if( !_.isEmpty( tpl.content ) ) {

        $.each( tpl.content, function(ind, item){

            tpl.content[ ind ] = that.getTplData( item )
        
        })

      }
      
      return tpl
    
    }, 

    /** Parse template and find the map associated with a clone ID. Recursive. */
    getObjectMap: function( map, clone ){

      var that    = this, 
          tpl     = false
    

      $.each( map, function(ind, item){

        if( ! tpl ){


          if( clone == item.clone ){

            tpl = map[ ind ]

          } 

          else if( !_.isEmpty( item.content ) ) {

            tpl = that.getObjectMap( item.content, clone )

          }

        }

    

      })

      return tpl

    }, 

    arrangementFields: function(){
    
      var fields = [
              { 
                label:   plTranslate( 'grid_controls' ),
                type:   'dragger',  
                opts: [
                  { key: 'col',     icon: 'caret-up',      min: 2, max: 12, unit: plTranslate( 'columns' ) }, 
                  { key: 'offset',   icon: 'caret-right',  min: 0, max: 10, unit: plTranslate( 'offset' ) }
                ]
              },
              { 
                label:  plTranslate( 'content_height_width' ), 
                type:   'dragger',        
                unit:   'vw',
                opts: [
                  { key: 'minheight',      icon: 'arrows-v', def: '0',     min: 0,     max: 200,   unit: plTranslate( 'min_height' ) }, 
                  { key: 'contentwidth',   icon: 'arrows-h', def: '1000',  min: 300,   max: 2000,  unit: plTranslate( 'max_width' ) }, 

                ]
              },
            ]


      return fields
    }, 


    spacingFields: function(){




      var fields = [
            { 
              label:   plTranslate( 'padding' ),
              type:   'dragger',  
              unit:   '%',
              scale:   .2,
              opts: [
                { key: 'padding_top',     icon: 'caret-up',      min: 0, max: 100 }, 
                { key: 'padding_right',   icon: 'caret-right',   min: 0, max: 100 }, 
                { key: 'padding_bottom',  icon: 'caret-down',    min: 0, max: 100 }, 
                { key: 'padding_left',    icon: 'caret-left',    min: 0, max: 100 }
              ]
            },
            { 
              label:   plTranslate( 'margin' ),
              type:   'dragger',    
              unit:   '%',
              scale:   .2,
              opts: [
                { key: 'margin_top',       icon: 'caret-up',      min: -100, max: 100 }, 
                { key: 'margin_right',     icon: 'caret-right',   min: -100, max: 100 }, 
                { key: 'margin_bottom',    icon: 'caret-down',    min: -100, max: 100 }, 
                { key: 'margin_left',      icon: 'caret-left',    min: -100, max: 100 }
              ]
            },

      ]

      return fields

    }, 

    backgroundFields: function(){


      
      var fields = [
              { 
                type: 'image_upload',      
                key: 'background',   
                label: plTranslate( 'background_image' )
              }, 
              { 
                type:   'radio', 
                key:     'theme',
                label:   plTranslate( 'text_element_base_color' ), 
                opts: [
                  { txt: plTranslate( 'default' ),      val: '' },
                  { txt: plTranslate( 'light_text' ),  icon: 'square-o',   val: 'pl-scheme-light' },
                  { txt: plTranslate( 'dark_text' ),   icon: 'square',     val: 'pl-scheme-dark' }
                ]
              },
              
              { 
                type:  'color', 
                key:   'color',
                label: plTranslate( 'background_color' )
              },
              { 
                type:  'color', 
                key:   'textcolor',
                label: plTranslate( 'font_color' )
              },
              
              { 
                type:   'radio', 
                key:    'effects',
                label:  plTranslate( 'size_and_scroll_effects' ), 
                opts: [
                  { txt: plTranslate( 'none' ),      val: '' },
                  { txt: plTranslate( 'window_height' ),  icon: 'arrows-alt',     val: 'pl-effect-window-height' }
                ]
              },
              { 
                type:   'multi', 
                toggle: 'closed',
                title:  plTranslate( 'background_advanced' ), 
                opts: [
                  { 
                    type:   'media_select_video',
                    key:    'video',     
                    label:  plTranslate( 'background_video' )
                  },
                  { 
                    type:  'image_upload',      
                    key:   'overlay',   
                    label: plTranslate( 'background_overlay' )
                  }, 
                  { 
                    type:  'radio', 
                    key:   'bgcover',
                    label: plTranslate( 'background_cover' ), 
                    opts: [
                      { txt: plTranslate( 'none' ),     val: '' },
                      { txt: plTranslate( 'cover' ),    icon: 'image',          val: 'cover' },
                      { txt: plTranslate( 'contain' ),  icon: 'file-image-o',   val: 'contain' }

                    ]
                  },

                  { 
                    type:    'dragger',        
                    label:   plTranslate( 'background_size' ), 
                    opts: [
                      { key: 'bgwidth',   icon: 'arrows-h', def: '500'}, 
                      { key: 'bgheight',   icon: 'arrows-v', def: '500'} 

                    ]
                  },
                  { 
                    type:    'dragger',    
                    label:   plTranslate( 'background_position' ), 
                    unit:    '%',
                    opts: [
                      { key: 'bgxpos',   icon: 'arrows-h', max: 100 }, 
                      { key: 'bgypos',   icon: 'arrows-v', max: 100 } 
                    ]
                  },
                  
                  { 
                    type:  'radio', 
                    key:   'bgrepeat',
                    label: plTranslate( 'background_tile' ), 
                    opts: [
                      { hover: plTranslate( 'tile' ),     icon: 'arrows',     val: 'repeat' },
                      { hover: plTranslate( 'no_tile' ),  icon: 'circle-o',   val: 'no-repeat' },
                      { hover: plTranslate( 'tile_h' ),   icon: 'arrows-h',   val: 'repeat-x' },
                      { hover: plTranslate( 'tile_v' ),   icon: 'arrows-v',   val: 'repeat-y' }
                    ]
                  },
                ]
              },
            ]
      return fields
    },     
  }
}(window.jQuery);
