!function ($) {

$.plShortcodesEngine = {

    init: function(){

      var that = this



      that.bindUIActions(); 


    }, 

    bindUIActions: function(){

      var that = this 

      /** Set height on click because its super tall if not, and not working as documented */
      $('body').delegate('.pl-shortcode-tb', 'click', function(){
        setTimeout( function(){
          $('#TB_ajaxContent').css('height', 'auto')
        }, 30)
            
      })

      $('body').delegate('.plsc-show-list', 'click', function(){
        that.showList()
      })

      $('body').delegate('.btn-add-shortcode', 'click', function(){

        var theBox  = $(this).closest('#TB_window'),
            key      = $(this).data('key'),
            out     = ''

        that.showEngine()

        $('#TB_ajaxContent').css('height', 'auto')

        /** Install Sprintf extension */

        var data = PLAdmin.shortcodes[key]

        var settings = ''

        if( ! $.isEmptyObject( data.settings ) ){
          $.each( data.settings, function(i, o){

            o.key = i

            settings += that.engine(o) 

          })
        }

        else
          settings += '<div class="plsc-option">No settings for this shortcode.</div>'
        

        var backBtn = '<a href="#" class="button plsc-show-list"><i class="pl-icon pl-icon-chevron-left"></i> Back to list</a>'

        out += sprintf( '<div class="plsc-iframe-label" ><span><i class="pl-icon pl-icon-%s"></i> %s Shortcode Setup</span>%s</div>', data.icon, data.title, backBtn)

        out += sprintf( '<div class="plsc-settings" data-key="%s">%s</div>', key, settings)

        $('.plsc-options-container').html(out)

      })

      $('.plsc').on('click', '.plsc-insert', function(e) {

        e.preventDefault()

        var shortcode = that.parseShortcode()

        window.wp.media.editor.insert(shortcode);
      })

    },

    showEngine: function(){
      $('.plsc').addClass('show-engine')
    }, 

    showList: function(){
      $('.plsc').removeClass('show-engine')
    },

    parseShortcode: function(){

      var theShortcodeName = $('.plsc-settings').data('key')

      var theShortcodeAtts = ''

      var theShortcodeContent = ''

      $('.plsc-settings').find('.plsc-input').each( function(){

        var val = $(this).val(), 
            key = $(this).data('key')

        if( val != '' && key != 'content'){
          theShortcodeAtts += sprintf(' %s="%s"', key, val )
        }

        if( key == 'content' ){
          theShortcodeContent += val
        }


      })

      var sc =  sprintf('[%s%s]', theShortcodeName, theShortcodeAtts );

      if( '' != theShortcodeContent )
        sc += sprintf('%s[/%s]', theShortcodeContent, theShortcodeName)
     
      return sc

    }, 

    /** Parses the shortcode options array and creates options */
    engine: function( o ){

      var out = sprintf('<label class="plsc-option-label">%s</label>', o.label)

      var defaultContent = o.default || ''

      if( 'text' == o.type )  {

        // text option
        out += sprintf('<input type="text" class="plsc-input" data-key="%s" value="%s" placeholder="%s" />', o.key, defaultContent, o.place)

      }

      else if( 'textarea' == o.type ){

        out += sprintf('<textarea rows="4" cols="50" class="plsc-input" data-key="%s" placeholder="%s">%s</textarea>', o.key, o.place, defaultContent )
      }

      else if( 'select' == o.type || 'select_same' == o.type || 'count_select' == o.type || 'select_section' == o.type ){

        //First Item is default
        //var options = '<option value="">Select...</option>'
        var options = ''

        if( 'count_select' == o.type ){

          o.opts = {}
                    
          for( i = o.count_start; i <= o.count_end; i++){
            o.opts[i] = i;
          }
                    
        }

        if( 'select_section' == o.type ){

          o.opts = {}
                    
          $.each( PLAdmin.sections, function(i, s){
            o.opts[i] = i;
          })
                    
        }


        $.each( o.opts, function( key, name ){

          var val

          if( 'select_same' == o.type ){
            val = name
          }
          else 
            val = key

          options += sprintf('<option value="%s">%s</option>', val, name)
        })

        out += sprintf('<select class="plsc-input" data-key="%s">%s</select>', o.key, options )

      }

      else{

        out += sprintf('"%s" shortcode option type is missing.', o.type)
      }

      if( o.desc ){
        out += sprintf('<span class="plsc-option-description">%s</span>', o.desc)
      }

      return sprintf('<div class="plsc-option">%s</div>', out);

    }

}

}(window.jQuery);