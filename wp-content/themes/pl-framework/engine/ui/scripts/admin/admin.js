!function ($) {



  // --> Initialize
  $(document).ready(function() {


    $.plAdmin.init()


    $.plOptions.init()

    $.plCustomizer.init()

    $.plShortcodesEngine.init()

    $.plAdminNotifications.init()


  })

  $.plAdminNotifications = {

    init: function(){

      $( '.pl-notice.pl-is-dismissible' ).each( function() {

        var notice = $( this );
        
        if ( notice.find( '.pl-notice-dismiss').empty() ) {

          var btn = $( '<div class="pl-notice-dismiss"><i class="pl-icon pl-icon-remove"></i></div>' );

          notice.find('p').prepend( btn );    

        }

      });

      $( '.pl-notice-dismiss').click( function() {

        var notice = $( this ).closest('.pl-notice')

        var theData = {
                id:       notice.data('id'), 
                exp:      notice.data('exp'),
                action:  'pl_admin_notice'
            }

        notice.remove()

        var args = {
            type:   'POST',
            url:     PLAdmin.ajaxurl,
            
            data:   theData,
            success: function( response ){

              var rsp  = response

            }

          }


        $.ajax( args )


      });



    }

  }


  $.plAdmin = {

    init: function(){

      var that = this,
          pagebase      = window.location.pathname.match(/.*\/([^/]+)\.([^?]+)/i)
          queryString   = window.location.href,
          urlVars       = that.getUrlVars( queryString ),
          refresh       = false, // defualt is no refresh of localstorage
          refresh_array = [ 'plugins', 'themes', 'update', 'update-core' ],
          doc           = ( null !== pagebase ) ? pagebase[1] : 'unknown'

      // possibly clear local storage, we want to flush it when the following occurs:
      // 1. User clicks a refresh button
      // 2. User installs/deletes a plugin or theme
      // 3. User is on the updates page.

      // user click refresh
      if( urlVars.refresh == 1 ){
        refresh = true
        delete urlVars.refresh
      }
      
      // matched pages that need to clear localstorage
      if( $.inArray( doc, refresh_array ) !== -1 ) {
        refresh = true
      }
      
      if( true === refresh ) {
        localStorage.clear();
      }

      if( $('.pl-cards').length > 0 ){

        that.doCardSetup( $('.pl-cards') )

      }

    },

    doCardSetup: function( cards ){

      var that = this
      that.doCardFilterLink( window.location.href )

      $('body').delegate('.pl-filter-links a', 'click', function(e){

        e.preventDefault()

        var href = $(this).attr('href')

        window.history.pushState( "", "", href );

        that.doCardFilterLink( href )

      })
    },

    doCardFilterLink: function( queryString ){

      var that = this,
          hook = $('.pl-cards').data('hook') || 'extend'


      var urlVars = that.getUrlVars( queryString )

      that.cardRequest({
        hook:   hook,
        query:   queryString
      })


    },

    cardRequest: function( config ){

      var that         = this,
          queryString = config.query || window.location.href,
          urlVars     = that.getUrlVars( queryString )

      // Remove wp admin page var  (not needed)
      delete urlVars.page
      delete urlVars.tab

      if( urlVars.refresh == 1 ){
        that.cardCacheReset()
        delete urlVars.refresh
      }

      if( $.isEmptyObject(urlVars) && $('.pl-filter-links a').first().length > 0 ){

        queryString = $('.pl-filter-links a').first().attr('href')

        urlVars = that.getUrlVars( queryString )


      }

      that.doCardTitle( urlVars )



      var dflt = {
          queryVars:  urlVars,
          key:         queryString,
          beforeSend: function(){
            $('.pl-cards').html('<div class="pl-loading-banner"><div class="ic"><i class="pl-icon pl-icon-cog pl-icon-spin"></i></div><div>Loading</div></div>')
          },
          postSuccess: function(r){


            that.doProductCards(r.cards, config.key)

            that.cardCache( config.key, r.cards )
          }
        }

      config = $.extend(dflt, config)

      var cached = that.cardCache( config.key )

      if( cached ){
        that.doProductCards( cached, config.key )

      }

      else{

        that.request( config )
      }


    },



    request: function( config ){

      var that       = this,
          theData   = $.extend({ action: 'pl_platform' }, config )
      
      theData.nonce = PLAdmin.security
        
      delete theData.beforeSend
      delete theData.postSuccess

      var args = {
          type:   'POST',
          url:     PLAdmin.ajaxurl,
          data:   theData,
          beforeSend: function(){

            /** Call the beforeSend callback */
            if ( $.isFunction( config.beforeSend ) )
              config.beforeSend.call( this )

          },

          success: function( response ){

            var rsp  = response

            if ( $.isFunction( config.postSuccess ) )
              config.postSuccess.call( this, rsp )

          }

        }


      $.ajax( args )

    },

    cardCacheReset: function(){
      localStorage.setItem( PLAdmin.cachekey, JSON.stringify({}) )
    },

    cardCache: function( key, data ){

      var that   = this,
          data   = data || false

      /** Set the cache if data var is set */
      if( data !== false ){

        cardCache = JSON.parse( localStorage.getItem( PLAdmin.cachekey ) )


        cardCache = ( typeof(cardCache) !== "undefined" && cardCache !== null) ? cardCache : {}


        cardCache[ key ] = data

        cardCache.timestamp = new Date().getTime().toString()

        localStorage.setItem( PLAdmin.cachekey, JSON.stringify(cardCache) )



        return true

      }

      /** Read the cache */
      else {

        cardCache = JSON.parse( localStorage.getItem( PLAdmin.cachekey) ) || {};

        var dateString   = cardCache.timestamp,
            now         = new Date().getTime().toString(),
            diff         = (now - dateString) / (1000 * 60 * 60 ) // diff in hours

        /** Expire cache after 8 hours */

        return ( diff >= 8 || cardCache[ key ] == null ) ? false : cardCache[ key ]

      }


    },


    doCardTitle: function( urlVars ){

      var that   = this,
          title = 'PageLines Store'

      if( typeof urlVars.s != 'undefined' )
        title = 'Search: ' + urlVars.s

      else if( $('.'+urlVars.navitem).length > 0 ){
        title = $('.'+urlVars.navitem).attr('title')

      }

      $('.pl-filter-links a').removeClass('current')

      $('.'+urlVars.navitem).addClass('current')

      $('.pl-store-title').html(title)

    },



    doProductCards: function(data, queryString){

      var that          = this,
          out           = '',
          pagination    = '', 
          wrapCards     = true


      if( $.isEmptyObject(data) ){

        out += that.banner({
          img:       '',
          header:   'Nothing Found',
          subhead:   'Nothing was found for the selected query.',
          content:   ''
        })
      }

      else if( typeof data.html != 'undefined' ){

        wrapCards = false

        out += data.html
        
      }

      else if( typeof data.error != 'undefined' ){

        out += that.banner({
          img:       '',
          header:   'Error Loading Data',
          subhead:   'If the problem persists please contact PageLines support.',
          content:   sprintf( '<a class="button button-refresh" href="%s"><i class="pl-icon pl-icon-refresh"></i> Refresh User Data</a>', PLAdmin.refreshURL )
        })
        console.log( 'Error: ' + data.error )
        console.log( 'Description: ' + data.error_description )
      }

      else {


        var numPages     = data[0].total_pages || 1,
            pagination   = that.pagination(numPages, queryString)

        $.each(data, function(i, card){

          var thumb        = sprintf( '<a target="_blank" href="%s" class="pl-product-thumb"><img src="%s"></a>', card.product_link, card.thumb),
              desc         = sprintf( '<div class="desc column-description"><p>%s</p></div>', card.post_excerpt),
              name         = sprintf( '<div class="name column-name"><h4><a href="%s">%s</a></h4>%s</div>', card.product_link, card.post_title, that.getCardMetahead(card))
              action       = sprintf( '<div class="action-links">%s</div>', card.actionlink)
              tags         = sprintf( '<div class="card-tags pl-filter-links">Tags: %s</div>', that.getCardTags( card ) ),
              modified     = sprintf( '<div class="modified">Updated: %s ago</div>', card.modified )
              meta         = sprintf( '<div class="product-meta">%s %s %s</div>', that.getCardMeta(card), tags, modified),
              content      = sprintf( '<div class="card-content">%s %s %s</div>%s', name, desc, action, meta)
          out += sprintf('<div class="pl-product-card pl-col-sm-3"><div class="pl-product-card-pad">%s %s</div></div>', thumb, content )
        })

      }




      var wrap = ( wrapCards ) ? sprintf('<div class="pl-row">%s</div>%s', out, pagination ) : sprintf('%s', out)

      $('.pl-cards').html( wrap )

      $('.pl-product-card').each(function(i){

        var element = $(this)

        setTimeout(
          function(){
            element
              .addClass('animation-loaded hovered')

            setTimeout(function(){
              element.removeClass('hovered');
            }, 700);
          }
          , (i * 100)
        );
      })

      $('.pl-cards-nav, .pl-cards-sidebar').addClass('loaded')

    },

    pagination: function( numPages, queryString ){

      var that         = this,
          out         = '',
          currentPage = that.getQueryVar('getpaged') || 1

      for( var i = 1; i <= numPages; i++){

        var cls = ( currentPage == i ) ? 'current' : ''

        out += sprintf('<a class="%s" href="%s&getpaged=%s">%s</a>', cls, queryString, i, i)
      }

      return sprintf('<div class="pl-store-pagination">%s</div>', out)

    },

    getCardMetahead: function( card ){

      var that = this
          meta = []

      if( card.slug.indexOf("pl-framework") > -1 ){

        if( card.slug.indexOf("pl-framework-") > -1 ){
          meta.push( 'Framework Child Theme' )
        }

        else{
          meta.push( 'Framework Theme' )
        }
      }

      else if( card.slug.indexOf("pl-plugin") > -1 ){

        meta.push( 'PageLines Plugin' )

      }

      else if( card.slug.indexOf("pl-section") > -1 ){

        meta.push( 'Drag &amp; Drop Section' )

      }

      else {
        meta.push( 'WordPress Plugin' )
      }


      return sprintf('<div class="metabar">%s%s</div>', meta.join(', '), card.version_html )
    },

    getCardTags: function( card ){


      var that = this,
          href = $('.pl-cards-ui').data('baseurl'),
          meta = []

      $.each( card.tags, function(i, tg ){

        meta.push( sprintf('<a href="%s&product_tag=%s">%s</a>', href, tg.slug, tg.name) )

      })

      return meta.join('<span class="comma">, </span>')

    },

    getCardMeta: function( card ){

      var that = this,
          meta = []

        if( card.product_link )
          meta.push( sprintf( '<a target="_blank" href="%s" class="demo-btn">View</a>', card.product_link ) )

        if( card.reviews > 0 )
          meta.push( sprintf( '<span class="star-rating">%s <span class="num-ratings">(%s)</span></span>', that.getStarRating(card.rating), card.reviews) )

        if( card.demo )
          meta.push( sprintf( '<a target="_blank" href="%s" class="demo-btn">Demo</a>', card.demo ) )

        // if( card.sales > 0 )
        //   meta.push( sprintf( '<span class="num-downloads"> %s sales </span>', card.sales ) )

        if( card.download_count > 0 ) {
          var cnt = card.download_count
          var txt = ( cnt > 1 ) ? 'Downloads' : 'Download'
          meta.push( sprintf( '<span class="num-downloads"> %s %s </span>', card.download_count, txt ) )
        }
        
      return meta.join('<span class="divider">|</span>')
    },

    getStarRating: function( rating ){

      var starRating   = '',
          ratingNum   = Math.round( rating * 2 )/2,
          cls         = 'star'

      for (i = 1; i <= 5; i++) {

          if( ratingNum <= 0 )
            cls = 'star-o'
          else if( ratingNum < 1 )
            cls = 'star-half-full'

          starRating += sprintf( '<i class="pl-icon pl-icon-%s"></i>', cls )

          ratingNum--
      }

      return starRating

    },

    getActionLink: function(card){

      return '<a href="#" class="button button-primary">Link</a>'
    },

    banner: function( args ){

      var that   = this

      banner = sprintf('<div class="pl-col-sm-12"><div class="pl-platform-banner"><div class="pl-platform-banner-inner">%s<div class="banner-body"><div class="banner-body-pad"><h2 class="banner-header" >%s</h2><div class="banner-subheader">%s</div><div class="pl-platform-banner-content">%s</div></div></div><div class="clear"></div></div></div></div>', args.img, args.header, args.subhead, args.content)

      return banner
    },

    getQueryVar: function(theVar) {
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === theVar) {
                return sParameterName[1];
            }
        }
    },

    getUrlVars: function( url ){

      var that = this,
          vars = {},
          hash


      var hashes = url.slice( url.indexOf('?') + 1).split('&')

      for( var i = 0; i < hashes.length; i++){

        hash = hashes[i].split('=')

        vars[ hash[0] ] = hash[1]
      }

      return vars
    },

    isset: function( variable ){
        if(typeof(variable) !== "undefined" && variable !== null)
            return true
        else
            return false
    }
  }

  $.plCustomizer = {

    init: function(){
      var that = this

      if( ! $('body').hasClass('wp-customizer') )
        return

      that.scriptOptions()

      that.editUI()
    },

    editUI: function(){

      var that         = this,
          api         = wp.customize


      $(api).on('ready', function(){

        $('.customize-control-description')
          .addClass('pl-hide')
          .parent()
          .find('.customize-control-title')
            .append('<span class="pl-more-info">About <i class="pl-icon pl-icon-caret-down"></i><i class="pl-icon pl-icon-caret-up"></i></span>')



        $('.pl-more-info').on('click', function(e){

          e.stopPropagation() // inside label
          e.preventDefault() // inside label

          if( $(this).hasClass('show-info') ){

            $(this)
              .removeClass('show-info')
              .closest('label')
              .find('.customize-control-description')
                .addClass('pl-hide')


          }

          else{

            $(this)
              .addClass('show-info')
              .closest('label')
              .find('.customize-control-description')
                .removeClass('pl-hide')

          }

        })
      })




    },

    scriptOptions: function(){

      var that         = this,
          codemirrors = {},
          api         = wp.customize


      $(api).on('ready', function(){

        /**
         * Deal With Codemirror/ Script Options
         */

        $('.pl-code-editor').each( function(i){

          var cm_mode       = $(this).data('mode'),
              cm_config     = $.extend( {}, cm_base_config, { mode : cm_mode } )


          codemirrors[ 'item' + i ] = CodeMirror.fromTextArea($(this).get(0), cm_config)

          codemirrors[ 'item' + i ].el = $(this)

          $(this).parent().addClass('is-ready')

        })

        $( api ).on('expanded', function(){

          $.each( codemirrors, function(i, cm){
            cm.refresh()
          })
        })

        $.each( codemirrors, function(i, cm){

          cm.on('change', function(){

            var value = cm.getValue()

            cm.el.parent().find('.the-value').val(value).trigger('change')

          })

        })



      })



    },
  }







  /** Main Options Panel */
  $.plOptions = {

    init: function(){

      that = this


      /** We are on a PL options page */
      if( $('.pl-admin-settings').length > 0 )
        that.specialOptions()

    },




    specialOptions: function(){

      var that     = this,
          codemirrors = {}


      // Use WP image upload script
      that.imageUploaders()

      // color pickers, using WP colorpicker API
      $('.pl-colorpicker').wpColorPicker().addClass('is-ready')

      // use hidden inputs for checkboxes (0 or 1)
      $('.checkbox-input').on('change', function(){

        var checkToggle = $(this).prev()

        if( $(this).is(':checked') )
            checkToggle.val(1)
        else
            checkToggle.val(0)
      })


      if( $('.pl-settings-tabs').length > 0 ){

        /** Selected tab tracked with hidden input, added as data on wrapper */
        var selTabIndex = $('.pl-settings-tabs').data('selected')

        if( selTabIndex != '' && selTabIndex != 'default' ){
          var selTab = $( '#' + selTabIndex )
        }
        else
          var selTab = $('.pl-tab-panel').first()


        selTab.addClass('selected')


        $( sprintf('[href="#%s"]', selTab.attr('id') ) ).addClass('selected')

        $('.pl-settings-tabs').find('.pl-settings-nav a').on('click', function(e){

          e.preventDefault()

          $('.pl-tab-panel').removeClass('selected')

          $('.pl-settings-nav a').removeClass('selected')

          var panel = $(this).attr('href')

          $( panel ).addClass('selected')

          $(this).addClass('selected')

          if( $(panel).hasClass('tab-disabled') ){
            $(panel).find('.pl-opt').attr('disabled', true)
          }

          /** Refresh */
          $.each( codemirrors, function(i, cm){
            cm.refresh()
          })

          $('.selected_tab_input').val($( panel ).attr('id'))


        })


        $('.pl-settings-tabs').addClass('loaded')



        /** CodeMirror Syntax Highlighted Textareas */
        $('.pl-code-editor').each( function(i){

          var cm_mode     = $(this).data('mode'),
              cm_config     = $.extend( {}, cm_base_config, { mode : cm_mode } )


          codemirrors[ 'item' + i ] = CodeMirror.fromTextArea($(this).get(0), cm_config)

          $(this).parent().addClass('is-ready')

        })

      }

    },

    imageUploaders: function(){

        var custom_uploader

        $('.image_upload_button').on('click', function(e) {

          e.preventDefault()

          var button       = $(this),
              theOption   = button.closest('.image_uploader')
              mode         = button.data('mode'),
              handling    = button.data('handling')


          var upArgs = {
            multiple:   false,
            library: {
               type: mode //Only allow mode
             },
          }

          //Extend the wp.media object
          custom_uploader = wp.media.frames.file_frame = wp.media( upArgs );

          //When a file is selected, grab the URL and set it as the text field's value
          custom_uploader.on('select', function() {

            attachment = custom_uploader.state().get('selection').first().toJSON()

            if( handling == 'id' ){
              var setvalue = attachment.id
            } else {
              var setvalue = attachment.url
            }


            theOption
              .find('.upload_image_option')
              .val( setvalue )

            theOption
              .find('.the_preview_image')
              .attr('src', attachment.url)


          })

          //Open the uploader dialog
          custom_uploader.open()

        });

    }
  }


}(window.jQuery);
