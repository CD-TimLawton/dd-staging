!function ($) {

  $(document).ready(function() {
    
    window.startTime = new Date().getTime();

    $.plModel.init()

    var end = new Date().getTime();
    var time = end - startTime;

    plTop()('.iframe-loading-overlay').removeClass('show-overlay')

    plTrigger('ready')

    
  })

  $.plModel = {

    init: function(){

      var that = this

      /** Set initial page data model */
      that.setMasterModel()

      $.plBinding.startUp()
      
      

    }, 

    /**
     * Get JSON in model (iframe) as if getting it from parent frame it wont work.
     * @return {json} model json string
     */
    getJSON: function(){

      return ko.toJSON( PLData.viewModel )
 
    }, 

    /**
     * Used for settings values when editing.
     */
    getData: function(){
 
      return ko.toJS( PLData.viewModel )
 
     }, 

    getSectionOption: function( UID, key ){

      return ( plIsset(PLData.viewModel[ UID ]) ) ? PLData.viewModel[ UID ][ key ]() : false

    }, 

    setSectionOption: function( UID, key, value ){

      PLData.viewModel[ UID ][ key ]( value )

    }, 

    getAllSectionData: function( UID ){

      var data = {}

      if( plIsset( PLData.viewModel[ UID ] ) ){

        $.each( PLData.viewModel[ UID ], function(key, val){



          var theValue = PLData.viewModel[ UID ][ key ]()



          if( plIsset(theValue) ){

            if( _.isArray(theValue) ){

              data[ key ] = {}

              $.each( theValue, function( ai, av ){

                data[ key ][ ai ] = {}

                $.each( av, function( arrayKey, arrayKeyObs ){

                  data[ key ][ ai ][ arrayKey ] = arrayKeyObs()

                })
                
              })

            }

            else{
              data[ key ] = theValue
            }
            
          }

          

        })
      
      }

      return data 
    }, 

     updateModelData: function(){

       var  that                = this,
            currentModelData   = that.getData()


         $.each( currentModelData, function(UID, keys){

           $.each( keys, function(key, val){

              if( ! plIsset( PLData.modelData[ UID ] ) ){
                PLData.modelData[ UID ] = { values: {} }
              }

             if( plIsset( val ) ){

              if( ! plIsset( PLData.modelData[ UID ].values[ key ] ) ){
                PLData.modelData[ UID ].values[ key ] = {}
              }

              PLData.modelData[ UID ].values[ key ].value = val

               // /** Standard data format */
               // if( plIsset(PLData.modelData[ UID ].values[ key ]) && plIsset(PLData.modelData[ UID ].values[ key ].value) ){
               //   PLData.modelData[ UID ].values[ key ].value = val
               // } 
               // /** For unknown options (js only) probably should be made consistent */
               // else {
               //   PLData.modelData[ UID ].values[ key ] = val

               // }

               
             }
           
           })

         })


     },


    

    

    /**
     * Loads all sections models based on page load.
     */
    setMasterModel: function(){

      var that     = this

      PLData.viewModel   = PLData.viewModel || {}

      $.each( PLData.modelData, function( UID, theData ) {
        
        that.loadSectionModel( UID, theData )  
        
      })

    

    },


    loadSectionModel: function( UID, theData ){

      var that = this

      /** If model entry exists, don't overwrite it */
      if( typeof PLData.viewModel[ UID ] == 'undefined' ){

        var mod = {}

        if(  !_.isEmpty( theData ) ){

          /** Set up basic observables */
          $.each( theData.values, function( key, info ) {

            if( ! plIsset( info ) ){
              console.log( 'View model for ' + key + ' is set incorrectly.')
            
            } 

            else {
              
              var val   = info.value, 
                  type   = info.type, 
                  opts   = info.opts
              

              
              mod = that.setObservables( mod, key, theData, type, opts)

            }

            
            
            

          })

          PLData.viewModel[ UID ] = mod


        } 



        /**
         * Standard settings
         */
        var standard = $.plStandardSettings.settingsArray()

        $.each( standard , function( title, panel ) {

          that.getSectionObservables( UID, panel.opts )

        })

      }


    }, 

    

    setObservables: function( mod, key, data, type, opts){

      var that = this, 
        val   = that.getValueFromKey( data, key )
      
        
      if( _.isArray( val ) || _.isObject( val ) ){

        mod[ key ] = that.makeObservableArray( val, type, opts )

      }

      else {
        mod[ key ] = ko.observable( val ).extend({ notify:'always' })
      }

      return mod

    },


    getValueFromKey: function( data, key ){

      return data.values[ key ].value;

    }, 

    asyncComputedArray: function(evaluator, async_function ) {
        var result = ko.observableArray();

        ko.computed( function() {
            // // Get the $.Deferred value, and then set up a callback so that when it's done,
            // // the output is transferred onto our "result" observable
            evaluator
              .call( async_function )
              .done( function( response ){

                var rsp  = response

                var theArray = []

                $.each( rsp.template, function(i, val){
                  theArray.push(val)
                })


                result(theArray)

              })
        });

        return result;
    },

    makeObservableArray: function( array, type, opts ) {

      
      var that = this,
        normalizedData = [];

      $.each( array, function (index, data) {

        /** Make sure all opts have observables */
        $.extend( opts, data )

        normalizedData.push( that.itemModel( opts ) );
      
      });


      return ko.observableArray(normalizedData);

    },

    itemModel: function( item ){

      var that  = this,
        model = {}


      $.each( item, function( key, val ){

        model[ key ] = ko.observable( val ).extend({ notify:'always' })

      })

      return model

    },

    

    getSectionObservables: function( UID, opts ){

      var that = this

      $.each( opts , function(i2, o) {

        /** If we have sub values, parse them */
        if( !_.isEmpty( o.opts ) ){

          that.getSectionObservables( UID, o.opts )

        } 

        /** If a key is set, then create observable */
        if( !_.isUndefined(o.key) && ! plIsset( PLData.viewModel[ UID ][ o.key ] ) ) {

          PLData.viewModel[ UID ][ o.key ] = ko.observable( that.getInitValue(UID, o.key) ).extend({ notify:'always' })

        }
        

      })


    },

    getInitValue: function( UID, key ){


      if( plIsset( PLData.modelData[ UID ] ) && plIsset( PLData.modelData[ UID ].values ) )
        return PLData.modelData[ UID ].values[ key ]
      else 
        return null
    }, 


    sortObservableArrayByArray: function( UID, key, sortArray ){

      var that     = this,
        newArray   = []

      theArray = PLData.viewModel[ UID ][ key ]()

      $.each( sortArray, function(i, val){

        newArray.push( theArray[ val ] )

      })      

      PLData.viewModel[ UID ][ key ].changeEvent = true

      PLData.viewModel[ UID ][ key ]( newArray )

    },

    RemoveItemByIndex: function( UID, key, index ){

      var that = this

      /** For triggers */
      PLData.changeEvent = true

      PLData.viewModel[ UID ][ key ].splice( index, 1 )

    }, 

    addItemToArray: function( UID, key, item ){

      var that = this

      /** For triggers */
      PLData.changeEvent = true

      observableItem = that.itemModel( item )

      PLData.viewModel[ UID ][ key ].push( observableItem )

    }, 

    setObservableArray: function( UID, key, array ){

      var that = this

      PLData.viewModel[ UID ][ key ] = ko.observableArray( array ).extend({ notify:'always' })

    },

    setNewObservable: function( UID, key, val){

      var that = this

      PLData.viewModel[ UID ][ key ] = ko.observable( val ).extend({ notify:'always' })



    }, 

  

  }



}(window.jQuery);
