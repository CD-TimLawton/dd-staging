!function ($) {


  /*
   * AJAX Actions
   */
  $.plServer = {

    run: function( opts ){
      
      var  that = this,
        config = {
            action:       'pl_server',               // tied to hook wp_ajax_pl_server
            hook:         '',                        // the PL hook to run. Ie: pl_server_ . $hook
            pageID:       PLData.config.pageID,       // ID for the current page
            typeID:       PLData.config.typeID,       // ID for current page type
            editID:       PLData.config.editID,       // ID currently being edited
            editslug:     PLData.config.editslug,       // ID for current page type
            tplMode:       PLData.config.tplMode,      // Data mode for page
            tplActive:     PLData.config.tplActive,    // Active Layout (if any)... 
            tplCapture:   PLData.config.tplCapture,    // Capture static template
            nonce:         PLData.config.nonce,
            beforeSend:   '',
            postSuccess:   '',
            args:         {}

        }

      /** merge args into config, overwriting config w/ opts */
      $.extend( config, opts )
      
      if( ! plIsset( config.hook ) ){

        console.log('No hook set for AJAX server request.');

        return false
      
      } else {

        return that.doAJAX( config )

      }

      
      

    },

    doAJAX: function( config ){

      var that = this

      /** Remove the callbacks from the data sent. Create a new object using extend */
      theData = $.extend({}, config)
      delete theData.beforeSend
      delete theData.postSuccess

      

      var args = {
          type:   'POST',         // POST request type
          url:   PLData.urls.ajaxURL,    // ajaxurl standard WP ajax URL
          data:   theData,         // the data we're sending
          beforeSend: function(){
              
            /** Call the beforeSend callback */      
            if ( $.isFunction( config.beforeSend ) )
              config.beforeSend.call( this )

          }, 
        
          success: function( response ){

            console.log(response)
            
            /** Turn response JSON string into JS object */      
            var rsp  = response
            
            console.log(rsp)

            /** Call the postSuccess callback */    
            if ( $.isFunction( config.postSuccess ) )
              config.postSuccess.call( this, rsp )

          },
          
          error: function( jqXHR, status, error ){
            
            console.log('------ AJAX Error ------')
            console.log( jqXHR )
            console.log( status )
            console.log( error )

          }
        }

      /** 
       * Do AJAX Request
       * NOTE that nested objects must be of consistent type, mixed numeric/associative objects cannot be passed.
       */
      return $.ajax( args )

    }


  }
}(window.jQuery);
