!function ($) {
  


  $.plAdd = {


    newSection: function( object, UID, newItem ){
      

      var that   = this, 
        args = {
          hook:     'load_section',
          object:   object,
          UID:       UID,
          query:     PLData.config.query,
          
          postSuccess: function( rsp ){

            var render    = PLData.config.tplRender,
                capture   = PLData.config.tplCapture, 
                bldAdd    = 'template'

            /** Add to Builder */
            $( newItem ).prependTo( plTop()( sprintf('.dd-list[data-region="%s"]', bldAdd) ) ).hide().fadeIn()

            /** Load template, add it after the loading section div */
            $( rsp.template ).prependTo( sprintf('.pl-region-wrap-%s', bldAdd ) )
            


            var newSection = $( sprintf('[data-clone="%s"]', UID))

            that.addScripts( rsp )

            $.plScrolling.scrollToSection( newSection )


            /** Reset D&D Interface */
            plTop().plEditing.reloadUI()

            $.plBinding.bindNewSection( UID, {
              object: object, 
              values: rsp.model
            })

            /** Update data to match the view */
            $.plModel.updateModelData()
            
            /** Update Map JSON */
            plTop().plBuilder.updateTemplateMap()

            plTop().plBuilder.doSortables()

          }
        }

      

      $.plServer.run( args )
    }, 

    /** 
     * Add Styles and Scripts.  
     * Must come after template is added for appropriate click event tracking.
     */
    addScripts: function( rsp ){

      var that = this

      /** Add CSS style.css */
      if( plIsset( rsp.css_style ) )
        $('head').append( sprintf('<link rel="stylesheet" href="%s?rand=%s" type="text/css" />', rsp.css_style, Math.floor(Math.random()*999999) ));

      /** Add CSS build.css */
      if( plIsset( rsp.css_build ) )
        $('head').append( sprintf('<link rel="stylesheet" href="%s?rand=%s" type="text/css" />', rsp.css_build , Math.floor(Math.random()*999999) ));

      /** Add JS to footer */
      if( plIsset( rsp.scripts ) && rsp.scripts.length != 0 ){

        $.each( rsp.scripts, function(index, value){
        
          $('body').append( sprintf('<script id="live-injection" src="%s"></script>', value ) );
        
        })
        
      }

    }

  }

  
}(window.jQuery);