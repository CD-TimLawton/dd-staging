!function ($) {
  
  $(window).on('pl_page_ready', function() {
    $.plAnimation.init()
  })

  $.plAnimation = {
      
    init: function(){
      
      var that = this 


      $('body').delegate('.pl-sn', 'template_ready', function(){

        $.plAnimation.doAnimation( $(this) )

      })
            
      
      
    },

    doAnimation: function( selector ){

      var selector = selector || $('body')
      
        
      selector.find('.pl-animation-group')
        .find('.pl-animation')
        .addClass('pla-group')
        
      selector.find('.pl-animation:not(.pla-group, .animation-loaded)').each(function(){

        var element = $(this)



        element.appear(function() {

            if( element.hasClass('pl-slidedown') ){

              var endHeight = element.find('.pl-end-height').outerHeight()
              
              element.css('height', endHeight)

            }


           $(this)
            .addClass('animation-loaded')
            .trigger('animation_loaded')

        })

      })
        


      selector.find('.pl-animation-group').each(function(){
        
        var element = $(this)

        element.imagesLoaded( function( instance ){

          element.appear(function() {
            
            var animationNum = $(this).find('.pl-animation').size()
            
            
            
               $(this)
              .find('.pl-animation:not(.animation-loaded)')
              .each( function(i){
                var element = $(this)
                
                setTimeout(
                  function(){ 
                    element
                      .addClass('animation-loaded hovered')
                    
                    setTimeout(function(){ 
                      element.removeClass('hovered');
                    }, 700); 
                  }
                  , (i * 200)
                );
              })

          })

        })

      })

      
      
      
    }
    
  }

  
}(window.jQuery);