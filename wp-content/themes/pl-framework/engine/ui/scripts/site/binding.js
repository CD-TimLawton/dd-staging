 !function ($) {



  $.plBinding = {

    startUp: function(){

      var that = this

      that.plExtensions()


      /** Extend Knockout for PageLines functionality */
      that.extensions()

      $('body').trigger('pl_extend_bindings')

      /** Iterate through each section and apply observable bindings (model to view) */
      that.applyModel()
    }, 

   //   renderedItem: function( wrapper ){

    //   var that       = this,
    //       source     = wrapper.find( '.rendered-item' ),
    //       container = source.parent()

    //   /** if a clone is there, remove it. */
    //   container.find('.rendered').remove()

    //   * Clone the hidden KO version 
    //   rendered = source.clone().removeAttr('data-bind').removeClass('rendered-item')

    //   /** Add identification class, add and show */
    //   rendered.addClass('rendered').prependTo( container ).show()
      
    //   return rendered

    // }, 

    /** 
     * Trigger for other JS effects
     * NOTE: must come after binding so new values are in place
     */
    doTemplateTriggers: function( element, valueAccessor, allBindings ){

      var that   = this,
        value   = valueAccessor()

      
      if( PLData.changeEvent ){
      
        if( $(element).hasClass('pl-trigger-el') ){
          $(element).closest('.pl-trigger-container').trigger('template_updated')
        }

        else if( $(element).hasClass('pl-trigger') ){
          $(element).closest('.pl-sn').trigger('template_ready')
        }

        else 
          $(element).trigger( 'template_updated' )

      }


    },

    doTemplateAJAX: function( mode, args, element, valueAccessor, allBindings ){

      var that     = this,
          value     = ko.unwrap( valueAccessor() )

      
      if( plIsset( value ) && ( PLData.changeEvent || $( element ).hasClass('pl-load-lazy') ) ){
      
        var config = {
          hook:     'async_binding',
          value:     value,
          mode:     mode,
          args:     args, 
          postSuccess: function( rsp ){


            wrap = sprintf('<div class="new">%s</div>', rsp.template)

            $( element )
              .html( wrap )

            var UID       = $(element).closest('.pl-sn').trigger('template_ready').data('clone')
                newWrapper   = $( element ).find('.new')

            ko.applyBindings( PLData.viewModel[ UID ], newWrapper[0] );

            /** Remove temporary wrapper*/
            newWrapper.children().first().unwrap()
              
            // if( plIsset( rsp.engine ) ){
            //   var styles = '', 
            //       out     = ''

            //   if( _.isObject( rsp.engine['scripts'] ) ){
            //     $.each( rsp.engine['scripts'], function(i, sc){
            //       out += sc
            //     })
            //   }
              
            //   if( _.isObject( rsp.engine['styles'] ) ){
            //     $.each( rsp.engine['styles'], function(i, sc){
            //       styles += sc
            //     })

            //     out += sprintf('<style>%s</style>', styles)
            //   }
              
            //   $('body').append(out)

            // }

            that.doTemplateTriggers( element, valueAccessor, allBindings )
            

          }
        }

        $.plServer.run( config )


      }



    },

    /**
     * This class is primarily used to set class names to option values 
     * It saves the previous or original value as a data attribute, so it can be specifically removed
     *
     */
    doClass: function( element, value, index ){

      var index = index || '', 
          saved = '__pl__previousClassValue__' + index

        
      if (element[ saved ]) {
          $(element).removeClass(element[ saved ]);
      }

      $(element).addClass(value);
    
      element[ saved ] = value;
    
    }, 

    plExtensions: function(){

      var that = this


      ko.bindingHandlers.plnav = {

          update: function( element, valueAccessor, allBindings ) {


              var value   = ko.unwrap( valueAccessor() ),
                    el       = $(element),
                  mode     = el.data('mode'), 
                  clss     = el.data('class')


              that.doTemplateAJAX( 'menu', { menu_class: clss, mode: mode }, element, valueAccessor, allBindings )
            


          }
      };

      ko.bindingHandlers.plsidebar = {

          update: function( element, valueAccessor, allBindings ) {
       
              var value   = ko.unwrap( valueAccessor() ),
                    el       = $(element)

              that.doTemplateAJAX( 'sidebar', { }, element, valueAccessor, allBindings )
  
          }
      };

      ko.bindingHandlers.plcallback = {

          update: function( element, valueAccessor, allBindings ) {

              var value   = ko.unwrap( valueAccessor() ),
                    el       = $(element),
                  mode     = el.data('callback')

              

              that.doTemplateAJAX( mode, { mode: mode }, element, valueAccessor, allBindings )
            


          }
      };

      ko.bindingHandlers.plshortcode = {

          update: function( element, valueAccessor, allBindings ) {


              var value = ko.unwrap( valueAccessor() );

              if( that.isset( value ) && value != '' ){

                
                /** Look for modify event because we dont change out these on load... */
                if(  valueAccessor().modifyEvent  )
                  $(element).html( value ).removeClass('js-unset')

              } 
              else 
                $(element).addClass('js-unset')

            
            that.doTemplateAJAX( 'shortcodes', {}, element, valueAccessor, allBindings )

          }
      };

      ko.bindingHandlers.plbtn = {

          update: function( element, valueAccessor, allBindings ) {
 
              /** Get Option Key */
              var value     = ko.unwrap( valueAccessor() ),
                    el         = $(element),
                  model     = ko.dataFor( element )

              

              var txt       = model[ value + '_text' ]()   || 'Button', 
                  clss       = model[ value + '_style' ]() || 'default', 
                  size       = model[ value + '_size' ]()   || 'st', 
                  link       = model[ value ]()

              


               el.html( txt )
                
             
              if( ! that.isset( link ) || link == '' )
                el.addClass('js-unset')
              else
                el.attr( 'href', link ).removeClass('js-unset')
                

            
              that.doClass( element, 'pl-btn-' + clss, 'style' )
               that.doClass( element, 'pl-btn-' + size, 'size' )


          }
      };


      /**
       * Use this binding to set an option value as a classname
       * Multiple classnames can be binded if an array is used [ key, key ]
       */
      ko.bindingHandlers.plclassname = {
            'update': function( element, valueAccessor, allBindings ) {

              var value = ko.unwrap( valueAccessor() );


              if( _.isArray( value ) ){

                $.each( value, function( i, val ){

                  that.doClass( element, val, i )
                })

              }

              else {
                that.doClass( element, value )
              }

              that.doTemplateTriggers( element, valueAccessor, allBindings )
          }
      };
      

      /**
       * This binding allows you set an arbitrary css class for an element. It requires jQuery.
       * https://github.com/knockout/knockout/wiki/Bindings---class
       */
      ko.bindingHandlers['class'] = {
            'update': function( element, valueAccessor, allBindings ) {

              var value = ko.unwrap( valueAccessor() );

              that.doClass( element, value )

              that.doTemplateTriggers( element, valueAccessor, allBindings )
          }
      };

      


      /** Set Background Images... */
      ko.bindingHandlers.plbg = {
          update: function( element, valueAccessor, allBindings ) {

            var value     = ko.unwrap( valueAccessor() ), 
                args       = allBindings.get( 'args' )    || {},
                url       = ''


            if( that.isset( value ) ){

              url = pl_do_shortcode( value )            

              $(element).css( 'backgroundImage', sprintf( 'url(%s)', url ) )
            
            }
            
            else
              $(element).css( 'backgroundImage', '' )

            

            that.doTemplateTriggers( element, valueAccessor, allBindings )

          }
      };

      /**
       * Handler for Images SRC.
       * Will apply on load if the value of the image src is not empty.
       * If it is empty, it will add the js-unset class which will hide the element.
       */
      ko.bindingHandlers.plimg = {
          update: function( element, valueAccessor, allBindings ) {

            var value = ko.unwrap( valueAccessor() )

            if( that.isset( value ) && value != '' )
              $(element).attr('src', pl_do_shortcode( value ) ).removeClass('js-unset')
            else 
              $(element).removeAttr('src').addClass('js-unset')

            /** do triggers on elements for redraw */
            that.doTemplateTriggers( element, valueAccessor, allBindings )

          }
      };


      /**
       * Handler for text and HTML
       * Will replace contents of the element on load if the value is not empty.
       * If it is empty, it will add the js-unset class which will hide the element.
       */
      ko.bindingHandlers.pltext = {
          update: function(element, valueAccessor, allBindings) {

            var value = ko.unwrap( valueAccessor() );

            

            if( that.isset( value ) && '' !== value ){

              $(element).html( value ).removeClass('js-unset')
  
            } 

            else 
              $(element).addClass('js-unset')

            that.doTemplateTriggers( element, valueAccessor, allBindings )

          }
      };

      /**
       * Set or remove HREF attribute for selected styling ([href])
       */
      ko.bindingHandlers.plhref = {
          update: function( element, valueAccessor, allBindings ) {


          var value = ko.unwrap( valueAccessor() );

          if( that.isset( value ) && value !== '' ){
            $(element).attr('href', pl_do_shortcode( value ) )
          }

          else
            $(element).removeAttr('href')

          that.doTemplateTriggers( element, valueAccessor, allBindings )

          }
      };




    }, 

    isset: function( value ){

      if( typeof value !== 'undefined' && value !== null ){
        return true; 
      }

      else
        return false

    }, 


    applyModel: function(){
      

      var that = this
      
      var count   = $('.pl-sn').length, 
          initial = 1


    
      $('.pl-sn').each( function(i) {
        

        var UID         = $(this).data('clone'), 
            theSection   = $(this),
            addPosts     = false


        if( plIsset(UID) ){

          

          setTimeout(function () {

              ko.applyBindings( PLData.viewModel[ UID ], theSection[0] );

              that.startSection( theSection )             
           
           });

          

        }

    

      })

      // $('.static-template-content').each( function(i){

      //   var UID   = PLData.config.editID, 
      //     theTpl  = $(this)

        

      //   ko.applyBindings( PLData.viewModel[ UID ], theTpl[0] );

      // })
      


    },

    startSection: function( theSection ){
      
      /** 
       * IF a temporary wrapper is used, we should remove now.
       * These are useful to prevent sub binds, better than comments which error out 
       */
      if( theSection.parent().hasClass('temp-wrap') ){
        theSection.unwrap()
      }

      /** Show the section and trigger ready event so we can do actions like animate. */
      theSection
        .trigger( 'template_ready' )
        .addClass( 'js-loaded' )
  
        

    }, 

    bindNewSection: function( UID, theData ){

      var that     = this, 
          theSection   = $( sprintf('[data-clone="%s"]', UID ) )

      /** In case new bindings are added. */
      $('body').trigger('pl_extend_bindings')


      if( theSection.length ){

        /** Create observables */
        $.plModel.loadSectionModel( UID, theData )

        $.each( PLData.viewModel[ UID ], function(key, func){
          PLData.viewModel[ UID ][ key ].modifyEvent = true
        })

        /** Apply binding */
        that.applySectionBinds( PLData.viewModel[ UID ], theSection )

      } else {
        console.log('Error: A bindable section was not found.')
      }

      


    }, 

    applySectionBinds: function( model, section ){

      var that = this

      ko.applyBindings( model, section[0] );

      that.startSection( section )

    }, 

    

    extensions: function(){

      var that = this


      /**
       * Main value replacement binding
       * Optionally pulls information from server based on input
       */
      // ko.bindingHandlers.plsync = {

      //     update: function( element, valueAccessor, allBindings ) {

    
      //       var runQuery = false 


      //         // First get the latest data that we're bound to
      //         var value = valueAccessor();

      //         // Next, whether or not the supplied model property is observable, get its current value
      //         var valueUnwrapped = ko.unwrap( value );

      //         /** The type of AJAX request to make, passed as sync_mode: [type] */
      //         var mode     = allBindings.get( 'sync_mode' )   || 'shortcodes', 
      //           args     = allBindings.get( 'args' )    || {}, 
      //           runOnStart   = args.runStart         || false 

      //         if( typeof valueUnwrapped !== 'undefined' && valueUnwrapped !== ''){

                

      //           /** Change Event value only triggers AJAX on change events not keyups or fast moving changes */
      //            if( PLData.changeEvent ){
                  
      //              runQuery = true

      //            } 

      //            * If shortcodes or autop (text), set value as we type 
      //            else if( value.modifyEvent && ( mode == 'shortcodes' || mode == 'autop') ) {

      //              $(element).html( valueUnwrapped )

      //            }

      //         } 

      //         else {
            
      //       valueUnwrapped = $(element).html()

      //         }

      //         /** Run server query */
      //         if( runQuery ){


      //           var config = {
      //            hook:   'async_binding',
      //            value:   valueUnwrapped,
      //            mode:   mode, 
      //            args:   args, 
      //           postSuccess: function( rsp ){

      //             value.changeEvent = false

      //             /** Trigger an event on the element so we can do more JS */
      //             trigger = rsp.trigger || 'sync'

      //             /** Set the AJAX response template into the element */
      //             $( element )
      //               .html( rsp.template )
      //               .trigger( trigger )
                
                  
    
      //           }
      //         }

      //       $.plServer.run( config )

            
      //       /** Trigger template change event */
      //       that.doTemplateTriggers( element, valueAccessor, allBindings )

      //         }
       
      

      //     }
      // };

      /**
       * Extends the KO foreach binding and triggers an event on the element whenever the obs is updated.
       */
      ko.bindingHandlers.plstyle = {
      
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
          
          var value           = valueAccessor(), 
            valueUnwrapped    = ko.utils.unwrapObservable( value );
          
          ko.bindingHandlers.style.update(element, valueAccessor, allBindings, viewModel, bindingContext);

          /** do triggers on elements for redraw */
          that.doTemplateTriggers( element, valueAccessor, allBindings )

          return { controlsDescendantBindings: true };

        }
      }

      /**
       * Extends the KO foreach binding and triggers an event on the element whenever the obs is updated.
       */
      ko.bindingHandlers.plforeach = {
        init: function(element, valueAccessor) {

          return ko.bindingHandlers.foreach.init(element, valueAccessor);

        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
          
          /** Note value must be unwrapped for this to be triggered. */
          var value       = valueAccessor(), 
            valueUnwrapped   = ko.utils.unwrapObservable( value );
          
          ko.bindingHandlers.foreach.update(element, valueAccessor, allBindings, viewModel, bindingContext);

          /** do triggers on elements for redraw */
          that.doTemplateTriggers( element, valueAccessor, allBindings )

          return { controlsDescendantBindings: true };

        }
      }

      /**
       * Extends the KO template binding and triggers an event on the element
       */
      ko.bindingHandlers.pltemplate = {
        init: function(element, valueAccessor) {
          return ko.bindingHandlers.template.init(element, valueAccessor);
        },
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

    
          var value = valueAccessor()
          
                if( _.isEmpty( value.foreach ))
                  return 
                

          ko.bindingHandlers.template.update(element, valueAccessor, allBindings, viewModel, bindingContext);

          /** do triggers on elements for redraw */
          that.doTemplateTriggers( element, valueAccessor, allBindings )

          return { controlsDescendantBindings: true };

        }
      }

      /**
       * Extends CSS binding to add an event
       */
      ko.bindingHandlers.plcss = {
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

          var value = valueAccessor()
          
                ko.bindingHandlers.css.update(element, valueAccessor, allBindings, viewModel, bindingContext);

          /** do triggers on elements for redraw */
          that.doTemplateTriggers( element, valueAccessor, allBindings )

          return { controlsDescendantBindings: true };

        }
      }


      ko.bindingHandlers.plvisible = {
        update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {

          var value = valueAccessor()
          
          ko.bindingHandlers.visible.update(element, valueAccessor, allBindings, viewModel, bindingContext);

          /** do triggers on elements for redraw */
          that.doTemplateTriggers( element, valueAccessor, allBindings )

          return { controlsDescendantBindings: true };

        }
      }




      /**
       * This prevents Knockout from removing non knockout bindings when we use ApplyBindings
       * http://knockoutjs.com/documentation/custom-bindings-disposal.html#overriding-the-clean-up-of-external-data
       */
      ko.utils.domNodeDisposal.cleanExternalData = function () {
          // Do nothing. Now any jQuery data associated with elements will
          // not be cleaned up when the elements are removed from the DOM.
          // 
      };

      

      /**
       * Take a \n based entry and split into icon/urls 
       * Used in sections like footer, sociallinks, mobilemenu (megamenu)
       */
      ko.bindingHandlers.plicons = {
          update: function( element, valueAccessor, allBindings ) {


            var value     = ko.unwrap( valueAccessor() ),
                items     = value.split("\n");

            var theList = ''


            if( typeof value !== 'undefined' && value !== '' ){              
              
              $.each(items, function(i, item){

                pieces   = item.split(" "), 
                icon     = ( plIsset(pieces[0]) ) ? pieces[0] : '',
                url     = ( plIsset(pieces[1]) ) ? pieces[1] : ''

                if( '' != icon ){
                  theList += sprintf('<a class="iconlist-link" href="%s"><i class="pl-icon pl-icon-%s"></i></a>', url, icon )
                }

              })              
              
            }

            $(element)
              .html(theList)

            /** Trigger template change event */
            that.doTemplateTriggers( element, valueAccessor, allBindings )

          }
      };
      

      /**
       * Take a \n based entry and create a list markup
       */
      ko.bindingHandlers.pllist = {
          update: function( element, valueAccessor, allBindings ) {


          var value     = ko.unwrap( valueAccessor() ),
              flag     = allBindings.get('flag') || false,
              items     = value.split("\n");



          if( typeof value !== 'undefined' && value !== '' ){

            var theList = ''
            
            $.each(items, function(i, item){

              var emph = ''

              if( item.substring(0, 1) == '*' ){

                emph = 'emphasis'
                item = item.substring(1)

              }
              
              if(item != '')
                theList += sprintf('<li class="pl-border %s">%s</li>', emph, item )

            })

            $(element)
              .html(theList)

            
            /** Trigger template change event */
            that.doTemplateTriggers( element, valueAccessor, allBindings )
          }

          }
      };

      

      
      /**
       * The main section editing binding.
       * 
       */
      ko.bindingHandlers.pledit = {
        init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {

          /** Do it on the parent so no conflicts */
          $(element).data( 'start_classes', $(element).attr('class') )

        },
        update: function( element ) {
     
          var model       = ko.dataFor( element ), 
              value       = '', 
              classEl     = $(element).parent(), 
              wrapperEl   = classEl.parent()

    
          /** Background Image */
          if( plIsset( model.background() ) ){

            value = sprintf( 'url(%s)', pl_do_shortcode( model.background() ) )

             classEl.css( 'backgroundImage', value)
          } 

          /** Background Color */
          if( plIsset( model.color() ) ){

               classEl.css( 'backgroundColor', model.color())
          } 

          /** Background Color */
          if( plIsset( model.textcolor() ) ){

               classEl.css( 'color', model.textcolor())

          } 

          /** Background Position */
          var xPos = model.bgxpos(),
              yPos = model.bgypos()

          if( plIsset(xPos) || plIsset(yPos) ){

                
            xPos = ( ! plIsset(xPos) || xPos == '') ? 0 : xPos
            yPos = ( ! plIsset(yPos) || yPos == '') ? 0 : yPos                

            classEl.css( 'backgroundPosition', xPos + "% " + yPos + "%" ) 

          } 

             
          /** Background Size */
          var bgWidth   = model.bgwidth(),
              bgHeight   = model.bgheight(),
              bgCover   = model.bgcover(), 
              sizeValue   = ''

          if( plIsset(bgCover) && bgCover != '' ){

            classEl.css( 'backgroundSize', bgCover ) 

          } 

          else if( plIsset(bgWidth) || plIsset(bgHeight) ){

            bgWidth   = ( ! plIsset(bgWidth)  || bgWidth == '' ) ? 'auto' : bgWidth + 'px'
            bgHeight   = ( ! plIsset(bgHeight) || bgHeight == '') ? 'auto' : bgHeight + 'px'            

            classEl.css( 'backgroundSize', bgWidth + ' ' + bgHeight ) 

          }

          

          /** Background Tile */
          if( plIsset( model.bgrepeat() ) ){
               classEl.css( 'backgroundRepeat', model.bgrepeat() ) 
          } 

          /** Text/Element Scheme */

          /** Remove Current Scheme Classes */
          that.doClass( classEl[0], model.theme(), 'scheme' )
    
          /** Scrolling and Sizing Effects */
          that.doClass( classEl[0], model.effects(), 'effect' )

          /** Minimum Height */
          if( plIsset( model.minheight() ) && '' != model.minheight() ){
            $(element).css( 'min-height', model.minheight() + 'vw' ) 
          } 
          else {
            $(element).css( 'min-height', '' ) 
          }


          /**
           * Columns / Offsets
           * Only applies to sections within the grid area.
           */
          
          var theSpan = ( plIsset( model.col() ) && model.col() != '' )   ? model.col()     : '12', 
              theOff   = ( plIsset( model.offset() ) && model.col() != '' )  ? model.offset()   : '0'

          that.doClass( wrapperEl[0], sprintf('pl-col-sm-%s pl-col-sm-offset-%s', theSpan, theOff ), 'grid' )

          
          /**
           * Layout Content MAX Width
           */
          if( plIsset( model.contentwidth() ) && '' !==  model.contentwidth() ){

            classEl.find('.pl-content-area').css('max-width', model.contentwidth() + 'px')
          } 
          else{
            classEl.find('.pl-content-area').css('max-width', '')
          }
           
          /** Section Alignment Format */
          that.doClass( classEl[0], model.alignment(), 'align' )

          /** Font Size */
          if( plIsset( model.font_size() ) ){
             
               $(element).css( 'fontSize', model.font_size() + 'em' ) 
          } 
        
          /** Special custom classes */
          that.doClass( wrapperEl[0], model.special_classes(), 'special' )


          /** Hiding on specific pages */
          if( plIsset( model.hide_on() ) ){

            var classes = '', 
                pageIDs = model.hide_on().split(',')

            $.each(pageIDs, function( i, PID ){
              if( PID == PLData.config.pageID ){
                classes += 'hide-on-page'
              }
            
            })

            that.doClass( wrapperEl[0], classes, 'hide' )
          }

          /** Background Video */
              if( plIsset( model.video() ) && model.video() != ''){

                var fieldValue   = pl_do_shortcode( model.video() ), 
                  
                format1 = fieldValue.substr( (fieldValue.lastIndexOf('.') +1) );
                    

                var videos   = sprintf('<source src="%s" type="video/%s">', fieldValue, format1),
                  HTML   = sprintf('<div class="pl-bg-video-container"><video class="pl-bg-video" preload autoplay loop muted>%s</video></div>', videos)
                  

                if( classEl.children('.pl-bg-video-container').length == 0 ){
                  classEl.prepend( HTML )
                } else {
                  
                  classEl.children('.pl-bg-video-container video').html( videos )
                }
          
          } else {
                classEl.children('.pl-bg-video-container').remove()
              }

              /** Background Overlay */
              if( plIsset( model.overlay() ) ){

                value = sprintf( 'url(%s)', pl_do_shortcode( model.overlay() ) ) 

               var HTML   = '<div class="pl-bg-overlay" ></div>'

               if( classEl.children('.pl-bg-overlay').length == 0 ){
                  classEl.prepend( HTML )
                } 

               classEl.find('.pl-bg-overlay').css( 'backgroundImage', value )

          } else {
            classEl.children('.pl-bg-overlay').remove()
          }



              /** Padding / Margin */

          var spUnit       = 'vw', 
              directions   = ['top', 'right', 'bottom', 'left'], 
              modes       = {
                  padding:   '',
                  margin:   ''
                }
              
              $.each( modes, function( mode, obj ) {

                $.each( directions, function( i, dir ) {

                  var spacingVal   = model[ mode + '_' + dir ](),
                      spacing     = parseFloat( spacingVal ) 


        
                  if( ! isNaN(spacing) && typeof spacing !== 'undefined'  ){
                    modes[ mode ] += spacing + spUnit + ' '
    
                  }

                  else 
                    modes[ mode ] += '0 '

                })


                /** If there is no */
                if( modes[ mode ].lastIndexOf(spUnit) != -1 ){

                  modes[ mode ] = modes[ mode ].substring( 0, modes[ mode ].lastIndexOf(spUnit) + 2 )

                  $(element).css( mode, modes[ mode ])  


                } 

                else{
                  $(element).css( mode, '')
                }
                


              })

             


          }
      };
    
      /**
       * Better attribute setter with event triggering and better zero state
       */
      ko.bindingHandlers.plattr = {
          update: function(element, valueAccessor, allBindings, viewModel, bindingContext) {

              var value = valueAccessor();

              var valueUnwrapped = ko.unwrap( value );

              var unit  = allBindings.get('unit')   || false

              if( typeof valueUnwrapped !== 'undefined' && valueUnwrapped !== '' ){

                var setValue = {}

                $.each(valueUnwrapped, function(attr, val){

                  if( unit == 'percent' ){
                    newVal = val * 100
                    newVal = newVal + '%'
                  } else {
                    newVal = val
                  }
                  
                  setValue[attr] = newVal

                })
          
            /** Do normal attr binding */
            ko.bindingHandlers.attr.update(element, function () { return setValue; }, allBindings, viewModel, bindingContext);


            /** do triggers on elements for redraw */
            that.doTemplateTriggers( element, valueAccessor, allBindings )

            

            
              }

          }
      };

      

      /**
       * Class changing binding.
       */
      ko.bindingHandlers.plclass = {
          update: function(element, valueAccessor, allBindings) {

            
            var value   = valueAccessor();

              var val   = ko.unwrap( valueAccessor() )
                
              
              if( _.isObject( val ) || _.isArray( val ) ){

                $.each( val, function(){

                  if( that.isset( val ) ){

                    var set   = $(this)[0],
                      partial = set.partial   ||   'pl-control-', 
                      wlabel   = set.wlabel   ||   [], 
                      child   = set.child   ||  false, 
                      dflt    = set.dflt    || 'default', 
                      tag   = String(set.tag)


                    tag = ( tag == '' ) ? dflt : tag;


                    tag = tag.replace( partial, '')
                      
                    

                    if( child ){

                      $(element).find( child ).each( function(){

                        pl_remove_class_partial( $(this), partial, wlabel)

                        $(this).addClass( partial + tag )

                      })

                    } else {

                      pl_remove_class_partial( $(element), partial, wlabel)

                      $(element).addClass( partial + tag )

                    }

                     

                  }

                })

              } 

              else if( typeof val !== 'undefined' ){

                var partial = allBindings.get('partial')   || 'pl-control-',
                  wlabel  = allBindings.get('wlabel')   || [], 
                  dflt    = allBindings.get('dflt')     || 'default', 
                  val   = ( val == '' ) ? dflt : val,
                  classes = val.split(" "), 
                  child   = allBindings.get('child')     ||  false


                if( child ){

                  $(element).find( child ).each( function(){

                    var theChild = $(this)

                    pl_remove_class_partial( theChild, partial, wlabel )

                    $.each(classes, function(i, theClass){
                      theClass = theClass.replace( partial, '')
                      theChild.addClass( partial + theClass ) 
                    })
                    

                  })

                } else {

            
                  pl_remove_class_partial( $(element), partial, wlabel)

                  $.each(classes, function(i, theClass){
                    theClass = theClass.replace( partial, '')
                    $(element).addClass( partial + theClass ) 
                  })

                }
                    
                
                
                 

              }

              that.doTemplateTriggers( element, valueAccessor, allBindings )

        

          }
      };


      

      // ko.bindingHandlers.href = {
      //     update: function (element, valueAccessor) {
      //         ko.bindingHandlers.attr.update(element, function () {
      //             return { href: valueAccessor()}
      //         });
      //     }
      // };

      // ko.bindingHandlers.src = {
      //     update: function (element, valueAccessor) {
      //         ko.bindingHandlers.attr.update(element, function () {
                
      //             return { src: valueAccessor()}
      //         });
      //     }
      // };

      // ko.bindingHandlers.hidden = {
      //     update: function (element, valueAccessor) {
      //         var value = ko.utils.unwrapObservable(valueAccessor());
      //         ko.bindingHandlers.visible.update(element, function () { return !value; });
      //     }
      // };

      ko.bindingHandlers.instantValue = {
          init: function (element, valueAccessor, allBindings) {
              var newAllBindings = function(){
                  // for backwards compatibility w/ knockout  < 3.0
                  return ko.utils.extend(allBindings(), { valueUpdate: 'afterkeydown' });
              };
              newAllBindings.get = function(a){
                  return a === 'valueupdate' ? 'afterkeydown' : allBindings.get(a);
              };
              newAllBindings.has = function(a){
                  return a === 'valueupdate' || allBindings.has(a);
              };
              ko.bindingHandlers.value.init(element, valueAccessor, newAllBindings);
          },
          update: ko.bindingHandlers.value.update
      };

      ko.bindingHandlers.toggle = {
          init: function (element, valueAccessor) {
              var value = valueAccessor();
              ko.applyBindingsToNode(element, {
                  click: function () {
                      value(!value());
                  }
              });
          }
      };

      ko.bindingHandlers.toJSON = {
          update: function(element, valueAccessor){
              return ko.bindingHandlers.text.update(element,function(){
                  return ko.toJSON(valueAccessor(), null, 2);
              });
          }
      };

      ko.virtualElements.allowedBindings.stopBinding = true;
      ko.bindingHandlers.stopBinding = {
          init: function () {
              return { controlsDescendantBindings: true };
          }
      };      



    }
  }

}(window.jQuery);