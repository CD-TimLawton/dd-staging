!function ($) {

  // --> Initialize
  $(window).on('pl_page_ready', function() {

    $.plStandard.init()

    $.plStandard.handleSearchfield()

    $(document).trigger( 'plReady' )

    $(window).trigger('resize')

  })

  window.$pl = function() {

    return window.PLData
  
  }

  window.$plServer = function() {
  
    return $.plServer
  
  }


  window.plTop = function(){

    return window.parent.jQuery;
  }

  window.plAddNewLinks = function(){

    if( typeof plTop().plFrame != 'undefined' )
      plTop().plFrame.handleFrameLinks()
  }


  window.plTrigger = function( type ){

    var type = type || 'ready'

    if( type == 'ready' )
      $(window).trigger('pl_page_ready')

    else if ( type == 'change' )
      $('body').trigger('pl_page_change')

    /** Always trigger window resize */
    $(window).trigger( 'resize' )
  }

  


  /**
   * plReady action fires on page load and after every successive section load
   */
  $( 'body' ).on('pl_load_event', function( e, location ){

    var location = location || 'None'

    //console.log('Page Load Event. Trigger Location: ' + location)

    plAddNewLinks()

  })



  $.plScrolling = {

    scrollToSection: function( section ){

      var that = this

      if( section.length == 0 )
        return

      section.addClass('section-glow')



      $('body').animate({
            scrollTop: section.offset().top - 40
          },
          1000, 
          'swing', 
          function(){

            setTimeout( function(){
              section.removeClass('section-glow')
            }, 1000)
            
          });

    }, 

  }
  



  $.plStandard = {

    init: function(){
      var that = this

          
      plAdjustAdminBar()

    }, 


    handleSearchfield: function(){
      
    
      
      $('.searchfield').on('focus', function(){
        
        $(this).parent().parent().addClass('has-focus')
          
      }).on( 'blur', function(){
        
        $(this).parent().parent().removeClass('has-focus')
      
      })
      
      $('.pl-searcher').on('click touchstart', function(e){
        
        e.stopPropagation()
        
        var searchForm = $(this), 
            container  = searchForm.parent().parent()
        
        $(this).addClass('has-focus').parent().find( '.search-field' ).focus()

        container.addClass('showing-search')
        
        $('body').on('click touchstart', function(){
          searchForm.removeClass('has-focus')

          container.removeClass('showing-search')
          
        })
      })
      
    }

  }

}(window.jQuery);