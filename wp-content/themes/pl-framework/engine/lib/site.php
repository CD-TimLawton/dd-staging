<?php
/**
 * PageLines Website Rendering Class
 *
 * @class     PL_UI_Site
 * @version   5.0.0
 * @package   PageLines/Classes
 * @category  Class
 * @author    PageLines
 */
class PL_UI_Site {


  function __construct( ) {
    

    global $plfactory;
    $this->factory = $plfactory;

    add_action( 'template_include',     array( $this->factory, 'preprocess' ), 100);

    /** Sections Factory: Enqueue scripts & styles */
    add_action( 'wp_enqueue_scripts',   array( $this->factory, 'process_styles' ));

    /** Sections Factory: Process section head and foot */
    add_action( 'wp_head',              array( $this->factory, 'process_head' ) );
    add_action( 'wp_footer',            array( $this->factory, 'process_foot' ) );

    /** Sections Factory: Render sections for each region of the page */
    add_action( 'pl_region_header',     array( $this, 'process_header' ) );
    add_action( 'pl_region_template',   array( $this, 'process_template' ) );
    add_action( 'pl_region_footer',     array( $this, 'process_footer' ) );

  }

  function process_header(){
    $this->factory->process_region('header');
  }

  function process_template(){

    $this->factory->process_region('template');
  }

  function process_footer(){
    $this->factory->process_region('footer');
  }

} /* fin */
