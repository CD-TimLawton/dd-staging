<?php
/**
 * i18n Strings Class 
 *
 * This class adds translation strings to the main JSON blob via pl_workarea_json filter
 *
 * To make a translatable string in use function PLTranslate(key) in your js and make sure 
 * there is a corresponding entry in the translate array.
 * (If there isnt you will trigger a warning in console logs)
 *
 * @class     Platform_i18n
 * @version   5.0.0
 * @package   PageLines/Classes
 * @category  Class
 * @author    PageLines
 */
class Platform_i18n {
  
  function __construct() {
    add_filter( 'pl_workarea_json', array( $this, 'translate' ) );
  }
  
  /**
   * Return array of strings used by pl_workarea_json filter
   */
  function translate( $settings ) {
    
    $strings = array(
      'add_custom_section_name'     => __( 'Add a custom name for this section.', 'pl-framework' ),
      'add_sections_to_page'        => __( 'Add Sections To Page', 'pl-framework' ),
      'additional_section_classes'  => __( 'Additional Section Classes', 'pl-framework' ),
      'advanced'                    => __( 'Advanced', 'pl-framework' ),
      'all_of_type'                 => __( 'All of Type', 'pl-framework' ),
      'are_you_sure'                => __( 'Are You Sure?', 'pl-framework' ),
      'background_advanced'         => __( 'Background Advanced', 'pl-framework' ),
      'background_and_color'        => __( 'Background and Color', 'pl-framework' ),
      'background_color'            => __( 'Background Color', 'pl-framework' ),
      'background_cover'            => __( 'Background Cover', 'pl-framework' ),
      'background_image'            => __( 'Background Image', 'pl-framework' ),
      'background_overlay'          => __( 'Background Overlay', 'pl-framework' ),
      'background_position'         => __( 'Background Position', 'pl-framework' ),
      'background_size'             => __( 'Background Size', 'pl-framework' ),
      'background_tile'             => __( 'Background Tile', 'pl-framework' ),
      'background_video'            => __( 'Background Video', 'pl-framework' ),
      'basic'                       => __( 'Basic', 'pl-framework' ),
      'carousel'                    => __( 'Carousel', 'pl-framework' ),
      'center'                      => __( 'Center', 'pl-framework' ),
      'clone'                       => __( 'Clone', 'pl-framework' ),
      'cols'                        => __( 'Cols', 'pl-framework' ),
      'columns'                     => __( 'Columns', 'pl-framework' ),
      'components'                  => __( 'Components', 'pl-framework' ),
      'contain'                     => __( 'Contain', 'pl-framework' ),
      'content_formats'             => __( 'Content Formats', 'pl-framework' ),
      'content_height_width'        => __( 'Content Height / Width', 'pl-framework' ),
      'cover'                       => __( 'Cover', 'pl-framework' ),
      'current_page_only'           => __( 'Current Page Only', 'pl-framework' ),
      'dark_text'                   => __( 'Dark Text', 'pl-framework' ),
      'default'                     => __( 'Default', 'pl-framework' ),
      'delete_section'              => __( 'Delete Section...', 'pl-framework' ),
      'delete'                      => __( 'Delete', 'pl-framework' ),
      'download_new_sections'       => __( 'Download New Sections', 'pl-framework' ),
      'edit_sets'                   => __( 'Edit Sets', 'pl-framework' ),
      'edit'                        => __( 'Edit', 'pl-framework' ),
      'font_color'                  => __( 'Font Color', 'pl-framework' ),
      'font_size_and_alignment'     => __( 'Font Size and Alignment', 'pl-framework'),
      'font_size'                   => __( 'Font Size', 'pl-framework' ),
      'gallery'                     => __( 'Gallery', 'pl-framework' ),
      'grid_and_sizing'             => __( 'Grid and Sizing', 'pl-framework'),
      'grid_controls'               => __( 'Grid Controls', 'pl-framework' ),
      'height'                      => __( 'Height', 'pl-framework' ),
      'hide_on_pages'               => __( 'Hide On Page(s)', 'pl-framework' ),
      'hide_with_comma'             => __( 'Hide this section on certain pages by adding their IDs here separated by a comma', 'pl-framework' ),
      'layout_containers'           => __( 'Layout / Containers', 'pl-framework' ),
      'left'                        => __( 'Left', 'pl-framework' ),
      'light_text'                  => __( 'Light Text', 'pl-framework' ),
      'margin'                      => __( 'Margin', 'pl-framework' ),
      'max_width'                   => __( 'Max Width (in px)', 'pl-framework' ),
      'min_height'                  => __( 'Min Height (in vw)', 'pl-framework' ),
      'navigation_menus'            => __( 'Navigation / Menus', 'pl-framework' ),
      'no_custom_options_added'     => __( 'No Custom Options Added.', 'pl-framework' ),
      'no_custom_options'           => __( 'No Custom Options', 'pl-framework'),
      'no_tile'                     => __( 'No Tile', 'pl-framework' ),
      'none'                        => __( 'None', 'pl-framework' ),
      'offset'                      => __( 'Offset', 'pl-framework' ),
      'padding_margin'              => __( 'Padding / Margin', 'pl-framework' ),
      'padding'                     => __( 'Padding', 'pl-framework' ),
      'page_builder'                => __( 'Page Editor', 'pl-framework' ),
      'page_layout'                 => __( 'Page Layout', 'pl-framework' ),
      'reference'                   => __( 'Reference', 'pl-framework' ),
      'relative_to_base'            => __( 'Relative to Base', 'pl-framework' ),
      'remove_from_page'            => __( 'This will remove this section and its settings from this page.', 'pl-framework' ),
      'right'                       => __( 'Right', 'pl-framework' ),
      'save_changes'                => __( 'Save Changes', 'pl-framework' ),
      'scope'                       => __( 'Scope', 'pl-framework' ),
      'section_copy_paste'          => __( 'Section Copy / Paste', 'pl-framework' ),
      'section_info_help'           => __( 'Use this ID in CSS/JS to target this specific section.', 'pl-framework' ),
      'section_info'                => __( 'Section Info', 'pl-framework' ),
      'seperate_space'              => __( 'Seperate classes with a space', 'pl-framework' ),
      'show_in_builder'             => __( 'Show in Builder', 'pl-framework' ),
      'show'                        => __( 'Show', 'pl-framework' ),
      'size_and_scroll_effects'     => __( 'Size and Scroll Effect', 'pl-framework' ),
      'sliders_features'            => __( 'Sliders / Features', 'pl-framework' ),
      'social_local'                => __( 'Social / Local', 'pl-framework' ),
      'taxonomy_archive'            => __( 'Taxonomy Archive', 'pl-framework' ),
      'text_element_align'          => __( 'Text/Element Alignment', 'pl-framework' ),
      'text_element_base_color'     => __( 'Text / Element Base Color', 'pl-framework' ),
      'tile_h'                      => __( 'Tile Horizontal', 'pl-framework' ),
      'tile_v'                      => __( 'Tile Vertical', 'pl-framework' ),
      'tile'                        => __( 'Tile', 'pl-framework' ),
      'using_a_theme'               => __( 'Using a Theme', 'pl-framework' ),
      'utilities'                   => __( 'Utilities', 'pl-framework' ),
      'widgets_sidebar'             => __( 'Widgets / Sidebar', 'pl-framework' ),
      'window_height'               => __( 'Window Height', 'pl-framework' ),
    );
    $settings['translate'] = $strings;
    return $settings;    
  }
}
new Platform_i18n;
