<?php 


class PL_Sections_Settings {
  
  function __construct() {
  
    /** Select Term AJAX Option */
    add_action( 'pl_server_select_term', array( $this, 'select_term' ), 10, 2 ); 

  }

  function select_term( $response, $data ){

    $response['opts'] = pl_get_terms_for_selection( $data['pt'] );

    return $response;
  }

 
}

global $pl_section_settings_handling;
$pl_section_settings_handling = new PL_Sections_Settings();
