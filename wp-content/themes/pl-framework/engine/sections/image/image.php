<?php
/*

  Plugin Name:   PageLines Section Image
  Description:   Simple image section

  Author:       PageLines
  Author URI:   http://www.pagelines.com

  PageLines:     PL_Image_Section
  Filter:       basic

*/

class PL_Image_Section extends PL_Section {

  function section_opts(){
    $opts = array(
      array(
        'type'      => 'image_upload',
        'key'       => 'image',
        'label'     => __( 'Image', 'pl-framework' ), 
        'default'   => pl_fallback_image()
      ),
      array(
        'type'      => 'text',
        'key'       => 'alt',
        'label'     => __( 'Alt Text', 'pl-framework' )
      ),
      array(
        'type'      => 'dragger',
        'label'     => __( 'Image Width / Height', 'pl-framework' ),
        'opts'      => array(
          array(
            'key'     => 'height',
            'min'     => 0,
            'max'     => 100,
            'default' => 20,
            'unit'    => __( 'Height (vw)', 'pl-framework' )
          ),
          array(
            'key'     => 'width',
            'min'     => 0,
            'max'     => 100,
            'unit'    => __( 'Width (vw)', 'pl-framework' )
          )
        )
      ), 
      array(
        'type'      => 'text',
        'key'       => 'link',
        'label'     => __( 'Link URL', 'pl-framework' )
      ),
      array(
        'type'      => 'check',
        'key'       => 'newwindow',
        'label'     => __( 'Open in new window?', 'pl-framework' )
      ),
    ); 

    return $opts;
  }
  
  function section_template( ) { ?>

  <div class="pl-img-wrap pl-alignment-default-center">
    <a data-bind="plhref: link, plattr: {'target': ( newwindow() == 1 ) ? '_blank' : ''}">
      <img src="<?php echo pl_fallback_image();?>" alt="" data-bind="plimg: image, attr: {alt: alt}, style: {'height': height() ? height() + 'vw' : '', 'width': width() ? width() + 'vw' : ''}" />
    </a>
  </div>
<?php
   }
}
