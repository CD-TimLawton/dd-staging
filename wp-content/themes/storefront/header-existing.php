<?php/** * The header for our theme. * * Displays all of the <head> section and everything up till <div id="content"> * * @package storefront */?><!doctype html>

<html <?php language_attributes(); ?>>

<head>
    <!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-WW72PCF');
    </script><!-- End Google Tag Manager -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>



    <?php if(is_front_page() && md_is_mobile()){ ?>
        <style>
            #content {background-image: url("./img/brickwork.jpeg");}
            .entry-content {display:none;}</style>
    <?php } ?>

    <?php if(md_is_mobile()){ ?>
        <style>
            .new-mobile-header-nav {position: relative; top: 5px; display:inline; font-weight:600;  z-index: 5; left: 20px;}
            .product-quantity {z-index:2; position:relative;}
            #masthead {display:none;}
            .storefront-handheld-footer-bar {display:none;}
            #new_cost_mobile {
                position: relative;
                top: -6px;
                font-weight: 600;
                color: #c86e1e;
                left: 33px;    }

            #new_count_mobile{
                border-radius: 100%;
                background-color: #6d6d6d;        /* padding: 5px; */
                color: white;
                position: relative;
                top: -17px;
                display: inline-block;
                width: 1.5em;
                height: 1.5em;
                line-height: 1.5;
                text-indent: 0;
                border: 1px solid;
                text-align: center;
                left:45px;    }

            .widget_product_search form:before {
                position:relative;
                top:30px;    }

            #new_mobile_nav {display:none;
                border-left: 40px #c86e1e solid;
                background-color: #c86e1e;
                color: white;
                z-index: 3;
                position: absolute;
                border-right: 20px #c86e1e solid;
                border-bottom: 8px #c86e1e solid;
                border-top: 8px #c86e1e solid;
                border-radius:0 0 5px 0;
                opacity: 0.9;
                font-weight: 600;}

            #new_mobile_nav a {color:white !important;}
            #mobile_nav {border-top: 0;        border-bottom: 0;        border-left: 20px #c86e1e solid;        border-right: 20px #c86e1e solid;        background-color:white;        height:50px;        padding:8px;}
            /*.site-footer {display:none !important;}*/
            #woocommerce_product_search-3 {display:none;}
            .custom-search-field {border-radius:8px; padding-top:5px !important; padding-bottom:5px !important; background-color:#edcfb5 !important; color:white !important; font-weight:600 !important; letter-spacing:0.1px;}
            .custom-search {border-top: 8px #c86e1e solid;        border-bottom: 8px #c86e1e solid;        border-left: 20px #c86e1e solid;        border-right: 20px #c86e1e solid;    background:#c86e1e;    }
            #dd_countdown_mobile {padding-left:12px; display:inline; color:#c86e1e; z-index:1; position:relative; font-size: 18px; letter-spacing: -1px; font-family: arial; top:-16px; font-weight:600;}
            #dd_header_image {position:relative; z-index:-1; top: -220px; margin-bottom: -220px;     border-top: 8px #c86e1e solid;        border-bottom: 8px #c86e1e solid;        border-left: 20px #c86e1e solid;        border-right: 20px #c86e1e solid;}
            @media only screen and (max-width: 320px) {
                #dd_header_image {            top:-190px;            margin-bottom: -190px;        }
                .widget_product_search form:before {            position:relative;            top:28px;        }
                .text-head {font-size:12px;}
            <?php if(is_front_page()) { ?>
                button {font-size:12px !important;}    <?php } ?>
                #woocommerce-product-search-field-0{font-size:13px;}
            }
            #hours {display:inline-block;}
            #minutes {display:inline-block;}
            #seconds {display:inline-block;}
            .time-label {font-size:16px; letter-spacing:-.5px; text-align:center; position:relative; font-family:Montserrat; color:white; font-weight:600;}
            .text-head {font-weight:600; color:white; display:inline-block; text-align:left; line-height:18px; padding-top:8px; }
            #ddtimerfalse {margin-bottom:63px;}
            #ddtimer {text-align:center; font-size:14px;}
            .menu-toggle {display:none !important;}
            .woocommerce-product-search {margin-top:-22px !important;}
        </style>
    <?php } ?>



</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) --><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WW72PCF"height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript><!-- End Google Tag Manager (noscript) -->


<?php if(is_front_page() && md_is_mobile()){ ?><?php $dd = date("d"); ?><?php $mm = date("m"); ?><?php $yy = date("Y"); ?>
<?php $_day = date('l'); ?>

<?php if($_day == "Thursday" || $_day == "Wednesday" || $_day == "Monday" || $_day == "Tuesday") { ?>
    <script>
        jQuery(document).ready(function() {
            jQuery('#ddtimer').delay(1000).show(0);
            jQuery('#ddtimerfalse').delay(1000).hide(0);
        });
    </script> <?php
    }
    else
    { ?>
    <script>
        jQuery(document).ready(function() {
    jQuery(".text-head").hide(0);
    jQuery("#ddtimerfalse").css("margin-bottom","10px");
        });
    </script>
    <?php } ?>

    <script>


    CountDownTimer('<?php echo $mm; ?>/<?php echo $dd; ?>/<?php echo $yy; ?> 3:00 PM', 'dd_countdown_mobile');



    function CountDownTimer(dt, id)    {
        var end = new Date(dt);
        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;
        var counterincrease = 500;

        function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {
                clearInterval(timer);
                document.getElementById(id).innerHTML = '';
                jQuery(".text-head").hide();
                return;
            }
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            document.getElementById(id).innerHTML = '';
            if(hours<10) {
                document.getElementById(id).innerHTML += '<div id="hours">0' + hours + ' <span class="time-label">hrs</span></div> ';
            }
            else {
                document.getElementById(id).innerHTML += '<div id="hours">' + hours + ' <span class="time-label">hrs</span></div> ';
            }
            if(minutes>=10){
                document.getElementById(id).innerHTML += '<div id="minutes">'+minutes+' <span class="time-label">mns</span></div> ';
            }
            if(minutes<10 && minutes>=0){
                document.getElementById(id).innerHTML += '<div id="minutes">0'+minutes+' <span class="time-label">mns</span></div> ';
            }
            if(seconds>=10){
                document.getElementById(id).innerHTML += '<div id="seconds">'+seconds+' <span class="time-label">sec</span></div> ';
            }
            if(seconds<10 && seconds>=0){
                document.getElementById(id).innerHTML += '<div id="seconds">0'+ seconds+' <span class="time-label">sec</div></div> ';
            }
        }

        timer = setInterval(showRemaining, counterincrease);
    }
    </script>

<?php } ?>

<?php if (md_is_mobile()){ ?>

    <?php $site_url = get_site_url(); ?>

    <div id="mobile_nav">
        <div style="float:left; z-index: 6; position: relative;">
            <div onClick="navtouch();" id="nav_button">
                <i class="fas fa-bars fa-2x"></i>
            </div>
        </div>

        <script>
        function navtouch() {
            var returned = jQuery("#nav_button").html();
            returned = (returned.trim());
            console.log(returned);

            if(returned == '<i class="fas fa-bars fa-2x"></i>') {
                jQuery("#nav_button").html("<i class='fas fa-times fa-2x'></i>");
                jQuery("#new_mobile_nav").slideDown(500);
            }
            else {
                jQuery("#nav_button").html("<i class='fas fa-bars fa-2x'></i>");
                jQuery("#new_mobile_nav").slideUp(500);
            }
        };
        </script>
        <div class="new-mobile-header-nav"><a href="<?php echo $site_url; ?>/my-account/">My Account</a></div>
        <div style="    position: relative;    top: -22px;    max-width: 345px;    /* margin: 0 auto; */    /* left: 150px; */    text-align: right;">
            <?php $currency = get_option('woocommerce_currency'); ?>
            <?php if($currency=="GBP"){$currency = "&pound;";} ?>
            <a href="<?php echo $site_url; ?>/cart/">
                <span id="new_cost_mobile" class="woocommerce-Price-amount amount">
                    <span class="woocommerce-Price-currencySymbol">
                        <?php echo $currency; ?></span>
                    <?php $_final_cart_cost = number_format((WC()->cart->cart_contents_total*1.2), 2, ".", ""); ?>
                    <?php echo $_final_cart_cost; ?></span>
            </a>
            <a href="<?php echo $site_url; ?>/cart/">

                <span id="new_count_mobile" class="count"><?php echo WC()->cart->get_cart_contents_count(); ?></span></a>
            <a href="<?php echo $site_url; ?>/cart/"><i class="fas fa-shopping-cart fa-2x"></i></a>
        </div>
    </div>
    <div id="new_mobile_nav" style="display:none;">
        <?php $hidden = array(); ?>
        <?php $menu = wp_get_nav_menu_items( "mobile-menu", $args); ?>    <?php $i=0; ?>    <?php $lastid = 0; ?>    <?php $newid = 0; ?>    <?php foreach($menu as $value){ ?>        <?php $i++; ?>        <?php if(($value->menu_item_parent)==0) { ?>            <?php if($i!=1){echo "<br />";} ?>            <a href="<?php echo $value->url; ?>"><?php echo $value->title; ?></a>        <?php } else { $newid = $value->menu_item_parent; ?>            <?php if($newid!=$lastid){ ?>            &nbsp;&nbsp;<i onClick="<?php echo 'x'.$newid; ?>();" class="fas fa-arrow-alt-circle-right"></i>            <?php $hidden[]=$value->menu_item_parent; } ?>            <div class="<?php echo $value->menu_item_parent; ?>">&nbsp;<a href="<?php echo $value->url; ?>"><?php echo $value->title; ?></a></div>            <?php $lastid = $value->menu_item_parent; ?>            <?php } ?>    <?php } ?>    <?php foreach($hidden as $value)    { ?>    <script>        jQuery(".<?php echo $value; ?>").hide();        function x<?php echo $value; ?>(){            if(jQuery(".<?php echo $value; ?>").css('display')==='none') {                jQuery(".<?php echo $value; ?>").slideDown();            }else{                jQuery(".<?php echo $value; ?>").slideUp();            }        }    </script>    <?php }?></div>

    <div id="woocommerce_product_search-4" class="custom-search woocommerce widget_product_search">
        <form role="search" method="get" class="woocommerce-product-search" action="<?php echo $site_url; ?>" style="margin:0;">
            <label class="screen-reader-text" for="woocommerce-product-search-field-0">Search for Dry Drinker products</label>
            <input type="search" id="woocommerce-product-search-field-0" class="search-field custom-search-field" placeholder="SEARCH FOR DRY DRINKER PRODUCTS" value="" name="s">
            <button type="submit" value="Search">Search</button>
            <input type="hidden" name="post_type" value="product">
        </form>
    </div>
<?php } ?>

<?php do_action( 'storefront_before_site' ); ?>


<div id="page" class="hfeed site">	<?php do_action( 'storefront_before_header' ); ?>

    <?php if( is_front_page() && md_is_mobile()){ ?>
    <div id="ddtimerfalse"></div>
    <div id="ddtimer" style="display:none;">
        <div class="text-head">countdown for <br />next day delivery<div style="font-size:10px;">UK* mainland only</div></div>
        <div id="dd_countdown_mobile"></div>
    </div><div><?php $site_url = get_site_url(); ?>

        <?php // if((strpos($_SERVER['HTTP_USER_AGENT'], 'Mobile') !== false)){$mobile = 1;} ?>

        <?php if (md_is_mobile()) { ?>
            <div id="dd_header_image"><img src="<?php echo $site_url; ?>\img\Mobile_header_02.jpg"></div>
        <?php } ?><?php }  ?>
        <header id="masthead" class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">
            <?php		/**		 * Functions hooked into storefront_header action		 *		 * @hooked storefront_header_container                 - 0		 * @hooked storefront_skip_links                       - 5		 * @hooked storefront_social_icons                     - 10		 * @hooked storefront_site_branding                    - 20		 * @hooked storefront_secondary_navigation             - 30		 * @hooked storefront_product_search                   - 40		 * @hooked storefront_header_container_close           - 41		 * @hooked storefront_primary_navigation_wrapper       - 42		 * @hooked storefront_primary_navigation               - 50		 * @hooked storefront_header_cart                      - 60		 * @hooked storefront_primary_navigation_wrapper_close - 68		 */		do_action( 'storefront_header' ); ?>	</header><!-- #masthead -->	<?php	/**	 * Functions hooked in to storefront_before_content	 *	 * @hooked storefront_header_widget_region - 10	 * @hooked woocommerce_breadcrumb - 10	 */


    do_action( 'storefront_before_content' ); ?>
        <div id="content" class="site-content" tabindex="-1">
            <div class="col-full">
<?php		do_action( 'storefront_content_top' );