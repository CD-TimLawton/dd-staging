<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package storefront
 */
?>
<?php if(!is_front_page() || !md_is_mobile()){ ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    /**
     * Functions hooked in to storefront_page add_action
     *
     * @hooked storefront_page_header          - 10
     * @hooked storefront_page_content         - 20
     */
    do_action( 'storefront_page' );
    ?>
</article><!-- #post-## -->
<?php } ?>


<?php if(is_front_page() && md_is_mobile()){ ?>
    <style>
        .mobile-container {padding-top:15px;}
        .black-button {background-color:#000 !important;}
        #new-social-media{text-align: center; width: 100%; position: relative; top: -50px;}

        @font-face {
            font-family: 'franchiseregular';
            src: url('./font/franchise-bold-webfont.woff2') format('woff2'),
            url('./font/franchise-bold-webfont.woff') format('woff');
            font-weight: normal;
            font-style: normal;

        }
    </style>

    <script>
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
    </script>

    <?php $args = array('posts_per_page'   => 8,"cat"=>"141",'orderby' => 'title', 'order' => 'ASC'); ?>
    <?php    $posts_array = get_posts( $args ); ?>
    <?php $king = array(); ?>
    <?php $imagehtml = array(); ?>
    <?php foreach($posts_array as $value)
    {


        $king[$value->post_title]=$value->post_content;
        $post_thumbnail_id = get_post_thumbnail_id($value->ID);
        if($post_thumbnail_id ) {
            $imagehtml[$value->post_title] = wp_get_attachment_image($post_thumbnail_id, 'full');
        }
    }

    ?>
    <div class="mobile-container">

        <div style="max-width:75%; margin:0 auto; padding-bottom:10px;">
            <img src="./img/Dry_Drinker_Poster_Portrait_02.jpg">
        </div>

        <div style="max-width:100%; padding-bottom:10px;">
            <img src="./img/Dry_Drinker_Poster_Landscape_01.jpg">
        </div>



        <!-- start of position 1 -->
        <div style="float:left; max-width:50%;">
            <a href="/shop/alcohol-free-new/">
                <div style="position: relative; text-align: center; color: black; padding-right:5px;">
                    <?php echo $imagehtml[1]; ?>
                    <div style="line-height:36px; font-family:'franchiseregular'; width:95%; position: absolute; top: 67%; left: 50%; transform: translate(-50%, -50%); font-weight:600; font-size:36px;">

                        <?php echo $king[1]; ?>

                    </div>
                </div>
            </a>
        </div>
        <!-- end of position 1 -->
        <!-- start of position 2 -->
        <?php $product = wc_get_product($king[2]); ?>
        <?php $title = $product->get_title(); ?>
        <?php $button_add = explode("-",$title); ?>
        <?php $button_final = trim($button_add[1]); ?>
        <?php $_type = $product->get_type(); ?>
        <?php $id = $product->get_id(); ?>
        <?php $url = get_permalink( $product->get_id() ); ?>
        <?php $value=99999; ?>
        <div style="float:right; max-width:50%; padding-bottom: 14px; margin-bottom:-20px;">
            <a href="<?php echo $url; ?>">
                <div style="position: relative; text-align: center; color: black; padding-left:5px;">
                    <?php echo $imagehtml[2]; ?>
                </div>
            </a>
            <?php $price = $product->get_regular_price(); ?>
            <?php $onsale = $product->is_on_sale();  ?>
            <?php if($onsale==1){$price = $product->get_sale_price();} ?>
            <?php $wccur = get_option('woocommerce_currency'); ?>
            <?php $sym = get_woocommerce_currency_symbol($wccur); ?>
            <?php $button_text = "Add ".$button_final." | ".$sym.$price; ?>
            <a id="buy<?php echo $id.$value; ?>" href="#"><button id="button<?php echo $id.$value; ?>" class="featured-button single_add_to_cart_button button alt" style="letter-spacing:-0.5px; font-size:14px; width:90%; padding:0 5px 0 5px; position:relative; height:25px; top: -31px;
    left: 12px;"><?php echo $button_text; ?></button></a>
        </div>
        <!-- end of position 2 -->

        <script>
            jQuery('#buy<?php echo $id.$value ?>').click(function(e) {
                e.preventDefault();
                addToCarta(<?php echo $id; ?>,<?php echo $value; ?>,<?php echo $price; ?>);
                return false;
            });
            function addToCarta(p_id,v_id,s_id) {
                

                /* var q_id = jQuery("#qty"+p_id+v_id).val(); */
                var q_id = 1;
                jQuery("#button"+p_id+v_id).html("Adding <i class='fas fa-spinner fa-spin'></i>");

                jQuery.get('/?post_type=product&add-to-cart=' + p_id + '&quantity=' + q_id, function() {
                    jQuery("#button"+p_id+v_id).html("Added");
                    s_id = (s_id * q_id);
                    var x = jQuery( ".count" ).first().text();



                    var res = x.split(" ",1);
                    res = parseInt(res) + parseInt(q_id);
                    jQuery(".count").text(res);
                    var cart = jQuery(".woocommerce-Price-amount").first().text();
                    var cart = cart.replace("£", "");
                    var middle = parseFloat(cart);
                    var final = "£"+(middle+s_id).toFixed(2);
                    jQuery(".woocommerce-Price-amount").first().html(final);

                });
            }
        </script>



        <div style="clear:both;"></div>
        <?php $product = wc_get_product($king[3]); ?>
        <?php $url = get_permalink( $product->get_id() ); ?>
        <?php $id = $product->get_id(); ?>
        <div style="float:left; max-width:50%; height:350px; margin-bottom:-40px;">
            <a href="<?php echo $url; ?>">
                <div style="position: relative; text-align: center; color: black; padding-right:5px;">
                    <?php echo $imagehtml[3]; ?>
                </div>
            </a>





            <?php $available_variations = $product->get_available_variations(); ?>

            <?php $count_variations = count($available_variations); ?>

            <?php foreach($available_variations as $value){ ?>

                <?php if($count_variations!=1) { ?>
                    <?php $_tmp = $value['attributes']['attribute_pa_select-size']; ?>
                    <?php $_tmp = str_replace("-"," ",$_tmp); ?>
                    <?php $_tmp = ucwords($_tmp); ?>
                    <?php $_tmp = str_replace("Case Of ","",$_tmp); ?>
                    <?php $_tmp = str_replace(" Bottles","",$_tmp); ?>
                    <?php $_tmp = str_replace(" Cans","",$_tmp); ?>
                    <?php $button_text = "Add ".$_tmp." To Cart"; ?>
                <?php } else { $button_text = "Add To Cart"; } ?>

                <?php $variable_product1= new WC_Product_Variation( $value['variation_id']); ?>



                <?php $sales_price = $variable_product1->get_regular_price(); ?>
                <?php  $onsale = $variable_product1->is_on_sale();  ?>
                <?php if($onsale==1){$sales_price = $variable_product1->sale_price;} ?>
                <?php $button_text .= " | ".$sym.$sales_price; ?>
                <a id="buy<?php echo $id.$value['variation_id']; ?>" href="#"><button id="button<?php echo $id.$value['variation_id']; ?>" class="<?php if($_tmp=="12"){echo "black-button ";} ?>featured-button single_add_to_cart_button button alt" style="letter-spacing:-0.5px; font-size:14px; width:90%; padding:0 5px 0 5px; position:relative; height:25px;     top: -58px;
    left: 6px;
    margin-bottom: 5px;"><?php echo $button_text; ?></button></a>

                <script>
                    jQuery('#buy<?php echo $id.$value['variation_id']; ?>').click(function(e) {
                        e.preventDefault();
                        addToCartva(<?php echo $id; ?>,<?php echo $value['variation_id']; ?>,<?php echo $sales_price; ?>);
                        return false;
                    });

                    function addToCartva(p_id,v_id,s_id)
                    {
                        jQuery("#button"+p_id+v_id).html("Adding <i class='fas fa-spinner fa-spin'></i>");
                        jQuery.get('/?post_type=product&add-to-cart=' + p_id + "&variation_id= " + v_id, function() {
                            jQuery("#button" + p_id + v_id).html("Added!");

                            var x = jQuery( ".count" ).first().text();
                            var res = x.split(" ",1);
                            res++;
                            jQuery(".count").text(res);
                            var cart = jQuery(".woocommerce-Price-amount").first().text();
                            var cart = cart.replace("£", "");
                            var middle = parseFloat(cart);
                            var final = "£"+(middle+s_id).toFixed(2);
                            jQuery(".woocommerce-Price-amount").first().html(final);

                        });
                    }
                </script>

            <?php } ?>







        </div>
        <?php $product = wc_get_product($king[4]); ?>
        <?php $url = get_permalink( $product->get_id() ); ?>
        <?php $id = $product->get_id(); ?>
        <div style="float:right; max-width:50%; padding-bottom:14px; margin-bottom:-60px;">
            <a href="<?php echo $url; ?>">
                <div style="position: relative; text-align: center; color: black; padding-left:5px;">
                    <?php echo $imagehtml[4]; ?>
                </div>
            </a>



            <?php $available_variations = $product->get_available_variations(); ?>

            <?php $count_variations = count($available_variations); ?>

            <?php foreach($available_variations as $value){ ?>

                <?php if($count_variations!=1) { ?>
                    <?php $_tmp = $value['attributes']['attribute_pa_select-size']; ?>
                    <?php $_tmp = str_replace("-"," ",$_tmp); ?>
                    <?php $_tmp = ucwords($_tmp); ?>
                    <?php $_tmp = str_replace("Case Of ","",$_tmp); ?>
                    <?php $_tmp = str_replace(" Bottles","",$_tmp); ?>
                    <?php $_tmp = str_replace(" Cans","",$_tmp); ?>
                    <?php $button_text = "Add ".$_tmp." To Cart"; ?>
                <?php } else { $button_text = "Add To Cart"; } ?>

                <?php $variable_product1= new WC_Product_Variation( $value['variation_id']); ?>



                <?php $sales_price = $variable_product1->get_regular_price(); ?>
                <?php  $onsale = $variable_product1->is_on_sale();  ?>
                <?php if($onsale==1){$sales_price = $variable_product1->sale_price;} ?>
                <?php $button_text .= " | ".$sym.$sales_price; ?>
                <a id="buy<?php echo $id.$value['variation_id']; ?>" href="#"><button id="button<?php echo $id.$value['variation_id']; ?>" class="<?php if($_tmp=="12"){echo "black-button ";} ?>featured-button single_add_to_cart_button button alt" style="letter-spacing:-0.5px; font-size:14px; width:90%; padding:0 5px 0 5px; position:relative; height:25px;     top: -58px;
    left: 11px;
    margin-bottom: 5px;"><?php echo $button_text; ?></button></a>

                <script>
                    jQuery('#buy<?php echo $id.$value['variation_id']; ?>').click(function(e) {
                        e.preventDefault();
                        addToCartvb(<?php echo $id; ?>,<?php echo $value['variation_id']; ?>,<?php echo $sales_price; ?>);
                        return false;
                    });

                    function addToCartvb(p_id,v_id,s_id)
                    {
                        jQuery("#button"+p_id+v_id).html("Adding <i class='fas fa-spinner fa-spin'></i>");
                        jQuery.get('/?post_type=product&add-to-cart=' + p_id + "&variation_id= " + v_id, function() {
                            jQuery("#button" + p_id + v_id).html("Added!");

                            var x = jQuery( ".count" ).first().text();
                            var res = x.split(" ",1);
                            res++;
                            jQuery(".count").text(res);
                            var cart = jQuery(".woocommerce-Price-amount").first().text();
                            var cart = cart.replace("£", "");
                            var middle = parseFloat(cart);
                            var final = "£"+(middle+s_id).toFixed(2);
                            jQuery(".woocommerce-Price-amount").first().html(final);

                        });
                    }
                </script>

            <?php } ?>



        </div>
        <div style="clear:both;"></div>
        <?php $product = wc_get_product($king[5]); ?>
        <?php $url = get_permalink( $product->get_id() ); ?>
        <div style="float:left; max-width:50%; height:350px; margin-bottom:-40px;">
            <a href="<?php echo $url; ?>">
                <div style="position: relative; text-align: center; color: black; padding-right:5px;">
                    <?php echo $imagehtml[5]; ?>
                </div>

                <?php $button_text = "SELECT BOX"; ?>
                <button id="button<?php echo $id.$value; ?>" class="featured-button single_add_to_cart_button button alt" style="letter-spacing:-0.5px; font-size:14px; width:90%; padding:0 5px 0 5px; position:relative; height:25px; top: -31px;
    left: 6px;"><?php echo $button_text; ?></button>

            </a>
        </div>


        <?php $product = wc_get_product($king[6]); ?>
        <?php $url = get_permalink( $product->get_id() ); ?>

        <?php $title = $product->get_title(); ?>
        <?php $button_add = explode("-",$title); ?>
        <?php $button_final = trim($button_add[1]); ?>
        <?php $_type = $product->get_type(); ?>
        <?php $id = $product->get_id(); ?>
        <?php $url = get_permalink( $product->get_id() ); ?>
        <?php $value=99999; ?>



        <div style="float:right; max-width:50%; padding-bottom:14px; margin-bottom: -25px;">
            <a href="<?php echo $url; ?>">
                <div style="position: relative; text-align: center; color: black; padding-left:5px;">
                    <?php echo $imagehtml[6]; ?>
                </div>
            </a>

            <?php $price = $product->get_regular_price(); ?>
            <?php $onsale = $product->is_on_sale();  ?>
            <?php if($onsale==1){$price = $product->get_sale_price();} ?>
            <?php $wccur = get_option('woocommerce_currency'); ?>
            <?php $sym = get_woocommerce_currency_symbol($wccur); ?>
            <?php $button_text = "Add ".$button_final." | ".$sym.$price; ?>
            <a id="buy<?php echo $id.$value; ?>" href="#"><button id="button<?php echo $id.$value; ?>" class="featured-button single_add_to_cart_button button alt" style="letter-spacing:-0.5px; font-size:14px; width:90%; padding:0 5px 0 5px; position:relative; height:25px; top: -31px;
    left: 11px;"><?php echo $button_text; ?></button></a>
        </div>

        <script>
            jQuery('#buy<?php echo $id.$value ?>').click(function(e) {
                e.preventDefault();
                addToCartb(<?php echo $id; ?>,<?php echo $value; ?>,<?php echo $price; ?>);
                return false;
            });
            function addToCartb(p_id,v_id,s_id) {


                // var q_id = jQuery("#qty"+p_id+v_id).val();
                var q_id = 1;
                jQuery("#button"+p_id+v_id).html("Adding <i class='fas fa-spinner fa-spin'></i>");
                jQuery.get('/?post_type=product&add-to-cart=' + p_id + '&quantity=' + q_id, function() {
                    jQuery("#button"+p_id+v_id).html("Added");
                    s_id = (s_id * q_id);
                    var x = jQuery( ".count" ).first().text();
                    var res = x.split(" ",1);
                    res = parseInt(res) + parseInt(q_id);
                    jQuery(".count").text(res);
                    var cart = jQuery(".woocommerce-Price-amount").first().text();
                    var cart = cart.replace("£", "");
                    var middle = parseFloat(cart);
                    var final = "£"+(middle+s_id).toFixed(2);
                    jQuery(".woocommerce-Price-amount").first().html(final);

                });
            }
        </script>


    </div>
    <div style="clear:both;"></div>
    <?php $product = wc_get_product($king[7]); ?>
    <?php $title = $product->get_title(); ?>

    <?php $button_final = "A Bottle"; ?>
    <?php $_type = $product->get_type(); ?>
    <?php $id = $product->get_id(); ?>
    <?php $url = get_permalink( $product->get_id() ); ?>
    <?php $value=99999; ?>


    <?php $url = get_permalink( $product->get_id() ); ?>
    <div style="float:left; max-width:50%; height:350px; margin-bottom:-40px;">
        <a href="https://drydrinker.com/vintense-wines/">
            <div style="position: relative; text-align: center; color: black; padding-right:5px;">
                <?php echo $imagehtml[7]; ?>
            </div>
        </a>

        <?php $price = $product->get_regular_price(); ?>
        <?php $onsale = $product->is_on_sale();  ?>
        <?php if($onsale==1){$price = $product->get_sale_price();} ?>
        <?php $wccur = get_option('woocommerce_currency'); ?>
        <?php $sym = get_woocommerce_currency_symbol($wccur); ?>
        <?php $button_text = "Add ".$button_final." | ".$sym.$price; ?>
        <a id="buy<?php echo $id.$value; ?>" href="#"><button id="button<?php echo $id.$value; ?>" class="featured-button single_add_to_cart_button button alt" style="letter-spacing:-0.5px; font-size:14px; width:90%; padding:0 5px 0 5px; position:relative; height:25px; top: -31px;
    left: 6px;"><?php echo $button_text; ?></button></a>

        <script>
            jQuery('#buy<?php echo $id.$value ?>').click(function(e) {
                e.preventDefault();
                addToCartc(<?php echo $id; ?>,<?php echo $value; ?>,<?php echo $price; ?>);
                return false;
            });
            function addToCartc(p_id,v_id,s_id) {


               // var q_id = jQuery("#qty"+p_id+v_id).val();
                var q_id = 1;
                jQuery("#button"+p_id+v_id).html("Adding <i class='fas fa-spinner fa-spin'></i>");
                jQuery.get('/?post_type=product&add-to-cart=' + p_id + '&quantity=' + q_id, function() {
                    jQuery("#button"+p_id+v_id).html("Added");
                    s_id = (s_id * q_id);
                    var x = jQuery( ".count" ).first().text();
                    var res = x.split(" ",1);
                    res = parseInt(res) + parseInt(q_id);
                    jQuery(".count").text(res);
                    var cart = jQuery(".woocommerce-Price-amount").first().text();
                    var cart = cart.replace("£", "");
                    var middle = parseFloat(cart);
                    var final = "£"+(middle+s_id).toFixed(2);
                    jQuery(".woocommerce-Price-amount").first().html(final);

                });
            }
        </script>

    </div>
    <?php $product = wc_get_product($king[8]); ?>
    <?php $url = get_permalink( $product->get_id() ); ?>
    <?php $title = $product->get_title(); ?>
    <?php $button_add = explode("-",$title); ?>
    <?php if(!$button_add[1]){ $button_add = explode("–",$title); } ?>
    <?php $button_final = trim($button_add[1]); ?>
    <?php $_type = $product->get_type(); ?>
    <?php $id = $product->get_id(); ?>
    <?php $url = get_permalink( $product->get_id() ); ?>
    <?php $value=99999; ?>

    <div style="float:right; max-width:50%; padding-bottom:14px;">
        <a href="<?php echo $url; ?>">
            <div style="position: relative; text-align: center; color: black; padding-left:5px;">
                <?php echo $imagehtml[8]; ?>
            </div>
        </a>

        <?php $value=9999; ?>
        <?php $price = $product->get_regular_price(); ?>
        <?php $onsale = $product->is_on_sale();  ?>
        <?php if($onsale==1){$price = $product->get_sale_price();} ?>
        <?php $wccur = get_option('woocommerce_currency'); ?>
        <?php $sym = get_woocommerce_currency_symbol($wccur); ?>
        <?php $button_text = "Add ".$button_final." | ".$sym.$price; ?>
        <a id="buy<?php echo $id.$value; ?>" href="#"><button id="button<?php echo $id.$value; ?>" class="featured-button single_add_to_cart_button button alt" style="letter-spacing:-0.5px; font-size:14px; width:90%; padding:0 5px 0 5px; position:relative; height:25px; top: -31px;
    left: 11px;"><?php echo $button_text; ?></button></a>



    </div>
    <div style="clear:both;"></div>
    </div>


    <script>
        jQuery('#buy<?php echo $id.$value ?>').click(function(e) {
            e.preventDefault();
            addToCartd(<?php echo $id; ?>,<?php echo $value; ?>,<?php echo $price; ?>);
            return false;
        });
        function addToCartd(p_id,v_id,s_id) {

            var q_id = 1;
           // var q_id = jQuery("#qty"+p_id+v_id).val();
            jQuery("#button"+p_id+v_id).html("Adding <i class='fas fa-spinner fa-spin'></i>");
            jQuery.get('/?post_type=product&add-to-cart=' + p_id + '&quantity=' + q_id, function() {
                jQuery("#button"+p_id+v_id).html("Added");
                s_id = (s_id * q_id);
                var x = jQuery( ".count" ).first().text();
                var res = x.split(" ",1);
                res = parseInt(res) + parseInt(q_id);
                jQuery(".count").text(res);
                var cart = jQuery(".woocommerce-Price-amount").first().text();
                var cart = cart.replace("£", "");
                var middle = parseFloat(cart);
                var final = "£"+(middle+s_id).toFixed(2);
                jQuery(".woocommerce-Price-amount").first().html(final);

            });
        }
    </script>


    <div id="new-social-media"><a href="https://instagram.com/drydrinker"><i class="fa fa-instagram fa-3x"></i></a> &nbsp; <a href="https://twitter.com/drydrinker"><i class="fa fa-twitter-square fa-3x"></i></a> &nbsp; <a href="https://facebook.com/drydrinker"><i class="fa fa-facebook-square fa-3x"></i></a></div>

<div style="float:left; color:#666;" onClick="topFunction();"><i class="fas fa-home fa-2x"></i></div>
<?php } ?>
