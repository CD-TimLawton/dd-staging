<?php
/*
Template Name: Pop-Up
*/

	$location = WC_Geolocation::geolocate_ip();
	$country = $location['country'];

	$magic = 0;

	global $woocommerce;
	$items = $woocommerce->cart->get_cart();
	$product_names = array();

	if($country!="GB"){$magic=1;}
	
	foreach($items as $item => $values) 
	{ 
	    $_woo_product = wc_get_product( $values['product_id'] );
	    $product_name = $_woo_product->get_sku(); 
	    if($product_name == "DDSKU166-2"){$magic=1;}
	}
	
	$debug = $_GET['debug'];
	$items = WC()->cart->get_cart_contents_count(); 
	if(($items>1 && $magic==0) || ($debug==1 && $magic==0))
	{
?>



<script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.js"></script>

<style type="text/css">
#overlayx {
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		background-color: #000;
		filter:alpha(opacity=70);
		-moz-opacity:0.7;
		-khtml-opacity: 0.7;
		opacity: 0.7;
		z-index: 1900;
		display: none;
	}
	
.cnt223 a{
	text-decoration: none;
	}
	
.popout{
	width: 95%;
	margin: 0 auto;
	display: none;
	position: fixed;
	z-index: 1901;
	}
	
.cnt223{
	max-width: 300px;
    	min-height: 150px;
    	margin: 0 auto;
	background: #f3f3f3;
	position: relative;
	z-index: 1903;
	padding: 10px;
	border-radius: 5px;
	box-shadow: 0 2px 5px #000;
}
.cnt223 p{
	clear: both;
	color: #555555;
	/* text-align: justify; */
	font-size: 20px;
    	font-family: sans-serif;
	}
.cnt223 p a{
	color: #d91900;
	font-weight: bold;
	}
.cnt223 .x{
	float: right;
	height: 35px;
	left: 22px;
	position: relative;
	top: -25px;
	width: 34px;
	}
.cnt223 .x:hover{
	cursor: pointer;
	}
</style>



<script type='text/javascript'>
$(function(){
var overlay = $('<div id="overlayx"></div>');
overlay.show();
overlay.appendTo(document.getElementById("post-5"));
$('.popout').show();
$('.close').click(function(){
$('.popout').hide();
overlay.appendTo(document.getElementById("post-5")).remove();
return false;
});

$('.x').click(function(){
$('.popout').hide();
overlay.appendTo(document.body).remove();
return false;
});
});
</script>

<div class='popout'>
	<div class='cnt223'>
		<a href='' class='close'><i class='fas fa-times-circle'></i></a>
		<div>
			<h1 style="text-align:center;">Special Offer</h1>
			<div style="float:left; width:105px;">
				<img src="https://drydrinker.com/wp-content/uploads/2018/04/St-Peters-Without-Gold-330ml-600x600.jpg" style="width:100px;">
			</div>
			<div style="float:right; width:170px;">
				Add a case of <a href="https://drydrinker.com/product/st-peters-without-gold-330ml/">12 St Peters Without Gold</a> and get <strong>free delivery</strong> on your whole order
			</div>
			<br/>
			<br/>
		</div>
		<br/>
		<br/>
		<br/>
		<br/>
	</div>
</div>

<?php } ?>



<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
	/**
	 * Functions hooked in to storefront_page add_action
	 *
	 * @hooked storefront_page_header          - 10
	 * @hooked storefront_page_content         - 20
	 */
	do_action( 'storefront_page' );
	?>
</article><!-- #post-## -->

