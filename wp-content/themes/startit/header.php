<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">    <?php qode_startit_wp_title(); ?>
    <?php
    /**
     * @see qode_startit_header_meta() - hooked with 10
     * @see qode_user_scalable - hooked with 10
     */
    ?>
	<?php do_action('qode_startit_header_meta'); ?>

	<?php wp_head(); ?>
    
    
    <?php
if ( is_user_logged_in() ) {
    echo '<style>#nav-menu-item-753 ul, #nav-menu-item-753 second inner , #nav-menu-item-753 second {display:none !important}</style>';	
} else {
   // echo '<h1>Welcome, visitor!</h1>';
}
?>

<script>
jQuery(document).ready(function(e) {
    //alert('aaaaaaa');
	 jQuery('#wpcf7-f624-p749-o1 .your-password input').attr('type', 'password'); 
});
</script>
</head>

<body <?php body_class(); ?>>


<?php qode_startit_get_side_area(); ?>

<div class="qodef-wrapper">
    <div class="qodef-wrapper-inner">
        <?php qode_startit_get_header(); ?>

        <?php if(qode_startit_options()->getOptionValue('show_back_button') == "yes") { ?>
            <a id='qodef-back-to-top'  href='#'>
                <span class="qodef-icon-stack">
                     <?php
                        qode_startit_icon_collections()->getBackToTopIcon('font_awesome');
                    ?>
                </span>
            </a>
        <?php } ?>
        <?php qode_startit_get_full_screen_menu(); ?>

        <div class="qodef-content" <?php qode_startit_content_elem_style_attr(); ?>>
 <div class="qodef-content-inner">
  