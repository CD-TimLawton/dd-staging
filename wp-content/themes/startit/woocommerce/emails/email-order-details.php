<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$text_align = is_rtl() ? 'right' : 'left';

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>

<?php if ( ! $sent_to_admin ) : ?>
	<?php /*?><h2><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2><?php */?>
<?php else : ?>
	<?php /*?><h2><a class="link" href="<?php echo esc_url( admin_url( 'post.php?post=' . $order->get_id() . '&action=edit' ) ); ?>"><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></a> (<?php printf( '<time datetime="%s">%s</time>', $order->get_date_created()->format( 'c' ), wc_format_datetime( $order->get_date_created() ) ); ?>)</h2><?php */?>
<?php endif; ?>
 
<?php

$text_align = is_rtl() ? 'right' : 'left';

?><table id="addresses" cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top;" border="0">
	<tr>
		<td class="td" style="text-align:<?php echo $text_align; ?>; font-family: 'Montserrat', sans-serif; padding:0px" valign="top" width="50%">
			<h3 style="color:#000; font-size:11px; font-weight:500; font-family:'Montserrat', sans-serif;"><?php _e( 'BILLED TO:', 'woocommerce' ); ?></h3>

			<p style=" font-size:11px; line-height=18px; font-weight:400; font-family:'Montserrat', sans-serif; class="text"><?php echo $order->get_formatted_billing_address(); ?></p>
		</td>
		<?php if ( ! wc_ship_to_billing_address_only() && $order->needs_shipping_address() && ( $shipping = $order->get_formatted_shipping_address() ) ) : ?>
			<td class="td" style="text-align:<?php echo $text_align; ?>; font-family: 'Montserrat', sans-serif; padding:0px" valign="top" width="50%">
				<h3 style="color:#000; font-size:11px;font-weight:500; font-family: font-family:'Montserrat', sans-serif;"><?php _e( 'SHIPPING TO:', 'woocommerce' ); ?></h3>

				<p style=" font-size:11px; line-height=18px; font-weight:400; font-family:'Montserrat', sans-serif; class="text"><?php echo $shipping; ?></p>
			</td>
		<?php endif; ?>
	</tr>
</table>
<p style="font-size:20px; font-family:'Helvetica Neue',Helvetica,Arial,sans-serif; font-weight:300">&nbsp;Order Summary</p>
<table class="td tplate" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
	<thead>
		<tr>
			<th class="td" scope="col" width="135"  style=" font-family:'Montserrat', sans-serif; padding:0px 0px 0px 0px;text-align:<?php echo $text_align; ?>;"><?php _e( 'ITEM', 'woocommerce' ); ?></th>
			<th class="td" scope="col" width="100" style=" font-family:'Montserrat', sans-serif;text-align:<?php echo $text_align; ?>;">&nbsp; </th>
			
            <th class="td qtypadd" scope="col" style=" font-family:'Montserrat', sans-serif;text-align:<?php echo $text_align; ?>; padding: 12px 12px 12px 0px"><?php _e( 'QTY', 'woocommerce' ); ?></th>
            <th class="td" scope="col" width="100" style=" font-family:'Montserrat', sans-serif;text-align:<?php echo $text_align; ?>;"><?php _e( 'UNIT PRICE', 'woocommerce' ); ?></th><th class="td" scope="col" style="font-family:'Montserrat', sans-serif;text-align:<?php echo $text_align; ?>;"><?php _e( 'SUBTOTAL', 'woocommerce' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php echo wc_get_email_order_items( $order, array(
			'show_sku'      => $sent_to_admin,
			'show_image'    => false,
			'image_size'    => array( 32, 32 ),
			'plain_text'    => $plain_text,
			'sent_to_admin' => $sent_to_admin,
		) ); ?>
	
  <tr>
  <td colspan="5" style="padding: 0;border: 0;height: 1px; line-height: 3px;border-top: 1px solid #716868">&nbsp;</td>
  </tr>
	<!--<tfoot>-->
		<?php
			if ( $totals = $order->get_order_item_totals() ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?><tr style="border-top:1px solid #000000;padding-top:10px;" class="btmTottl">
						<td colspan="2" style="padding:0;border:0;font-family:'Montserrat', sans-serif;font-weight:300;"> </td>
                       
                        <td class="td" colspan="2" scope="row"  style="font-family:'Montserrat', sans-serif; padding:0;text-align:<?php echo $text_align; ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 0px;' : ''; ?>"><?php echo $total['label']; ?></td>
						<td class="td" style="line-height: 15px;font-weight:300;font-family:'Montserrat', sans-serif;padding:0;text-align:<?php echo $text_align; ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 0px;' : ''; ?>"><?php echo $total['value']; ?></td>
					</tr><?php
				}
			}
		?> </tbody>
<!--	</tfoot>-->
</table>
<h2 style="color:#000; font-weight:300; font-family:'Montserrat', sans-serif;">Delivery Instructions</h2>

<p style="font-weight:300; font-size:13px; font-family:'Montserrat', sans-serif;"><?php _e( 'Delivery Contact Number:', 'woocommerce' ); ?> <?php echo $order->billing_phone; ?></p>
<p style="font-weight:300;font-size:13px;font-family:'Montserrat', sans-serif;"><?php _e( 'Delivery Instructions if unavailable:', 'woocommerce' ); ?><?php echo $order->billing_order_comment ?>
<p style="font-weight:300;font-size:13px;font-family:'Montserrat', sans-serif;"><?php _e( 'Where did you hear about us:', 'woocommerce' ); ?> <?php echo $order->billing_where_did_you_hear_about_us; ?></p>
<p style="font-weight:300;font-size:13px;font-family:'Montserrat', sans-serif;> <?php _e( 'Email:', 'woocommerce' ); ?> <?php echo $order->billing_last_name; ?></p>


<?php if ( $show_purchase_note && is_object( $product ) && ( $purchase_note = $product->get_purchase_note() ) ) : ?>
        <tr>
            <td colspan="3" style="text-align:<?php echo $text_align; ?>; vertical-align:middle; border: none; font-family: 'Montserrat', sans-serif;"><?php echo wpautop( do_shortcode( wp_kses_post( $purchase_note ) ) ); ?></td>
        </tr>
    <?php endif; ?>
<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
