<?php do_action('qode_startit_before_top_navigation'); ?>

<nav class="qodef-main-menu qodef-drop-down <?php echo esc_attr($additional_class); ?>">
    <?php
    wp_nav_menu( array(
        'theme_location' => 'main-navigation' ,
        'container'  => '',
        'container_class' => '',
        'menu_class' => 'clearfix',
        'menu_id' => '',
        'fallback_cb' => 'top_navigation_fallback',
        'link_before' => '<span>',
        'link_after' => '</span>',
        'walker' => new QodeStartitTopNavigationWalker()
    ));
    ?>
    <script>
	jQuery(document).ready(function(e) {
        jQuery('.momSearch').click(function(e) {
		    jQuery('form.momSearchfrmm').slideToggle();
        });
    });
	</script>
 <a data-icon-close-same-position="yes" class="qodef-search-opener momSearch" href="javascript:void(0)">
            <i class="qodef-icon-ion-icon ion-ios-search-strong "></i></a>
      <?php /*?>  <form role="search" action="<?php echo esc_url(home_url('/')); ?>" class="qodef-search-cover" method="get">
	<?php if ( $search_in_grid ) { ?>
	<div class="qodef-container">
		<div class="qodef-container-inner clearfix">
			<?php } ?>
			<div class="qodef-form-holder-outer">
				<div class="qodef-form-holder">
					<div class="qodef-form-holder-inner">
						<input type="text" placeholder="<?php esc_html_e('Search', 'startit'); ?>" name="s" class="qode_search_field no-livesearch" autocomplete="off" />
						<div class="qodef-search-close">
							<a href="#">
								<?php print $search_icon_close; ?>
							</a>
						</div>
					</div>
				</div>
			</div>
			<?php if ( $search_in_grid ) { ?>
		</div>
	</div>
	<?php } ?>
</form><?php */?>

<?php /*?><form role="search" method="get" class="woocommerce-product-search momSearchfrmm" action="<?php echo esc_url( home_url( '/'  ) ); ?>">
 
	<input type="search" class="search-field searchFrminp" placeholder="<?php echo esc_attr_x( 'Search Products&hellip;', 'placeholder', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'label', 'woocommerce' ); ?>" /> 
	<input type="hidden" name="post_type" value="product" />
</form><?php */?>

<form action="/" method="get" class="momSearchfrmm">
    <input class="search-field searchFrminp" type="text" name="s" placeholder="Search..." id="search" value="<?php the_search_query(); ?>" />
   <?php /*?> <input type="image" alt="Search" src="<?php bloginfo( 'template_url' ); ?>/images/search.png" /><?php */?>
</form>


</nav>
<div class="top-social"><?php dynamic_sidebar('qodef-top-bar-right'); ?></div>
<?php do_action('qode_startit_after_top_navigation'); ?>

