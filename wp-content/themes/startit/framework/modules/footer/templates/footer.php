<?php
/**
 * Footer template part
 */

qode_startit_get_content_bottom_area(); ?>
</div> <!-- close div.content_inner -->
</div>  <!-- close div.content -->

<footer <?php qode_startit_class_attribute($footer_classes); ?>>
	<div class="qodef-footer-inner clearfix">

		<?php

		if($display_footer_top) {
			qode_startit_get_footer_top();
		}
		if($display_footer_bottom) {
			qode_startit_get_footer_bottom();
		}
		?>

	 </div>
</footer>

</div> <!-- close div.qodef-wrapper-inner  -->
</div> <!-- close div.qodef-wrapper -->
<?php wp_footer(); ?>
<script>
(function () {
var _tsid = 'XDDEA5F642DE242B1F9FF3F845F722C8C';
_tsConfig = {
'yOffset': '0', /* offset from page bottom */
'variant': 'reviews', /* text, default, small, reviews, custom, custom_reviews */
'customElementId': '', /* required for variants custom and custom_reviews */
'trustcardDirection': '', /* for custom variants: topRight, topLeft, bottomRight, bottomLeft */
'customBadgeWidth': '', /* for custom variants: 40 - 90 (in pixels) */
'customBadgeHeight': '', /* for custom variants: 40 - 90 (in pixels) */
'disableResponsive': 'false', /* deactivate responsive behaviour */
'disableTrustbadge': 'false', /* deactivate trustbadge */
'trustCardTrigger': 'mouseenter' /* set to 'click' if you want the trustcard to be opened on click instead */
};
var _ts = document.createElement('script');
_ts.type = 'text/javascript';
_ts.charset = 'utf-8';
_ts.async = true;
_ts.src = '//http://widgets.trustedshops.com/js/'; + _tsid + '.js';
var __ts = document.getElementsByTagName('script')[0];
__ts.parentNode.insertBefore(_ts, __ts);
})();



window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript';; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '33787032-3bfc-4a17-a0e5-10f5bdbe30b8', f: true }); done = true; } }; })();


/*

$(window).load(function() {
$('div.sold-out').html('coming soon');
});*/
</script>
</body>
</html>