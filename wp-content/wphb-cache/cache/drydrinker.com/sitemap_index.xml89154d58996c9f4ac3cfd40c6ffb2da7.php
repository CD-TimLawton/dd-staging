<?php die(); ?><!-- This page is cached by the Hummingbird Performance plugin v1.9.3 - https://wordpress.org/plugins/hummingbird-performance/. --><?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="//drydrinker.com/wp-content/plugins/wordpress-seo/css/main-sitemap.xsl"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<sitemap>
		<loc>https://drydrinker.com/post-sitemap.xml</loc>
		<lastmod>2019-02-12T09:01:23+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://drydrinker.com/page-sitemap.xml</loc>
		<lastmod>2019-02-13T15:22:44+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://drydrinker.com/attachment-sitemap.xml</loc>
		<lastmod>2019-02-13T10:00:02+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://drydrinker.com/product-sitemap.xml</loc>
		<lastmod>2019-02-13T15:25:29+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://drydrinker.com/popupbuilder-sitemap.xml</loc>
		<lastmod>2019-01-28T18:05:30+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://drydrinker.com/category-sitemap.xml</loc>
		<lastmod>2019-02-12T09:01:23+00:00</lastmod>
	</sitemap>
	<sitemap>
		<loc>https://drydrinker.com/product_cat-sitemap.xml</loc>
		<lastmod>2019-01-20T10:50:47+00:00</lastmod>
	</sitemap>
</sitemapindex>
<!-- XML Sitemap generated by Yoast SEO --><!-- Hummingbird cache file was created in 0.22736811637878 seconds, on 13-02-19 15:30:05 -->